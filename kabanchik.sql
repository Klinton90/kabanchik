-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.26-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for kabanchik
CREATE DATABASE IF NOT EXISTS `kabanchik` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kabanchik`;

-- Dumping structure for table kabanchik.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(21500) NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `Unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table kabanchik.categories: ~4 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `name`, `description`, `is_active`) VALUES
	(1, 'Categ1', 'Categ1_Descr', b'1'),
	(2, 'Categ2', 'Categ2_Descr', b'1'),
	(3, 'Categ3', 'Categ3_Desc', b'1'),
	(4, 'Categ4', 'Categ4_Descr', b'0');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table kabanchik.sub_categories
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `sub_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(21500) NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`sub_category_id`),
  UNIQUE KEY `Unique` (`name`),
  KEY `FKjwy7imy3rf6r99x48ydq45otw` (`category_id`),
  CONSTRAINT `FKjwy7imy3rf6r99x48ydq45otw` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table kabanchik.sub_categories: ~3 rows (approximately)
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` (`sub_category_id`, `name`, `description`, `is_active`, `category_id`) VALUES
	(1, 'SubCat1', 'SubCat1_Descr', b'1', 1),
	(2, 'SubCat2', 'SubCat2_Descr', b'1', 1),
	(3, 'SubCat3', 'SubCat3_Descr', b'1', 2),
	(4, 'SubCat4', 'SubCat4_Descr', b'1', 3);
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;

-- Dumping structure for table kabanchik.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('USER','EXECUTOR','ADMIN') NOT NULL DEFAULT 'USER',
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `test_datetime` datetime DEFAULT NULL,
  `test_timestamp` timestamp NULL DEFAULT NULL,
  `test_date` date DEFAULT NULL,
  `test_time` time DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `Unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Dumping data for table kabanchik.users: ~21 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `email`, `password`, `role`, `is_active`, `test_datetime`, `test_timestamp`, `test_date`, `test_time`) VALUES
	(1, 'admina', 'password2', 'ADMIN', b'1', '2016-11-12 10:11:12', '2016-11-18 00:26:45', '2016-12-17', '02:21:34'),
	(2, 'adminb', '$2a$10$kdXCTzd1x2Wdr.fnRgzaO.x.I3g58iXREkkqrEXtpN9XunqQL4FOW', 'ADMIN', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(13, 'admina@mail.ru', 'password', 'ADMIN', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(14, 'adminc', 'qqq', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(15, 'admind', 'www', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-18 00:31:03', '2016-11-17', '12:21:35'),
	(16, 'admine', 'eee', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(17, 'adminf', 'rrr', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(18, 'adming', 'ttt', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(19, 'adminj', 'yyy', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(20, 'admini', 'uuu', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(21, 'admink', 'iii', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(22, 'adminm', 'ooo', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(23, 'adminl', 'ppp', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(24, 'adminn', 'aaa', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(25, 'admino', 'sss', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(26, 'adminp', 'ddd', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(27, 'adminr', 'fff', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(28, 'admins', 'ggg', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(29, 'admint', 'hhh', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(30, 'adminu', 'jjj', 'USER', b'1', '2016-11-10 10:11:12', '2016-11-17 23:50:53', '2016-11-17', '12:21:34'),
	(31, 'adminv', 'kkk', 'USER', b'0', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table kabanchik.user_profiles
CREATE TABLE IF NOT EXISTS `user_profiles` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_profile_id`),
  CONSTRAINT `FK_user_profiles_users` FOREIGN KEY (`user_profile_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table kabanchik.user_profiles: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
