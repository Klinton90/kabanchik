package kabanchik.system.department.domain;

import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.validator.Unique;
import kabanchik.system.department.dto.DepartmentDTO;
import kabanchik.system.departmentaccess.domain.DepartmentAccess;
import kabanchik.system.progress.domain.Progress;
import kabanchik.system.progress.domain.ProgressParent;
import kabanchik.system.user.domain.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.util.List;

@Unique
@Entity
@Getter
@Setter
@Table(name = "department")
public class Department implements ProgressParent<DepartmentDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "department_id")
    private Integer id;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @ContainsFilterField
    @Column(name = "name", nullable = false, length = 50, unique = true)
    private String name;

    @ContainsFilterField
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "forWorkload", nullable = false)
    private boolean forWorkload;

    @OneToMany(mappedBy = "department", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<User> users;

    @OneToMany(mappedBy = "department")
    @Filter(name = "progressParentDepartment")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private List<Progress> progressList;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "department", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<DepartmentAccess> departmentAccesses;

    @Override
    public String toString(){
        String id = this.id != null ? this.id.toString() : "null";
        return id + ":" + name;
    }

}
