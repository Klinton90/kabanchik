package kabanchik.system.department;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.department.domain.Department;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.department.domain.QDepartment.department;
import static kabanchik.system.departmentaccess.domain.QDepartmentAccess.departmentAccess;
import static kabanchik.system.timesheet.domain.QTimesheet.timesheet;
import static kabanchik.system.user.domain.QUser.user;
import static kabanchik.system.timesheetaccess.domain.QTimesheetAccess.timesheetAccess;
import static kabanchik.system.progress.domain.QProgress.progress;

@Repository
public interface DepartmentRepository extends RestRepository<Department, Integer> {

    default List<Department> getForProgressModal(
            Integer timesheetCategoryId,
            Boolean isForPlan,
            String queryString,
            Integer limit,
            Integer offset
    ) {
        BooleanExpression booleanExpression = timesheetAccess.timesheetCategoryId.eq(timesheetCategoryId)
                .and(department.forWorkload.isTrue())
                .and(departmentAccess.isActive.isTrue())
                .and(departmentAccess.timesheetCategoryId.eq(timesheetCategoryId));

        if (isForPlan != null) {
            if (isForPlan) {
                booleanExpression = booleanExpression.and(timesheetAccess.timesheets.any().status.eq(TimesheetStatus.PLANNED));
            } else {
                booleanExpression = booleanExpression.and(timesheetAccess.timesheets.any().status.eq(TimesheetStatus.APPROVED));
            }
        }

        if (StringUtils.isNotBlank(queryString)) {
            booleanExpression = booleanExpression.and(department.name.contains(queryString)
                    .or(department.description.contains(queryString)));
        }

        JPAQuery<Department> query = new JPAQuery<Department>(getEntityManager())
                .distinct()
                .from(department)
                .innerJoin(department.users, user)
                .innerJoin(user.timesheetAccesses, timesheetAccess)
                .innerJoin(department.departmentAccesses, departmentAccess)
                .where(booleanExpression);

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

    // Do not add `departmentAccesses` to criteria as this is used for both: timesheets and progress
    default List<Department> getForPSR(Integer timesheetCategoryId) {
        JPAQuery<Integer> subQuery = new JPAQuery<TimesheetAccess>()
                .select(timesheetAccess.id)
                .from(timesheetAccess)
                .innerJoin(timesheetAccess.timesheets, timesheet)
                .where(
                        timesheetAccess.timesheetCategoryId.eq(timesheetCategoryId)
                        .and(timesheet.status.in(TimesheetStatus.PLANNED, TimesheetStatus.APPROVED))
                );

        return new JPAQuery<Department>(getEntityManager())
                .distinct()
                .from(department)
                .innerJoin(department.users, user)
                .leftJoin(user.timesheetAccesses, timesheetAccess)
                .leftJoin(department.progressList, progress)
                .leftJoin(department.departmentAccesses, departmentAccess)
                .where(timesheetAccess.id.in(subQuery)
                        .or(
                                progress.timesheetCategoryId.eq(timesheetCategoryId)
                                .and(departmentAccess.isActive.isTrue())
                                .and(departmentAccess.timesheetCategoryId.eq(timesheetCategoryId))
                        ))
                .fetch();
    }
}
