package kabanchik.system.department;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.department.domain.Department;
import kabanchik.system.department.dto.DepartmentDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/department")
public class DepartmentController extends RestController<Department, DepartmentDTO, Integer> {

    @Getter
    @Autowired
    private DepartmentService service;

    @RequestMapping(value = "/getForPSR", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForPSR(
            @RequestParam Integer timesheetCategoryId
    ){
        List<DepartmentDTO> result = service.getForPSR(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getForProgressModal", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForProgressModal(
            @RequestParam Integer timesheetCategoryId,
            @RequestParam(required = false) Boolean isForPlan,
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ){
        List<DepartmentDTO> result = service.getForProgressModal(timesheetCategoryId, isForPlan, queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
