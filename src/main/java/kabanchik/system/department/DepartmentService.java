package kabanchik.system.department;

import kabanchik.core.service.RestService;
import kabanchik.system.department.domain.Department;
import kabanchik.system.department.dto.DepartmentDTO;
import kabanchik.system.departmentaccess.DepartmentAccessService;
import kabanchik.system.progress.ProgressService;
import kabanchik.system.user.UserService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService extends RestService<Department, DepartmentDTO, Integer> {

    @Getter
    @Autowired
    private DepartmentRepository repository;

    @Getter
    @Autowired
    private DepartmentService self;

    @Autowired
    private UserService userService;

    @Autowired
    private DepartmentAccessService departmentAccessService;

    @Autowired
    private ProgressService progressService;

    @Override
    public boolean isInUse(Integer id) {
        return userService.countByDepartmentId(id) > 0 || departmentAccessService.countByDepartmentId(id) > 0 || progressService.countByDepartmentId(id) > 0;
    }

    public List<DepartmentDTO> getForProgressModal(
            Integer timesheetCategoryId,
            Boolean isForPlan,
            String queryString,
            Integer limit,
            Integer offset
    ) {
        return repository.getForProgressModal(timesheetCategoryId, isForPlan, queryString, limit, offset)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

    public List<DepartmentDTO> getForPSR(Integer timesheetCategoryId) {
        return repository.getForPSR(timesheetCategoryId)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }
}
