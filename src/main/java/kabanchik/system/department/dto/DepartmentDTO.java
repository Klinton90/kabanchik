package kabanchik.system.department.dto;

import kabanchik.system.department.domain.Department;
import kabanchik.system.progress.dto.ProgressParentDTO;
import kabanchik.system.progress.domain.ProgressType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class DepartmentDTO implements ProgressParentDTO<Department> {

    private Integer id;

    @NotNull
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    private boolean progressReportable;

    @NotNull
    private boolean forWorkload;

    @Override
    public ProgressType getProgressType() {
        return ProgressType.DEPARTMENT;
    }
}
