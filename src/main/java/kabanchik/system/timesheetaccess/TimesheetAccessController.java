package kabanchik.system.timesheetaccess;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.timesheetaccess.dto.BulkTimesheetAccessCategoriesInsertDTO;
import kabanchik.system.timesheetaccess.dto.BulkTimesheetAccessUsersInsertDTO;
import kabanchik.system.timesheetaccess.dto.TimesheetAccessDTO;
import kabanchik.system.timesheetaccess.dto.ToggleTimesheetAccessDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/timesheetAccess")
public class TimesheetAccessController extends RestController<TimesheetAccess, TimesheetAccessDTO, Integer> {

    @Getter
    @Autowired
    private TimesheetAccessService service;

    @RequestMapping(value = "/bulkUsersInsert", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> bulkUsersInsert(
        @RequestBody BulkTimesheetAccessUsersInsertDTO dto
    ) throws CustomValidationException {
        List<Integer> result = service.bulkUsersInsert(dto);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/bulkCategoriesInsert", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> bulkCategoriesInsert(
            @RequestBody BulkTimesheetAccessCategoriesInsertDTO dto
    ) throws CustomValidationException {
        List<Integer> result = service.bulkCategoriesInsert(dto);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/resetRates", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> resetRates(
            @RequestBody List<Integer> timesheetAccessIds
    ){
        List<TimesheetAccess> result = service.resetRates(timesheetAccessIds);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/toggleRates", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> toggleRates(
            @RequestBody ToggleTimesheetAccessDTO dto
    ){
        List<TimesheetAccess> result = service.toggleRates(dto);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
