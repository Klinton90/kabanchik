package kabanchik.system.timesheetaccess.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.core.util.BeanUtils;
import kabanchik.core.validator.Unique;
import kabanchik.core.validator.UniqueColumn;
import kabanchik.system.bonuspayment.domain.Bonuspayment;
import kabanchik.system.timesheet.domain.Timesheet;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.timesheetaccess.dto.TimesheetAccessDTO;
import kabanchik.system.user.domain.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name="timesheet_access")
@Unique(columns = @UniqueColumn(fields = {"userId", "timesheetCategoryId"}))
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "TimesheetAccess.default",
        attributeNodes = {
            @NamedAttributeNode(value = "user"),
            @NamedAttributeNode(value = "timesheetCategory")
        }
    )
})
public class TimesheetAccess implements IDomain<TimesheetAccessDTO, Integer>, ActivatableDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timesheet_access_id")
    private Integer id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Column(name = "rate", nullable = false, precision = 19, scale = 2)
    private BigDecimal rate;

    @Column(name = "internal_rate", nullable = false, precision = 19, scale = 2)
    private BigDecimal internalRate;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private Integer userId;

    @NotNull
    @Column(name = "timesheet_category_id", nullable = false)
    private Integer timesheetCategoryId;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "timesheet_category_id", insertable = false, updatable = false)
    private TimesheetCategory timesheetCategory;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "timesheetAccess", cascade = CascadeType.ALL)
    private List<Timesheet> timesheets;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "timesheetAccess", cascade = CascadeType.ALL)
    private List<Bonuspayment> bonuspayments;

    @Override
    public TimesheetAccess createFromDto(TimesheetAccessDTO dto) {
        BeanUtils.copyProperties(dto, this, true);
        userId = dto.getUserId();
        return this;
    }

    @Override
    public String toString(){
        String id = this.id != null ? this.id.toString() : "null";
        String _user = user != null ? user.toString() : "null";
        String _category = timesheetCategory != null ? timesheetCategory.toString() : "null";
        return id + ":" + "(" + _user + "," + _category + ")";
    }

}
