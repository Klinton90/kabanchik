package kabanchik.system.timesheetaccess;

import com.querydsl.core.BooleanBuilder;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.SpringValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.timesheet.TimesheetService;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.timesheetcategory.TimesheetCategoryService;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.timesheetaccess.dto.BulkTimesheetAccessCategoriesInsertDTO;
import kabanchik.system.timesheetaccess.dto.BulkTimesheetAccessUsersInsertDTO;
import kabanchik.system.timesheetaccess.dto.TimesheetAccessDTO;
import kabanchik.system.timesheetaccess.dto.ToggleTimesheetAccessDTO;
import kabanchik.system.timesheetrate.TimesheetRateService;
import kabanchik.system.timesheetrate.domain.TimesheetRate;
import kabanchik.system.user.UserService;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class TimesheetAccessService extends RestService<TimesheetAccess, TimesheetAccessDTO, Integer> {

    @Getter
    @Autowired
    private TimesheetAccessRepository repository;

    @Getter
    @Autowired
    private TimesheetAccessService self;

    @Autowired
    private TimesheetCategoryService timesheetCategoryService;

    @Autowired
    private UserService userService;

    @Autowired
    private TimesheetService timesheetService;

    @Autowired
    private TimesheetRateService timesheetRateService;

    @Override
    public boolean isInUse(Integer id) {
        return timesheetService.countByTimesheetAccessId(id) > 0;
    }

    @Override
    protected BooleanBuilder getDefaultBuilder(){
        BooleanBuilder booleanBuilder = super.getDefaultBuilder();
        if (!UserService.getPrincipal().getRole().equals(UserRole.ADMIN)) {
            booleanBuilder = booleanBuilder.and(repository.getBooleanExpressionByManagerId(UserService.getPrincipal().getId()));
        }
        return booleanBuilder;
    }

    @Override
    public void validate(TimesheetAccess timesheetAccess) throws CustomValidationException {
        if (!UserService.getPrincipal().getRole().equals(UserRole.ADMIN)) {
            getManagedTimesheetCategory(timesheetAccess.getTimesheetCategoryId());
        }
    }

    public TimesheetCategory getManagedTimesheetCategory(Integer timesheetCategoryId) throws CustomValidationException {
        TimesheetCategory result;

        if (!UserService.getPrincipal().getRole().equals(UserRole.ADMIN)) {
            result = timesheetCategoryService.getByIdAndManagerId(timesheetCategoryId, UserService.getPrincipal().getId());

            if (result == null) {
                throw new CustomValidationException(
                        "managerAccessDenied",
                        TimesheetCategory.class.getSimpleName(),
                        "timesheetCategoryId",
                        timesheetCategoryId.toString(),
                        null,
                        TimesheetCategory.class.getSimpleName(),
                        timesheetCategoryId.toString());
            }
        } else {
            result = timesheetCategoryService.getOne(timesheetCategoryId);
        }

        return result;
    }

    public TimesheetAccess findByUserIdAndTimesheetCategoryId(Integer userId, Integer timesheetCategoryId) throws CustomValidationException {
        TimesheetAccess timesheetAccess = repository.findByUserIdAndTimesheetCategoryId(userId, timesheetCategoryId);

        if (timesheetAccess == null) {
            Object[] messageParams = {TimesheetAccess.class.getSimpleName(), timesheetCategoryId};
            throw new CustomValidationException("timesheetAccessAccessDenied", TimesheetAccess.class.getSimpleName(), messageParams);
        }

        if (!timesheetAccess.getTimesheetCategory().getIsActive()) {
            Object[] messageParams = {TimesheetAccess.class.getSimpleName(), timesheetAccess.getId()};
            throw new CustomValidationException("inactive", TimesheetAccess.class.getSimpleName(), messageParams);
        }

        return timesheetAccess;
    }

    public List<TimesheetAccess> findAllByUserId(Integer userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    public TimesheetAccess initEntity(TimesheetAccessDTO dto) throws CustomValidationException {
        TimesheetAccess rate = super.initEntity(dto);

        User user = userService.getOne(dto.getUserId());
        rate.setUser(user);

        if (rate.getRate() == null || rate.getRate().equals(BigDecimal.ZERO) || !UserRole.ADMIN.equals(UserService.getPrincipal().getRole())) {
            rate.setRate(user.getBaseRate());
        }

        TimesheetCategory category = timesheetCategoryService.getOne(dto.getTimesheetCategoryId());
        rate.setTimesheetCategory(category);

        return rate;
    }

    // Do not add transactional, as it cause Exception on Duplicates
//    @Transactional(rollbackFor = CustomValidationException.class)
    public List<Integer> bulkUsersInsert(BulkTimesheetAccessUsersInsertDTO dto) throws CustomValidationException {
        List<Integer> result = new ArrayList<>();
        TimesheetCategory category = getManagedTimesheetCategory(dto.getTimesheetCategoryId());

        for (Integer userId: dto.getUserIds()) {
            User user = userService.getOne(userId);

            TimesheetAccess timesheetAccess = repository.findByUserIdAndTimesheetCategoryId(userId, dto.getTimesheetCategoryId());
            if (timesheetAccess == null) {
                timesheetAccess = new TimesheetAccess()
                        .setId(0)
                        .setIsActive(true)
                        .setUserId(user.getId())
                        .setUser(user)
                        .setTimesheetCategoryId(category.getId())
                        .setTimesheetCategory(category)
                        .setRate(user.getBaseRate())
                        .setInternalRate(user.getInternalBaseRate());

                try {
                    timesheetAccess = getSelf()._save(timesheetAccess);
                } catch (SpringValidationException ex){
                    result.add(userId);
                }
            }

            if (dto.getTo() != null && dto.getFrom() != null) {
                TimesheetRate timesheetRate = new TimesheetRate()
                        .setId(0)
                        .setFrom(dto.getFrom())
                        .setTo(dto.getTo())
                        .setRate(user.getBaseRate())
                        .setInternalRate(user.getInternalBaseRate())
                        .setTimesheetAccessId(timesheetAccess.getId());

                try {
                    timesheetRateService.create(timesheetRate);
                } catch (Exception ex) {

                }
            }
        }

        return result;
    }

    // Do not add transactional, as it cause Exception on Duplicates
//    @Transactional(rollbackFor = CustomValidationException.class)
    public List<Integer> bulkCategoriesInsert(BulkTimesheetAccessCategoriesInsertDTO dto) throws CustomValidationException {
        List<Integer> result = new ArrayList<>();
        User user = userService.getOne(dto.getUserId());

        List<TimesheetCategory> timesheetCategories = timesheetCategoryService.guessManagedTimesheetCategories(dto.getTimesheetCategoryIds());

        for (Integer categoryId: dto.getTimesheetCategoryIds()) {
            TimesheetCategory timesheetCategory = timesheetCategories.stream()
                    .filter(_timesheetCategory -> _timesheetCategory.getId().equals(categoryId))
                    .findAny()
                    .orElseThrow(() -> new CustomValidationException(
                            "managerAccessDenied",
                            TimesheetCategory.class.getSimpleName(),
                            "timesheetCategoryId",
                            categoryId.toString(),
                            dto,
                            TimesheetCategory.class.getSimpleName(),
                            categoryId.toString()));
            TimesheetAccess rate = new TimesheetAccess()
                    .setId(0)
                    .setIsActive(true)
                    .setUserId(user.getId())
                    .setUser(user)
                    .setTimesheetCategoryId(timesheetCategory.getId())
                    .setTimesheetCategory(timesheetCategory)
                    .setRate(user.getBaseRate())
                    .setInternalRate(user.getInternalBaseRate());

            try {
                getSelf()._save(rate);
            } catch (SpringValidationException ex) {
                result.add(categoryId);
            }
        }

        return result;
    }

    public void updateDefaultRatesByUser(User user, BigDecimal prevBaseRate){
        repository.updateDefaultRatesByUser(user, prevBaseRate);
    }

    public void updateDefaultInternalRatesByUser(User user, BigDecimal prevBaseRate){
        repository.updateDefaultInternalRatesByUser(user, prevBaseRate);
    }

    public Integer countByTimesheetCategoryId(int id) {
        return repository.countByTimesheetCategoryId(id);
    }

    public Integer countByUserId(int id) {
        return repository.countByUserId(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TimesheetAccess> resetRates(List<Integer> timesheetAccessIds) {
        List<TimesheetAccess> result = repository.findAllByIdIn(timesheetAccessIds);
        result.forEach(timesheetAccess -> timesheetAccess.setRate(timesheetAccess.getUser().getBaseRate()));
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TimesheetAccess> toggleRates(ToggleTimesheetAccessDTO dto) {
        List<TimesheetAccess> result = repository.findAllByIdIn(dto.getTimesheetAccessIds());
        result.forEach(timesheetAccess -> timesheetAccess.setIsActive(dto.getIsActive()));
        return result;
    }
}
