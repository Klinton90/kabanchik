package kabanchik.system.timesheetaccess;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAUpdateClause;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.user.domain.User;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

import static kabanchik.system.timesheetaccess.domain.QTimesheetAccess.timesheetAccess;

@Repository
public interface TimesheetAccessRepository extends RestRepository<TimesheetAccess, Integer> {

    List<TimesheetAccess> findAllByIdIn(List<Integer> ids);

    List<TimesheetAccess> findAllByUserId(Integer userId);

    TimesheetAccess findByUserIdAndTimesheetCategoryId(Integer userId, Integer timesheetCategoryId);

    Integer countByTimesheetCategoryId(int id);

    Integer countByUserId(int id);

    default void updateDefaultRatesByUser(User user, BigDecimal prevBaseRate){
        JPAUpdateClause update = new JPAUpdateClause(getEntityManager(), timesheetAccess);
        update.set(timesheetAccess.rate, user.getBaseRate())
                .where(timesheetAccess.userId.eq(user.getId())
                        .and(timesheetAccess.rate.eq(prevBaseRate)))
                .execute();
    }

    default void updateDefaultInternalRatesByUser(User user, BigDecimal prevBaseRate){
        JPAUpdateClause update = new JPAUpdateClause(getEntityManager(), timesheetAccess);
        update.set(timesheetAccess.internalRate, user.getInternalBaseRate())
                .where(timesheetAccess.userId.eq(user.getId())
                        .and(timesheetAccess.internalRate.eq(prevBaseRate)))
                .execute();
    }

    default BooleanExpression getBooleanExpressionByManagerId(Integer userId) {
        return timesheetAccess.timesheetCategory.managers.any().id.eq(userId);
    }

}
