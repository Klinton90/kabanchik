package kabanchik.system.timesheetaccess.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.user.domain.User;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class TimesheetAccessDTO implements IDto<TimesheetAccess, Integer>, ActivatableDomain {

    private Integer id;

    @NotNull
    private Boolean isActive;

    @NotNull
    @DecimalMin(value = "0.01")
    @DecimalMax(value = "999999.99")
    @Digits(integer = 6, fraction = 2)
    private BigDecimal rate;

    @NotNull
    @DecimalMin(value = "0.01")
    @DecimalMax(value = "999999.99")
    @Digits(integer = 6, fraction = 2)
    private BigDecimal internalRate;

    private String userName;

    private String timesheetCategoryName;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer timesheetCategoryId;

    @Override
    public TimesheetAccessDTO createFromEntity(TimesheetAccess timesheetAccess){
        BeanUtils.copyProperties(timesheetAccess, this, true);
        User profile = timesheetAccess.getUser();

        this.setTimesheetCategoryName(timesheetAccess.getTimesheetCategory().getName())
                .setTimesheetCategoryId(timesheetAccess.getTimesheetCategory().getId())
                .setUserName(profile.getFirstName() + " " + profile.getLastName())
                .setUserId(profile.getId());

        return this;
    }

}
