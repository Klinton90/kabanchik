package kabanchik.system.timesheetaccess.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ToggleTimesheetAccessDTO {

    private List<Integer> timesheetAccessIds;
    private Boolean isActive;

}
