package kabanchik.system.timesheetaccess.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class BulkTimesheetAccessCategoriesInsertDTO {

    @NotNull
    private Integer userId;

    @NotNull
    @Size(min = 1)
    private List<Integer> timesheetCategoryIds;

//    private LocalDate dateFrom;
//
//    private LocalDate dateTo;

}
