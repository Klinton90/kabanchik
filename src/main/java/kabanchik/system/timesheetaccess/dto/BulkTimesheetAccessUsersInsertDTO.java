package kabanchik.system.timesheetaccess.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class BulkTimesheetAccessUsersInsertDTO {

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    @Size(min = 1)
    private List<Integer> userIds;

    @NotNull
    private LocalDate from;

    @NotNull
    private LocalDate to;

}
