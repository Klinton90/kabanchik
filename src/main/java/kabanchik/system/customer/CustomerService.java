package kabanchik.system.customer;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.customer.domain.Customer;
import kabanchik.system.customer.dto.CustomerDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService extends RestService<Customer, CustomerDTO, Integer> {

    @Getter
    @Autowired
    private CustomerRepository repository;

    @Getter
    @Autowired
    private CustomerService self;

    @Override
    public boolean isInUse(Integer id) {
        return false;
    }

    public Customer getOneByGovernmentId(String governmentId) throws CustomValidationException {
        Customer entity =  repository.findOneByGovernmentId(governmentId);
        if (entity == null) {
            String objectName = getDomainClass().getSimpleName();
            Object[] messageParams = {objectName, governmentId};
            throw new CustomValidationException("rowNotFound", objectName, messageParams);
        }
        return entity;
    }

    public List<CustomerDTO> getForPSR(Integer timesheetCategoryId) {
        return repository.getForPSR(timesheetCategoryId)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }
}
