package kabanchik.system.customer;

import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.customer.domain.Customer;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.customer.domain.QCustomer.customer;
import static kabanchik.system.invoice.domain.QInvoice.invoice;

@Repository
public interface CustomerRepository extends RestRepository<Customer, Integer> {

    Customer findOneByGovernmentId(String governmentId);

    default List<Customer> getForPSR(Integer timesheetCategoryId) {
        return new JPAQuery<Customer>(getEntityManager())
                .distinct()
                .from(customer)
                .innerJoin(customer.invoices, invoice)
                .where(invoice.timesheetCategoryId.eq(timesheetCategoryId))
                .fetch();
    }

}
