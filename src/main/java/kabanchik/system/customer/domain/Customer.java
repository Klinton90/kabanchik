package kabanchik.system.customer.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.domain.IDomain;
import kabanchik.core.validator.Unique;
import kabanchik.system.customer.dto.CustomerDTO;
import kabanchik.system.invoice.domain.Invoice;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Unique
@Entity
@Getter
@Setter
@Table(name = "customer")
public class Customer implements IDomain<CustomerDTO, Integer>, ActivatableDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id", nullable = false)
    private Integer id;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @ContainsFilterField
    @Column(name = "name", nullable = false, length = 50, unique = true)
    private String name;

    @ContainsFilterField
    @Column(name = "description", nullable = false)
    private String description;

    @ContainsFilterField
    @Column(name = "government_id", nullable = false, length = 10, unique = true)
    private String governmentId;

    @OneToMany(mappedBy = "customer")
    private List<Invoice> invoices;

    @Override
    public String toString(){
        String id = this.id != null ? this.id.toString() : "null";
        return id + ":" + name;
    }

}
