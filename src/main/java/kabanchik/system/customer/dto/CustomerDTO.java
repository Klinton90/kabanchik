package kabanchik.system.customer.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.customer.domain.Customer;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CustomerDTO implements IDto<Customer, Integer>, ActivatableDomain {

    private Integer id;

    @NotNull
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    @Size(min = 8, max = 10)
    private String governmentId;

}
