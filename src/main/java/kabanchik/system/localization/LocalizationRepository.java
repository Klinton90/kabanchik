package kabanchik.system.localization;

import kabanchik.core.repository.RestRepository;
import kabanchik.system.localization.domain.LocalizationDomain;
import org.springframework.stereotype.Repository;

@Repository
public interface LocalizationRepository extends RestRepository<LocalizationDomain, String>{

    LocalizationDomain findFirstByIsDefault(Boolean isDefault);

}