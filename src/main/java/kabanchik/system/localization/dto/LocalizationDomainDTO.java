package kabanchik.system.localization.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.core.validator.NotValue;
import kabanchik.system.localization.domain.LocalizationDomain;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class LocalizationDomainDTO implements IDto<LocalizationDomain, String>, ActivatableDomain {

    @NotNull
    @NotBlank
    @Size(min = 2, max = 2)
    @NotValue(value = "en", isCaseSensitive = false)
    private String id;

    private Boolean isActive;

    @NotNull
    @NotBlank
    @Size(max = 255)
    private String name;

    private Boolean isDefault = false;

}
