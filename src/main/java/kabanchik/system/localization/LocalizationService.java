package kabanchik.system.localization;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.core.service.RestService;
import kabanchik.core.config.CustomConfig;
import kabanchik.system.localization.domain.LocalizationDomain;
import kabanchik.system.localization.dto.LocalizationDomainDTO;
import kabanchik.system.staticcontent.StaticContentService;
import kabanchik.system.user.UserService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

@Slf4j
@Service
public class LocalizationService extends RestService<LocalizationDomain, LocalizationDomainDTO, String>{

    private static final String FILE_NAME = "messages";
    private static final String FILE_EXT = "properties";

    private LocalizationDomain defaultLocalization;

    @Autowired
    protected ExposedResourceMessageBundleSource msgSource;

    @Autowired
    private CustomConfig config;

    @Getter
    @Autowired
    private LocalizationRepository repository;

    @Getter
    @Autowired
    private LocalizationService self;

    @Autowired
    private UserService userService;

    @Autowired
    private StaticContentService staticContentService;

    public static Locale getLocaleFromCode(String localizationId){
        return new Locale(localizationId.toUpperCase());
    }

    @Override
    public boolean isInUse(String id){
        return userService.countByLocalizationId(id) > 0;
    }

    public LocalizationDomain getOneByLocalizationId(String localizationId) throws CustomValidationException {
        LocalizationDomain entity = getOne(localizationId);
        if (entity == null) {
            String objectName = LocalizationDomain.class.getSimpleName();
            Object[] messageParams = {objectName, localizationId};
            throw new CustomValidationException("rowNotFound", objectName, messageParams);
        }

        return entity;
    }

    public LocalizationDomain getDefaultLocalization(){
        if(defaultLocalization == null){
            defaultLocalization = repository.findFirstByIsDefault(true);
        }
        return defaultLocalization;
    }

    public Properties updateMessages(Map<String, String> newProps, Locale locale) throws IOException {
        Properties existingProps = msgSource.getMessages(locale);
        newProps.forEach((String key, String value) -> {
            if (value.length() > 0) {
                existingProps.put(key, value);
            } else {
                existingProps.remove(key);
            }
        });

        File folder = new File(config.getFiles().getBase() + config.getFiles().getI18n());
        folder.mkdirs();
        File file = new File(folder.getAbsolutePath() + "/" + FILE_NAME + "_" + locale.toString() + "." + FILE_EXT);

        existingProps.store(new FileOutputStream(file), "");

        msgSource.clearCache();

        return msgSource.getMessages(locale);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String localizationId) throws CustomValidationException{
        super.delete(localizationId);
        staticContentService.deleteFilesByLocale(localizationId);
    }

}
