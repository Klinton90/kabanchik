package kabanchik.system.localization.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.core.formatter.StringFormat;
import kabanchik.core.formatter.StringFormatter;
import kabanchik.core.validator.Unique;
import kabanchik.system.localization.dto.LocalizationDomainDTO;
import kabanchik.system.user.domain.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Unique
@Getter
@Setter
@Entity
@Table(name="localizations")
public class LocalizationDomain implements IDomain<LocalizationDomainDTO, String>, ActivatableDomain{

    @Id
    @StringFormat(style = StringFormatter.Style.UPPERCASE)
    @Column(name = "localization_id", length = 2)
    private String id;

    @Column(name = "name", nullable = false, length = 50, unique = true)
    private String name;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Column(name = "is_default", nullable = false)
    private Boolean isDefault = false;

    @OneToMany(mappedBy = "localization", fetch = FetchType.LAZY)
    private List<User> users;

}