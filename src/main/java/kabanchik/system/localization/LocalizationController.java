package kabanchik.system.localization;

import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.core.controller.RestController;
import kabanchik.system.localization.domain.LocalizationDomain;
import kabanchik.system.localization.dto.LocalizationDomainDTO;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

@Slf4j
@Controller
@RequestMapping(value={"/localization"})
public class LocalizationController extends RestController<LocalizationDomain, LocalizationDomainDTO, String>{

    @Autowired
    protected ExposedResourceMessageBundleSource msgSource;

    @Getter
    @Autowired
    protected LocalizationService service;

    @RequestMapping(value = "/getMessagesBySession", method = RequestMethod.GET)
    public @ResponseBody Properties getMessagesBySession(){
        return msgSource.getMessages(LocaleContextHolder.getLocale());
    }

    @RequestMapping(value = "/getMessagesByLocalizationId", method = RequestMethod.GET)
    public @ResponseBody Properties getMessagesByLocalizationId(
        @RequestParam String localizationId
    ){
        return msgSource.getMessages(LocalizationService.getLocaleFromCode(localizationId));
    }

    @RequestMapping(value = "/refreshLocalization", method = RequestMethod.GET)
    public @ResponseBody Properties refreshLocalization(){
        msgSource.clearCache();
        return msgSource.getMessages(LocaleContextHolder.getLocale());
    }

    @RequestMapping(value = {"/updateMessages/{localizationId}", "/updateMessages/{localizationId}/"}, method = RequestMethod.PUT)
    public @ResponseBody Properties updateMessages(
            @PathVariable String localizationId,
            @RequestBody Map<String, String> newProps
    ) throws IOException {
        return service.updateMessages(newProps, LocalizationService.getLocaleFromCode(localizationId));
    }

    @RequestMapping(value = "/listAllPublic", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> listAllPublic (
            Pageable pageable,
            @RequestParam(name = "showActive", required = false, defaultValue = "false") boolean showActive
    ) {
        return listAll(pageable, showActive);
    }

}