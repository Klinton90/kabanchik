package kabanchik.system.staticcontent.domain;

import kabanchik.core.domain.LocalizedEnum;

public enum StaticContentType implements LocalizedEnum {
    CODE_OF_CONDUCT, HOME;
}
