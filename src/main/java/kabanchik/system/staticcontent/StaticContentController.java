package kabanchik.system.staticcontent;

import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.localization.LocalizationService;
import kabanchik.system.staticcontent.domain.StaticContentType;
import kabanchik.system.staticcontent.dto.StaticContentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping(value={"/staticContent"})
public class StaticContentController {

    @Autowired
    private StaticContentService service;

    @Autowired
    private LocalizationService localizationService;

    @RequestMapping(value = {"/get", "/get/"}, method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getCodeOfConduct (
            @RequestParam(required = false) String localizationId,
            @RequestParam() StaticContentType type
    ) throws IOException {
        localizationId = localizationId != null && !localizationId.isEmpty() ? localizationId : localizationService.getDefaultLocalization().getId();
        String text = service.getContent(localizationId, type);
        return ResponseEntity.ok(new RestResponse(text));
    }

    @RequestMapping(value = {"/save", "/save/"}, method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<RestResponse> saveCodeOfConduct (
        @RequestBody StaticContentDTO dto
    ) throws IOException {
        service.saveContent(dto);
        return ResponseEntity.ok(new RestResponse(true));
    }

}
