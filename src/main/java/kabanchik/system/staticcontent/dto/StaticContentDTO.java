package kabanchik.system.staticcontent.dto;

import kabanchik.system.staticcontent.domain.StaticContentType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaticContentDTO {

    private String localizationId;

    private StaticContentType type;

    private String text;
}
