package kabanchik.system.staticcontent;

import kabanchik.core.config.CustomConfig;
import kabanchik.system.staticcontent.domain.StaticContentType;
import kabanchik.system.staticcontent.dto.StaticContentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

@Service
public class StaticContentService {

    private static final String FILE_EXT = "html";
    private static final String HTML_PATH = "templates/html";

    @Autowired
    private CustomConfig config;

    public String getContent(String localizationId, StaticContentType type) throws IOException {
        byte[] data;

        File file = new File(config.getFiles().getBase() + config.getFiles().getStaticContent() + "/" + type.toString().toLowerCase() + "_" + localizationId.toLowerCase() + "." + FILE_EXT);

        if (!file.exists()) {
            Resource defaultTemplate = new ClassPathResource(HTML_PATH + "/" + type.toString().toLowerCase() + "." + FILE_EXT);
            file = defaultTemplate.getFile();
        }

        FileInputStream fis = new FileInputStream(file);
        data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        return new String(data, "UTF-8");
    }


    public void saveContent(StaticContentDTO dto) throws IOException {
        File folder = new File(config.getFiles().getBase() + config.getFiles().getStaticContent());
        folder.mkdirs();
        File file = new File(folder.getAbsolutePath() + "/" + dto.getType().toString().toLowerCase() + "_" + dto.getLocalizationId().toLowerCase() + "." + FILE_EXT);

        FileWriter fileWriter = new FileWriter(file, false);
        try {
            fileWriter.write(dto.getText());
        } finally {
            fileWriter.close();
        }
    }

    public void deleteFilesByLocale(String localizationId){
        for (StaticContentType type: StaticContentType.values()){
            File file = new File(config.getFiles().getBase() + config.getFiles().getStaticContent() + "/" + type.toString().toLowerCase() + "_" + localizationId.toLowerCase() + "." + FILE_EXT);

            if (file.exists()) {
                file.delete();
            }
        }
    }
}
