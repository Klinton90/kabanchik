package kabanchik.system.timesheetrate;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.timesheetrate.domain.TimesheetRate;
import kabanchik.system.timesheetrate.dto.TimesheetRateDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimesheetRateService extends RestService<TimesheetRate, TimesheetRateDTO, Integer> {

    @Getter
    @Autowired
    private TimesheetRateRepository repository;

    @Getter
    @Autowired
    private TimesheetRateService self;

    @Override
    public boolean isInUse(Integer id) {
        return false;
    }

    @Override
    public void validate(TimesheetRate timesheetRate) throws CustomValidationException {
        String objectName = TimesheetRate.class.getSimpleName();
        Object[] messageParams = {objectName, timesheetRate.getTimesheetAccessId()};
        if (timesheetRate.getFrom().isAfter(timesheetRate.getTo())) {
            throw new CustomValidationException("wrongDates", objectName, messageParams);
        }
        if (repository.hasCollidingRates(timesheetRate)) {
            throw new CustomValidationException("collidingDates", objectName, messageParams);
        }
    }

}
