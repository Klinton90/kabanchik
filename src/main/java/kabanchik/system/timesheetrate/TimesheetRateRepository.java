package kabanchik.system.timesheetrate;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.currencyrate.domain.CurrencyRate;
import kabanchik.system.timesheetrate.domain.TimesheetRate;
import org.springframework.stereotype.Repository;

import static kabanchik.system.timesheetrate.domain.QTimesheetRate.timesheetRate;

@Repository
public interface TimesheetRateRepository extends RestRepository<TimesheetRate, Integer> {

    default boolean hasCollidingRates(TimesheetRate timesheetRateToValidate) {
        BooleanExpression booleanExpression =
                timesheetRate.timesheetAccessId.eq(timesheetRateToValidate.getTimesheetAccessId())
                .and(
                        timesheetRate.to.goe(timesheetRateToValidate.getFrom())
                        .and(timesheetRate.from.loe(timesheetRateToValidate.getFrom()))
                        .or(
                                timesheetRate.from.loe(timesheetRateToValidate.getTo())
                                .and(timesheetRate.to.goe(timesheetRateToValidate.getTo()))
                        )
                        .or(
                                timesheetRate.from.loe(timesheetRateToValidate.getFrom())
                                .and(timesheetRate.to.goe(timesheetRateToValidate.getTo()))
                        )
                        .or(
                                timesheetRate.from.goe(timesheetRateToValidate.getFrom())
                                .and(timesheetRate.to.loe(timesheetRateToValidate.getTo()))
                        )
                );

        if (timesheetRateToValidate.getId() != null) {
            booleanExpression = booleanExpression.and(timesheetRate.id.ne(timesheetRateToValidate.getId()));
        }

        return !new JPAQuery<CurrencyRate>(getEntityManager())
                .from(timesheetRate)
                .where(booleanExpression)
                .fetchResults()
                .isEmpty();
    }

}
