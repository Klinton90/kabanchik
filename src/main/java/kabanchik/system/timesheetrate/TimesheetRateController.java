package kabanchik.system.timesheetrate;

import kabanchik.core.controller.RestController;
import kabanchik.system.timesheetrate.domain.TimesheetRate;
import kabanchik.system.timesheetrate.dto.TimesheetRateDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/timesheetRate")
public class TimesheetRateController extends RestController<TimesheetRate, TimesheetRateDTO, Integer> {

    @Getter
    @Autowired
    private TimesheetRateService service;

}
