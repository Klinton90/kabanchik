package kabanchik.system.timesheetrate.dto;

import kabanchik.core.domain.IDto;
import kabanchik.system.timesheetrate.domain.TimesheetRate;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class TimesheetRateDTO implements IDto<TimesheetRate, Integer> {

    private Integer id;

    @NotNull
    @DecimalMin(value = "0.01")
    @DecimalMax(value = "999999.99")
    @Digits(integer = 6, fraction = 2)
    private BigDecimal rate;

    @NotNull
    @DecimalMin(value = "0.01")
    @DecimalMax(value = "999999.99")
    @Digits(integer = 6, fraction = 2)
    private BigDecimal internalRate;

    @NotNull
    private LocalDate from;

    @NotNull
    private LocalDate to;

    @NotNull
    private Integer timesheetAccessId;

}
