package kabanchik.system.timesheetrate.domain;

import kabanchik.core.domain.IDomain;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.timesheetrate.dto.TimesheetRateDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name="timesheet_rate")
public class TimesheetRate implements IDomain<TimesheetRateDTO, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timesheet_rate_id")
    private Integer id;

    @Column(name = "rate", nullable = false, precision = 19, scale = 2)
    private BigDecimal rate;

    @Column(name = "internal_rate", nullable = false, precision = 19, scale = 2)
    private BigDecimal internalRate;

    @Column(name = "date_from", nullable = false)
    private LocalDate from;

    @Column(name = "date_to", nullable = false)
    private LocalDate to;

    @Column(name = "timesheet_access_id", nullable = false)
    private Integer timesheetAccessId;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "timesheet_access_id", insertable = false, updatable = false)
    private TimesheetAccess timesheetAccess;

}
