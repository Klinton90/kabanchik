package kabanchik.system.currencyrate;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.currencyrate.domain.CurrencyRate;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.currencyrate.domain.QCurrencyRate.currencyRate;

@Repository
public interface CurrencyRateRepository extends RestRepository<CurrencyRate, Integer> {

    default boolean hasCollidingRates(CurrencyRate currencyRateToValidate) {
        BooleanExpression booleanExpression =
                currencyRate.currency.eq(currencyRateToValidate.getCurrency())
                .and(
                        currencyRate.to.goe(currencyRateToValidate.getFrom())
                        .and(currencyRate.from.loe(currencyRateToValidate.getFrom()))
                        .or(
                                currencyRate.from.loe(currencyRateToValidate.getTo())
                                .and(currencyRate.to.goe(currencyRateToValidate.getTo()))
                        )
                        .or(
                                currencyRate.from.loe(currencyRateToValidate.getFrom())
                                .and(currencyRate.to.goe(currencyRateToValidate.getTo()))
                        )
                        .or(
                                currencyRate.from.goe(currencyRateToValidate.getFrom())
                                .and(currencyRate.to.loe(currencyRateToValidate.getTo()))
                        )
                );

        if (currencyRateToValidate.getId() != null) {
            booleanExpression = booleanExpression.and(currencyRate.id.ne(currencyRateToValidate.getId()));
        }

        return !new JPAQuery<CurrencyRate>(getEntityManager())
                .from(currencyRate)
                .where(booleanExpression)
                .fetchResults()
                .isEmpty();
    }

    default List<CurrencyRate> getForPSR(Integer timesheetCategoryId) {
        return new JPAQuery<CurrencyRate>(getEntityManager())
                .from(currencyRate)
                .where(currencyRate.currencyDomain.invoices.any().timesheetCategoryId.eq(timesheetCategoryId)
                        .or(currencyRate.currencyDomain.outcomeEntries.any().timesheetCategoryId.eq(timesheetCategoryId))
                        .or(currencyRate.currencyDomain.payments.any().invoice.timesheetCategoryId.eq(timesheetCategoryId)))
                .fetch();
    }
}
