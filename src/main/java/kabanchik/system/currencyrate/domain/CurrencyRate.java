package kabanchik.system.currencyrate.domain;

import kabanchik.core.domain.IDomain;
import kabanchik.system.currency.domain.Currency;
import kabanchik.system.currencyrate.dto.CurrencyRateDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "currency_rate")
public class CurrencyRate implements IDomain<CurrencyRateDTO, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "currency_rate_id")
    private Integer id;

    @Column(name = "currency_id", length = 3, nullable = false)
    private String currency;

    @Column(name = "rate", nullable = false)
    private BigDecimal rate;

    @Column(name = "date_from", nullable = false)
    private LocalDate from;

    @Column(name = "date_to", nullable = false)
    private LocalDate to;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "currency_id", nullable = false, insertable = false, updatable = false)
    private Currency currencyDomain;

}
