package kabanchik.system.currencyrate;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.currencyrate.domain.CurrencyRate;
import kabanchik.system.currencyrate.dto.CurrencyRateDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/currencyRate")
public class CurrencyRateController extends RestController<CurrencyRate, CurrencyRateDTO, Integer> {

    @Getter
    @Autowired
    private CurrencyRateService service;

    @RequestMapping(value = "/getForPSR", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForPSR(
            @RequestParam Integer timesheetCategoryId
    ){
        List<CurrencyRateDTO> result = service.getForPSR(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
