package kabanchik.system.currencyrate;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.currencyrate.domain.CurrencyRate;
import kabanchik.system.currencyrate.dto.CurrencyRateDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CurrencyRateService extends RestService<CurrencyRate, CurrencyRateDTO, Integer> {

    @Getter
    @Autowired
    private CurrencyRateRepository repository;

    @Getter
    @Autowired
    private CurrencyRateService self;

    @Override
    public boolean isInUse(Integer integer) {
        return false;
    }

    @Override
    public void validate(CurrencyRate currencyRate) throws CustomValidationException {
        String objectName = CurrencyRate.class.getSimpleName();
        Object[] messageParams = {objectName, currencyRate.getCurrency()};
        if (currencyRate.getFrom().isAfter(currencyRate.getTo())) {
            throw new CustomValidationException("wrongDates", objectName, messageParams);
        }
        if (repository.hasCollidingRates(currencyRate)) {
            throw new CustomValidationException("collidingDates", objectName, messageParams);
        }
    }

    public List<CurrencyRateDTO> getForPSR(Integer timesheetCategoryId) {
        return repository.getForPSR(timesheetCategoryId)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }
}
