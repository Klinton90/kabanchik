package kabanchik.system.currencyrate.dto;

import kabanchik.core.domain.IDto;
import kabanchik.system.currencyrate.domain.CurrencyRate;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class CurrencyRateDTO implements IDto<CurrencyRate, Integer> {

    @NotNull
    private Integer id;

    @NotNull
    @Size(min = 3, max = 3)
    private String currency;

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("1000.00")
    private BigDecimal rate;

    @NotNull
    private LocalDate from;

    @NotNull
    private LocalDate to;

}
