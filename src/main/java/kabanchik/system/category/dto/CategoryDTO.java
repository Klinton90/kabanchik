package kabanchik.system.category.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.category.domain.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryDTO implements IDto<Category, Integer>, ActivatableDomain {

    private Integer id;

    private Boolean isActive;

}
