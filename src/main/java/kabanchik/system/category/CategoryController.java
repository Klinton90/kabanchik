package kabanchik.system.category;

import kabanchik.core.controller.RestController;
import kabanchik.system.category.domain.Category;
import kabanchik.system.category.dto.CategoryDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value={"/category"})
public class CategoryController extends RestController<Category, CategoryDTO, Integer>{

    @Getter
    @Autowired
    private CategoryService service;

}
