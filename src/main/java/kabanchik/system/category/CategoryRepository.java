package kabanchik.system.category;

import kabanchik.core.repository.RestRepository;
import kabanchik.system.category.domain.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends RestRepository<Category, Integer>{
}
