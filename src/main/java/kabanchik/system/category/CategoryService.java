package kabanchik.system.category;

import kabanchik.core.service.RestService;
import kabanchik.system.category.domain.Category;
import kabanchik.system.category.dto.CategoryDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends RestService<Category, CategoryDTO, Integer>{

    @Getter
    @Autowired
    private CategoryRepository repository;

    @Getter
    @Autowired
    private CategoryService self;

    @Override
    public boolean isInUse(Integer id){
        return false;
    }
}
