package kabanchik.system.category.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.validator.Unique;
import kabanchik.system.category.dto.CategoryDTO;
import kabanchik.system.subcategory.domain.SubCategory;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Unique
@Getter
@Setter
@Table(name="categories")
@NamedEntityGraphs(value = {
        @NamedEntityGraph(name = "Category.default", attributeNodes = {
                @NamedAttributeNode("subCategories")
        })
})
public class Category implements IDomain<CategoryDTO, Integer>, ActivatableDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Integer id;

    @Column(name = "is_active")
    private Boolean isActive;

    @NotNull
    @Size(min = 5, max = 50)
    @Column(name = "name", nullable = false, length = 50, unique = true)
    @ContainsFilterField
    private String name;

    @NotNull
    @Size(min = 5, max = 255)
    @Column(name = "description", nullable = false, unique = true)
    @ContainsFilterField
    private String description;

    @OneToMany(mappedBy = "parentCategory", orphanRemoval = true)
    private List<SubCategory> subCategories;

}
