package kabanchik.system.supplier;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.outcomeentry.OutcomeEntryService;
import kabanchik.system.outcomeentry.domain.OutcomeEntryType;
import kabanchik.system.supplier.domain.Supplier;
import kabanchik.system.supplier.dto.SupplierDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SupplierService extends RestService<Supplier, SupplierDTO, Integer> {

    @Getter
    @Autowired
    private SupplierRepository repository;

    @Getter
    @Autowired
    private SupplierService self;

    @Autowired
    private OutcomeEntryService outcomeEntryService;

    @Override
    public boolean isInUse(Integer id) {
        return outcomeEntryService.countByOutcomeEntryTypeAndParentId(OutcomeEntryType.SUPPLIER, id) > 0;
    }

    public Supplier getOneByGovernmentId(String governmentId) throws CustomValidationException {
        Supplier entity = repository.findOneByGovernmentId(governmentId);
        if (entity == null) {
            String objectName = getDomainClass().getSimpleName();
            Object[] messageParams = {objectName, governmentId};
            throw new CustomValidationException("rowNotFound", objectName, messageParams);
        }
        return entity;
    }

    public List<SupplierDTO> getForPSR(Integer timesheetCategoryId) {
        return repository.getForPSR(timesheetCategoryId)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

}
