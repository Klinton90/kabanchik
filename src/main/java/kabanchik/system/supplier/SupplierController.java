package kabanchik.system.supplier;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.supplier.domain.Supplier;
import kabanchik.system.supplier.dto.SupplierDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/supplier")
public class SupplierController extends RestController<Supplier, SupplierDTO, Integer> {

    @Getter
    @Autowired
    private SupplierService service;

    @RequestMapping(value = "/getForPSR", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForPSR(
            @RequestParam Integer timesheetCategoryId
    ){
        List<SupplierDTO> result = service.getForPSR(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
