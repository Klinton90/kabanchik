package kabanchik.system.supplier;

import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.supplier.domain.Supplier;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.outcomeentry.domain.QOutcomeEntry.outcomeEntry;
import static kabanchik.system.supplier.domain.QSupplier.supplier;

@Repository
public interface SupplierRepository extends RestRepository<Supplier, Integer> {
    Supplier findOneByGovernmentId(String governmentId);

    default List<Supplier> getForPSR(Integer timesheetCategoryId) {
        return new JPAQuery<Supplier>(getEntityManager())
                .distinct()
                .from(supplier)
                .leftJoin(supplier.outcomeEntries, outcomeEntry)
                .on(outcomeEntry.timesheetCategoryId.eq(timesheetCategoryId))
                .where(outcomeEntry.id.isNotNull())
                .fetch();
    }
}
