package kabanchik.system.supplier.dto;

import kabanchik.system.outcomeentry.domain.OutcomeEntryType;
import kabanchik.system.outcomeentry.dto.OutcomeEntryParentDTO;
import kabanchik.system.supplier.domain.Supplier;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class SupplierDTO implements OutcomeEntryParentDTO<Supplier> {

    private Integer id;

    @NotNull
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    @Size(min = 8, max = 10)
    private String governmentId;

    @Override
    public OutcomeEntryType getOutcomeEntryType() {
        return OutcomeEntryType.SUPPLIER;
    }

}
