package kabanchik.system.timesheetcategory;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import static kabanchik.system.timesheet.domain.QTimesheet.timesheet;
import static kabanchik.system.timesheetcategory.domain.QTimesheetCategory.timesheetCategory;
import static kabanchik.system.timesheetaccess.domain.QTimesheetAccess.timesheetAccess;

@Repository
public interface TimesheetCategoryRepository extends RestRepository<TimesheetCategory, Integer> {

    TimesheetCategory findOneByName(String name);

    @Deprecated
    default List<TimesheetCategory> findDistinctAll(){
        return new JPAQuery<TimesheetCategory>(getEntityManager()).from(timesheetCategory)
                .distinct()
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .orderBy(timesheetCategory.name.asc())
                .fetch();
    }

    @Deprecated
    default List<TimesheetCategory> getManagedTimesheetCategoriesByManagerId(Integer managerId){
        BooleanExpression booleanExpression = timesheetCategory.managers.any().id.eq(managerId);

        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .distinct()
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .where(booleanExpression)
                .orderBy(timesheetCategory.name.asc())
                .fetch();
    }

    default List<TimesheetCategory> getManagedTimesheetCategories(
            User principal,
            Boolean forPsr,
            String queryString,
            Integer limit,
            Integer offset
    ) {
        BooleanBuilder booleanBuilder = new BooleanBuilder();

        if (!principal.getRole().equals(UserRole.ADMIN)) {
            booleanBuilder = booleanBuilder.and(timesheetCategory.managers.any().id.eq(principal.getId()));
        }

        if (forPsr) {
            booleanBuilder = booleanBuilder.and(timesheetCategory.allowForPsr.isTrue());
        }

        if (StringUtils.isNotBlank(queryString)) {
            booleanBuilder = booleanBuilder.and(timesheetCategory.name.contains(queryString)
                    .or(timesheetCategory.description.contains(queryString)));
        }

        JPAQuery<TimesheetCategory> query = new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .distinct()
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .where(booleanBuilder)
                .orderBy(timesheetCategory.isActive.desc(), timesheetCategory.name.asc());

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

    default List<TimesheetCategory> getManagedTimesheetCategoriesForSummaryByManagerAndPeriod(User manager, Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo, Integer departmentId, Boolean forPsr) {
        Pair<LocalDate, LocalDate> datesPair = ConfigurationService.getDatesByDateCriteria(yearFrom, monthFrom, yearTo, monthTo);
        BooleanExpression booleanExpression = timesheet.date.between(datesPair.getFirst(), datesPair.getSecond());

        if (forPsr) {
            booleanExpression = booleanExpression.and(timesheet.status.eq(TimesheetStatus.PLANNED))
                    .and(timesheetCategory.isActive.isTrue());
        } else {
            booleanExpression = booleanExpression.and(timesheet.status.notIn(TimesheetStatus.DECLINED, TimesheetStatus.PLANNED));

            if (!manager.getRole().equals(UserRole.ADMIN)) {
                booleanExpression = booleanExpression.and(timesheetCategory.managers.any().id.eq(manager.getId()));
            }
        }

        if (departmentId != null && departmentId > 0) {
            booleanExpression = booleanExpression.and(timesheet.timesheetAccess.user.departmentId.eq(departmentId));
        }

        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .distinct()
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .innerJoin(timesheetCategory.timesheetAccesses, timesheetAccess)
                .innerJoin(timesheetAccess.timesheets, timesheet)
                .where(booleanExpression)
                .orderBy(timesheetCategory.name.asc())
                .fetch();
    }

    default List<TimesheetCategory> findAllByUserIdAndPeriod(Integer userId, Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo){
        return findAllTimesheetCategoriesByUserIdAndPeriodAndPredicate(userId, yearFrom, monthFrom, yearTo, monthTo, new BooleanBuilder());
    }

    default List<TimesheetCategory> findAllManagedTimesheetCategoriesByManagerAndUserIdAndPeriod(User manager, Integer userId, Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo){
        BooleanBuilder builder = new BooleanBuilder();
        if (!manager.getRole().equals(UserRole.ADMIN)) {
            builder = builder.and(timesheetCategory.managers.any().id.eq(manager.getId()));
        }

        return findAllTimesheetCategoriesByUserIdAndPeriodAndPredicate(userId, yearFrom, monthFrom, yearTo, monthTo, builder);
    }

    default List<TimesheetCategory> findAllTimesheetCategoriesByUserIdAndPeriodAndPredicate(Integer userId, Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo, BooleanBuilder builder){
        Pair<LocalDate, LocalDate> datesPair = ConfigurationService.getDatesByDateCriteria(yearFrom, monthFrom, yearTo, monthTo);

        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .distinct()
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .innerJoin(timesheetCategory.timesheetAccesses, timesheetAccess)
                .leftJoin(timesheetAccess.timesheets, timesheet)
                .on(timesheet.date.between(datesPair.getFirst(), datesPair.getSecond())
                        .and(timesheet.status.notIn(TimesheetStatus.PLANNED)))
                .where(builder
                        .and(timesheetAccess.userId.eq(userId)
                        .and(timesheetCategory.isActive.isTrue()
                                .and(timesheetAccess.isActive.isTrue())
                                .or(timesheet.id.isNotNull()))))
                .orderBy(timesheetCategory.name.asc())
                .fetch();
    }

    @Deprecated
    default List<TimesheetCategory> getManagedTimesheetCategoriesByManagerIdAndUserId(Integer managerId, Integer userId) {
        BooleanExpression booleanExpression = timesheetCategory.managers.any().id.eq(managerId)
                .and(timesheetCategory.timesheetAccesses.any().userId.eq(userId));

        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .distinct()
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .where(booleanExpression)
                .orderBy(timesheetCategory.name.asc())
                .fetch();
    }

    @Deprecated
    default List<TimesheetCategory> findAllByUserId(Integer userId){
        return getTimesheetCategoriesByUserIdAndPredicate(userId, new BooleanBuilder());
    }

    @Deprecated
    default List<TimesheetCategory> getManagedTimesheetCategoriesByManagerAndUserId(User manager, Integer userId){
        BooleanBuilder builder = new BooleanBuilder();
        if (!manager.getRole().equals(UserRole.ADMIN)) {
            builder = builder.and(timesheetCategory.managers.any().id.eq(manager.getId()));
        }

        return getTimesheetCategoriesByUserIdAndPredicate(userId, builder);
    }

    @Deprecated
    default List<TimesheetCategory> getTimesheetCategoriesByUserIdAndPredicate(Integer userId, BooleanBuilder builder){
        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .distinct()
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .innerJoin(timesheetCategory.timesheetAccesses, timesheetAccess)
                .leftJoin(timesheetAccess.timesheets, timesheet)
                .on(timesheet.status.in(TimesheetStatus.POSTED, TimesheetStatus.EDITABLE))
                .where(builder
                        .and(timesheetAccess.userId.eq(userId))
                        .and(timesheetCategory.isActive.isTrue()
                                .and(timesheetAccess.isActive.isTrue())
                                .or(timesheet.id.isNotNull())))
                .orderBy(timesheetCategory.name.asc())
                .fetch();
    }

    default TimesheetCategory findByIdAndManagerId(Integer id, Integer managerId){
        BooleanExpression booleanExpression = timesheetCategory.id.eq(id)
                .and(timesheetCategory.managers.any().id.eq(managerId));

        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .where(booleanExpression)
                .fetchOne();
    }

    default Long countByManagerId(int id){
        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("TimesheetCategory.empty"))
                .where(timesheetCategory.managers.any().id.eq(id))
                .fetchCount();
    }

    @Deprecated
    default List<TimesheetCategory> getForPsr() {
        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .distinct()
                .from(timesheetCategory)
                .where(timesheetCategory.allowForPsr.isTrue()
                        .and(timesheetCategory.isActive.isTrue()))
                .orderBy(timesheetCategory.name.asc())
                .fetch();
    }

    default List<TimesheetCategory> findAllManagedTimesheetCategoriesByManagerIdAndIdIn(Integer managerId, List<Integer> timesheetCategoryIds) {
        return new JPAQuery<TimesheetCategory>(getEntityManager())
                .from(timesheetCategory)
                .where(timesheetCategory.managers.any().id.eq(managerId)
                        .and(timesheetCategory.id.in(timesheetCategoryIds)))
                .fetch();
    }

    List<TimesheetCategory> findAllByIdIn(List<Integer> timesheetCategoryIds);
}
