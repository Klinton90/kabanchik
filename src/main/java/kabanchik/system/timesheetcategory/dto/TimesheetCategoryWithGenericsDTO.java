package kabanchik.system.timesheetcategory.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IGenericFieldAwareDto;
import kabanchik.system.genericfieldvalue.dto.GenericFieldValueDTO;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TimesheetCategoryWithGenericsDTO extends TimesheetCategoryWithManagersDTO implements IGenericFieldAwareDto<TimesheetCategory>, ActivatableDomain {

    private List<GenericFieldValueDTO> genericFieldValues;

}
