package kabanchik.system.timesheetcategory.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Getter
@Setter
public class TimesheetCategorySimpleDTO implements IDto<TimesheetCategory, Integer>, ActivatableDomain {

    private Integer id;

    @NotNull
    private Boolean isActive;

    @NotNull
    private Boolean requireApproval;

    @NotNull
    @Size(min = 1, max=50)
    private String name;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    private Boolean allowForPsr;

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("99999999999999999.99")
    @Digits(integer = 17, fraction = 2)
    private BigDecimal initialBudget;

    @NotNull
    @Min(1)
    private Integer contractDays;

    @Override
    public TimesheetCategorySimpleDTO createFromEntity(TimesheetCategory entity){
        BeanUtils.copyProperties(entity, this, true);
        return this;
    }

}
