package kabanchik.system.timesheetcategory.dto;

import kabanchik.system.user.dto.UserSimpleDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TimesheetCategoryWithManagersDTO extends TimesheetCategorySimpleDTO {

    private List<UserSimpleDTO> managers;

}
