package kabanchik.system.timesheetcategory.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.domain.IGenericFieldAwareDomain;
import kabanchik.core.validator.Unique;
import kabanchik.system.departmentaccess.domain.DepartmentAccess;
import kabanchik.system.genericfieldvalue.domain.GenericFieldValue;
import kabanchik.system.invoice.domain.Invoice;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import kabanchik.system.psrrevision.domain.PsrRevision;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.timesheetcategory.dto.TimesheetCategoryWithGenericsDTO;
import kabanchik.system.user.domain.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Unique
@Entity
@Getter
@Setter
@Table(name="timesheet_category")
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "TimesheetCategory.default",
        attributeNodes =  {
            @NamedAttributeNode(value = "managers")
        }
    ),
    @NamedEntityGraph(
        name = "TimesheetCategory.generics",
        attributeNodes =  {
            @NamedAttributeNode(value = "genericFieldValues")
        }
    ),
    @NamedEntityGraph(
        name = "TimesheetCategory.empty"
    )
})
public class TimesheetCategory implements IGenericFieldAwareDomain<TimesheetCategoryWithGenericsDTO>, ActivatableDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timesheet_category_id")
    private Integer id;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Column(name = "require_approval", nullable = false)
    private Boolean requireApproval;

    @ContainsFilterField
    @Column(name = "name", nullable = false, length = 50, unique = true)
    private String name;

    @ContainsFilterField
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "allow_for_psr", nullable = false)
    private Boolean allowForPsr;

    @Column(name = "initial_budget", nullable = false, precision = 19, scale = 2)
    private BigDecimal initialBudget;

    @Column(name="contract_days", nullable = false)
    private Integer contractDays;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "timesheetCategory", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<TimesheetAccess> timesheetAccesses;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "timesheetCategory", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<DepartmentAccess> departmentAccesses;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "timesheetCategory", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<PsrRevision> psrRevisions;

    @ManyToMany
    @JoinTable(
            name = "timesheet_category_managers",
            joinColumns = @JoinColumn(name = "timesheet_category_id", referencedColumnName = "timesheet_category_id"),
            inverseJoinColumns = @JoinColumn(name = "manager_id", referencedColumnName = "user_id")
    )
    @Fetch(FetchMode.SUBSELECT)
    private List<User> managers;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "timesheetCategory", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<OutcomeEntry> outcomeEntries;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "timesheetCategory", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Invoice> invoices;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(mappedBy = "timesheetCategory", cascade = CascadeType.ALL)
    @Filter(name = "genericFieldValueTimesheetCategory")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private List<GenericFieldValue> genericFieldValues;

    @Override
    public String toString(){
        String id = this.id != null ? this.id.toString() : "null";
        return id + ":" + name;
    }

}
