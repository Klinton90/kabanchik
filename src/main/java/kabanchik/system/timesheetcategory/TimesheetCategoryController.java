package kabanchik.system.timesheetcategory;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.timesheetcategory.dto.TimesheetCategorySimpleDTO;
import kabanchik.system.timesheetcategory.dto.TimesheetCategoryWithGenericsDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value="/timesheetCategory")
public class TimesheetCategoryController extends RestController<TimesheetCategory, TimesheetCategoryWithGenericsDTO, Integer> {

    @Getter
    @Autowired
    private TimesheetCategoryService service;

    @RequestMapping(value = "/listAllSimple", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> listAllSimple(
            Pageable pageable,
            @RequestParam(required = false, defaultValue = "false") boolean showActive
    ) {
        Iterable<TimesheetCategorySimpleDTO> result = getService().getAllSimple(pageable.getSort(), showActive);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @Deprecated
    @RequestMapping(value = "/getListByCurrentUser", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> getListByCurrentUser(){
        List<TimesheetCategorySimpleDTO> result = getService().getByCurrentUser();
        return ResponseEntity.ok(new RestResponse(result));
    }

    @Deprecated
    @RequestMapping(value = "/getListManagedTimesheetCategoriesByUser", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> getListManagedTimesheetCategoriesByUser(@RequestParam Integer userId){
        List<TimesheetCategorySimpleDTO> result = getService().getManagedTimesheetCategoriesByUser(userId);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getManagedTimesheetCategories", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> getManagedTimesheetCategories(
            @RequestParam Boolean forPsr,
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ){
        List<TimesheetCategoryWithGenericsDTO> result = getService().getManagedTimesheetCategories(forPsr, queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getManagedTimesheetCategoriesForSummaryByPeriod", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getManagedTimesheetCategoriesForSummaryByPeriod(
            @RequestParam Integer yearFrom,
            @RequestParam Integer monthFrom,
            @RequestParam Integer yearTo,
            @RequestParam Integer monthTo,
            @RequestParam(required = false) Integer departmentId
    ){
        List<TimesheetCategorySimpleDTO> result = service.getManagedTimesheetCategoriesForSummaryByPeriod(yearFrom, monthFrom, yearTo, monthTo, departmentId, false);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getTimesheetCategoriesForPsrSummaryByPeriod", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getTimesheetCategoriesForPsrSummaryByPeriod(
            @RequestParam Integer yearFrom,
            @RequestParam Integer monthFrom,
            @RequestParam Integer yearTo,
            @RequestParam Integer monthTo,
            @RequestParam(required = false) Integer departmentId
    ){
        List<TimesheetCategorySimpleDTO> result = service.getManagedTimesheetCategoriesForSummaryByPeriod(yearFrom, monthFrom, yearTo, monthTo, departmentId, true);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/findAllByCurrentUserAndPeriod", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllByCurrentUserAndPeriod(
            @RequestParam Integer yearFrom,
            @RequestParam Integer monthFrom,
            @RequestParam Integer yearTo,
            @RequestParam Integer monthTo
    ){
        List<TimesheetCategorySimpleDTO> result = service.findAllByCurrentUserAndPeriod(yearFrom, monthFrom, yearTo, monthTo);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/findAllManagedTimesheetCategoriesByUserIdAndPeriod", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllManagedTimesheetCategoriesByUserIdAndPeriod(
            @RequestParam Integer userId,
            @RequestParam Integer yearFrom,
            @RequestParam Integer monthFrom,
            @RequestParam Integer yearTo,
            @RequestParam Integer monthTo
    ){
        List<TimesheetCategorySimpleDTO> result = service.findAllManagedTimesheetCategoriesByUserIdAndPeriod(userId, yearFrom, monthFrom, yearTo, monthTo);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @Deprecated
    @RequestMapping(value = "/getForPsr", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForPsr() {
        List<TimesheetCategorySimpleDTO> result = service.getForPsr();
        return ResponseEntity.ok(new RestResponse(result));
    }

    @Deprecated
    @PutMapping(value = "/updateGenericFieldValues")
    public @ResponseBody ResponseEntity<RestResponse> updateGenericFieldValues(@RequestBody TimesheetCategoryWithGenericsDTO dto) throws CustomValidationException {
        service.updateGenericFieldValues(dto);
        return ResponseEntity.ok(new RestResponse(null));
    }

    @PutMapping(value = "/updateManagerFields")
    public @ResponseBody ResponseEntity<RestResponse> updateManagerFields(@RequestBody TimesheetCategoryWithGenericsDTO dto) throws CustomValidationException {
        return ResponseEntity.ok(new RestResponse(service.updateManagerFields(dto)));
    }

}
