package kabanchik.system.timesheetcategory;

import com.cosium.spring.data.jpa.entity.graph.domain.EntityGraphUtils;
import com.querydsl.core.BooleanBuilder;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.GenericFieldAwareRestService;
import kabanchik.system.department.DepartmentService;
import kabanchik.system.departmentaccess.domain.DepartmentAccess;
import kabanchik.system.genericfield.domain.GenericFieldEntity;
import kabanchik.system.outcomeentry.OutcomeEntryService;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.timesheetcategory.dto.TimesheetCategorySimpleDTO;
import kabanchik.system.timesheetaccess.TimesheetAccessService;
import kabanchik.system.user.UserService;
import kabanchik.system.user.domain.User;
import kabanchik.system.timesheetcategory.dto.TimesheetCategoryWithGenericsDTO;
import kabanchik.system.user.domain.UserRole;
import kabanchik.system.user.dto.UserSimpleDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TimesheetCategoryService extends GenericFieldAwareRestService<TimesheetCategory, TimesheetCategoryWithGenericsDTO> {

    @Getter
    @Autowired
    private TimesheetCategoryRepository repository;

    @Autowired
    private TimesheetAccessService timesheetAccessService;

    @Autowired
    private UserService userService;

    @Getter
    @Autowired
    private TimesheetCategoryService self;

    @Autowired
    private OutcomeEntryService outcomeEntryService;

    @Autowired
    private DepartmentService departmentService;

    @Override
    public boolean isInUse(Integer id) {
        return timesheetAccessService.countByTimesheetCategoryId(id) > 0 || outcomeEntryService.countByTimesheetCategoryId(id) > 0;
    }

    public TimesheetCategory getOneByName(String name) throws CustomValidationException {
        TimesheetCategory entity = repository.findOneByName(name);
        if (entity == null) {
            String objectName = getDomainClass().getSimpleName();
            Object[] messageParams = {objectName, name};
            throw new CustomValidationException("rowNotFound", objectName, messageParams);
        }
        return entity;
    }

    @Deprecated
    public List<TimesheetCategorySimpleDTO> getByCurrentUser() {
        User principal = UserService.getPrincipal();
        return getRepository().findAllByUserId(principal.getId())
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public List<TimesheetCategoryWithGenericsDTO> getManagedTimesheetCategories(
            Boolean forPsr,
            String queryString,
            Integer limit,
            Integer offset
    ) {
        return repository.getManagedTimesheetCategories(UserService.getPrincipal(), forPsr, queryString, limit, offset)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

    public List<TimesheetCategorySimpleDTO> getManagedTimesheetCategoriesForSummaryByPeriod(Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo, Integer departmentId, Boolean forPsr) {
        return repository.getManagedTimesheetCategoriesForSummaryByManagerAndPeriod(UserService.getPrincipal(), yearFrom, monthFrom, yearTo, monthTo, departmentId, forPsr)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public List<TimesheetCategorySimpleDTO> findAllByCurrentUserAndPeriod(Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo){
        return repository.findAllByUserIdAndPeriod(UserService.getPrincipal().getId(), yearFrom, monthFrom, yearTo, monthTo)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public List<TimesheetCategorySimpleDTO> findAllManagedTimesheetCategoriesByUserIdAndPeriod(Integer userId, Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo){
        return repository.findAllManagedTimesheetCategoriesByManagerAndUserIdAndPeriod(UserService.getPrincipal(), userId, yearFrom, monthFrom, yearTo, monthTo)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    @Deprecated
    public List<TimesheetCategorySimpleDTO> getManagedTimesheetCategoriesByUser(Integer userId) {
        return getRepository().getManagedTimesheetCategoriesByManagerAndUserId(UserService.getPrincipal(), userId).stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public TimesheetCategory getByIdAndManagerId(Integer id, Integer managerId){
        return repository.findByIdAndManagerId(id, managerId);
    }

    @Deprecated
    public List<TimesheetCategorySimpleDTO> getForPsr() {
        return repository
                .getForPsr()
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public Long countByManagerId(int id) {
        return repository.countByManagerId(id);
    }

    public Iterable<TimesheetCategorySimpleDTO> getAllSimple(Sort sort, boolean showActive) {
        BooleanBuilder where = applyIsActivePredicate(showActive, getDefaultBuilder());

        Iterable<TimesheetCategory> iterable = repository.findAll(where, sort, EntityGraphUtils.fromName("TimesheetCategory.empty"));
        return StreamSupport.stream(sortIterable(iterable, sort).spliterator(), false)
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    @Override
    protected GenericFieldEntity getGenericFieldEntityType() {
        return GenericFieldEntity.TIMESHEET_CATEGORY;
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public TimesheetCategory create(@Valid TimesheetCategory timesheetCategory) throws CustomValidationException {
        List<DepartmentAccess> departmentAccesses = departmentService.getRepository()
                .findAll()
                .stream()
                .map(department -> new DepartmentAccess()
                        .setIsActive(true)
                        .setDepartmentId(department.getId())
                        .setTimesheetCategory(timesheetCategory))
                .collect(Collectors.toList());

        timesheetCategory.setDepartmentAccesses(departmentAccesses);

        return repository.save(timesheetCategory);
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public TimesheetCategory update(@Valid TimesheetCategory dto) throws CustomValidationException {
        TimesheetCategory entity = findOne(dto.getId());

        if (!dto.getIsActive() && entity.getIsActive()) {
            entity.getTimesheetAccesses().forEach(timesheetAccess -> timesheetAccess.setIsActive(false));
//            entity.getDepartmentAccesses().forEach(departmentAccess -> departmentAccess.setIsActive(false));
        }

        return super.update(dto);
    }

    @Override
    public TimesheetCategory initEntity(TimesheetCategoryWithGenericsDTO dto) throws CustomValidationException {
        TimesheetCategory result = super.initEntity(dto);

        if (dto.getManagers() != null) {
            List<Integer> userIds = dto.getManagers()
                    .stream()
                    .map(UserSimpleDTO::getId)
                    .collect(Collectors.toList());

            List<User> users = userService.findAllByIdIn(userIds);
            if (users.size() == userIds.size()) {
                result.setManagers(users);
            } else {
                Integer userIdNotFound = userIds.stream()
                        .filter(userId -> users.stream().anyMatch(user -> user.getId().equals(userId)))
                        .findAny()
                        .orElse(null);

                throw new CustomValidationException("userNotFound", User.class.getSimpleName(), User.class.getSimpleName(), userIdNotFound);
            }
        }

        return result;
    }

    @Override
    public TimesheetCategoryWithGenericsDTO initDto(TimesheetCategory entity){
        TimesheetCategoryWithGenericsDTO result = super.initDto(entity);

        result.setManagers(entity.getManagers()
                .stream()
                .map(manager -> new UserSimpleDTO().createFromEntity(manager))
                .collect(Collectors.toList()));

        return result;
    }

    public TimesheetCategorySimpleDTO initSimpleDTO(TimesheetCategory entity){
        TimesheetCategorySimpleDTO result = new TimesheetCategorySimpleDTO();
        return result.createFromEntity(entity);
    }

    @Transactional
    public TimesheetCategoryWithGenericsDTO updateManagerFields(TimesheetCategoryWithGenericsDTO dto) throws CustomValidationException {
        TimesheetCategory timesheetCategory = updateGenericFieldValues(dto);
        timesheetCategory.setInitialBudget(dto.getInitialBudget());
        timesheetCategory.setContractDays(dto.getContractDays());
        return initDto(timesheetCategory);
    }

    public List<TimesheetCategory> findAllManagedTimesheetCategoriesByManagerIdAndIdIn(Integer managerId, List<Integer> timesheetCategoryIds) {
        return repository.findAllManagedTimesheetCategoriesByManagerIdAndIdIn(managerId, timesheetCategoryIds);
    }

    public List<TimesheetCategory> findAllByIdIn(List<Integer> timesheetCategoryIds) {
        return repository.findAllByIdIn(timesheetCategoryIds);
    }

    public List<TimesheetCategory> guessManagedTimesheetCategories(List<Integer> timesheetCategoryids) {
        List<TimesheetCategory> timesheetCategories;

        if (!UserService.getPrincipal().getRole().equals(UserRole.ADMIN)) {
            timesheetCategories = findAllManagedTimesheetCategoriesByManagerIdAndIdIn(
                    UserService.getPrincipal().getId(),
                    timesheetCategoryids);
        } else {
            timesheetCategories = findAllByIdIn(timesheetCategoryids);
        }

        return timesheetCategories;
    }

}
