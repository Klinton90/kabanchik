package kabanchik.system.holiday.dto;

import kabanchik.core.domain.IDto;
import kabanchik.system.holiday.domain.Holiday;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
public class HolidayDTO implements IDto<Holiday, Integer> {

    private Integer id;

    @NotNull
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max=50)
    private String name;

    @NotNull
    @Size(max = 255)
    private String description;

    private LocalDate date;

}
