package kabanchik.system.holiday;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.holiday.domain.Holiday;
import kabanchik.system.holiday.dto.HolidayDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value="/holiday")
public class HolidayController extends RestController<Holiday, HolidayDTO, Integer> {

    @Getter
    @Autowired
    private HolidayService service;

    @RequestMapping(value = "/getWeeklyMaxList", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> getWeeklyMaxList(@RequestBody List<LocalDate> weeks) {
        Map<Long, Integer> result = service.getWeeklyMaxList(weeks);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
