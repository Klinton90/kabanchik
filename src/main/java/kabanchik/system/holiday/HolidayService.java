package kabanchik.system.holiday;

import kabanchik.core.service.RestService;
import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.holiday.domain.Holiday;
import kabanchik.system.holiday.dto.HolidayDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HolidayService extends RestService<Holiday, HolidayDTO, Integer> {

    @Getter
    @Autowired
    private HolidayRepository repository;

    @Getter
    @Autowired
    private HolidayService self;

    @Override
    public boolean isInUse(Integer integer) {
        return false;
    }

    public List<Holiday> findAllByDateBetween(LocalDate from, LocalDate to){
        return repository.findAllByDateBetweenAndIsActive(from, to, true);
    }

    public Integer calcWeeklyMax(LocalDate startDate, boolean monthlyOrWeeklyMode) {
        Integer startDay = startDate.getDayOfWeek().getValue();
        LocalDate endDate = getFridayOrLastDayOfWeek(startDate, monthlyOrWeeklyMode);
        Integer endDay = endDate.getDayOfWeek().getValue();

        int weeklyMax = Math.min(40, Math.max(0, 1 + endDay - startDay) * 8);
        List<Holiday> holidays = findAllByDateBetween(startDate, endDate);
        weeklyMax -= (holidays.size() * 8);
        return weeklyMax;
    }

    public static LocalDate getFridayOrLastDayOfWeek(LocalDate date, boolean monthlyOrWeeklyMode) {
        LocalDate lastDayOfMonth = date.withDayOfMonth(date.lengthOfMonth());
        LocalDate friday = LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
        if (!friday.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
            friday = friday.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
        }
        return monthlyOrWeeklyMode && (friday.getYear() > lastDayOfMonth.getYear() || friday.getMonthValue() > lastDayOfMonth.getMonthValue()) ? lastDayOfMonth : friday;
    }

    public Map<Long, Integer> getWeeklyMaxList(List<LocalDate> weeks) {
        Map<Long, Integer> result = new HashMap<>();

        LocalDate min = weeks.get(0);
        LocalDate max = weeks.get(0);

        for (LocalDate week : weeks) {
            if (min.isAfter(week)) {
                min = week;
            }
            if (max.isBefore(week)) {
                max = week;
            }
        }

        List<Holiday> holidays = findAllByDateBetween(min, max);

        for (LocalDate week : weeks) {
            Integer startDay = week.getDayOfWeek().getValue();
            LocalDate endDate = getFridayOrLastDayOfWeek(week, ConfigurationService.cachedConfig.isTimesheetMode() && week.isEqual(max));
            Integer endDay = endDate.getDayOfWeek().getValue();

            int weeklyMax = Math.min(40, Math.max(0, 1 + endDay - startDay) * 8);
            long numberOfHolidays = holidays.stream()
                    .filter(holiday -> holiday.getDate().isAfter(week) && holiday.getDate().isBefore(endDate))
                    .count();
            weeklyMax -= (numberOfHolidays * 8);

            result.put(week.atStartOfDay().toEpochSecond(ZoneOffset.UTC) * 1000, weeklyMax);
        }

        return result;
    }
}
