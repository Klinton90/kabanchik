package kabanchik.system.holiday.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.domain.IDomain;
import kabanchik.core.validator.Unique;
import kabanchik.system.holiday.dto.HolidayDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Unique
@Getter
@Setter
@Entity
@Table(name="holiday")
public class Holiday implements IDomain<HolidayDTO, Integer>, ActivatableDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "holiday_id")
    private Integer id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @ContainsFilterField
    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @ContainsFilterField
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "date", nullable = false, unique = true)
    private LocalDate date;

}
