package kabanchik.system.holiday;

import kabanchik.core.repository.RestRepository;
import kabanchik.system.holiday.domain.Holiday;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface HolidayRepository extends RestRepository<Holiday, Integer> {

    List<Holiday> findAllByDateBetweenAndIsActive(LocalDate from, LocalDate to, Boolean isActive);

}
