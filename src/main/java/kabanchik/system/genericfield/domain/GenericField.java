package kabanchik.system.genericfield.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.system.genericfield.dto.GenericFieldDTO;
import kabanchik.system.genericfieldoption.domain.GenericFieldOption;
import kabanchik.system.genericfieldvalue.domain.GenericFieldValue;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "generic_field", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "generic_field_entity"})})
@NamedEntityGraphs(value = {
        @NamedEntityGraph(
                name = "GenericField.genericFieldOptions",
                attributeNodes = {
                        @NamedAttributeNode(value = "genericFieldOptions")
                }
        )
})
public class GenericField implements IDomain<GenericFieldDTO, Integer>, ActivatableDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "generic_field_id")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "generic_field_entity", nullable = false, columnDefinition = "enum('TIMESHEET_CATEGORY', 'USER')")
    private GenericFieldEntity entity;

    @Enumerated(EnumType.STRING)
    @Column(name = "generic_field_type", nullable = false, columnDefinition = "enum('TEXT', 'NUMBER', 'SELECT', 'DATE', 'BOOLEAN')")
    private GenericFieldType type;

    @Column(name = "required", nullable = false)
    private boolean required;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "genericField", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<GenericFieldOption> genericFieldOptions;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "genericField", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<GenericFieldValue> genericFieldValues;

}
