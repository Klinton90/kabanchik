package kabanchik.system.genericfield.domain;

import kabanchik.core.domain.LocalizedEnum;

public enum GenericFieldType implements LocalizedEnum {
    TEXT, NUMBER, SELECT, DATE, BOOLEAN;
}
