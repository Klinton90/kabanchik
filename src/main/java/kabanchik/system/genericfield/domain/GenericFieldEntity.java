package kabanchik.system.genericfield.domain;

import kabanchik.core.domain.LocalizedEnum;

public enum  GenericFieldEntity implements LocalizedEnum {
    TIMESHEET_CATEGORY, USER
}
