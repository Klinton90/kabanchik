package kabanchik.system.genericfield;

import kabanchik.core.service.RestService;
import kabanchik.system.genericfield.domain.GenericFieldEntity;
import kabanchik.system.genericfield.dto.GenericFieldDTO;
import kabanchik.system.genericfield.domain.GenericField;
import kabanchik.system.genericfield.dto.GenericFieldWithOptionsDTO;
import kabanchik.system.genericfieldoption.GenericFieldOptionService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Service
public class GenericFieldService extends RestService<GenericField, GenericFieldDTO, Integer> {

    @Autowired
    private GenericFieldRepository repository;

    @Autowired
    private GenericFieldService self;

    @Autowired
    private GenericFieldOptionService genericFieldOptionService;

    @Override
    public boolean isInUse(Integer genericFieldId) {
        return genericFieldOptionService.countByGenericFieldId(genericFieldId) > 0;
    }

    public List<GenericFieldDTO> findAllOfSelectType() {
        return repository.findAllOfSelectType()
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

    public List<GenericFieldWithOptionsDTO> findAllByEntity(GenericFieldEntity entity) {
        return repository.findAllByEntity(entity)
                .stream()
                .map(GenericFieldWithOptionsDTO::new)
                .collect(Collectors.toList());
    }

}
