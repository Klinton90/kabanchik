package kabanchik.system.genericfield.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.genericfield.domain.GenericField;
import kabanchik.system.genericfield.domain.GenericFieldEntity;
import kabanchik.system.genericfield.domain.GenericFieldType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class GenericFieldDTO implements IDto<GenericField, Integer>, ActivatableDomain {

    private Integer id;

    @NotNull
    private GenericFieldEntity entity;

    @NotNull
    private GenericFieldType type;

    private boolean required;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;

    private Boolean isActive;

}
