package kabanchik.system.genericfield.dto;

import kabanchik.core.util.BeanUtils;
import kabanchik.system.genericfield.domain.GenericField;
import kabanchik.system.genericfieldoption.domain.GenericFieldOption;
import kabanchik.system.genericfieldoption.dto.GenericFieldOptionDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class GenericFieldWithOptionsDTO extends GenericFieldDTO {

    private List<GenericFieldOptionDTO> genericFieldOptions;

    public GenericFieldWithOptionsDTO(GenericField genericField) {
        BeanUtils.copyProperties(genericField, this, false);
        genericFieldOptions = genericField.getGenericFieldOptions()
                .stream()
                .filter(GenericFieldOption::getIsActive)
                .map(GenericFieldOptionDTO::new)
                .collect(Collectors.toList());
    }

}
