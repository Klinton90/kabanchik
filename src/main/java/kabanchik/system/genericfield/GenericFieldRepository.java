package kabanchik.system.genericfield;

import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.genericfield.domain.GenericField;
import kabanchik.system.genericfield.domain.GenericFieldEntity;
import kabanchik.system.genericfield.domain.GenericFieldType;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.genericfield.domain.QGenericField.genericField;

@Repository
public interface GenericFieldRepository extends RestRepository<GenericField, Integer> {

    default List<GenericField> findAllOfSelectType() {
        return new JPAQuery<GenericField>(getEntityManager())
                .from(genericField)
                .where(genericField.type.eq(GenericFieldType.SELECT))
                .fetch();
    }

    default List<GenericField> findAllByEntity(GenericFieldEntity entity){
        return new JPAQuery<GenericField>(getEntityManager())
                .from(genericField)
                .distinct()
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("GenericField.genericFieldOptions"))
                .where(genericField.entity.eq(entity)
                        .and(genericField.isActive.isTrue()))
                .fetch();
    }
}
