package kabanchik.system.genericfield;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.genericfield.domain.GenericFieldEntity;
import kabanchik.system.genericfield.dto.GenericFieldDTO;
import kabanchik.system.genericfield.domain.GenericField;
import kabanchik.system.genericfield.dto.GenericFieldWithOptionsDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Getter
@Controller
@RequestMapping(value = {"/genericField"})
public class GenericFieldController extends RestController<GenericField, GenericFieldDTO, Integer> {

    @Autowired
    private GenericFieldService service;

    @GetMapping("/findAllOfSelectType")
    public @ResponseBody ResponseEntity<RestResponse> findAllOfSelectType() {
        List<GenericFieldDTO> result = service.findAllOfSelectType();
        return ResponseEntity.ok(new RestResponse(result));
    }

    @GetMapping("/findAllByEntity")
    public @ResponseBody ResponseEntity<RestResponse> findAllByEntity(@RequestParam GenericFieldEntity entity) {
        List<GenericFieldWithOptionsDTO> result = service.findAllByEntity(entity);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
