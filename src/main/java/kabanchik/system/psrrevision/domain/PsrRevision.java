package kabanchik.system.psrrevision.domain;

import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.domain.IDomain;
import kabanchik.core.validator.Unique;
import kabanchik.core.validator.UniqueColumn;
import kabanchik.system.psrrevision.dto.PsrRevisionDTO;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Table(name="psr_revision")
@Unique(columns = {@UniqueColumn(fields = {"name", "timesheetCategoryId"})})
public class PsrRevision implements IDomain<PsrRevisionDTO, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "psr_revision_id")
    private Integer id;

    @ContainsFilterField
    @Column(name = "name", nullable = false, length = 255)
    private String name;

    @NotNull
    @Column(name = "timesheet_category_id", nullable = false)
    private Integer timesheetCategoryId;

    @NotNull
    @Column(name = "revision_data", columnDefinition = "json", nullable = false)
    private String data;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "timesheet_category_id", insertable = false, updatable = false)
    private TimesheetCategory timesheetCategory;

}
