package kabanchik.system.psrrevision;

import kabanchik.core.service.RestService;
import kabanchik.system.psrrevision.domain.PsrRevision;
import kabanchik.system.psrrevision.dto.PsrRevisionDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PsrRevisionService extends RestService<PsrRevision, PsrRevisionDTO, Integer> {

    @Getter
    @Autowired
    private PsrRevisionRepository repository;

    @Getter
    @Autowired
    private PsrRevisionService self;

    @Override
    public boolean isInUse(Integer integer) {
        return false;
    }

    public List<PsrRevisionDTO> getByTimesheetCategoryId(Integer timesheetCategoryId, String queryString, Integer limit, Integer offset) {
        return getRepository().getByTimesheetCategoryId(timesheetCategoryId, queryString, limit, offset)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }
}
