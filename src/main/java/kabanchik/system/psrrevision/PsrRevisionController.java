package kabanchik.system.psrrevision;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.psrrevision.domain.PsrRevision;
import kabanchik.system.psrrevision.dto.PsrRevisionDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/psrRevision")
public class PsrRevisionController extends RestController<PsrRevision, PsrRevisionDTO, Integer> {

    @Getter
    @Autowired
    private PsrRevisionService service;

    @RequestMapping(value = "/getByTimesheetCategoryId", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> getByTimesheetCategoryId(
            @RequestParam Integer timesheetCategoryId,
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        List<PsrRevisionDTO> result = getService().getByTimesheetCategoryId(timesheetCategoryId, queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
