package kabanchik.system.psrrevision.dto;

import kabanchik.core.domain.IDto;
import kabanchik.system.psrrevision.domain.PsrRevision;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class PsrRevisionDTO implements IDto<PsrRevision, Integer> {

    private Integer id;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    @NotBlank
    private String data;


}
