package kabanchik.system.psrrevision;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.psrrevision.domain.PsrRevision;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.psrrevision.domain.QPsrRevision.psrRevision;

@Repository
public interface PsrRevisionRepository extends RestRepository<PsrRevision, Integer> {

    default List<PsrRevision> getByTimesheetCategoryId(Integer timesheetCategoryId, String queryString, Integer limit, Integer offset) {
        BooleanExpression booleanExpression = psrRevision.timesheetCategoryId.eq(timesheetCategoryId);

        if (StringUtils.isNotBlank(queryString)) {
            booleanExpression = booleanExpression.and(psrRevision.name.contains(queryString));
        }

        JPAQuery<PsrRevision> query = new JPAQuery<PsrRevision>(getEntityManager())
                .from(psrRevision)
                .where(booleanExpression)
                .orderBy(psrRevision.name.asc());

        if (limit != null) {
            query.limit(limit);
        }

        if (offset != null) {
            query.offset(offset);
        }

        return query.fetch();
    }

}
