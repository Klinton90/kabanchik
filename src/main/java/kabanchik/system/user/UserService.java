package kabanchik.system.user;

import com.querydsl.core.BooleanBuilder;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.core.config.CustomConfig;
import kabanchik.core.service.EmailService;
import kabanchik.core.service.GenericFieldAwareRestService;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.department.DepartmentService;
import kabanchik.system.department.domain.Department;
import kabanchik.system.genericfield.domain.GenericFieldEntity;
import kabanchik.system.holiday.HolidayService;
import kabanchik.system.localization.LocalizationService;
import kabanchik.system.timesheet.TimesheetService;
import kabanchik.system.timesheetcategory.TimesheetCategoryService;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.timesheetaccess.TimesheetAccessService;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import kabanchik.system.user.dto.UserDTO;
import kabanchik.system.user.dto.UserProfileDTO;
import kabanchik.system.user.dto.UserSimpleDTO;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.context.Context;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class UserService extends GenericFieldAwareRestService<User, UserDTO> implements UserDetailsService{

    private final SecureRandom random = new SecureRandom();

    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private static final String TEMP_PASSWORD = "TEMP";

    @Autowired
    private EmailService emailService;

    @Autowired
    private CustomConfig config;

    @Autowired
    private ExposedResourceMessageBundleSource msgSource;

    @Autowired
    private LocalizationService localizationService;

    @Getter
    @Autowired
    private UserRepository repository;

    @Autowired
    private DepartmentService departmentService;

    @Getter
    @Autowired
    private UserService self;

    @Autowired
    private TimesheetCategoryService timesheetCategoryService;

    @Autowired
    private TimesheetAccessService timesheetAccessService;

    @Autowired
    private TimesheetService timesheetService;

    @Autowired
    private HolidayService holidayService;

    /**
     * Get Current {@link User}
     */
    public static User getPrincipal() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @Override
    public boolean isInUse(Integer id){
        return timesheetCategoryService.countByManagerId(id) > 0 || timesheetAccessService.countByUserId(id) > 0;
    }

    /**
     * Returns a populated {@link UserDetails} object.
     * The username is first retrieved from the database and then mapped to
     * a {@link UserDetails} object.
     */
    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException{
        User user = getRepository().findByEmail(username);
        if(user == null){
            //that text is never shown anywhere, that's why we don't have to localize that text
            throw new UsernameNotFoundException("User with 'email': '" + username + "' not found ");
        }

        return user;
    }

    public List<UserSimpleDTO> getUsersByTimesheetCategoryId(Integer timesheetCategoryId, Boolean withInternal) {
        return repository.getUsersByTimesheetCategoryId(timesheetCategoryId, withInternal)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    /**
     * Get all {@link User}s that reports to Current user.
     * If {@link User#getRole()} equals to {@link UserRole#ADMIN} - return all users.
     * If {@link User#getRole()} equals to {@link UserRole#EXECUTOR} - check also for provided permissions on {@link TimesheetCategory#getManagers()}
     * If {@link User#getRole()} equals to {@link UserRole#USER} - never tested, but should not be called by business logic. In theory should not throw any {@link Exception}...
     */
    public List<UserSimpleDTO> getReportingUsers(String queryString, Integer limit, Integer offset) {
        return repository.getReportingUsersByManager(getPrincipal(), queryString, limit, offset)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    /**
     * Get {@link User}s that reports to Current user only if there are {@link kabanchik.system.timesheet.domain.Timesheet}s for him,
     * in between of provided date range NOT in {@link kabanchik.system.timesheet.domain.TimesheetStatus#DECLINED}.
     * Exposes same rules as {@link UserService#getReportingUsers(String, Integer, Integer)} for {@link UserRole} validation.
     */
    public List<UserSimpleDTO> getReportingUsersByPeriod(Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo) {
        return repository.getReportingUsersByManagerAndPeriod(getPrincipal(), yearFrom, monthFrom, yearTo, monthTo)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    /**
     * Get list {@link User}s with {@link UserRole#ADMIN} or {@link UserRole#EXECUTOR}.
     */
    public List<UserSimpleDTO> getManagersDto(String queryString, Integer limit, Integer offset) {
        return getManagers(queryString, limit, offset)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public List<User> getManagers() {
        return getManagers(null, null, null);
    }

    public List<User> getManagers(String queryString, Integer limit, Integer offset) {
        return repository.getManagers(queryString, limit, offset);
    }

    public List<User> findAllForUserReminderReport(LocalDate weekDate) {
        Pair<LocalDate, LocalDate> weekStartEnd = TimesheetService.getWeekStartEnd(weekDate, false);
        Integer weeklyMax = holidayService.calcWeeklyMax(weekStartEnd.getLeft(), false);
        return repository.findAllForUserReminderReport(weekStartEnd.getLeft(), weekStartEnd.getRight(), weeklyMax);
    }

    public List<User> findAllForManagerReminderReport(Integer managerId, LocalDate weekDate) {
        Pair<LocalDate, LocalDate> weekStartEnd = TimesheetService.getWeekStartEnd(weekDate, false);
        Integer weeklyMax = holidayService.calcWeeklyMax(weekStartEnd.getLeft(), false);
        return repository.findAllForManagerReminderReport(managerId, weekStartEnd.getLeft(), weekStartEnd.getRight(), weeklyMax);
    }

    /**
     * Get number of Users assigned to provided {@link Department#getId()}
     */
    public long countByDepartmentId(int id) {
        return getRepository().countByDepartmentId(id);
    }

    /**
     * Need to override this in order to provide specific {@link Validated} rules
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Validated(UserDTO.UpdateValidationGroup.class)
    public UserDTO update(@Valid UserDTO json) throws CustomValidationException{
        return super.update(json);
    }

    /**
     * Need to override this in order to provide specific {@link Validated} rules.
     * And add post saving logic to Reset TimesheetAccess values.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Validated(UserDTO.UpdateValidationGroup.class)
    public User update(@Valid User user) throws CustomValidationException {
//        User existingUser = findOne(user.getId());
//        BigDecimal prevBaseRate = existingUser.getBaseRate();
//        BigDecimal prevInternalBaseRate = existingUser.getInternalBaseRate()

        User result = super.update(user);
        updateAuthentication(user.getId());

        // Postprocessing
        // Update Timesheet Access
        // TODO: removed by request from Diana
//        if (prevBaseRate.compareTo(user.getBaseRate()) != 0) {
//            timesheetAccessService.updateDefaultRatesByUser(user, prevBaseRate);
//        }
//        if (prevBaseRate.compareTo(user.getInternalBaseRate()) != 0) {
//            timesheetAccessService.updateDefaultInternalRatesByUser(user, prevInternalBaseRate);
//        }

        return result;
    }

    public UserDTO updateProfile(UserProfileDTO dto) throws CustomValidationException {
        User user = UserService.getPrincipal();

        BeanUtils.copyProperties(dto, user, true);
        user.setLocalization(localizationService.getOne(dto.getLocalizationId()));

        //updateAuthentication
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, user.getUsername(), user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return initDto(getSelf()._save(user));
    }

    /**
     * Need to override this in order to provide specific {@link Validated} rules
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @Validated(UserDTO.CreateValidationGroup.class)
    public UserDTO create(@Valid UserDTO json) throws CustomValidationException {
        User user = initEntity(json).setAuthenticationFailureCount(0).setId(0);
        if (user.getSecurityCode() == null) {
            user.setSecurityCode("");
        }

        return initDto(getSelf()._save(user));
    }

    /**
     * Register user: do create and send confirmation email
     * @param dto to be processed
     */
    //TODO: change input to `RegisterUserDto`
    @Validated(UserDTO.RegistrationValidationGroup.class)
    public void register(@Valid UserDTO dto) {
        User user = new User()
                .setRole(dto.getRole())
                .setEmail(dto.getEmail())
                .setIsActive(false)
                .setId(0)
                .setSecurityCode(_getSecurityCode())
                .setPassword(TEMP_PASSWORD)
                .setAuthenticationFailureCount(0)
//                .setLocalization(localizationService.getDefaultLocalization())
                .setFirstName("")
                .setLastName("")
                .setBaseRate(BigDecimal.ZERO);

        if(!UserRole.isExecutor(user.getRole())){
            user.setRole(UserRole.USER);
        }

        getSelf()._save(user);

        final String subject = msgSource.getMessage("app.email.registration.subject");
        final Context ctx = new Context(LocaleContextHolder.getLocale());
        ctx.setVariable("isUser", user.getRole() == UserRole.USER);
        ctx.setVariable("userEmail", user.getEmail());
        ctx.setVariable("securityLink", _getSecurityLink(user.getSecurityCode()));
        emailService.sendEmail("email/registration-confirmation", ctx, user, subject);
    }

    /**
     * Update password
     * @param securityCode criteria for enabling account
     * @param password password
     * @return {@link true} if `securityCode` is valid
     */
    public Boolean updatePassword(String securityCode, String password) {
        User user = getRepository().findBySecurityCode(securityCode);
        if (user != null) {
            //TODO: remove this after resolving issue with locked passwords
            log.warn("Locked password for user `" + user.toString() + "`. Call trace:");
            for (StackTraceElement el: Thread.currentThread().getStackTrace()) {
                log.warn(el.toString());
            }

            user.setIsActive(true)
                .setSecurityCode("")
                .setAuthenticationFailureCount(0)
                .setPassword(encryptPassword(password));

            getSelf()._save(user);
            return true;
        }
        return false;
    }

    /**
     * Disable account and send email with instructions
     * @param email to search user by in database
     */
    public void forgetPassword(String email) {
        User user = getRepository().findByEmail(email);
        if(user != null){
            user.setSecurityCode(_getSecurityCode());

            final String subject = msgSource.getMessage("app.email.forgetPassword.subject");
            final Context ctx = new Context(LocaleContextHolder.getLocale());
            ctx.setVariable("userEmail", user.getEmail());
            ctx.setVariable("securityLink", _getSecurityLink(user.getSecurityCode()));
            emailService.sendEmail("email/forget-password", ctx, user, msgSource.getMessage(subject));

            getSelf()._save(user);
        }
    }

    /**
     * Disable account and send email with instructions
     * @param user to be blocked
     */
    public void lockOnFailedAuthentication(User user) {
        int failureCount = user.getAuthenticationFailureCount();
        if (ConfigurationService.cachedConfig.getAllowedAuthFailureCount() > 0 && failureCount < ConfigurationService.cachedConfig.getAllowedAuthFailureCount()) {
            user.setAuthenticationFailureCount(++failureCount);

            if(failureCount >= ConfigurationService.cachedConfig.getAllowedAuthFailureCount()){
                //TODO: remove this after resolving issue with locked passwords
                log.warn("User `" + user.toString() + "` has been locked due to `" + ConfigurationService.cachedConfig.getAllowedAuthFailureCount() + "` unsuccessful login attempts.");

                user.setIsActive(false)
                        .setSecurityCode(_getSecurityCode())
                        .setPassword(TEMP_PASSWORD);

                final String subject = msgSource.getMessage("app.email.authBlocked.subject");
                final Context ctx = new Context(LocaleContextHolder.getLocale());
                ctx.setVariable("userEmail", user.getEmail());
                ctx.setVariable("securityLink", _getSecurityLink(user.getSecurityCode()));
                emailService.sendEmail("email/authentication-blocked", ctx, user, subject);
            }

            getSelf()._save(user);
        }
    }

    /**
     * Remove {@link User#getAuthenticationFailureCount()} value on successful login.
     */
    public User cleanupOnAuthenticationSuccess(User user) {
        if(user.getAuthenticationFailureCount() > 0){
            user.setAuthenticationFailureCount(0);
            return getSelf()._save(user);
        }
        return user;
    }

    /**
     * Update password to current user.
     */
    public void updateOwnPassword(String password) throws CustomValidationException {
        User principal = getPrincipal();
        principal.setPassword(password);
        update(initDto(principal));
    }

    /**
     * Reset activation for {@link User} with provided {@link User#getId()}.
     */
    public void resetActivation(Integer id) {
        User user = findOne(id)
                .setAuthenticationFailureCount(0)
                .setSecurityCode("")
                .setIsActive(true);
        getSelf()._save(user);
    }

    /**
     * Overrides to encrypt password parameter
     * @param preparedEntity prepared {@link User} domain
     * @param originalEntity originally received by endpoint {@link User} domain
     * @return updated {@link User}
     */
    @Override
    @Validated
    @Transactional(rollbackFor = Exception.class)
    public User _save(@Valid User preparedEntity, User originalEntity){
        String password = preparedEntity.getPassword();
        if((password == null || !password.equals(TEMP_PASSWORD)) && (preparedEntity.getId() == 0 || !originalEntity.getPassword().equals(password))){
            if (password == null || password.length() == 0) {
                if (originalEntity.getPassword().length() == 0) {
                    //TODO: remove this after resolving issue with locked passwords
                    log.warn("Locked password for user `" + preparedEntity.toString() + "`. Call trace:");
                    for (StackTraceElement el: Thread.currentThread().getStackTrace()) {
                        log.warn(el.toString());
                    }
                }
                preparedEntity.setPassword(originalEntity.getPassword());
            } else {
                //TODO: remove this after resolving issue with locked passwords
                log.warn("Locked password for user `" + preparedEntity.toString() + "`. Call trace:");
                for (StackTraceElement el: Thread.currentThread().getStackTrace()) {
                    log.warn(el.toString());
                }

                preparedEntity.setPassword(encryptPassword(password));
            }
        }
        return getRepository().save(preparedEntity);
    }

    /**
     * When edit fields on current {@link User}, need to update {@link Authentication} data for Spring Session.
     */
    private void updateAuthentication(int id) {
        User principal = getPrincipal();
        if (id == principal.getId()) {
            Authentication authentication = new UsernamePasswordAuthenticationToken(findOne(id), principal.getUsername(), principal.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }

    /**
     * Encrypt password before save.
     */
    private String encryptPassword(String password){
        return passwordEncoder.encode(password);
    }

    /**
     * Build URL for using in email with access to update Password data.
     */
    private String _getSecurityLink(String securityCode){
        return UriComponentsBuilder
                .fromHttpUrl(config.getUiHref()[0] + config.getUiBasePath() + "/user/update-password")
                .queryParam("securityCode", securityCode)
                .build()
                .toUriString();
    }

    /**
     * Generate security code to be used on Emails for security URL generation.
     */
    private String _getSecurityCode(){
        return new BigInteger(130 /*160*/, random).toString(32);
    }

    @Override
    protected GenericFieldEntity getGenericFieldEntityType() {
        return GenericFieldEntity.USER;
    }

    @Override
    public void validate(User json) throws CustomValidationException {
        departmentService.getOne(json.getDepartmentId());
        localizationService.getOneByLocalizationId(json.getLocalizationId());
//        throw new CustomValidationException("userNotFound", User.class.getSimpleName(), "firstName", json.getId().toString(), json, User.class.getSimpleName(), json.getId());
    }

    public List<UserSimpleDTO> getForPSR(String queryString, Integer limit, Integer offset) {
        return repository.getForPSR(queryString, limit, offset)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public List<UserSimpleDTO> getUsersForTimesheetAccessBulkUserAdd(String queryString, Integer limit, Integer offset) {
        return repository.getUsersForTimesheetAccessBulkUserAdd(queryString, limit, offset)
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public List<User> findAllByIdIn(List<Integer> ids){
        return repository.findAllByIdIn(ids);
    }

    public Integer countByLocalizationId(String id) {
        return repository.countByLocalizationId(id);
    }

    public Iterable<UserSimpleDTO> getAllSimple(Sort sort, boolean showActive) {
        BooleanBuilder where = applyIsActivePredicate(showActive, getDefaultBuilder());

        Iterable<User> iterable = repository.findAll(where, sort);
        return StreamSupport.stream(sortIterable(iterable, sort).spliterator(), false)
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }

    public UserSimpleDTO initSimpleDTO(User entity){
        UserSimpleDTO result = new UserSimpleDTO();
        return result.createFromEntity(entity);
    }

    public List<UserSimpleDTO> getUsersForWorkload() {
        return repository.getUsersForWorkload()
                .stream()
                .map(this::initSimpleDTO)
                .collect(Collectors.toList());
    }
}
