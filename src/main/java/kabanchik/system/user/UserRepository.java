package kabanchik.system.user;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import static kabanchik.system.timesheet.domain.QTimesheet.timesheet;
import static kabanchik.system.timesheetaccess.domain.QTimesheetAccess.timesheetAccess;
import static kabanchik.system.user.domain.QUser.user;

@Repository
public interface UserRepository extends RestRepository<User, Integer>{

    User findBySecurityCode(String securityCode);

    int countByLocalizationId(String id);

    List<User> findAllByIdIn(List<Integer> ids);

    default User findByEmail(String email) {
        return new JPAQuery<User>(getEntityManager())
                .from(user)
                .where(user.email.eq(email)
                        .and(user.role.ne(UserRole.INTERNAL)))
                .fetchOne();
    }

    default List<User> getUsersByTimesheetCategoryId(Integer timesheetCategoryId, Boolean withInternal) {
        BooleanExpression booleanExpression = timesheetAccess.timesheetCategoryId.eq(timesheetCategoryId);

        if (!withInternal) {
            booleanExpression = booleanExpression.and(user.role.ne(UserRole.INTERNAL));
        }

        return new JPAQuery<User>(getEntityManager())
                .distinct()
                .from(user)
                .innerJoin(user.timesheetAccesses, timesheetAccess)
                .where(booleanExpression)
                .orderBy(user.firstName.asc(), user.lastName.asc())
                .fetch();
    }

    default List<User> getReportingUsersByManager(User manager, String queryString, Integer limit, Integer offset) {
        BooleanExpression booleanExpression = user.role.ne(UserRole.INTERNAL)
                .and(
                        user.isActive.isTrue()
                                .and(timesheetAccess.isActive.isTrue())
                                .or(timesheet.id.isNotNull())
                );

        if (!manager.getRole().equals(UserRole.ADMIN)) {
            booleanExpression = booleanExpression.and(user.timesheetAccesses.any().timesheetCategory.managers.any().id.eq(manager.getId()));
        }

        if (StringUtils.isNotBlank(queryString)) {
            booleanExpression = booleanExpression.and(user.firstName.contains(queryString)
                    .or(user.lastName.contains(queryString)));
        }

        JPAQuery<User> query = new JPAQuery<User>(getEntityManager())
                .from(user)
                .distinct()
                .innerJoin(user.timesheetAccesses, timesheetAccess)
                .leftJoin(timesheetAccess.timesheets, timesheet)
                .on(timesheet.status.in(TimesheetStatus.POSTED, TimesheetStatus.EDITABLE))
                .where(booleanExpression)
                .orderBy(user.firstName.asc());

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

    default List<User> getReportingUsersByManagerAndPeriod(User manager, Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo) {
        BooleanExpression mainExpression = user.role.ne(UserRole.INTERNAL);

        Pair<LocalDate, LocalDate> datesPair = ConfigurationService.getDatesByDateCriteria(yearFrom, monthFrom, yearTo, monthTo);
        BooleanExpression subExpression = timesheet.date.between(datesPair.getFirst(), datesPair.getSecond())
                .and(timesheet.status.notIn(TimesheetStatus.DECLINED, TimesheetStatus.PLANNED));

        if (!manager.getRole().equals(UserRole.ADMIN)) {
            mainExpression = mainExpression.and(timesheetAccess.timesheetCategory.managers.any().id.eq(manager.getId()));
        }

        return new JPAQuery<User>(getEntityManager())
                .from(user)
                .distinct()
                .innerJoin(user.timesheetAccesses, timesheetAccess)
                .innerJoin(timesheetAccess.timesheets, timesheet)
                    .on(subExpression)
                .where(mainExpression)
                .orderBy(user.firstName.asc())
                .fetch();
    }

    default List<User> getManagers(String queryString, Integer limit, Integer offset) {
        BooleanExpression booleanExpression = user.role.in(UserRole.EXECUTOR, UserRole.ADMIN)
                .and(user.isActive.isTrue());

        if (StringUtils.isNotBlank(queryString)) {
            booleanExpression = booleanExpression.and(user.firstName.contains(queryString)
                    .or(user.lastName.contains(queryString)));
        }

        JPAQuery<User> query = new JPAQuery<User>(getEntityManager())
                .from(user)
                .where(booleanExpression)
                .orderBy(user.firstName.asc(), user.lastName.asc());

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

    default long countByDepartmentId(int id) {
        return new JPAQuery<User>(getEntityManager()).from(user)
                .where(user.departmentId.eq(id))
                .fetchCount();
    }

    default List<User> findAllForUserReminderReport(LocalDate from, LocalDate to, Integer weeklyMax) {
        return new JPAQuery<User>(getEntityManager())
                .from(user)
                .innerJoin(user.timesheetAccesses, timesheetAccess)
                .leftJoin(timesheetAccess.timesheets, timesheet)
                    .on(timesheet.status.in(TimesheetStatus.APPROVED, TimesheetStatus.POSTED)
                            .and(timesheet.date.between(from, to)))
                .where(user.isActive.isTrue()
                        .and(user.role.in(UserRole.USER, UserRole.EXECUTOR))
                        .and(timesheetAccess.isActive.isTrue())
                        .and(user.receiveReminder.isTrue()))
                .groupBy(user.id)
                .having(timesheet.amount.sum().lt(weeklyMax)
                        .or(timesheet.amount.sum().isNull()))
                .fetch();
    }

    default List<User> findAllForManagerReminderReport(Integer managerId, LocalDate from, LocalDate to, Integer weeklyMax){
        JPAQuery<Integer> subQuery = new JPAQuery<>(getEntityManager())
                .from(timesheetAccess)
                .select(timesheetAccess.userId)
                .where(timesheetAccess.timesheetCategory.managers.any().id.eq(managerId));

        return new JPAQuery<User>(getEntityManager())
                .from(user)
                .innerJoin(user.timesheetAccesses, timesheetAccess)
                .leftJoin(timesheetAccess.timesheets, timesheet)
                    .on(timesheet.status.in(TimesheetStatus.APPROVED, TimesheetStatus.POSTED)
                            .and(timesheet.date.between(from, to)))
                .where(user.isActive.isTrue()
                        .and(user.role.in(UserRole.USER, UserRole.EXECUTOR))
                        .and(timesheetAccess.isActive.isTrue())
                        .and(timesheetAccess.userId.in(subQuery)))
                .groupBy(user.id)
                .having(timesheet.amount.sum().lt(weeklyMax)
                        .or(timesheet.amount.sum().isNull()))
                .fetch();
    }

    default List<User> getForPSR(String queryString, Integer limit, Integer offset) {
        BooleanExpression booleanExpression = user.department.forWorkload.isTrue();

        if (StringUtils.isNotBlank(queryString)) {
            booleanExpression = booleanExpression.and(user.firstName.contains(queryString)
                    .or(user.lastName.contains(queryString)));
        }

        JPAQuery<User> query = new JPAQuery<User>(getEntityManager())
                .distinct()
                .from(user)
                .where(booleanExpression)
                .orderBy(user.firstName.asc(), user.lastName.asc());

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

    default List<User> getUsersForTimesheetAccessBulkUserAdd(String queryString, Integer limit, Integer offset) {
        BooleanExpression booleanExpression = user.isActive.isTrue().and(user.role.ne(UserRole.INTERNAL));

        if (StringUtils.isNotBlank(queryString)) {
            booleanExpression = booleanExpression.and(user.firstName.contains(queryString)
                    .or(user.lastName.contains(queryString)));
        }

        JPAQuery<User> query = new JPAQuery<User>(getEntityManager())
                .from(user)
                .where(booleanExpression)
                .orderBy(user.firstName.asc(), user.lastName.asc());

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

    // This MUST return inactive users as we need to show them in grid in case when user became inactive during selected period.
    default List<User> getUsersForWorkload() {
        return new JPAQuery<User>(getEntityManager())
                .from(user)
//                .where(user.role.ne(UserRole.ADMIN)
//                        .and(user.department.forWorkload.isTrue()))
                .where(user.department.forWorkload.isTrue())
                .orderBy(user.firstName.asc(), user.lastName.asc())
                .fetch();
    }
}