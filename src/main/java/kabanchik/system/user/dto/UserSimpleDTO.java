package kabanchik.system.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.core.util.BeanUtils;
import kabanchik.core.validator.PasswordSize;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Getter
@Setter
public class UserSimpleDTO implements IDto<User, Integer>, ActivatableDomain {

    private Integer id;

    @NotNull(groups = {CreateValidationGroup.class, UpdateValidationGroup.class, RegistrationValidationGroup.class})
    @Size(min = 8, max = 255, groups = {CreateValidationGroup.class, UpdateValidationGroup.class, RegistrationValidationGroup.class})
    @Email(groups = {CreateValidationGroup.class, UpdateValidationGroup.class, RegistrationValidationGroup.class})
    private String email;

    @Size(min = 8, max = 255, groups = CreateValidationGroup.class)
    @PasswordSize(min = 8, max = 255, groups = UpdateValidationGroup.class)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private Boolean isActive;

    @NotNull(groups = {CreateValidationGroup.class, UpdateValidationGroup.class, RegistrationValidationGroup.class})
    private UserRole role;

    @NotNull(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @NotBlank(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @Size(max = 255)
    private String firstName;

    @NotNull(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @NotBlank(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @Size(max = 255)
    private String lastName;

    @NotNull(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @DecimalMin(value = "0.01", groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @DecimalMax(value = "999999.99", groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @Digits(integer = 6, fraction = 2, groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    private BigDecimal baseRate;

    @NotNull(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @DecimalMin(value = "0.01", groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @DecimalMax(value = "999999.99", groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @Digits(integer = 6, fraction = 2, groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    private BigDecimal internalBaseRate;

    @NotBlank(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    @NotNull(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    private String localizationId;

    @NotNull(groups = {CreateValidationGroup.class, UpdateValidationGroup.class})
    private Integer departmentId;

    private Boolean receiveReminder;

    @Override
    public UserSimpleDTO createFromEntity(User user){
        BeanUtils.copyProperties(user, this, true);
        return this;
    }

    public interface CreateValidationGroup{}

    public interface UpdateValidationGroup{}

    public interface RegistrationValidationGroup{}

}
