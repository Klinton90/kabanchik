package kabanchik.system.user.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UpdatePasswordDTO{

    @NotNull
    private String securityCode;

    @Size(min = 8, max = 255)
    private String password;
}
