package kabanchik.system.user.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IGenericFieldAwareDto;
import kabanchik.system.genericfieldvalue.dto.GenericFieldValueDTO;
import kabanchik.system.user.domain.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDTO extends UserSimpleDTO implements IGenericFieldAwareDto<User>, ActivatableDomain {

    private List<GenericFieldValueDTO> genericFieldValues;

}
