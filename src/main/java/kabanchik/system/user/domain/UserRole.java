package kabanchik.system.user.domain;

import kabanchik.core.domain.LocalizedEnum;
import lombok.Getter;

@Getter
public enum UserRole implements LocalizedEnum {
    ADMIN, EXECUTOR, USER, INTERNAL;

    public static Boolean isExecutor(UserRole value) {
        return value.equals(UserRole.ADMIN) || value.equals(UserRole.EXECUTOR);
    }
}
