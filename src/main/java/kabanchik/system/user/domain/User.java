package kabanchik.system.user.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.domain.IGenericFieldAwareDomain;
import kabanchik.core.util.BeanUtils;
import kabanchik.core.validator.Unique;
import kabanchik.core.validator.UniqueColumn;
import kabanchik.system.department.domain.Department;
import kabanchik.system.genericfieldvalue.domain.GenericFieldValue;
import kabanchik.system.localization.domain.LocalizationDomain;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.user.dto.UserDTO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name="users_new")
@Unique(
    columns = {
        @UniqueColumn(fields = "email"),
        @UniqueColumn(fields = "securityCode", orValue = "")
    }
)
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "User.default",
        attributeNodes = {
            @NamedAttributeNode(value = "localization")
        }
    ),
    @NamedEntityGraph(
        name = "User.empty"
    )
})
public class User implements UserDetails, IGenericFieldAwareDomain<UserDTO>, ActivatableDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id;

    @Column(name = "email", nullable = false)
    @ContainsFilterField
    private String email;

    @Column(name = "password", nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    @ContainsFilterField
    private UserRole role;

    @JsonIgnore
    @Column(name = "security_code", nullable = false, length = 32)
    private String securityCode;

    @JsonIgnore
    @Column(name = "auth_failure_count", nullable = false)
    private Integer authenticationFailureCount;

    @Column(name = "first_name", nullable = false)
    @ContainsFilterField
    private String firstName;

    @Column(name = "last_name", nullable = false)
    @ContainsFilterField
    private String lastName;

    @Column(name = "base_rate", nullable = false, precision = 19, scale = 2)
    private BigDecimal baseRate;

    @Column(name = "internal_base_rate", nullable = false, precision = 19, scale = 2)
    private BigDecimal internalBaseRate;

    @Column(name = "localization_id", nullable = false)
    private String localizationId;

    @Column(name = "department_id", nullable = false)
    private Integer departmentId;

    @Column(name = "receive_reminder", nullable = false)
    private Boolean receiveReminder;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "localization_id", nullable = false, insertable = false, updatable = false)
    private LocalizationDomain localization;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "department_id", nullable = false, insertable = false, updatable = false)
    private Department department;

    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<TimesheetAccess> timesheetAccesses;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(mappedBy = "timesheetCategory", cascade = CascadeType.ALL)
    @Filter(name = "genericFieldValueUser")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private List<GenericFieldValue> genericFieldValues;

    @ManyToMany(mappedBy = "managers")
    private List<TimesheetCategory> timesheetCategories;

    //    @PrimaryKeyJoinColumn
//    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
//    private List<Bonuspayment> bonuspayments;

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities(){
        List<GrantedAuthority> authorities = new ArrayList<>();
        if(role != null){
            authorities.add(new SimpleGrantedAuthority("ROLE_" + getRole().toString()));
        }
        return authorities;
    }

    @Override
    @JsonIgnore
    public String getUsername(){
        return email;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired(){
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked(){
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired(){
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled(){
        return isActive;
    }

    @Override
    public User createFromDto(UserDTO dto) {
        BeanUtils.copyProperties(dto, this, true);
        if (dto.getFirstName() != null) {
            firstName = dto.getFirstName();
        }

        if (dto.getLastName() != null) {
            lastName = dto.getLastName();
        }
        return this;
    }

    public String getName(){
        if (firstName.length() > 0 || lastName.length() > 0) {
            return firstName + (firstName.length() > 0 && lastName.length() > 0 ? " " : "") + lastName;
        } else {
            return email;
        }
    }

    @Override
    public String toString(){
        return id.toString() + ":" + firstName + " " + lastName;
    }

}
