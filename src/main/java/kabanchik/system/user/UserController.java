package kabanchik.system.user;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.jackson.LocalDateDeserializer;
import kabanchik.core.service.ReminderService;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.dto.UpdatePasswordDTO;
import kabanchik.system.user.dto.UserDTO;
import kabanchik.system.user.dto.UserProfileDTO;
import kabanchik.system.user.dto.UserSimpleDTO;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Controller
@RequestMapping(value={"/user"})
public class UserController extends RestController<User, UserDTO, Integer> {

    @Getter
    @Autowired
    private UserService service;

    @Autowired
    private ReminderService reminderService;

    @RequestMapping(value = "/listAllSimple", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> listAllSimple(
            Pageable pageable,
            @RequestParam(required = false, defaultValue = "false") boolean showActive
    ) {
        Iterable<UserSimpleDTO> result = getService().getAllSimple(pageable.getSort(), showActive);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getReportingUsers", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getReportingUsers(
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        List<UserSimpleDTO> result = service.getReportingUsers(queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getReportingUsersByPeriod", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getReportingUsersByPeriod(
            @RequestParam Integer yearFrom,
            @RequestParam Integer monthFrom,
            @RequestParam Integer yearTo,
            @RequestParam Integer monthTo
    ) {
        List<UserSimpleDTO> result = service.getReportingUsersByPeriod(yearFrom, monthFrom, yearTo, monthTo);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getManagers", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getManagers(
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        List<UserSimpleDTO> result = service.getManagersDto(queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getForPSR", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForPSR(
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        List<UserSimpleDTO> result = service.getForPSR(queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getUsersByTimesheetCategoryId", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getUsersByTimesheetCategoryId(
            @RequestParam Integer timesheetCategoryId,
            @RequestParam Boolean withInternal
    ) {
        List<UserSimpleDTO> result = service.getUsersByTimesheetCategoryId(timesheetCategoryId, withInternal);
        return ResponseEntity.ok(new RestResponse(result));
    }

    /**
     * Register new {@link User} available by public
     * Disabled for now as registration is prohibited
     */
    /*@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity register(@RequestBody UserDTO json) throws CustomValidationException {
        getService().register(json);
        return ResponseEntity.ok(true);
    }*/

    /**
     * Admin-specific endpoint to reset activation for account
     */
    @RequestMapping(value = "/resetActivation/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity resetActivation(@PathVariable int id) throws CustomValidationException {
        getService().resetActivation(id);
        return ResponseEntity.ok(true);
    }

    /**
     * Standard endpoint for users to restore password.
     */
    @RequestMapping(value = "/forgetPassword", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity forgetPassword(@RequestParam String email) {
        getService().forgetPassword(email);
        return ResponseEntity.ok(true);
    }

    /**
     * Endpoint to update password after `forgetPassword` or locking on multiple auth failures. Available by public.
     * @param data email to identify {@link User} domain in DB; confirmation code to compare the one assigned to domain; new password for update
     * @throws CustomValidationException - is thrown when user is not found or securityCode is not valid
     */
    @RequestMapping(value = "/updatePassword", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<RestResponse> updatePassword(
        @Validated @RequestBody UpdatePasswordDTO data
    ) throws CustomValidationException {
        if (getService().updatePassword(data.getSecurityCode(), data.getPassword())) {
            return ResponseEntity.ok(new RestResponse(true));
        } else {
            throw new CustomValidationException("securityCode", "User");
        }
    }

    @RequestMapping(value = "/updateOwnPassword", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity updateOwnPassword(
        @RequestParam String password
    ) throws CustomValidationException {
        getService().updateOwnPassword(password);
        return ResponseEntity.ok(true);
    }

    @RequestMapping(value = "/sendUserReminder", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity sendUserReminder (@RequestParam Integer userId, @RequestParam Long timestamp) {
        return ResponseEntity.ok(new RestResponse(reminderService.sendUserReminder(userId, LocalDateDeserializer.timestampToDate(timestamp))));
    }

    @RequestMapping(value = "/sendManagerReminder", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity sendManagerReminder (@RequestParam Integer managerId,  @RequestParam Long timestamp) {
        return ResponseEntity.ok(new RestResponse(reminderService.sendManagerReminder(managerId, LocalDateDeserializer.timestampToDate(timestamp))));
    }

    @GetMapping("/getUsersForTimesheetAccessBulkUserAdd")
    public @ResponseBody ResponseEntity getUsersForTimesheetAccessBulkUserAdd(
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        return ResponseEntity.ok(new RestResponse(getService().getUsersForTimesheetAccessBulkUserAdd(queryString, limit, offset)));
    }

    @RequestMapping(value = {"/updateProfile", "/updateProfile/"}, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<RestResponse> updateProfile(@RequestBody UserProfileDTO dto) throws CustomValidationException{
        UserDTO result = getService().updateProfile(dto);
        return ResponseEntity.ok(new RestResponse(result).setId(result.getId()));
    }

    @RequestMapping(value = {"/getUsersForWorkload", "/getUsersForWorkload"}, method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getUsersForWorkload() {
        return ResponseEntity.ok(new RestResponse(getService().getUsersForWorkload()));
    }

}
