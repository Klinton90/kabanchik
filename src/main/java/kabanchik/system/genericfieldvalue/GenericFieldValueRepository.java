package kabanchik.system.genericfieldvalue;

import com.cosium.spring.data.jpa.entity.graph.domain.EntityGraph;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.genericfieldvalue.domain.GenericFieldValue;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenericFieldValueRepository extends RestRepository<GenericFieldValue, Integer> {

    List<GenericFieldValue> findAllByIdIn(List<Integer> ids);

    List<GenericFieldValue> findAllByIdIn(List<Integer> ids, EntityGraph entityGraph);

}
