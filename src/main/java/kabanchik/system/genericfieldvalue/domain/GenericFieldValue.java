package kabanchik.system.genericfieldvalue.domain;

import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.IGenericFieldAwareDomain;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.genericfield.domain.GenericField;
import kabanchik.system.genericfield.domain.GenericFieldEntity;
import kabanchik.system.genericfieldvalue.dto.GenericFieldValueDTO;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.user.domain.User;
import lombok.*;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "generic_field_value", uniqueConstraints = {@UniqueConstraint(columnNames = {"parent_id", "generic_field_id"})})
@NoArgsConstructor
@FilterDefs({
        @FilterDef(name = "genericFieldValueTimesheetCategory", defaultCondition = "EXISTS(SELECT 1 FROM generic_field AS gf WHERE gf.generic_field_id = generic_field_id AND gf.generic_field_entity = 'TIMESHEET_CATEGORY')"),
        @FilterDef(name = "genericFieldValueUser", defaultCondition = "EXISTS(SELECT 1 FROM generic_field AS gf WHERE gf.generic_field_id = generic_field_id AND gf.generic_field_entity = 'USER')")
})
@NamedEntityGraphs(value = {
        @NamedEntityGraph(
                name = "GenericFieldValue.genericField",
                attributeNodes = {
                        @NamedAttributeNode(value = "genericField")
                }
        )
})
public class GenericFieldValue implements IDomain<GenericFieldValueDTO, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "generic_field_value_id")
    private Integer id;

    @Column(name = "value", nullable = false)
    private String value;

    @Column(name = "parent_id", nullable = false)
    private Integer parentId;

    @Column(name = "generic_field_id", nullable = false)
    private Integer genericFieldId;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "generic_field_id", insertable = false, updatable = false)
    private GenericField genericField;

    @ToString.Exclude
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    @PrimaryKeyJoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @JoinColumn(name = "parent_id", insertable = false, updatable = false, foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @ManyToOne(fetch = FetchType.LAZY)
    @org.hibernate.annotations.ForeignKey(name = "none")
    private TimesheetCategory timesheetCategory;

    @ToString.Exclude
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    @PrimaryKeyJoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @JoinColumn(name = "parent_id", insertable = false, updatable = false, foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @ManyToOne(fetch = FetchType.LAZY)
    @org.hibernate.annotations.ForeignKey(name = "none")
    private User user;

    public IGenericFieldAwareDomain getParent() {
        switch (genericField.getEntity()) {
            case TIMESHEET_CATEGORY:
                return timesheetCategory;
            case USER:
                return user;
            default:
                return null;
        }
    }

    public GenericFieldValue(GenericFieldValueDTO dto) {
        BeanUtils.copyProperties(dto, this, true);
    }

}
