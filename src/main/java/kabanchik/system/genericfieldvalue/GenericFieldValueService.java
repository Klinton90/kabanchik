package kabanchik.system.genericfieldvalue;

import com.cosium.spring.data.jpa.entity.graph.domain.EntityGraphUtils;
import kabanchik.system.genericfieldvalue.domain.GenericFieldValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenericFieldValueService {

    @Autowired
    private GenericFieldValueRepository repository;

    public List<GenericFieldValue> findAllByIdIn(List<Integer> ids){
        return repository.findAllByIdIn(ids);
    }

    public List<GenericFieldValue> findAllByIdInWithGenericFields(List<Integer> ids){
        return repository.findAllByIdIn(ids, EntityGraphUtils.fromName("GenericFieldValue.genericField"));
    }

}
