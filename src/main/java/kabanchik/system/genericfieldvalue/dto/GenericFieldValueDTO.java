package kabanchik.system.genericfieldvalue.dto;

import kabanchik.core.domain.IDto;
import kabanchik.system.genericfieldvalue.domain.GenericFieldValue;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class GenericFieldValueDTO implements IDto<GenericFieldValue, Integer> {

    private Integer id;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 255)
    private String value;

    @NotNull
    private Integer genericFieldId;

    @NotNull
    private Integer parentId;

    public GenericFieldValueDTO (GenericFieldValue entity) {
        id = entity.getId();
        value = entity.getValue();
        genericFieldId = entity.getGenericFieldId();
        parentId = entity.getParentId();
    }

}
