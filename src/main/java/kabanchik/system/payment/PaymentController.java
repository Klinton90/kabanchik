package kabanchik.system.payment;


import kabanchik.core.controller.RestController;
import kabanchik.system.payment.domain.Payment;
import kabanchik.system.payment.dto.PaymentDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/payment")
public class PaymentController extends RestController<Payment, PaymentDTO, Integer> {

    @Getter
    @Autowired
    private PaymentService service;

}
