package kabanchik.system.payment;

import kabanchik.core.repository.RestRepository;
import kabanchik.system.payment.domain.Payment;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends RestRepository<Payment, Integer> {
    long countByInvoiceId(Integer invoiceId);
}
