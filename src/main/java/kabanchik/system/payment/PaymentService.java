package kabanchik.system.payment;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.currency.CurrencyService;
import kabanchik.system.invoice.InvoiceService;
import kabanchik.system.payment.domain.Payment;
import kabanchik.system.payment.dto.PaymentDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService extends RestService<Payment, PaymentDTO, Integer> {

    @Getter
    @Autowired
    private PaymentRepository repository;

    @Getter
    @Autowired
    private PaymentService self;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private InvoiceService invoiceService;

    @Override
    public boolean isInUse(Integer integer) {
        return false;
    }

    @Override
    public void validate(Payment entity) throws CustomValidationException {
        entity.setCurrency(currencyService.getOne(entity.getCurrencyId()));
        entity.setInvoice(invoiceService.getOne(entity.getInvoiceId()));
    }

    public long countByInvoiceId(Integer invoiceId) {
        return repository.countByInvoiceId(invoiceId);
    }
}
