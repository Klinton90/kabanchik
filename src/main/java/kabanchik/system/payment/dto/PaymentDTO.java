package kabanchik.system.payment.dto;

import kabanchik.core.domain.IDto;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.invoice.domain.Invoice;
import kabanchik.system.payment.domain.Payment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class PaymentDTO implements IDto<Payment, Integer> {

    private Integer id;

    @NotNull
    private LocalDate date;

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("999999999.99")
    @Digits(integer = 7, fraction = 2)
    private BigDecimal amount;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    private Boolean isForPlan;

    @NotNull
    private String currencyId;

    @NotNull
    private Integer invoiceId;

    private String currencyName;

    public PaymentDTO(Invoice invoice) {
        setDate(invoice.getDatePlanned());
        setAmount(invoice.getAmount());
        setIsForPlan(true);
        setCurrencyId(invoice.getCurrencyId());
        setInvoiceId(invoice.getId());
    }

    @Override
    public PaymentDTO createFromEntity(Payment entity) {
        BeanUtils.copyProperties(entity, this, true);

        setCurrencyName(entity.getCurrency().getName());

        return this;
    }
}
