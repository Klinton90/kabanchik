package kabanchik.system.payment.domain;

import kabanchik.core.domain.IDomain;
import kabanchik.system.currency.domain.Currency;
import kabanchik.system.invoice.domain.Invoice;
import kabanchik.system.payment.dto.PaymentDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

//@Unique
@Entity
@Getter
@Setter
@Table(name = "payment")
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "Payment.default",
        attributeNodes = {
            @NamedAttributeNode(value = "currency"),
        }
    ),
    @NamedEntityGraph(
        name = "Payment.empty"
    )
})
public class Payment implements IDomain<PaymentDTO, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "payment_id")
    private Integer id;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @Column(name = "amount", nullable = false, precision = 19, scale = 2)
    private BigDecimal amount;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "is_for_plan", nullable = false)
    private Boolean isForPlan;

    @Column(name = "currency_id", nullable = false, insertable = false, updatable = false)
    private String currencyId;

    @Column(name = "invoice_id", nullable = false, insertable = false, updatable = false)
    private Integer invoiceId;

    @ToString.Exclude
    @PrimaryKeyJoinColumn
    @JoinColumn(name = "currency_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Currency currency;

    @ToString.Exclude
    @JoinColumn(name = "invoice_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Invoice invoice;

}
