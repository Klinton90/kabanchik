package kabanchik.system.currency;

import kabanchik.core.exception.InvalidInputException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class BankCurrencyUpdaterRunnable implements Runnable {

    private CurrencyService currencyService;

    @Override
    public void run() {
        try {
            currencyService.updateBankRates();
        } catch (InvalidInputException e) {
            log.warn(e.getMessage());
        }
    }
}
