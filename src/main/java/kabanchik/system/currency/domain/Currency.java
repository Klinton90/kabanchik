package kabanchik.system.currency.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.domain.IDomain;
import kabanchik.core.formatter.StringFormat;
import kabanchik.core.formatter.StringFormatter;
import kabanchik.core.validator.Unique;
import kabanchik.system.currencyrate.domain.CurrencyRate;
import kabanchik.system.invoice.domain.Invoice;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import kabanchik.system.currency.dto.CurrencyDTO;
import kabanchik.system.payment.domain.Payment;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Unique
@Entity
@Getter
@Setter
@Table(name = "currency")
public class Currency implements IDomain<CurrencyDTO, String>, ActivatableDomain {

    public static final String UAH = "UAH";

    @Override
    public Object getDefaultIdValue() {
        return "";
    }

    @Id
    @ContainsFilterField
    @StringFormat(style = StringFormatter.Style.UPPERCASE)
    @Column(name = "currency_id", length = 3)
    private String id;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @ContainsFilterField
    @Column(name = "name", nullable = false, length = 50, unique = true)
    private String name;

    @ContainsFilterField
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "rate", nullable = false)
    private BigDecimal rate;

    @OneToMany(mappedBy = "currency", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<OutcomeEntry> outcomeEntries;

    @OneToMany(mappedBy = "currency", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Invoice> invoices;

    @OneToMany(mappedBy = "currency", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Payment> payments;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(mappedBy = "currencyDomain", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<CurrencyRate> currencyRates;

}
