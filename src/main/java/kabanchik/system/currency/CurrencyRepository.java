package kabanchik.system.currency;

import kabanchik.core.repository.RestRepository;
import kabanchik.system.currency.domain.Currency;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends RestRepository<Currency, String> {
}
