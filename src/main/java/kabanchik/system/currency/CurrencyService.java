package kabanchik.system.currency;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.service.RestService;
import kabanchik.system.outcomeentry.OutcomeEntryService;
import kabanchik.system.currency.domain.Currency;
import kabanchik.system.currency.dto.CurrencyDTO;
import kabanchik.system.currency.dto.ExchangeDTO;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class CurrencyService extends RestService<Currency, CurrencyDTO, String> {

    @Getter
    @Autowired
    private CurrencyRepository repository;

    @Autowired
    private OutcomeEntryService outcomeEntryService;

    @Getter
    @Autowired
    private CurrencyService self;

    @Override
    public boolean isInUse(String id) {
        return outcomeEntryService.countByCurrencyId(id) > 0;
    }

    @Override
    public void validate(Currency json) throws CustomValidationException {
        super.validate(json);
        if (StringUtils.isBlank(json.getId()) || json.getId().length() != 3) {
            throw new CustomValidationException("size", Currency.class.getSimpleName(), Currency.class.getSimpleName(), 3, 3);
        }
    }

    @Override
    public Currency initEntity(CurrencyDTO dto) throws CustomValidationException {
        if (StringUtils.isBlank(dto.getId()) && !StringUtils.isBlank(dto.getNewId())) {
            dto.setId(dto.getNewId());
        }
        Currency currency = super.initEntity(dto);
        return currency;
    }

    public ExchangeDTO getBankRate(String currency) {
        UriComponents uri = UriComponentsBuilder
                .fromHttpUrl("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange")
                .queryParam("valcode", currency)
                .queryParam("date", LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .build();
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(uri.toUri(), ExchangeDTO.class);
    }

    @Transactional
    public void updateBankRates() throws InvalidInputException {
        for (Currency currency : repository.findAll()) {
            if (currency.getId().equals(Currency.UAH)) {
                currency.setRate(BigDecimal.ONE);
            } else {
                ExchangeDTO exchangeDTO = getBankRate(currency.getId());
                if (exchangeDTO == null || exchangeDTO.getCurrency() == null) {
                    throw new InvalidInputException("app.error.currency.unknownBankRate");
                }
                currency.setRate(getBankRate(currency.getId()).getCurrency().rate);
            }

            repository.save(currency);
        }
    }

    @Transactional
    public void updateBankRate(String id) throws CustomValidationException {
        Currency currency = getOne(id);
        ExchangeDTO exchangeDTO = getBankRate(id);
        if (exchangeDTO == null || exchangeDTO.getCurrency() == null) {
            Object[] params = new Object[]{Currency.class.getSimpleName()};
            throw new CustomValidationException("unknownBankRate", Currency.class.getSimpleName(), "id", id, currency, params);
        }
        currency.setRate(exchangeDTO.getCurrency().rate);
    }
}
