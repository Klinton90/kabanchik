package kabanchik.system.currency.dto;

import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

@Setter
public class BankCurrencyDTO {
    @XmlElement(name = "r030")
    public String r030;

    @XmlElement(name = "txt")
    public String txt;

    @XmlElement(name = "rate")
    public BigDecimal rate;

    @XmlElement(name = "cc")
    public String cc;

    @XmlElement(name = "exchangedate")
    public String exchangedate;
}
