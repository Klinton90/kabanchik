package kabanchik.system.currency.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.currency.domain.Currency;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
public class CurrencyDTO implements IDto<Currency, String>, ActivatableDomain {

    @Override
    public Object getDefaultIdValue() {
        return "";
    }

    private String id;

    private String newId;

    @NotNull
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @NotNull
    @Size(max = 255)
    private String description;

    private BigDecimal rate;

}
