package kabanchik.system.currency.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@XmlRootElement(name="exchange")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeDTO {

    @XmlElement(name = "currency", required = true)
    public BankCurrencyDTO currency;

}


//<exchange>
    //<currency>
        //<r030>978</r030>
        //<txt>Євро</txt>
        //<rate>33.025991</rate>
        //<cc>EUR</cc>
        //<exchangedate>21.09.2018</exchangedate>
    //</currency>
//</exchange>
