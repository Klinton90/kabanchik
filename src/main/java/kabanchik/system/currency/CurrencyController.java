package kabanchik.system.currency;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.system.currency.domain.Currency;
import kabanchik.system.currency.dto.CurrencyDTO;
import kabanchik.system.currency.dto.ExchangeDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/currency")
public class CurrencyController extends RestController<Currency, CurrencyDTO, String> {

    @Getter
    @Autowired
    private CurrencyService service;

    @RequestMapping(path = "/getBankRate", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getBankRate(@RequestParam String currency) {
        ExchangeDTO result = service.getBankRate(currency);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(path = "/updateBankRates", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> updateBankRates() throws InvalidInputException {
        service.updateBankRates();
        return ResponseEntity.ok(null);
    }

    @RequestMapping(path = "/updateBankRate/{id}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> updateBankRate(@PathVariable String id) throws CustomValidationException {
        service.updateBankRate(id);
        return ResponseEntity.ok(null);
    }

}
