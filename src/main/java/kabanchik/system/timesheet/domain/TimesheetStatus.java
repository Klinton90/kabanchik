package kabanchik.system.timesheet.domain;

import kabanchik.core.domain.LocalizedEnum;
import lombok.Getter;

@Getter
public enum TimesheetStatus implements LocalizedEnum {
    EDITABLE,
    POSTED,
    APPROVED,
    DECLINED,
    PLANNED
}
