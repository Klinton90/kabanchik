package kabanchik.system.timesheet.domain;

import kabanchik.system.timesheet.dto.TimesheetSummaryDTO;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Immutable
@Table(name = "timesheet_with_rate")
public class TimesheetSummary extends TimesheetBase<TimesheetSummaryDTO> {

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "timesheet_category_id")
    private Integer timesheetCategoryId;

    @Column(name = "department_id")
    private Integer departmentId;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "internal_rate")
    private BigDecimal internalRate;

    @Column(name = "timesheet_category_is_active")
    private Boolean timesheetCategoryIsActive;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "timesheet_access_id", nullable = false)
    private TimesheetAccess timesheetAccess;

}
