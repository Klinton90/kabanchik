package kabanchik.system.timesheet.domain;

import kabanchik.core.domain.IDomain;
import kabanchik.system.timesheet.dto.TimesheetBaseDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@MappedSuperclass
public abstract class TimesheetBase<D extends TimesheetBaseDTO> implements IDomain<D, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timesheet_id")
    private Integer id;

    @Column(name = "amount", nullable = false, precision = 4, scale = 2)
    private BigDecimal amount;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private TimesheetStatus status;

    @Column(name = "comment", nullable = false, columnDefinition = "TEXT")
    private String comment;

    @Column(name = "wbs_id", nullable = false)
    private String wbsId;

    @Column(name = "timesheet_access_id", nullable = false, insertable = false, updatable = false)
    private Integer timesheetAccessId;

}
