package kabanchik.system.timesheet.domain;

import com.querydsl.core.annotations.QueryInit;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.timesheet.dto.TimesheetDTO;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Table(name = "timesheet")
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "Timesheet.default",
        attributeNodes = {
            @NamedAttributeNode(value = "timesheetAccess")
        }
    ),
    @NamedEntityGraph(
        name = "Timesheet.withCategory",
        attributeNodes = {
            @NamedAttributeNode(value = "timesheetAccess", subgraph = "Timesheet.timesheetAccess.timesheetCategory")
        },
        subgraphs = {
            @NamedSubgraph(
                name = "Timesheet.timesheetAccess.timesheetCategory",
                attributeNodes = {
                    @NamedAttributeNode("timesheetCategory")
                }
            )
        }
    ),
})
public class Timesheet extends TimesheetBase<TimesheetDTO> {

    public static final String WBS_FIELD = "wbsId";

    @NotNull
    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "timesheet_access_id", nullable = false)
    @QueryInit(value = {"user.*", "timesheetCategory.*"})
    private TimesheetAccess timesheetAccess;

    @Override
    public Timesheet createFromDto(TimesheetDTO dto) {
        BeanUtils.copyProperties(dto, this, true);
        return this;
    }

}
