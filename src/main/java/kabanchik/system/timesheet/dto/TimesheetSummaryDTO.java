package kabanchik.system.timesheet.dto;

import kabanchik.core.util.BeanUtils;
import kabanchik.system.timesheet.domain.TimesheetSummary;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TimesheetSummaryDTO extends TimesheetBaseDTO<TimesheetSummary> {

    private Integer departmentId;

    private BigDecimal rate;

    private BigDecimal internalRate;

    public TimesheetSummaryDTO createFromEntity(TimesheetSummary timesheet){
        BeanUtils.copyProperties(timesheet, this, true);
        return this;
    }

}
