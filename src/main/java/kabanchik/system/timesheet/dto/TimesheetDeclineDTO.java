package kabanchik.system.timesheet.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class TimesheetDeclineDTO {

    @NotNull
    private Integer id;

    @Size(max = 65025)
    private String comment;

}
