package kabanchik.system.timesheet.dto;

import kabanchik.core.util.BeanUtils;
import kabanchik.system.timesheet.domain.Timesheet;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TimesheetDTO extends TimesheetBaseDTO<Timesheet> {

    @Override
    public TimesheetDTO createFromEntity(Timesheet timesheet){
        BeanUtils.copyProperties(timesheet, this, true);
        setTimesheetCategoryId(timesheet.getTimesheetAccess().getTimesheetCategoryId());
        setUserId(timesheet.getTimesheetAccess().getUserId());
        return this;
    }

    public TimesheetDTO(BulkTimesheetAddDTO dto) {
        setId(0);
        setComment("");
        setUserId(dto.getUserId());
        setAmount(dto.getAmount());
        setStatus(TimesheetStatus.PLANNED);
        setTimesheetCategoryId(dto.getTimesheetCategoryId());
    }

}
