package kabanchik.system.timesheet.dto;

import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public abstract class TimesheetBaseDTO<E extends IDomain> implements IDto<E, Integer> {

    @NotNull
    private Integer id;

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("99.99")
    @Digits(integer = 2, fraction = 2)
    private BigDecimal amount;

    @NotNull
    private TimesheetStatus status;

    @Size(max = 65025)
    private String comment;

    @Size(max = 255)
    private String wbsId;

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    private Integer userId;

    private LocalDate date;

}
