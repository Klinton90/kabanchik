package kabanchik.system.timesheet.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class BulkTimesheetAdjustmentDTO {

    @NotNull
    private Integer adjustmentWeeks;

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer adjustmentUserId;

    private BigDecimal adjustmentAmount;

    private LocalDate adjustmentFromDate;

    private LocalDate adjustmentToDate;

}
