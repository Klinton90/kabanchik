package kabanchik.system.timesheet.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class BulkTimesheetAddDTO {

    @NotNull
    private LocalDate dateFrom;

    @NotNull
    private LocalDate dateTo;

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    private Integer userId;

    @NotNull
    private BigDecimal amount;

}
