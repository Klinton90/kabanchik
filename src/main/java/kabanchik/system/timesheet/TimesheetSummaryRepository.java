package kabanchik.system.timesheet;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import kabanchik.system.timesheet.domain.TimesheetSummary;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import org.springframework.data.util.Pair;

import java.time.LocalDate;
import java.util.List;

import static kabanchik.system.timesheet.domain.QTimesheetSummary.timesheetSummary;

public interface TimesheetSummaryRepository extends RestRepository<TimesheetSummary, Integer> {

    default BooleanExpression getBooleanExpressionByPeriod(
            Integer yearFrom,
            Integer monthFrom,
            Integer yearTo,
            Integer monthTo
    ) {
        Pair<LocalDate, LocalDate> datesPair = ConfigurationService.getDatesByDateCriteria(yearFrom, monthFrom, yearTo, monthTo);

        return timesheetSummary.date.between(datesPair.getFirst(), datesPair.getSecond());
    }

    default List<TimesheetSummary> findAllForPsr(Integer timesheetCategoryId) {
        BooleanExpression booleanExpression = timesheetSummary.timesheetCategoryId.eq(timesheetCategoryId)
                .and(timesheetSummary.status.in(TimesheetStatus.PLANNED, TimesheetStatus.APPROVED));

        return new JPAQuery<TimesheetSummary>(getEntityManager())
                .from(timesheetSummary)
                .where(booleanExpression)
                .fetch();
    }

    default List<TimesheetSummary> findAllManagedTimesheetsForUser(
            User user,
            Integer yearFrom,
            Integer monthFrom,
            Integer yearTo,
            Integer monthTo,
            Integer departmentId,
            Boolean forPsr
    ){
        BooleanExpression booleanExpression = getBooleanExpressionByPeriod(yearFrom, monthFrom, yearTo, monthTo);

        if (forPsr) {
            booleanExpression = booleanExpression.and(timesheetSummary.status.eq(TimesheetStatus.PLANNED))
                    .and(timesheetSummary.timesheetCategoryIsActive.isTrue());
        } else {
            booleanExpression = booleanExpression.and(timesheetSummary.status.notIn(TimesheetStatus.DECLINED, TimesheetStatus.PLANNED));

            if (!user.getRole().equals(UserRole.ADMIN)) {
                booleanExpression = booleanExpression.and(timesheetSummary.timesheetAccess.timesheetCategory.managers.any().id.eq(user.getId()));
            }
        }

        if (departmentId != null && departmentId > 0) {
            booleanExpression = booleanExpression.and(timesheetSummary.departmentId.eq(departmentId));
        }

        return new JPAQuery<TimesheetSummary>(getEntityManager())
                .from(timesheetSummary)
                .where(booleanExpression)
                .fetch();
    }

}
