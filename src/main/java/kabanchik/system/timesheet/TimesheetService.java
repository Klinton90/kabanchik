package kabanchik.system.timesheet;

import kabanchik.core.controller.request.BulkSaveAbstractDTO;
import kabanchik.core.controller.request.BulkSaveDTO;
import kabanchik.core.controller.request.BulkSaveDeleteDTO;
import kabanchik.core.controller.request.BulkSaveInsertUpdateDTO;
import kabanchik.core.exception.CustomValidationCollectionException;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.exception.SpringValidationException;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.holiday.HolidayService;
import kabanchik.system.timesheet.domain.Timesheet;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import kabanchik.system.timesheet.dto.*;
import kabanchik.system.timesheet.exception.TimesheetDuplicate;
import kabanchik.system.timesheetcategory.TimesheetCategoryService;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.timesheetaccess.TimesheetAccessService;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.timesheetaccess.dto.TimesheetAccessDTO;
import kabanchik.system.user.UserRepository;
import kabanchik.system.user.UserService;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TimesheetService {

    @Autowired
    private TimesheetRepository repository;

    @Autowired
    private TimesheetAccessService timesheetAccessService;

    @Autowired
    private TimesheetCategoryService timesheetCategoryService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    protected ExposedResourceMessageBundleSource msgSource;

    @Autowired
    protected HolidayService holidayService;

    @Autowired
    private TimesheetSummaryRepository timesheetSummaryRepository;

    @Getter
    @Autowired
    TimesheetService self;

    public Timesheet getTimesheetForLoggedUserById(Integer timesheetId, Boolean withCategory) throws CustomValidationException {
        Timesheet timesheet = repository.findTimesheetByIdAndTimesheetAccessUserId(timesheetId, UserService.getPrincipal().getId(), withCategory);

        if (timesheet == null) {
            Object[] messageParams = {TimesheetAccess.class.getSimpleName(), timesheetId};
            throw new CustomValidationException("accessDenied", Timesheet.class.getSimpleName(), messageParams);
        }

        return timesheet;
    }

    public Timesheet getManagedTimesheetForLoggedUserById(Integer timesheetId) throws CustomValidationException {
        Timesheet timesheet;
        if (UserService.getPrincipal().getRole() == UserRole.ADMIN) {
            timesheet = repository.findOne(timesheetId);
        } else {
            timesheet = repository.findReportingTimesheet(timesheetId, UserService.getPrincipal().getId());
        }

        if (timesheet == null) {
            Object[] messageParams = {TimesheetAccess.class.getSimpleName(), timesheetId};
            throw new CustomValidationException("accessDenied", Timesheet.class.getSimpleName(), messageParams);
        }

        return timesheet;
    }

    public List<TimesheetDTO> findAllTimesheetsByUserIdAndYearMonth(Integer userId, Integer year, Integer month) {
        return repository.findAllTimesheetsByUserIdAndYearMonth(userId, year, month)
                .stream()
                .map(this::initDTO)
                .collect(Collectors.toList());
    }

    public List<Timesheet> findAllTimesheetsByUserIdAndPeriod(Integer userId, LocalDate from, LocalDate to){
        return repository.findAllTimesheetsByUserIdAndPeriod(userId, from, to);
    }

    public List<TimesheetSummaryDTO> findAllManagedTimesheetsForLoggedUser(Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo, Integer departmentId, Boolean forPsr) {
        return timesheetSummaryRepository.findAllManagedTimesheetsForUser(UserService.getPrincipal(), yearFrom, monthFrom, yearTo, monthTo, departmentId, forPsr)
                .stream()
                .map(timesheet -> new TimesheetSummaryDTO().createFromEntity(timesheet))
                .collect(Collectors.toList());
    }

    public List<TimesheetDTO> findAllManagedTimesheetsByUserId(Integer userId, Integer year, Integer month) {
        if (UserService.getPrincipal().getRole() == UserRole.ADMIN) {
            return findAllTimesheetsByUserIdAndYearMonth(userId, year, month);
        } else {
            return repository.findAllManagedTimesheetsByManagerIdAndUserId(UserService.getPrincipal().getId(), userId, year, month)
                    .stream()
                    .map(this::initDTO)
                    .collect(Collectors.toList());
        }
    }

    public Integer countByTimesheetAccessId(int id) {
        return repository.countByTimesheetAccessId(id);
    }

    @Validated
    @Transactional(rollbackFor = {Exception.class})
    public List<TimesheetDTO> createDoubleTimesheet(@Valid TimesheetDTO dto, boolean autoAdjustMaxHours) throws InvalidInputException, CustomValidationException {
        List<TimesheetDTO> result = new ArrayList<>();
        User targetUser = getTargetUserByDTO(dto);
        LocalDate lastDayOfWeek = HolidayService.getFridayOrLastDayOfWeek(dto.getDate(), ConfigurationService.cachedConfig.isTimesheetMode());

        if (!lastDayOfWeek.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
            BigDecimal weeklyTotal = getWeeklyTotalAmount(dto, targetUser.getId());
            BigDecimal weeklyMax = new BigDecimal(holidayService.calcWeeklyMax(dto.getDate(), ConfigurationService.cachedConfig.isTimesheetMode()));

            if (weeklyTotal.compareTo(weeklyMax) > 0) {
                BigDecimal secondDtoAmount = dto.getAmount().subtract(weeklyMax);

                TimesheetDTO secondDTO = new TimesheetDTO();
                BeanUtils.copyProperties(dto, secondDTO);
                dto.setAmount(weeklyMax);
                secondDTO.setAmount(secondDtoAmount);
                secondDTO.setDate(lastDayOfWeek.plusDays(1));

                // In case when second week has all days as holidays - do not try to create 2nd record
                BigDecimal secondWeeklyMax = new BigDecimal(holidayService.calcWeeklyMax(secondDTO.getDate(), ConfigurationService.cachedConfig.isTimesheetMode()));
                if (secondWeeklyMax.compareTo(BigDecimal.ZERO) > 0) {
                    if (secondDtoAmount.compareTo(BigDecimal.ZERO) < 0) {
                        Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getAmount(), weeklyMax};
                        throw new CustomValidationException("timesheetAccessWeeklyTotalTooHigh", Timesheet.class.getSimpleName(), messageParams);
                    }

                    result.add(create(secondDTO, autoAdjustMaxHours));
                }

                result.add(create(dto, false));
            } else {
                result.add(create(dto, autoAdjustMaxHours));
            }
        } else {
            result.add(create(dto, autoAdjustMaxHours));
        }

        return result;
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public TimesheetDTO create(@Valid TimesheetDTO dto, boolean autoAdjustMaxHours) throws CustomValidationException, InvalidInputException {
        User targetUser = getTargetUserByDTO(dto);

        if (!isManagedTimesheetCategory(dto.getTimesheetCategoryId()) && (dto.getStatus().equals(TimesheetStatus.APPROVED) || dto.getStatus().equals(TimesheetStatus.PLANNED))) {
            Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getId(), TimesheetStatus.class.getSimpleName(), dto.getStatus()};
            throw new CustomValidationException("invalid", TimesheetStatus.class.getSimpleName(), messageParams);
        }

        validateWbsId(dto, false);
        validateDayOfWeek(dto);
        validateTimesheetDuplicate(dto, targetUser.getId());
        validateWeeklyTotalAmount(dto, targetUser.getId(), autoAdjustMaxHours);

        LocalDate minDate = LocalDate.now().minusMonths(1).withDayOfMonth(1);
        if (!UserService.getPrincipal().getRole().equals(UserRole.ADMIN) && !dto.getStatus().equals(TimesheetStatus.PLANNED) && minDate.compareTo(dto.getDate()) > 0) {
            throw new CustomValidationException("tooOld", Timesheet.class.getSimpleName(), null, dto.getId().toString(), dto,
                    Timesheet.class.getSimpleName(), dto.getId(), dto.getDate().getMonthValue(), dto.getDate().getYear());
        }

        TimesheetAccess timesheetAccess;
        try {
            timesheetAccess = timesheetAccessService.findByUserIdAndTimesheetCategoryId(targetUser.getId(), dto.getTimesheetCategoryId());
        } catch (CustomValidationException e) {
            if (dto.getStatus().equals(TimesheetStatus.PLANNED)) {
                timesheetAccess = createTimesheetAccess(dto.getTimesheetCategoryId(), targetUser.getId());
            } else {
                throw e;
            }
        }

        Timesheet timesheet = new Timesheet().createFromDto(dto);
        timesheet.setTimesheetAccess(timesheetAccess)
                .setTimesheetAccessId(timesheetAccess.getId())
                .setId(0);

        return initDTO(getSelf()._save(timesheet));
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public TimesheetDTO update(@Valid TimesheetDTO dto) throws CustomValidationException, InvalidInputException {
        Timesheet timesheet;
        User targetUser = getTargetUserByDTO(dto);

        validateWbsId(dto, true);
        validateDayOfWeek(dto);
        validateTimesheetDuplicate(dto, targetUser.getId());
        validateWeeklyTotalAmount(dto, targetUser.getId(), false);

        boolean isManagedTimesheetCategory = isManagedTimesheetCategory(dto.getTimesheetCategoryId());

        if (isManagedTimesheetCategory) {
            timesheet = getManagedTimesheetForLoggedUserById(dto.getId());

            if (timesheet.getStatus().equals(TimesheetStatus.PLANNED)) {
                TimesheetAccess timesheetAccess;
                try {
                    timesheetAccess = timesheetAccessService.findByUserIdAndTimesheetCategoryId(targetUser.getId(), dto.getTimesheetCategoryId());
                } catch (CustomValidationException e) {
                    timesheetAccess = createTimesheetAccess(dto.getTimesheetCategoryId(), targetUser.getId());
                }

                timesheet.setTimesheetAccess(timesheetAccess)
                        .setTimesheetAccessId(timesheetAccess.getId())
                        .setDate(dto.getDate());
            }
        } else {
            timesheet = getTimesheetForLoggedUserById(dto.getId(), true);

            if (dto.getStatus().equals(TimesheetStatus.APPROVED) || dto.getStatus().equals(TimesheetStatus.PLANNED)) {
                Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getId(), TimesheetStatus.class.getSimpleName(), dto.getStatus()};
                throw new CustomValidationException("invalid", TimesheetStatus.class.getSimpleName(), messageParams);
            }
        }

        LocalDate minDate = LocalDate.now().minusMonths(1).withDayOfMonth(1);
        if (!UserService.getPrincipal().getRole().equals(UserRole.ADMIN)
                && !dto.getStatus().equals(TimesheetStatus.PLANNED)
                && minDate.compareTo(dto.getDate()) > 0
                && !dto.getDate().equals(timesheet.getDate())
        ) {
            throw new CustomValidationException("tooOld", Timesheet.class.getSimpleName(), null, dto.getId().toString(), dto,
                    Timesheet.class.getSimpleName(), dto.getId(), dto.getDate().getMonthValue(), dto.getDate().getYear());
        }

        if (!UserService.getPrincipal().getRole().equals(UserRole.ADMIN)) {
            if (timesheet.getStatus().equals(TimesheetStatus.APPROVED)) {
                Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getId()};
                throw new CustomValidationException("lockedStatus", Timesheet.class.getSimpleName(), messageParams);
            }
        }

        timesheet.setStatus(dto.getStatus())
                .setAmount(dto.getAmount())
//                .setWbsId(dto.getWbsId())
                .setComment(getCommentForUpdate(isManagedTimesheetCategory, dto.getComment(), timesheet.getComment()));

        return initDTO(getSelf()._save(timesheet));
    }

    @Transactional
    public void delete(Integer id) {
        repository.delete(id);
    }

    public TimesheetDTO submitTimesheet(Integer timesheetId, TimesheetStatus status) throws CustomValidationException {
        Timesheet timesheet;
        if (UserService.getPrincipal().getRole() == UserRole.ADMIN) {
            timesheet = repository.findOne(timesheetId);
        } else if (UserService.getPrincipal().getRole() == UserRole.EXECUTOR) {
            timesheet = repository.findReportingTimesheet(timesheetId, UserService.getPrincipal().getId());
        } else {
            timesheet = getTimesheetForLoggedUserById(timesheetId, false);
        }
        timesheet.setStatus(status);
        return initDTO(getSelf()._save(timesheet));
    }

    public TimesheetDTO approveTimesheet(Integer timesheetId, TimesheetStatus status) throws CustomValidationException {
        Timesheet timesheet = getManagedTimesheetForLoggedUserById(timesheetId);
        timesheet.setStatus(status);
        return initDTO(getSelf()._save(timesheet));
    }

    @Deprecated
    @Validated
    public TimesheetDTO decline(@Valid TimesheetDeclineDTO dto) throws CustomValidationException {
        Timesheet timesheet = getManagedTimesheetForLoggedUserById(dto.getId());

        String comment = timesheet.getComment()
                + "\r\n"
                + msgSource.getMessage("app.text.timesheet.fromManagerStart")
                + "\r\n"
                + dto.getComment()
                + "\r\n"
                + msgSource.getMessage("app.text.timesheet.fromManagerEnd");
        timesheet.setComment(comment)
                .setStatus(TimesheetStatus.DECLINED);

        return initDTO(getSelf()._save(timesheet));
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public Timesheet _save(@Valid Timesheet preparedEntity){
        return repository.save(preparedEntity);
    }

    public TimesheetDTO initDTO(Timesheet timesheet) {
        return new TimesheetDTO().createFromEntity(timesheet);
    }

    public static Pair<LocalDate, LocalDate> getWeekStartEnd(LocalDate weekDate, boolean isWeeklyTimesheetMode) {
        LocalDate from = weekDate.with(DayOfWeek.MONDAY);
        LocalDate to = weekDate.with(DayOfWeek.SUNDAY);

        if (isWeeklyTimesheetMode) {
            if (from.getMonthValue() != weekDate.getMonthValue()) {
                from = weekDate.withDayOfMonth(1);
            }
            if (to.getMonthValue() != weekDate.getMonthValue()) {
                to = weekDate.withDayOfMonth(weekDate.lengthOfMonth());
            }
        }

        return new ImmutablePair<>(from, to);
    }

    public List<TimesheetSummaryDTO> findAllForPsr(Integer timesheetCategoryId) {
        return timesheetSummaryRepository.findAllForPsr(timesheetCategoryId)
                .stream()
                .map(timesheet -> new TimesheetSummaryDTO().createFromEntity(timesheet))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<Long> bulkTimesheetInsert(BulkTimesheetAddDTO dto) throws InvalidInputException, CustomValidationException {
        List<Long> result = new ArrayList<>();

        LocalDate dateFrom = dto.getDateFrom().with(DayOfWeek.MONDAY);
        if (dateFrom.getYear() < dto.getDateFrom().getYear()) {
            dateFrom = dto.getDateFrom().withDayOfMonth(1);
        }
        LocalDate dateTo = dto.getDateTo().with(DayOfWeek.THURSDAY);
        while (dateFrom.isBefore(dateTo)) {
            try {
                TimesheetDTO timesheetDTO = new TimesheetDTO(dto);
                timesheetDTO.setDate(dateFrom);
                createDoubleTimesheet(timesheetDTO, true);
            } catch (TimesheetDuplicate e) {
                result.add(dateFrom.atStartOfDay().toEpochSecond(ZoneOffset.UTC) * 1000);
            }
            dateFrom = dateFrom.plusDays(7);
        }
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public void bulkTimesheetAdjustment(BulkTimesheetAdjustmentDTO dto) throws CustomValidationException, InvalidInputException {
        List<Timesheet> timesheets = repository.findAllTimesheetsForBulkTimesheetAdjustment(
                dto.getTimesheetCategoryId(),
                dto.getUserId(),
                TimesheetStatus.PLANNED,
                dto.getAdjustmentFromDate(),
                dto.getAdjustmentToDate());

        if (timesheets != null && timesheets.size() > 0) {
            LocalDate min = timesheets.get(0).getDate();
            LocalDate max = timesheets.get(0).getDate();

            for (Timesheet timesheet : timesheets) {
                if (min.isAfter(timesheet.getDate())) {
                    min = timesheet.getDate();
                }
                if (max.isBefore(timesheet.getDate())) {
                    max = timesheet.getDate();
                }
            }

            min = min.with(DayOfWeek.MONDAY);
            max = max.with(DayOfWeek.FRIDAY);

            List<Timesheet> timesheetsForSave = new ArrayList<>();

            while (min.isBefore(max)) {
                Timesheet _timesheet = null;
                LocalDate _max = min.with(DayOfWeek.FRIDAY);

                for (Timesheet timesheet : timesheets) {
                    if ((timesheet.getDate().isEqual(min) || timesheet.getDate().isAfter(min))
                            && (timesheet.getDate().isEqual(_max) || timesheet.getDate().isBefore(_max))) {
                        if (_timesheet == null) {
                            _timesheet = timesheet;
                        } else {
                            _timesheet.setAmount(_timesheet.getAmount().add(timesheet.getAmount()));
                        }
                    }
                }

                if (_timesheet != null) {
                    if (dto.getAdjustmentAmount() != null && dto.getAdjustmentAmount().compareTo(BigDecimal.ZERO) > 0) {
                        _timesheet.setAmount(dto.getAdjustmentAmount());
                    }

                    timesheetsForSave.add(_timesheet);
                }

                min = min.plusDays(7);
            }

            timesheetsForSave.sort(Comparator.comparing(Timesheet::getDate));
            Collections.reverse(timesheetsForSave);

            for (Timesheet timesheet : timesheets) {
                repository.delete(timesheet);
            }

            for (Timesheet timesheet : timesheetsForSave) {
                TimesheetDTO timesheetDTO = new TimesheetDTO().createFromEntity(timesheet);
                timesheetDTO.setId(0)
                        .setDate(timesheetDTO.getDate().plusDays(7 * dto.getAdjustmentWeeks()))
                        .setUserId(dto.getAdjustmentUserId());

                createDoubleTimesheet(timesheetDTO, true);
            }
        }
    }

    @Transactional
    public void bulkTimesheetRemoval(Integer userId, Integer timesheetCategoryId) {
        repository.bulkTimesheetRemovalByUserIdAndTimesheetCategoryIdAndStatus(userId, timesheetCategoryId, TimesheetStatus.PLANNED);
    }

    @Deprecated
    private TimesheetStatus calcTimesheetStatus(TimesheetCategory timesheetCategory, Boolean submit) {
        TimesheetStatus timesheetStatus;
        if (submit) {
            timesheetStatus = timesheetCategory.getRequireApproval() ? TimesheetStatus.POSTED : TimesheetStatus.APPROVED;
        } else {
            timesheetStatus = TimesheetStatus.EDITABLE;
        }
        return timesheetStatus;
    }

    private BigDecimal getWeeklyTotalAmount(TimesheetDTO dto, Integer userId) {
        BigDecimal weeklyTotal = repository.getTimesheetAmountTotalByTimesheetAndUserId(dto, userId);
        if (weeklyTotal == null) {
            weeklyTotal = BigDecimal.ZERO;
        }
        return weeklyTotal.add(dto.getAmount());
    }

    private void validateWeeklyTotalAmount(TimesheetDTO dto, Integer userId, boolean autoAdjustMaxHours) throws CustomValidationException {
        BigDecimal weeklyTotal = getWeeklyTotalAmount(dto, userId);
        BigDecimal weeklyMax = new BigDecimal(holidayService.calcWeeklyMax(dto.getDate(), ConfigurationService.cachedConfig.isTimesheetMode()));

        if (weeklyTotal.compareTo(weeklyMax) > 0) {
            BigDecimal adjustmentAmount = weeklyMax.subtract(weeklyTotal.subtract(dto.getAmount()));
            if (autoAdjustMaxHours && adjustmentAmount.compareTo(BigDecimal.ZERO) > 0) {
                dto.setAmount(adjustmentAmount);
            } else {
                Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getAmount(), weeklyMax};
                throw new CustomValidationException("timesheetAccessWeeklyTotalTooHigh", Timesheet.class.getSimpleName(), messageParams);
            }
        }
    }

    private void validateDayOfWeek(TimesheetDTO dto) throws CustomValidationException {
        if (dto.getDate().getDayOfWeek().equals(DayOfWeek.SUNDAY) || dto.getDate().getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
            Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getId()};
            throw new CustomValidationException("dayOfWeek", Timesheet.class.getSimpleName(), messageParams);
        }
    }

    private void validateWbsId(TimesheetDTO dto, boolean isUpdate) throws CustomValidationException {
        if (!isUpdate && !dto.getStatus().equals(TimesheetStatus.PLANNED) && StringUtils.isBlank(dto.getWbsId())) {
            Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getId(), Timesheet.WBS_FIELD};
            throw new CustomValidationException("required", TimesheetStatus.class.getSimpleName(), messageParams);
        }
        if (dto.getWbsId() == null) {
            dto.setWbsId("");
        }
    }

    private boolean isManagedTimesheetCategory(int timesheetCategoryId) {
        User principal = UserService.getPrincipal();
        return principal.getRole() == UserRole.ADMIN
                || (principal.getRole() == UserRole.EXECUTOR
                && timesheetCategoryService.getByIdAndManagerId(timesheetCategoryId, principal.getId()) != null);
    }

    private User getTargetUserByDTO(TimesheetDTO dto) throws InvalidInputException {
        User principal = UserService.getPrincipal();

        User targetUser;
        if (UserRole.isExecutor(principal.getRole())) {
            targetUser = userRepository.getOne(dto.getUserId());
            if (targetUser.getRole().equals(UserRole.INTERNAL) && !dto.getStatus().equals(TimesheetStatus.PLANNED)) {
                Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getId(), dto.getTimesheetCategoryId()};
                throw new InvalidInputException("app.validation.user.internalUserOnRealTimesheet", messageParams, dto);
            }
        } else {
            targetUser = principal;
        }

        return targetUser;
    }

    @Transactional
    private TimesheetAccess createTimesheetAccess(Integer timesheetCategoryId, Integer userId) throws CustomValidationException {
        TimesheetAccessDTO timesheetAccessDTO = new TimesheetAccessDTO()
                .setTimesheetCategoryId(timesheetCategoryId)
                .setUserId(userId)
                .setIsActive(true);
        return timesheetAccessService.createSkipValidation(timesheetAccessDTO);
    }

    private String getCommentForUpdate(boolean isManagedTimesheetCategory, String newComment, String oldComment) {
        if (newComment != null && newComment.length() > 0) {
            if (isManagedTimesheetCategory) {
                return oldComment
                        + "\r\n"
                        + msgSource.getMessage("app.text.timesheet.fromManagerStart")
                        + "\r\n"
                        + newComment
                        + "\r\n"
                        + msgSource.getMessage("app.text.timesheet.fromManagerEnd");
            } else {
                return oldComment
                        + "\r\n"
                        + msgSource.getMessage("app.text.timesheet.fromUserStart")
                        + "\r\n"
                        + newComment
                        + "\r\n"
                        + msgSource.getMessage("app.text.timesheet.fromUserEnd");
            }
        } else {
            return oldComment;
        }
    }

    private void validateTimesheetDuplicate(TimesheetDTO dto, Integer userId) throws TimesheetDuplicate {
        Timesheet existingTimesheet = repository.findByTimesheetCategoryIdAndUserIdAndDateAndWbsId(
                dto.getTimesheetCategoryId(),
                userId,
                dto.getDate(),
                dto.getWbsId(),
                dto.getStatus().equals(TimesheetStatus.PLANNED));

        if (existingTimesheet != null && !dto.getId().equals(existingTimesheet.getId())) {
            Object[] messageParams = {Timesheet.class.getSimpleName(), dto.getTimesheetCategoryId(), userId, dto.getDate()};
            throw new TimesheetDuplicate(messageParams, dto);
        }
    }

    public List<Timesheet> findAllByIdIn(List<Integer> ids) {
        return repository.findAllByIdIn(ids);
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveAll(BulkSaveDTO<TimesheetDTO, Timesheet, Integer> bulkSaveDTO) throws CustomValidationCollectionException {
        CustomValidationCollectionException errors = new CustomValidationCollectionException();

        if (bulkSaveDTO.getRowsForUpdate() != null && bulkSaveDTO.getRowsForUpdate().size() > 0) {
            Map<Boolean, List<BulkSaveInsertUpdateDTO<TimesheetDTO, Timesheet, Integer>>> collect = bulkSaveDTO.getRowsForUpdate().stream()
                    .collect(Collectors.groupingBy(d ->
                            d.getData().getId() != null
                            && StringUtils.isNotBlank(d.getData().getId().toString())
                            // TODO: I think this is not needed. UI should send `null` in order to initialize ID
                            && !d.getData().getId().equals(d.getData().getDefaultIdValue())
                    ));

            // Create
            if (collect.containsKey(false)) {
                for (BulkSaveInsertUpdateDTO<TimesheetDTO, Timesheet, Integer> dto : collect.get(false)) {
                    try {
                        getSelf().create(dto.getData(), false);
                    } catch (SpringValidationException e) {
                        errors.add(buildExceptionFromBindingResult(e.getBindingResult(), dto));
                    } catch (CustomValidationException e) {
                        errors.add(e.setId(dto.getUiId().toString()));
                    } catch (Exception e) {
                        errors.add(new CustomValidationException(e.getMessage(), null, null, dto.getUiId().toString(), null));
                    }
                }
            }

            // Update
            if (collect.containsKey(true)) {
                List<Integer> ids = collect.get(true)
                        .stream()
                        .map(d -> d.getData().getId())
                        .collect(Collectors.toList());
                List<Timesheet> dbEntities = findAllByIdIn(ids);

                if (dbEntities.size() != ids.size()) {
                    throw new RuntimeException("Some records doesn't exist in Database");
                }

                for (BulkSaveInsertUpdateDTO<TimesheetDTO, Timesheet, Integer> dto : collect.get(true)) {
                    try {
                        getSelf().update(dto.getData());
                    } catch (SpringValidationException e) {
                        errors.add(buildExceptionFromBindingResult(e.getBindingResult(), dto));
                    } catch (CustomValidationException e) {
                        errors.add(e.setId(dto.getUiId().toString()));
                    } catch (Exception e) {
                        errors.add(new CustomValidationException(e.getMessage(), null, null, dto.getUiId().toString(), null));
                    }
                }
            }
        }

        // Delete
        if (bulkSaveDTO.getIdsToDelete() != null && bulkSaveDTO.getIdsToDelete().size() > 0) {
            for (BulkSaveDeleteDTO<Integer> dto : bulkSaveDTO.getIdsToDelete()) {
                try {
                    getSelf().delete(dto.getId());
                } catch (SpringValidationException e) {
                    errors.add(buildExceptionFromBindingResult(e.getBindingResult(), dto));
//                } catch (CustomValidationException e) {
//                    errors.add(e.setId(dto.getUiId().toString()));
                } catch (Exception e) {
                    errors.add(new CustomValidationException(e.getMessage(), null, null, dto.getUiId().toString(), null));
                }
            }
        }

        errors.throwIfAny();
    }

    private CustomValidationException buildExceptionFromBindingResult(BindingResult bindingResult, BulkSaveAbstractDTO dto) {
        return new CustomValidationException(bindingResult)
                .setId(dto.getUiId().toString());
    }

}
