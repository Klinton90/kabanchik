package kabanchik.system.timesheet;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.timesheet.domain.Timesheet;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import kabanchik.system.timesheet.dto.TimesheetDTO;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static kabanchik.system.timesheet.domain.QTimesheet.timesheet;
import static kabanchik.system.timesheetaccess.domain.QTimesheetAccess.timesheetAccess;

@Repository
public interface TimesheetRepository extends RestRepository<Timesheet, Integer> {

    Integer countByTimesheetAccessId(Integer timesheetAccessId);

    List<Timesheet> findAllByIdIn(List<Integer> ids);

    default Timesheet findByTimesheetCategoryIdAndUserIdAndDateAndWbsId(
            Integer timesheetCategoryId,
            Integer userId,
            LocalDate date,
            String wbsId,
            boolean forPlan
    ) {
        BooleanExpression booleanExpression = timesheet.timesheetAccess.timesheetCategoryId.eq(timesheetCategoryId)
                .and(timesheet.timesheetAccess.userId.eq(userId))
                .and(timesheet.date.eq(date))
                .and(timesheet.wbsId.eq(wbsId == null ? "" : wbsId));

        if (forPlan) {
            booleanExpression = booleanExpression.and(timesheet.status.eq(TimesheetStatus.PLANNED));
        } else {
            booleanExpression = booleanExpression.and(timesheet.status.ne(TimesheetStatus.PLANNED));
        }

        return new JPAQuery<Timesheet>(getEntityManager())
                .from(timesheet)
                .where(booleanExpression)
                .fetchOne();
    }

    default BigDecimal getTimesheetAmountTotalByTimesheetAndUserId(TimesheetDTO dto, Integer userId) {
        BooleanExpression booleanExpression = timesheet.date.eq(dto.getDate())
                .and(timesheet.id.ne(dto.getId()))
                .and(timesheet.timesheetAccess.userId.eq(userId));

        if (dto.getStatus().equals(TimesheetStatus.PLANNED)) {
            booleanExpression = booleanExpression.and(timesheet.status.eq(TimesheetStatus.PLANNED))
                    .and(timesheet.timesheetAccess.userId.eq(dto.getUserId())
                    .and(timesheet.timesheetAccess.timesheetCategoryId.eq(dto.getTimesheetCategoryId())));
        } else {
            booleanExpression = booleanExpression.and(timesheet.status.notIn(TimesheetStatus.DECLINED, TimesheetStatus.PLANNED));
        }

        return new JPAQuery<Timesheet>(getEntityManager())
                .select(timesheet.amount.sum())
                .from(timesheet)
                .where(booleanExpression)
                .fetchOne();
    }

    default List<Timesheet> findAllTimesheetsByUserIdAndYearMonth(Integer userId, Integer year, Integer month){
        BooleanExpression booleanExpression = getBooleanExpressionByPeriod(year, month, year, month)
                .and(timesheet.timesheetAccess.userId.eq(userId))
                .and(timesheet.status.ne(TimesheetStatus.PLANNED));

        return new JPAQuery<Timesheet>(getEntityManager())
                .from(timesheet)
                .where(booleanExpression)
                .fetch();
    }

    default List<Timesheet> findAllTimesheetsByUserIdAndPeriod(Integer userId, LocalDate from, LocalDate to) {
        return new JPAQuery<Timesheet>(getEntityManager())
                .from(timesheet)
                .where(timesheet.date.between(from, to)
                        .and(timesheet.status.in(TimesheetStatus.POSTED, TimesheetStatus.APPROVED))
                        .and(timesheet.timesheetAccess.userId.eq(userId)))
                .fetch();
    }

    default List<Timesheet> findAllManagedTimesheetsByManagerIdAndUserId(
            Integer managerId,
            Integer userId,
            Integer year,
            Integer month
    ) {
        BooleanExpression booleanExpression = getBooleanExpressionByPeriod(year, month, year, month)
                .and(timesheet.timesheetAccess.userId.eq(userId))
                .and(timesheet.timesheetAccess.timesheetCategory.managers.any().id.eq(managerId))
                .and(timesheet.status.ne(TimesheetStatus.PLANNED));

        return new JPAQuery<Timesheet>(getEntityManager())
                .from(timesheet)
                .where(booleanExpression)
                .fetch();
    }

    default Timesheet findReportingTimesheet(Integer id, Integer userId){
        BooleanExpression booleanExpression = timesheet.id.eq(id)
                .and(timesheet.timesheetAccess.timesheetCategory.managers.any().id.eq(userId));

        return new JPAQuery<Timesheet>(getEntityManager())
                .from(timesheet)
                .where(booleanExpression)
                .fetchOne();
    }

    default Timesheet findTimesheetByIdAndTimesheetAccessUserId(Integer id, Integer userId, Boolean withCategory){
        JPAQuery<Timesheet> query = new JPAQuery<>(getEntityManager());

        BooleanExpression booleanExpression = timesheet.id.eq(id)
                .and(timesheet.timesheetAccess.userId.eq(userId));

        if (withCategory) {
            query.setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("Timesheet.withCategory"));
        }

        return query.from(timesheet)
                .where(booleanExpression)
                .fetchOne();
    }

    default BooleanExpression getBooleanExpressionByPeriod(
            Integer yearFrom,
            Integer monthFrom,
            Integer yearTo,
            Integer monthTo
    ) {
        Pair<LocalDate, LocalDate> datesPair = ConfigurationService.getDatesByDateCriteria(yearFrom, monthFrom, yearTo, monthTo);

        return timesheet.date.between(datesPair.getFirst(), datesPair.getSecond());
    }

    default BooleanExpression getBooleanExpressionByTimesheetCategoryIdAndUserIdAndStatus(
            Integer timsheetCategoryId,
            Integer userId,
            TimesheetStatus status
    ) {
        return timesheet.timesheetAccess.timesheetCategoryId.eq(timsheetCategoryId)
                .and(timesheet.timesheetAccess.userId.eq(userId))
                .and(timesheet.status.eq(status));
    }

    default List<Timesheet> findAllTimesheetsForBulkTimesheetAdjustment(
            Integer timsheetCategoryId,
            Integer userId,
            TimesheetStatus status,
            LocalDate fromDate,
            LocalDate toDate
    ) {
        BooleanExpression booleanExpression = getBooleanExpressionByTimesheetCategoryIdAndUserIdAndStatus(timsheetCategoryId, userId, status);

        if (fromDate != null) {
            booleanExpression = booleanExpression.and(timesheet.date.after(fromDate));
        }

        if (toDate != null) {
            booleanExpression = booleanExpression.and(timesheet.date.before(toDate));
        }

        return new JPAQuery<Timesheet>(getEntityManager())
                .from(timesheet)
                .where(booleanExpression)
                .orderBy(timesheet.date.asc())
                .fetch();
    }

    default void bulkTimesheetRemovalByUserIdAndTimesheetCategoryIdAndStatus(
            Integer userId,
            Integer timesheetCategoryId,
            TimesheetStatus status
    ) {
        JPAQuery<Integer> subQuery = new JPAQuery<>(getEntityManager())
                .from(timesheetAccess)
                .select(timesheetAccess.id)
                .where(timesheetAccess.timesheetCategoryId.eq(timesheetCategoryId)
                        .and(timesheetAccess.userId.eq(userId)));

        new JPADeleteClause(getEntityManager(), timesheet)
                .where(timesheet.status.eq(status)
                        .and(timesheet.timesheetAccessId.in(subQuery)))
                .execute();
    }

}
