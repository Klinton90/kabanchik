package kabanchik.system.timesheet.exception;

import kabanchik.core.exception.InvalidInputException;

public class TimesheetDuplicate extends InvalidInputException {

    private static final String MESSAGE_CODE = "app.validation.timesheetAccess.unique";

    public TimesheetDuplicate() {
        super(MESSAGE_CODE);
    }

    public TimesheetDuplicate(Object[] messageParams) {
        super(MESSAGE_CODE, messageParams);
    }

    public TimesheetDuplicate(Object[] messageParams, Object data) {
        super(MESSAGE_CODE, messageParams, data);
    }
}
