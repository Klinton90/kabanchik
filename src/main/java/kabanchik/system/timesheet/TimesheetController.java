package kabanchik.system.timesheet;

import kabanchik.core.controller.request.BulkSaveDTO;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.exception.CustomValidationCollectionException;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.system.timesheet.domain.Timesheet;
import kabanchik.system.timesheet.domain.TimesheetStatus;
import kabanchik.system.timesheet.dto.*;
import kabanchik.system.user.UserService;
import kabanchik.system.user.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Controller
@RequestMapping(value={"/timesheet"})
public class TimesheetController {

    @Autowired
    private TimesheetService service;

    @RequestMapping(value = "/findAllForPsr", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllForPsr(@RequestParam Integer timesheetCategoryId) {
        List<TimesheetSummaryDTO> result = service.findAllForPsr(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

    /**
     * For Reporting and User
     * @param year
     * @param month
     * @return
     */
    @RequestMapping(value = "/getTimesheetForLoggedUser", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getTimesheetForLoggedUser(
            @RequestParam Integer year,
            @RequestParam Integer month
    ) {
        User principal = UserService.getPrincipal();
        List<TimesheetDTO> result = service.findAllTimesheetsByUserIdAndYearMonth(principal.getId(), year, month);
        return ResponseEntity.ok(new RestResponse(result));
    }

    /**
     * For Summary
     * @param yearFrom
     * @param monthFrom
     * @param yearTo
     * @param monthTo
     * @param departmentId
     * @return
     */
    @RequestMapping(value = "/findAllManagedTimesheetsForLoggedUser", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllManagedTimesheetsForLoggedUser(
            @RequestParam Integer yearFrom,
            @RequestParam Integer monthFrom,
            @RequestParam Integer yearTo,
            @RequestParam Integer monthTo,
            @RequestParam(required = false) Integer departmentId
    ){
        List<TimesheetSummaryDTO> result = service.findAllManagedTimesheetsForLoggedUser(yearFrom, monthFrom, yearTo, monthTo, departmentId, false);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/findAllTimesheetsForPsrByLoggedUser", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllTimesheetsForPsrByLoggedUser(
            @RequestParam Integer yearFrom,
            @RequestParam Integer monthFrom,
            @RequestParam Integer yearTo,
            @RequestParam Integer monthTo,
            @RequestParam(required = false) Integer departmentId
    ){
        List<TimesheetSummaryDTO> result = service.findAllManagedTimesheetsForLoggedUser(yearFrom, monthFrom, yearTo, monthTo, departmentId, true);
        return ResponseEntity.ok(new RestResponse(result));
    }

    /**
     * For Reporting and Manager
     * @param userId
     * @param year
     * @param month
     * @return
     */
    @RequestMapping(value = "/findAllManagedTimesheetsByUserId", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllManagedTimesheetsByUserId(
            @RequestParam Integer userId,
            @RequestParam Integer year,
            @RequestParam Integer month
    ){
        List<TimesheetDTO> result = service.findAllManagedTimesheetsByUserId(userId, year, month);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/createDoubleTimesheet", method = RequestMethod.POST)
    public ResponseEntity<RestResponse> createDoubleTimesheet(@RequestBody TimesheetDTO json) throws InvalidInputException, CustomValidationException {
        List<TimesheetDTO> result = service.createDoubleTimesheet(json, false);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = {"/", "/create", "/create/"}, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<RestResponse> create(
            @RequestBody TimesheetDTO json
    ) throws CustomValidationException, InvalidInputException {
        TimesheetDTO result = service.create(json, false);
        return ResponseEntity.ok(new RestResponse(result).setId(result.getId()));
    }

    @RequestMapping(value = {"/{id}", "/{id}/", "/update/{id}", "/update/{id}/"}, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RestResponse> update(
            @RequestBody TimesheetDTO json
    ) throws CustomValidationException, InvalidInputException {
        TimesheetDTO result = service.update(json);
        return ResponseEntity.ok(new RestResponse(result).setId(result.getId()));
    }

    @RequestMapping(value = {"/submit", "/submit/"}, method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<RestResponse> submit(@RequestParam Integer timesheetId) throws CustomValidationException {
        TimesheetDTO result = service.submitTimesheet(timesheetId, TimesheetStatus.POSTED);
        return ResponseEntity.ok(new RestResponse(result).setId(timesheetId));
    }

    @RequestMapping(value = {"/approve", "/approve/"}, method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<RestResponse> approve(@RequestParam Integer timesheetId) throws CustomValidationException {
        TimesheetDTO result = service.approveTimesheet(timesheetId, TimesheetStatus.APPROVED);
        return ResponseEntity.ok(new RestResponse(result).setId(timesheetId));
    }

    @Deprecated
    @RequestMapping(value = {"/decline", "/decline/"}, method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<RestResponse> decline(@RequestBody TimesheetDeclineDTO json) throws CustomValidationException {
        TimesheetDTO result = service.decline(json);
        return ResponseEntity.ok(new RestResponse(result).setId(result.getId()));
    }

    @RequestMapping(value = {"/delete/{id}", "/delete/{id}/"}, method = RequestMethod.DELETE)
    public ResponseEntity<RestResponse> delete(@PathVariable Integer id) {
        service.delete(id);
        return ResponseEntity.ok(new RestResponse(null).setId(id));
    }

    @RequestMapping(value = "/bulkTimesheetInsert", method = RequestMethod.POST)
    public ResponseEntity<RestResponse> bulkTimesheetInsert(
            @RequestBody BulkTimesheetAddDTO dto
    ) throws InvalidInputException, CustomValidationException {
        List<Long> result = service.bulkTimesheetInsert(dto);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/bulkTimesheetAdjustment", method = RequestMethod.POST)
    public ResponseEntity<RestResponse> bulkTimesheetAdjustment(
            @RequestBody BulkTimesheetAdjustmentDTO dto
    ) throws CustomValidationException, InvalidInputException {
        service.bulkTimesheetAdjustment(dto);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/bulkTimesheetRemoval", method = RequestMethod.DELETE)
    public ResponseEntity<RestResponse> bulkTimesheetRemoval(
            @RequestParam Integer userId,
            @RequestParam Integer timesheetCategoryId
    ) {
        service.bulkTimesheetRemoval(userId, timesheetCategoryId);
        return ResponseEntity.ok(null);
    }

    @PostMapping(value = {"/saveAll", "/saveAll/"})
    public ResponseEntity<RestResponse> saveAll(@RequestBody BulkSaveDTO<TimesheetDTO, Timesheet, Integer> bulkSaveDTO) throws CustomValidationCollectionException {
        service.saveAll(bulkSaveDTO);
        return ResponseEntity.ok(new RestResponse(null));
    }

}
