package kabanchik.system.subcontractor;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.subcontractor.domain.Subcontractor;
import kabanchik.system.subcontractor.dto.SubcontractorDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/subcontractor")
public class SubcontractorController extends RestController<Subcontractor, SubcontractorDTO, Integer> {

    @Getter
    @Autowired
    private SubcontractorService service;

    @RequestMapping(value = "/getForPSR", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForPSR(
            @RequestParam Integer timesheetCategoryId
    ){
        List<SubcontractorDTO> result = service.getForPSR(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getForProgressModal", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForProgressModal(
            @RequestParam Integer timesheetCategoryId,
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ){
        List<SubcontractorDTO> result = service.getForProgressModal(timesheetCategoryId, queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
