package kabanchik.system.subcontractor.dto;

import kabanchik.system.outcomeentry.domain.OutcomeEntryType;
import kabanchik.system.outcomeentry.dto.OutcomeEntryParentDTO;
import kabanchik.system.progress.dto.ProgressParentDTO;
import kabanchik.system.progress.domain.ProgressType;
import kabanchik.system.subcontractor.domain.Subcontractor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class SubcontractorDTO implements OutcomeEntryParentDTO<Subcontractor>, ProgressParentDTO<Subcontractor> {

    private Integer id;

    @NotNull
    private Boolean isActive;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    @Size(min = 8, max = 10)
    private String governmentId;

    @NotNull
    private boolean progressReportable;

    @Override
    public OutcomeEntryType getOutcomeEntryType() {
        return OutcomeEntryType.SUB_CONTRACTOR;
    }

    @Override
    public ProgressType getProgressType() {
        return ProgressType.SUB_CONTRACTOR;
    }

}
