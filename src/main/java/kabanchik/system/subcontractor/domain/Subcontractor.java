package kabanchik.system.subcontractor.domain;

import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.validator.Unique;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import kabanchik.system.outcomeentry.domain.OutcomeEntryParent;
import kabanchik.system.progress.domain.Progress;
import kabanchik.system.progress.domain.ProgressParent;
import kabanchik.system.subcontractor.dto.SubcontractorDTO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Unique
@Entity
@Getter
@Setter
@Table(name = "subcontractor")
public class Subcontractor implements OutcomeEntryParent<SubcontractorDTO>, ProgressParent<SubcontractorDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subcontractor_id")
    private Integer id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @ContainsFilterField
    @Column(name = "name", nullable = false, length = 50, unique = true)
    private String name;

    @NotNull
    @ContainsFilterField
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @ContainsFilterField
    @Column(name = "government_id", nullable = false, length = 10, unique = true)
    private String governmentId;

    @Column(name = "progress_reportable", nullable = false)
    private boolean progressReportable;

    @OneToMany(mappedBy = "subcontractor")
    @Filter(name = "outcomeEntryParentSubcontractor")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private List<OutcomeEntry> outcomeEntries;

    @OneToMany(mappedBy = "subcontractor")
    @Filter(name = "progressParentSubcontractor")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private List<Progress> progressList;

    @Override
    public String toString(){
        String id = this.id != null ? this.id.toString() : "null";
        return id + ":" + name;
    }

}
