package kabanchik.system.subcontractor;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.outcomeentry.OutcomeEntryService;
import kabanchik.system.outcomeentry.domain.OutcomeEntryType;
import kabanchik.system.subcontractor.domain.Subcontractor;
import kabanchik.system.subcontractor.dto.SubcontractorDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubcontractorService extends RestService<Subcontractor, SubcontractorDTO, Integer> {

    @Getter
    @Autowired
    private SubcontractorRepository repository;

    @Getter
    @Autowired
    private SubcontractorService self;

    @Autowired
    private OutcomeEntryService outcomeEntryService;

    @Override
    public boolean isInUse(Integer id) {
        return outcomeEntryService.countByOutcomeEntryTypeAndParentId(OutcomeEntryType.SUB_CONTRACTOR, id) > 0;
    }

    public Subcontractor getOneByGovernmentId(String governmentId) throws CustomValidationException {
        Subcontractor entity = repository.findOneByGovernmentId(governmentId);
        if (entity == null) {
            String objectName = getDomainClass().getSimpleName();
            Object[] messageParams = {objectName, governmentId};
            throw new CustomValidationException("rowNotFound", objectName, messageParams);
        }
        return entity;
    }

    public List<SubcontractorDTO> getForPSR(Integer timesheetCategoryId) {
        return repository.getForPSR(timesheetCategoryId)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

    public List<SubcontractorDTO> getForProgressModal(
            Integer timesheetCategoryId,
            String queryString,
            Integer limit,
            Integer offset
    ) {
        return repository.getForProgressModal(timesheetCategoryId, queryString, limit, offset)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

}
