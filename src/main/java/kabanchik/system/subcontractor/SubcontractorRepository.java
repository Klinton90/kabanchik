package kabanchik.system.subcontractor;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.subcontractor.domain.Subcontractor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.outcomeentry.domain.QOutcomeEntry.outcomeEntry;
import static kabanchik.system.subcontractor.domain.QSubcontractor.subcontractor;
import static kabanchik.system.progress.domain.QProgress.progress;

@Repository
public interface SubcontractorRepository extends RestRepository<Subcontractor, Integer> {

    Subcontractor findOneByGovernmentId(String governmentId);

    default List<Subcontractor> getForPSR(Integer timesheetCategoryId) {
        return new JPAQuery<Subcontractor>(getEntityManager())
                .distinct()
                .from(subcontractor)
                .leftJoin(subcontractor.outcomeEntries, outcomeEntry)
                .on(outcomeEntry.timesheetCategoryId.eq(timesheetCategoryId))
                .leftJoin(subcontractor.progressList, progress)
                .on(progress.timesheetCategoryId.eq(timesheetCategoryId))
                .where(outcomeEntry.id.isNotNull()
                        .or(progress.id.isNotNull()))
                .fetch();
    }

    default List<Subcontractor> getForProgressModal(
            Integer timesheetCategoryId,
            String queryString,
            Integer limit,
            Integer offset
    ) {
        BooleanExpression booleanExpression = outcomeEntry.timesheetCategoryId.eq(timesheetCategoryId)
                .and(subcontractor.progressReportable.isTrue())
                .and(outcomeEntry.isForPlan.isTrue());

        if (StringUtils.isNotBlank(queryString)) {
            booleanExpression = booleanExpression.and(subcontractor.name.contains(queryString)
                    .or(subcontractor.description.contains(queryString)));
        }


        JPAQuery<Subcontractor> query = new JPAQuery<Subcontractor>(getEntityManager())
                .distinct()
                .from(subcontractor)
                .innerJoin(subcontractor.outcomeEntries, outcomeEntry)
                .where(booleanExpression);

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

}
