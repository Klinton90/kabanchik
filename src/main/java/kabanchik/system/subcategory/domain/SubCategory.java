package kabanchik.system.subcategory.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.validator.Unique;
import kabanchik.system.category.domain.Category;
import kabanchik.system.subcategory.dto.SubCategoryDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Unique
@Getter
@Setter
@Table(name="sub_categories")
@NamedEntityGraphs(value = {
        @NamedEntityGraph(name = "SubCategory.default", attributeNodes = {
                @NamedAttributeNode("parentCategory")
        })
})
public class SubCategory implements IDomain<SubCategoryDTO, Integer>, ActivatableDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sub_category_id")
    private Integer id;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Size(min = 5, max = 50)
    @Column(name = "name", nullable = false, length = 50, unique = true)
    @ContainsFilterField
    private String name;

    @NotNull
    @Size(min = 5, max = 255)
    @Column(name = "description", nullable = false, unique = true)
    @ContainsFilterField
    private String description;

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private Category parentCategory;

}
