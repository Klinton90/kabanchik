package kabanchik.system.subcategory;

import kabanchik.core.service.RestService;
import kabanchik.system.subcategory.domain.SubCategory;
import kabanchik.system.subcategory.dto.SubCategoryDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubCategoryService extends RestService<SubCategory, SubCategoryDTO, Integer>{

    @Getter
    @Autowired
    private SubCategoryRepository repository;

    @Getter
    @Autowired
    private SubCategoryService self;

    @Override
    public boolean isInUse(Integer id){
        return false;
    }
}
