package kabanchik.system.subcategory.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.subcategory.domain.SubCategory;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubCategoryDTO implements IDto<SubCategory, Integer>, ActivatableDomain {

    private Integer id;

    private Boolean isActive;

    @Override
    public SubCategoryDTO createFromEntity(SubCategory entity) {
        return null;
    }
}
