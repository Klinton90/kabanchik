package kabanchik.system.subcategory;

import kabanchik.core.controller.RestController;
import kabanchik.system.subcategory.domain.SubCategory;
import kabanchik.system.subcategory.dto.SubCategoryDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value={"/subCategory"})
public class SubCategoryController extends RestController<SubCategory, SubCategoryDTO, Integer>{

    @Getter
    @Autowired
    private SubCategoryService service;

}
