package kabanchik.system.subcategory;

import kabanchik.core.repository.RestRepository;
import kabanchik.system.subcategory.domain.SubCategory;
import org.springframework.stereotype.Repository;

@Repository
public interface SubCategoryRepository extends RestRepository<SubCategory, Integer>{
}
