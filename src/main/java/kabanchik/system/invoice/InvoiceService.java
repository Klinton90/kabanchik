package kabanchik.system.invoice;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.core.service.RestService;
import kabanchik.system.currency.CurrencyService;
import kabanchik.system.customer.CustomerService;
import kabanchik.system.invoice.domain.Invoice;
import kabanchik.system.invoice.dto.*;
import kabanchik.system.payment.PaymentService;
import kabanchik.system.payment.dto.PaymentDTO;
import kabanchik.system.timesheetcategory.TimesheetCategoryService;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InvoiceService extends RestService<Invoice, InvoiceDTO, Integer> {

    @Getter
    @Autowired
    private InvoiceRepository repository;

    @Getter
    @Autowired
    private InvoiceService self;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private TimesheetCategoryService timesheetCategoryService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private PaymentService paymentService;
    
    @Autowired
    private ExposedResourceMessageBundleSource messageSource;

    @Getter
    protected String[] nullableProperties = {"datePosted"};

    @Override
    public boolean isInUse(Integer invoiceId) {
        return paymentService.countByInvoiceId(invoiceId) > 0;
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Invoice create(@Valid Invoice json) throws CustomValidationException {
        Invoice result = super.create(json);

        PaymentDTO paymentDTO = new PaymentDTO(result)
                .setDescription(messageSource.getMessage("app.text.outcomeEntry.autoInvoiceDescription"))
                .setDate(result.getDatePlanned().plusDays(result.getTimesheetCategory().getContractDays()));
        paymentService.create(paymentDTO);

        return result;
    }

    @Override
    public void validate(Invoice entity) throws CustomValidationException {
        entity.setCurrency(currencyService.getOne(entity.getCurrencyId()));
        entity.setCustomer(customerService.getOne(entity.getCustomerId()));
        entity.setTimesheetCategory(timesheetCategoryService.getOne(entity.getTimesheetCategoryId()));
    }

    public List<ExtendedInvoiceDTO> getAllByTimesheetCategoryId(Integer timesheetCategoryId) {
        return repository.findAllByTimesheetCategoryId(timesheetCategoryId)
                .stream()
                .map(invoice -> {
                    ExtendedInvoiceDTO dto = (ExtendedInvoiceDTO) new ExtendedInvoiceDTO().createFromEntity(invoice);

                    List<PaymentDTO> paymentDTOS = invoice.getPayments()
                            .stream()
                            .map(payment -> paymentService.initDto(payment))
                            .collect(Collectors.toList());
                    dto.setPaymentDTOS(paymentDTOS);

                    return dto;
                })
                .collect(Collectors.toList());
    }
    
}
