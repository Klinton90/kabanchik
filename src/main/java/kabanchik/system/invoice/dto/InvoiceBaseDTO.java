package kabanchik.system.invoice.dto;

import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.invoice.domain.InvoiceStatus;
import kabanchik.system.payment.dto.PaymentDTO;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public abstract class InvoiceBaseDTO<E extends IDomain, ID extends Serializable> implements IDto<E, ID> {

    @NotNull
    private LocalDate datePlanned;

    private LocalDate datePosted;

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("999999999.99")
    @Digits(integer = 7, fraction = 2)
    private BigDecimal amount;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    private InvoiceStatus status;

//    private Boolean workComplete;
//
//    private Boolean workAccepted;
//
//    private Boolean invoiceIssued;
//
//    private Boolean acceptedByCustomer;
//
//    private Boolean actAccepted;
//
//    private Boolean endClientPaymentComplete;
//
//    private Boolean invoicePaid;

    @NotNull
    private String currencyId;

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    private Integer customerId;

    private List<PaymentDTO> paymentDTOS;

}
