package kabanchik.system.invoice.dto;

import kabanchik.core.util.BeanUtils;
import kabanchik.system.invoice.domain.Invoice;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceDTO extends InvoiceBaseDTO<Invoice, Integer> {

    private Integer id;

    private String customerName;

    private String currencyName;

    private String timesheetCategoryName;

    @Override
    public InvoiceDTO createFromEntity(Invoice entity) {
        BeanUtils.copyProperties(entity, this, true);

        setCurrencyName(entity.getCurrency().getName());
        setTimesheetCategoryName(entity.getTimesheetCategory().getName());
        setCustomerName(entity.getCustomer().getName());

        return this;
    }

}
