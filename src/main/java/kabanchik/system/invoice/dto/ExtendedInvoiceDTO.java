package kabanchik.system.invoice.dto;

import kabanchik.system.payment.dto.PaymentDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExtendedInvoiceDTO extends InvoiceDTO {

    private List<PaymentDTO> paymentDTOS;

}
