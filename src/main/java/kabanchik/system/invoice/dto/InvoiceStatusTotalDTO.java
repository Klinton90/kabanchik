package kabanchik.system.invoice.dto;

import kabanchik.system.invoice.domain.InvoiceStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
public class InvoiceStatusTotalDTO {

    private InvoiceStatus status;

    private BigDecimal amount;

}
