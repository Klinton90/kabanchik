package kabanchik.system.invoice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class DebtorResultDTO {

    private List<InvoiceStatusTotalDTO> statusTotals;

    private Page<DebtorModelDTO> page;

    private BigDecimal total;

    private BigDecimal totalUnpaid;

}
