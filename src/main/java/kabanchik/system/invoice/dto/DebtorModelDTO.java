package kabanchik.system.invoice.dto;

import kabanchik.system.invoice.domain.DebtorModel;
import kabanchik.system.invoice.domain.DebtorModelId;
import kabanchik.system.timesheetcategory.dto.TimesheetCategoryWithManagersDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class DebtorModelDTO extends InvoiceBaseDTO<DebtorModel, DebtorModelId> {

    private DebtorModelId id;

    private BigDecimal invoiceRate;

    private String invoiceCurrencyId;

    private String invoiceCurrencyName;

    private BigDecimal paymentAmount;

    private BigDecimal paymentRate;

    private LocalDate paymentDate;

    private TimesheetCategoryWithManagersDTO timesheetCategory;

    private String paymentCurrencyId;

    private String paymentCurrencyName;

}
