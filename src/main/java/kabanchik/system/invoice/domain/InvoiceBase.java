package kabanchik.system.invoice.domain;

import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@MappedSuperclass
public abstract class InvoiceBase<D extends IDto, ID extends Serializable> implements IDomain<D, ID> {

    @Column(name = "date_planned", nullable = false)
    private LocalDate datePlanned;

    @Column(name = "date_posted")
    private LocalDate datePosted;

    @Column(name = "amount", nullable = false, precision = 19, scale = 2)
    private BigDecimal amount;

    @Column(name = "description", nullable = false)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private InvoiceStatus status;

    @Column(name = "currency_id", nullable = false)
    private String currencyId;

    @Column(name = "timesheet_category_id", nullable = false)
    private Integer timesheetCategoryId;

    @Column(name = "customer_id", nullable = false)
    private Integer customerId;

    @ToString.Exclude
    @PrimaryKeyJoinColumn
    @JoinColumn(name = "timesheet_category_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private TimesheetCategory timesheetCategory;

}
