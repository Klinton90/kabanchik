package kabanchik.system.invoice.domain;

import kabanchik.core.domain.LocalizedEnum;

public enum InvoiceStatus implements LocalizedEnum {
    PENDING,
    DELIVERABLES_READY,
    READY_FOR_INVOICING,
    INVOICE_ISSUED,
    CLIENT_CONFIRMED,
    WAITING_PAYMENT_FROM_END_CLIENT,
    WAITING_FOR_PAYMENT,
    PAID
}
