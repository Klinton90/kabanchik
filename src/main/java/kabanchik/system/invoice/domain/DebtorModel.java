package kabanchik.system.invoice.domain;

import kabanchik.system.invoice.dto.DebtorModelDTO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Immutable
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "Invoice.debtorList",
        attributeNodes = {
            @NamedAttributeNode(value = "timesheetCategory")
        }
    ),
})
@Table(name = "invoice_rate")
public class DebtorModel extends InvoiceBase<DebtorModelDTO, DebtorModelId> {

    @EmbeddedId
    private DebtorModelId id;

    @Column(name = "invoice_rate")
    private BigDecimal invoiceRate;

    @Column(name = "invoice_currency_id")
    private String invoiceCurrencyId;

    @Column(name = "invoice_currency_name")
    private String invoiceCurrencyName;

    @Column(name = "payment_amount")
    private BigDecimal paymentAmount;

    @Column(name = "payment_rate")
    private BigDecimal paymentRate;

    @Column(name = "payment_date")
    private LocalDate paymentDate;

    @Column(name = "payment_currency_id")
    private String paymentCurrencyId;

    @Column(name = "payment_currency_name")
    private String paymentCurrencyName;

}
