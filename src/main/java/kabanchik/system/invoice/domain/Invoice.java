package kabanchik.system.invoice.domain;

import kabanchik.system.currency.domain.Currency;
import kabanchik.system.customer.domain.Customer;
import kabanchik.system.invoice.dto.InvoiceDTO;
import kabanchik.system.payment.domain.Payment;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "invoice")
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "Invoice.default",
        attributeNodes = {
            @NamedAttributeNode(value = "currency"),
            @NamedAttributeNode(value = "timesheetCategory"),
            @NamedAttributeNode(value = "customer")
        }
    ),
    @NamedEntityGraph(
        name = "Invoice.payments",
        attributeNodes = {
            @NamedAttributeNode(value = "payments")
        }
    ),
    @NamedEntityGraph(
        name = "Invoice.empty"
    )
})
public class Invoice extends InvoiceBase<InvoiceDTO, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "invoice_id")
    private Integer id;

    @ToString.Exclude
    @JoinColumn(name = "customer_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;

    @OneToMany(mappedBy = "invoice")
    private List<Payment> payments;

    @ToString.Exclude
    @PrimaryKeyJoinColumn
    @JoinColumn(name = "currency_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Currency currency;

}
