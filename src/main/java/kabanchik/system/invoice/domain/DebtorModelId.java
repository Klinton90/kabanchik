package kabanchik.system.invoice.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class DebtorModelId implements Serializable {

    @Column(name = "payment_id")
    private Integer paymentId;

    @Column(name = "invoice_id")
    private Integer invoiceId;

}
