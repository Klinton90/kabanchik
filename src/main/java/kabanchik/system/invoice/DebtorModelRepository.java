package kabanchik.system.invoice;

import kabanchik.core.repository.RestRepository;
import kabanchik.system.invoice.domain.DebtorModel;
import kabanchik.system.invoice.domain.DebtorModelId;
import org.springframework.stereotype.Repository;

@Repository
public interface DebtorModelRepository extends RestRepository<DebtorModel, DebtorModelId> {
}
