package kabanchik.system.invoice;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.system.invoice.domain.Invoice;
import kabanchik.system.invoice.dto.DebtorResultDTO;
import kabanchik.system.invoice.dto.ExtendedInvoiceDTO;
import kabanchik.system.invoice.dto.InvoiceDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value = "/invoice")
public class InvoiceController extends RestController<Invoice, InvoiceDTO, Integer> {

    @Getter
    @Autowired
    private InvoiceService service;

    @Autowired
    private DebtorModelService debtorModelService;

    @RequestMapping(value = "/getAllByTimesheetCategoryId", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getAllByTimesheetCategoryId(
            @RequestParam Integer timesheetCategoryId
    ) {
        List<ExtendedInvoiceDTO> result = service.getAllByTimesheetCategoryId(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/findAllForDebtorList", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllForDebtorList(
            Pageable pageable,
            @RequestParam(required = false, defaultValue = "false") boolean showOnlyActiveTimesheetCategories,
            HttpServletRequest request
    ) throws InvalidInputException {
        DebtorResultDTO result = debtorModelService.findAllForDebtorList(pageable, showOnlyActiveTimesheetCategories, new HashMap<>(request.getParameterMap()));
        return ResponseEntity.ok(new RestResponse(result));
    }

//    @Deprecated
//    @RequestMapping(value = {"/updateStatus", "/updateStatus/"}, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public @ResponseBody ResponseEntity<RestResponse> updateStatus(InvoiceDTO dto) {
//        DebtorInvoiceDTO result = getService().updateStatus(dto);
//        return ResponseEntity.ok(new RestResponse(result).setId(result.getId()));
//    }

}
