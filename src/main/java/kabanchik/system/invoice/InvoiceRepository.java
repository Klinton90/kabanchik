package kabanchik.system.invoice;

import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.invoice.domain.Invoice;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.invoice.domain.QInvoice.invoice;

@Repository
public interface InvoiceRepository extends RestRepository<Invoice, Integer> {

    default List<Invoice> findAllByTimesheetCategoryId(Integer timesheetCategoryId) {
        return new JPAQuery<Invoice>(getEntityManager())
                .distinct()
                .from(invoice)
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("Invoice.payments"))
                .where(invoice.timesheetCategoryId.eq(timesheetCategoryId))
                .fetch();
    }

}
