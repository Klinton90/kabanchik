package kabanchik.system.invoice;

import com.google.common.collect.Lists;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.service.RestService;
import kabanchik.system.invoice.domain.DebtorModel;
import kabanchik.system.invoice.domain.DebtorModelId;
import kabanchik.system.invoice.domain.InvoiceStatus;
import kabanchik.system.invoice.dto.DebtorResultDTO;
import kabanchik.system.invoice.dto.DebtorModelDTO;
import kabanchik.system.invoice.dto.InvoiceStatusTotalDTO;
import kabanchik.system.timesheetcategory.dto.TimesheetCategoryWithManagersDTO;
import kabanchik.system.user.UserService;
import kabanchik.system.user.domain.UserRole;
import kabanchik.system.user.dto.UserSimpleDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

//import static kabanchik.system.currency.domain.QCurrency.currency;
import static kabanchik.system.invoice.domain.QDebtorModel.debtorModel;
import static kabanchik.system.timesheetcategory.domain.QTimesheetCategory.timesheetCategory;

@Service
public class DebtorModelService extends RestService<DebtorModel, DebtorModelDTO, DebtorModelId> {

    private static final String SHOW_ONLY_ACTIVE_TIMESHEET_CATEGORIES_FILTER = "showOnlyActiveTimesheetCategories";
    private static final String BASE_CURRENCY_INVOICE_AMOUNT_FIELD = "baseCurrencyInvoiceAmount";
    private static final String BASE_CURRENCY_PAYMENT_AMOUNT_FIELD = "baseCurrencyPaymentAmount";

    @Getter
    @Autowired
    private DebtorModelRepository repository;

    @Getter
    @Autowired
    private DebtorModelService self;

    @Override
    public boolean isInUse(DebtorModelId id) {
        return false;
    }

    public DebtorResultDTO findAllForDebtorList(
            Pageable pageable,
            Boolean showOnlyActiveTimesheetCategories,
            Map<String, String[]> requestParams
    ) throws InvalidInputException {
        BooleanBuilder booleanBuilder = _getParsedGenericFilters(new BooleanBuilder(), requestParams, Lists.newArrayList(SHOW_ONLY_ACTIVE_TIMESHEET_CATEGORIES_FILTER));

        if (UserService.getPrincipal().getRole() != UserRole.ADMIN) {
            booleanBuilder = booleanBuilder.and(timesheetCategory.managers.any().id.eq(UserService.getPrincipal().getId()));
        }

        if (showOnlyActiveTimesheetCategories) {
            booleanBuilder = booleanBuilder.and(timesheetCategory.isActive.isTrue());
        }

        JPAQuery<DebtorModel> jpaQuery = new JPAQuery<DebtorModel>(getRepository().getEntityManager())
                .from(debtorModel)
                .join(debtorModel.timesheetCategory, timesheetCategory)
//                .join(debtorModel.currency, currency)
                .where(booleanBuilder);

        List<InvoiceStatusTotalDTO> statusTotals = jpaQuery.clone(getRepository().getEntityManager())
                .select(Projections.constructor(
                        InvoiceStatusTotalDTO.class,
                        debtorModel.status,
                        debtorModel.amount.multiply(debtorModel.invoiceRate).sum()))
                .groupBy(debtorModel.status)
                .fetch();

        BigDecimal total = jpaQuery.clone(getRepository().getEntityManager())
                .select(debtorModel.amount.multiply(debtorModel.invoiceRate).sum())
                .fetchOne();

        BigDecimal totalUnpaid = jpaQuery.clone(getRepository().getEntityManager())
                .select(debtorModel.amount.multiply(debtorModel.invoiceRate).sum())
                .where(debtorModel.status.ne(InvoiceStatus.PAID))
                .fetchOne();

        if (pageable.getSort() != null) {
            Sort.Order baseCurrencyInvoiceAmountOrder = pageable.getSort().getOrderFor(BASE_CURRENCY_INVOICE_AMOUNT_FIELD);
            Sort.Order baseCurrencyPaymentAmountOrder = pageable.getSort().getOrderFor(BASE_CURRENCY_PAYMENT_AMOUNT_FIELD);

            if (baseCurrencyInvoiceAmountOrder != null) {
                if (baseCurrencyInvoiceAmountOrder.isAscending()) {
                    jpaQuery.orderBy(debtorModel.amount.multiply(debtorModel.invoiceRate).asc());
                } else {
                    jpaQuery.orderBy(debtorModel.amount.multiply(debtorModel.invoiceRate).desc());
                }
            }

            if (baseCurrencyPaymentAmountOrder != null) {
                if (baseCurrencyPaymentAmountOrder.isAscending()) {
                    jpaQuery.orderBy(debtorModel.paymentAmount.multiply(debtorModel.paymentRate).asc());
                } else {
                    jpaQuery.orderBy(debtorModel.paymentAmount.multiply(debtorModel.paymentRate).desc());
                }
            }

            List<Sort.Order> filteredSort = StreamSupport.stream(pageable.getSort().spliterator(), false)
                    .filter(order -> !order.getProperty().equalsIgnoreCase(BASE_CURRENCY_INVOICE_AMOUNT_FIELD)
                            && !order.getProperty().equalsIgnoreCase(BASE_CURRENCY_PAYMENT_AMOUNT_FIELD))
                    .collect(Collectors.toList());

            if (filteredSort.size() > 0) {
                pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort(filteredSort));
            } else {
                pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize());
            }
        }

        Page<DebtorModelDTO> pageResult = convertToPage(
                jpaQuery.distinct(),
                pageable,
                getRepository().getEntityManager().getEntityGraph("Invoice.debtorList"))
                .map(this::initDebtorInvoiceDto);

        return new DebtorResultDTO(statusTotals, pageResult, total, totalUnpaid);
    }

    public DebtorModelDTO initDebtorInvoiceDto(DebtorModel invoice) {
        DebtorModelDTO result = (DebtorModelDTO)new DebtorModelDTO().createFromEntity(invoice);

        TimesheetCategoryWithManagersDTO timesheetCategoryWithManagersDTO =
                (TimesheetCategoryWithManagersDTO) new TimesheetCategoryWithManagersDTO().createFromEntity(invoice.getTimesheetCategory());
        timesheetCategoryWithManagersDTO.setManagers(invoice.getTimesheetCategory().getManagers()
                .stream()
                .map(manager -> new UserSimpleDTO().createFromEntity(manager))
                .collect(Collectors.toList()));

        result.setTimesheetCategory(timesheetCategoryWithManagersDTO);

        return result;
    }

}
