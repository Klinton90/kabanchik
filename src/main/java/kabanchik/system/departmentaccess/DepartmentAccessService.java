package kabanchik.system.departmentaccess;

import com.querydsl.core.BooleanBuilder;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.SpringValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.department.DepartmentService;
import kabanchik.system.department.domain.Department;
import kabanchik.system.departmentaccess.domain.DepartmentAccess;
import kabanchik.system.departmentaccess.dto.BulkDepartmentAccessDepartmentInsertDTO;
import kabanchik.system.departmentaccess.dto.BulkDepartmentAccessTimesheetCategoryInsertDTO;
import kabanchik.system.departmentaccess.dto.DepartmentAccessDTO;
import kabanchik.system.timesheetcategory.TimesheetCategoryService;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import kabanchik.system.timesheetaccess.TimesheetAccessService;
import kabanchik.system.user.UserService;
import kabanchik.system.user.domain.UserRole;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentAccessService extends RestService<DepartmentAccess, DepartmentAccessDTO, Integer> {

    @Getter
    @Autowired
    private DepartmentAccessRepository repository;

    @Getter
    @Autowired
    private DepartmentAccessService self;

    @Autowired
    private TimesheetAccessService timesheetAccessService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private TimesheetCategoryService timesheetCategoryService;

    @Override
    public boolean isInUse(Integer integer) {
        return false;
    }

    @Override
    protected BooleanBuilder getDefaultBuilder(){
        BooleanBuilder booleanBuilder = super.getDefaultBuilder();
        if (!UserService.getPrincipal().getRole().equals(UserRole.ADMIN)) {
            booleanBuilder = booleanBuilder.and(repository.getBooleanExpressionByManagerId(UserService.getPrincipal().getId()));
        }
        return booleanBuilder;
    }

    public List<Integer> bulkDepartmentInsert(BulkDepartmentAccessDepartmentInsertDTO dto) throws CustomValidationException {
        List<Integer> result = new ArrayList<>();
        // validate
        TimesheetCategory category = timesheetAccessService.getManagedTimesheetCategory(dto.getTimesheetCategoryId());

        for (Integer departmentId: dto.getDepartmentIds()) {
            // validate
            Department department = departmentService.getOne(departmentId);
            DepartmentAccess departmentAccess = new DepartmentAccess()
                    .setId(0)
                    .setIsActive(true)
                    .setDepartmentId(departmentId)
                    .setTimesheetCategoryId(dto.getTimesheetCategoryId());

            try {
                getSelf()._save(departmentAccess);
            } catch (SpringValidationException ex){
                result.add(departmentId);
            }
        }

        return result;
    }


    public List<Integer> bulkCategoriesInsert(BulkDepartmentAccessTimesheetCategoryInsertDTO dto) throws CustomValidationException {
        List<Integer> result = new ArrayList<>();
        // validate
        Department department = departmentService.getOne(dto.getDepartmentId());

        List<TimesheetCategory> timesheetCategories = timesheetCategoryService.guessManagedTimesheetCategories(dto.getTimesheetCategoryIds());

        for (Integer categoryId: dto.getTimesheetCategoryIds()) {
            // validate
            TimesheetCategory timesheetCategory = timesheetCategories.stream()
                    .filter(_timesheetCategory -> _timesheetCategory.getId().equals(categoryId))
                    .findAny()
                    .orElseThrow(() -> new CustomValidationException(
                            "managerAccessDenied",
                            TimesheetCategory.class.getSimpleName(),
                            "timesheetCategoryId",
                            categoryId.toString(),
                            dto,
                            TimesheetCategory.class.getSimpleName(),
                            categoryId.toString()));

            DepartmentAccess departmentAccess = new DepartmentAccess()
                    .setId(0)
                    .setIsActive(true)
                    .setDepartmentId(dto.getDepartmentId())
                    .setTimesheetCategoryId(categoryId);

            try {
                getSelf()._save(departmentAccess);
            } catch (SpringValidationException ex){
                result.add(categoryId);
            }
        }

        return result;
    }

    public long countByDepartmentId(Integer departmentId) {
        return repository.countByDepartmentId(departmentId);
    }

    public DepartmentAccess findOneByDepartmentIdAndTimesheetCategoryId(Integer departmentId, Integer timesheetCategoryId) {
        return repository.findOneByDepartmentIdAndTimesheetCategoryId(departmentId, timesheetCategoryId);
    }

    public List<DepartmentAccessDTO> findByTimesheetCategoryId(Integer timesheetCategoryId) {
        return repository.findByTimesheetCategoryIdAndIsActive(timesheetCategoryId, true)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

    @Override
    public DepartmentAccess initEntity(DepartmentAccessDTO dto) throws CustomValidationException {
        DepartmentAccess departmentAccess = super.initEntity(dto);
        TimesheetCategory timesheetCategory = timesheetCategoryService.getOne(dto.getTimesheetCategoryId());
        departmentAccess.setTimesheetCategory(timesheetCategory);
        return departmentAccess;
    }
}
