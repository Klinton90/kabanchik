package kabanchik.system.departmentaccess.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
public class BulkDepartmentAccessTimesheetCategoryInsertDTO {

    @NotNull
    private Integer departmentId;

    @NotNull
    @Size(min = 1)
    private List<Integer> timesheetCategoryIds;

}
