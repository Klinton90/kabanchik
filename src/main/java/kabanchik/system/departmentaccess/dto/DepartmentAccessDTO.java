package kabanchik.system.departmentaccess.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.departmentaccess.domain.DepartmentAccess;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DepartmentAccessDTO implements IDto<DepartmentAccess, Integer>, ActivatableDomain {

    private Integer id;

    @NotNull
    private Boolean isActive;

    @NotNull
    private Integer departmentId;

    @NotNull
    private Integer timesheetCategoryId;

    private String timesheetCategoryName;

    private String departmentName;

    @Override
    public DepartmentAccessDTO createFromEntity(DepartmentAccess entity) {
        BeanUtils.copyProperties(entity, this, true);
        if (entity.getTimesheetCategory() != null) {
            setTimesheetCategoryName(entity.getTimesheetCategory().getName());
        }
        if (entity.getDepartment() != null) {
            setDepartmentName(entity.getDepartment().getName());
        }
        return this;
    }

}
