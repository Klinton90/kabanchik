package kabanchik.system.departmentaccess;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.system.departmentaccess.domain.DepartmentAccess;
import kabanchik.system.departmentaccess.dto.BulkDepartmentAccessDepartmentInsertDTO;
import kabanchik.system.departmentaccess.dto.BulkDepartmentAccessTimesheetCategoryInsertDTO;
import kabanchik.system.departmentaccess.dto.DepartmentAccessDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/departmentAccess")
public class DepartmentAccessController extends RestController<DepartmentAccess, DepartmentAccessDTO, Integer> {

    @Getter
    @Autowired
    private DepartmentAccessService service;

    @RequestMapping(value = "/bulkDepartmentInsert", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> bulkUsersInsert(
            @RequestBody BulkDepartmentAccessDepartmentInsertDTO dto
    ) throws CustomValidationException {
        List<Integer> result = service.bulkDepartmentInsert(dto);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/bulkCategoriesInsert", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> bulkCategoriesInsert(
            @RequestBody BulkDepartmentAccessTimesheetCategoryInsertDTO dto
    ) throws CustomValidationException {
        List<Integer> result = service.bulkCategoriesInsert(dto);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/findByTimesheetCategoryId", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findByTimesheetCategoryId(@RequestParam Integer timesheetCategoryId) {
        List<DepartmentAccessDTO> result = service.findByTimesheetCategoryId(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
