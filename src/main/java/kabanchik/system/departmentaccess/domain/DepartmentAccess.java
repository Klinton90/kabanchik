package kabanchik.system.departmentaccess.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.core.validator.Unique;
import kabanchik.core.validator.UniqueColumn;
import kabanchik.system.department.domain.Department;
import kabanchik.system.departmentaccess.dto.DepartmentAccessDTO;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name="department_access")
@Unique(columns = @UniqueColumn(fields = {"departmentId", "timesheetCategoryId"}))
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "DepartmentAccess.default",
        attributeNodes = {
            @NamedAttributeNode(value = "timesheetCategory"),
            @NamedAttributeNode(value = "department")
        }
    ),
})
public class DepartmentAccess implements IDomain<DepartmentAccessDTO, Integer>, ActivatableDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "department_access_id")
    private Integer id;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @NotNull
    @Column(name = "department_id", nullable = false)
    private Integer departmentId;

    @NotNull
    @Column(name = "timesheet_category_id", nullable = false)
    private Integer timesheetCategoryId;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "department_id", insertable = false, updatable = false)
    private Department department;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "timesheet_category_id", insertable = false, updatable = false)
    private TimesheetCategory timesheetCategory;

}
