package kabanchik.system.departmentaccess;

import com.querydsl.core.types.dsl.BooleanExpression;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.departmentaccess.domain.DepartmentAccess;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.departmentaccess.domain.QDepartmentAccess.departmentAccess;

@Repository
public interface DepartmentAccessRepository extends RestRepository<DepartmentAccess, Integer> {

    default BooleanExpression getBooleanExpressionByManagerId(Integer userId) {
        return departmentAccess.timesheetCategory.managers.any().id.eq(userId);
    }

    long countByDepartmentId(Integer departmentId);

    DepartmentAccess findOneByDepartmentIdAndTimesheetCategoryId(Integer departmentId, Integer timesheetCategoryId);

    List<DepartmentAccess> findByTimesheetCategoryIdAndIsActive(Integer timesheetCategoryId, Boolean isActive);

}
