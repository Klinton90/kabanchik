package kabanchik.system.genericfieldoption;

import kabanchik.core.repository.RestRepository;
import kabanchik.system.genericfieldoption.domain.GenericFieldOption;
import org.springframework.stereotype.Repository;

@Repository
public interface GenericFieldOptionRepository extends RestRepository<GenericFieldOption, Integer> {
    long countByGenericFieldId(Integer genericFieldId);
}
