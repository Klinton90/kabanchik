package kabanchik.system.genericfieldoption;

import kabanchik.core.controller.RestController;
import kabanchik.system.genericfieldoption.dto.GenericFieldOptionDTO;
import kabanchik.system.genericfieldoption.domain.GenericFieldOption;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Getter
@Controller
@RequestMapping(value={"/genericFieldOption"})
public class GenericFieldOptionController extends RestController<GenericFieldOption, GenericFieldOptionDTO, Integer> {

    @Autowired
    private GenericFieldOptionService service;

}
