package kabanchik.system.genericfieldoption;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.genericfield.GenericFieldService;
import kabanchik.system.genericfieldoption.dto.GenericFieldOptionDTO;
import kabanchik.system.genericfieldoption.domain.GenericFieldOption;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Getter
public class GenericFieldOptionService extends RestService<GenericFieldOption, GenericFieldOptionDTO, Integer> {

    @Autowired
    private GenericFieldOptionRepository repository;

    @Autowired
    private GenericFieldOptionService self;

    @Autowired
    private GenericFieldService genericFieldService;

    @Override
    public boolean isInUse(Integer integer) {
        return false;
    }

    @Override
    public void validate(GenericFieldOption json) throws CustomValidationException {
        genericFieldService.getOne(json.getGenericFieldId());
    }

    public long countByGenericFieldId(Integer genericFieldId) {
        return repository.countByGenericFieldId(genericFieldId);
    }
}
