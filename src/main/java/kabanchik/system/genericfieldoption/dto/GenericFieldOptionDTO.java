package kabanchik.system.genericfieldoption.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.genericfieldoption.domain.GenericFieldOption;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class GenericFieldOptionDTO implements IDto<GenericFieldOption, Integer>, ActivatableDomain {

    private Integer id;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 255)
    private String value;

    @Size(max = 255)
    private String description;

    @NotNull
    private Integer genericFieldId;

    private Boolean isActive;

    public GenericFieldOptionDTO(GenericFieldOption genericFieldOption) {
        BeanUtils.copyProperties(genericFieldOption, this, false);
    }

}
