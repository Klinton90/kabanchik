package kabanchik.system.genericfieldoption.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.genericfield.domain.GenericField;
import kabanchik.system.genericfieldoption.dto.GenericFieldOptionDTO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "generic_field_option")
public class GenericFieldOption implements IDomain<GenericFieldOptionDTO, Integer>, ActivatableDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "generic_field_option_id")
    private Integer id;

    @Column(name = "value", nullable = false)
    private String value;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Column(name = "generic_field_id", nullable = false)
    private Integer genericFieldId;

    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "generic_field_id", insertable = false, updatable = false)
    private GenericField genericField;

    @Override
    public GenericFieldOption createFromDto(GenericFieldOptionDTO dto){
        BeanUtils.copyProperties(dto, this, true);
        if (StringUtils.isBlank(description)) {
            description = value;
        }
        return this;
    }

}
