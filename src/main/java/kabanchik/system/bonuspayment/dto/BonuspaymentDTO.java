package kabanchik.system.bonuspayment.dto;

import kabanchik.core.domain.IDto;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.bonuspayment.domain.Bonuspayment;
import kabanchik.system.user.domain.User;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class BonuspaymentDTO implements IDto<Bonuspayment, Integer> {

    private Integer id;

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("999999.99")
    @Digits(integer = 6, fraction = 2)
    private BigDecimal amount;

    @Size(max = 65025)
    private String comment;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    private LocalDate date;

    private String userName;

    private String timesheetCategoryName;

    @Override
    public BonuspaymentDTO createFromEntity(Bonuspayment bonuspayment){
        BeanUtils.copyProperties(bonuspayment, this, true);
        User profile = bonuspayment.getTimesheetAccess().getUser();

        this.setTimesheetCategoryName(bonuspayment.getTimesheetAccess().getTimesheetCategory().getName())
                .setTimesheetCategoryId(bonuspayment.getTimesheetAccess().getTimesheetCategory().getId())
                .setUserName(profile.getFirstName() + " " + profile.getLastName())
                .setUserId(profile.getId());

        return this;
    }

}
