package kabanchik.system.bonuspayment;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.bonuspayment.domain.Bonuspayment;
import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import static kabanchik.system.bonuspayment.domain.QBonuspayment.bonuspayment;

@Repository
public interface BonuspaymentRepository extends RestRepository<Bonuspayment, Integer> {

//    default List<Bonuspayment> findAllBonuspaymentsByUserIdAndYearMonth(Integer userId, Integer year, Integer month){
//        BooleanExpression booleanExpression = getBooleanExpressionByPeriod(year, month, year, month)
//                .and(bonuspayment.timesheetAccess.user.id.eq(userId));
//
//        return new JPAQuery<Bonuspayment>(getEntityManager())
//                .from(bonuspayment)
//                .where(booleanExpression)
//                .fetch();
//    }
//
//    default List<Bonuspayment> findAllManagedBonuspaymentsByUserId(Integer managerId, Integer userId, Integer year, Integer month) {
//        BooleanExpression booleanExpression = getBooleanExpressionByPeriod(year, month, year, month)
//                .and(bonuspayment.timesheetAccess.user.id.eq(userId))
//                .and(bonuspayment.timesheetAccess.timesheetCategory.managers.any().id.eq(managerId));
//
//        return new JPAQuery<Bonuspayment>(getEntityManager())
//                .from(bonuspayment)
//                .where(booleanExpression)
//                .fetch();
//    }

    default List<Bonuspayment> findAllManagedBonuspaymentsByUser(User user, Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo, Integer departmentId){
        BooleanExpression booleanExpression = getBooleanExpressionByPeriod(yearFrom, monthFrom, yearTo, monthTo);

        if (departmentId != null && departmentId > 0) {
            booleanExpression = booleanExpression.and(bonuspayment.timesheetAccess.user.departmentId.eq(departmentId));
        }

        if (!user.getRole().equals(UserRole.ADMIN)) {
            booleanExpression = booleanExpression.and(bonuspayment.timesheetAccess.timesheetCategory.managers.any().id.eq(user.getId()));
        }

        return new JPAQuery<Bonuspayment>(getEntityManager())
                .from(bonuspayment)
                .where(booleanExpression)
                .fetch();
    }

    default BooleanExpression getBooleanExpressionByPeriod(Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo) {
        Pair<LocalDate, LocalDate> datesPair = ConfigurationService.getDatesByDateCriteria(yearFrom, monthFrom, yearTo, monthTo);

        return bonuspayment.date.between(datesPair.getFirst(), datesPair.getSecond());
    }

}
