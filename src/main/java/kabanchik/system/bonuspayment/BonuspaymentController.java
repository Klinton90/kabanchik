package kabanchik.system.bonuspayment;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.bonuspayment.domain.Bonuspayment;
import kabanchik.system.bonuspayment.dto.BonuspaymentDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/bonuspayment")
public class BonuspaymentController extends RestController<Bonuspayment, BonuspaymentDTO, Integer> {

    @Getter
    @Autowired
    private BonuspaymentService service;

//    @RequestMapping(value = "/getBonuspaymentsForLoggedUser", method = RequestMethod.GET)
//    public @ResponseBody ResponseEntity<RestResponse> getBonuspaymentsForLoggedUser(
//            @RequestParam Integer year,
//            @RequestParam Integer month
//    ) {
//        User principal = UserService.getPrincipal();
//        List<BonuspaymentDTO> result = service.findAllBonuspaymentsByUserIdAndYearMonth(principal.getId(), year, month);
//        return ResponseEntity.ok(new RestResponse(result));
//    }
//
//    @RequestMapping(value = "/findAllManagedBonuspaymentsByUserId", method = RequestMethod.GET)
//    public @ResponseBody ResponseEntity<RestResponse> findAllManagedBonuspaymentsByUserId(
//            @RequestParam Integer userId,
//            @RequestParam Integer year,
//            @RequestParam Integer month
//    ){
//        List<BonuspaymentDTO> result = service.findAllManagedBonuspaymentsByUserId(userId, year, month);
//        return ResponseEntity.ok(new RestResponse(result));
//    }

    @RequestMapping(value = "/findAllManagedBonuspaymentsForLoggedUser", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllManagedBonuspaymentsForLoggedUser(
            @RequestParam Integer yearFrom,
            @RequestParam Integer monthFrom,
            @RequestParam Integer yearTo,
            @RequestParam Integer monthTo,
            @RequestParam(required = false) Integer departmentId
    ){
        List<BonuspaymentDTO> result = service.findAllManagedBonuspaymentsForLoggedUser(yearFrom, monthFrom, yearTo, monthTo, departmentId);
        return ResponseEntity.ok(new RestResponse(result));
    }

}
