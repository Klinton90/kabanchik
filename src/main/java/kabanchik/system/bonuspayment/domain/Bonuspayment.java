package kabanchik.system.bonuspayment.domain;

import com.querydsl.core.annotations.QueryInit;
import kabanchik.core.domain.IDomain;
import kabanchik.system.bonuspayment.dto.BonuspaymentDTO;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "bonuspayment")
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "Bonuspayment.default",
        attributeNodes = {
            @NamedAttributeNode(value = "timesheetAccess", subgraph = "Bonuspayment.timesheetAccess")
        },
        subgraphs = {
            @NamedSubgraph(
                name = "Bonuspayment.timesheetAccess",
                attributeNodes = {
                    @NamedAttributeNode("timesheetCategory"),
                    @NamedAttributeNode("user")
                }
            )
        }
    ),
    @NamedEntityGraph(
        name = "Bonuspayment.withCategory",
        attributeNodes = {
            @NamedAttributeNode(value = "timesheetAccess", subgraph = "Bonuspayment.timesheetAccess.timesheetCategory")
        },
        subgraphs = {
            @NamedSubgraph(
                name = "Bonuspayment.timesheetAccess.timesheetCategory",
                attributeNodes = {
                    @NamedAttributeNode("timesheetCategory")
                }
            )
        }
    ),
    @NamedEntityGraph(
        name = "Bonuspayment.withUser",
        attributeNodes = {
            @NamedAttributeNode(value = "timesheetAccess", subgraph = "Bonuspayment.timesheetAccess.user")
        },
        subgraphs = {
            @NamedSubgraph(
                name = "Bonuspayment.timesheetAccess.user",
                attributeNodes = {
                    @NamedAttributeNode("user")
                }
            )
        }
    )
})
public class Bonuspayment implements IDomain<BonuspaymentDTO, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bonuspayment_id")
    private Integer id;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "comment", nullable = false)
    private String comment;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @Column(name = "timesheet_access_id", insertable = false, updatable = false, nullable = false)
    private Integer timesheetAccessId;

    @NotNull
    @PrimaryKeyJoinColumn
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "timesheet_access_id", nullable = false)
    @QueryInit(value = {"user.*", "timesheetCategory.*"})
    private TimesheetAccess timesheetAccess;

}
