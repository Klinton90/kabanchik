package kabanchik.system.bonuspayment;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.bonuspayment.domain.Bonuspayment;
import kabanchik.system.bonuspayment.dto.BonuspaymentDTO;
import kabanchik.system.timesheetaccess.TimesheetAccessService;
import kabanchik.system.timesheetaccess.domain.TimesheetAccess;
import kabanchik.system.user.UserService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BonuspaymentService extends RestService<Bonuspayment, BonuspaymentDTO, Integer> {

    @Getter
    @Autowired
    private BonuspaymentRepository repository;

    @Getter
    @Autowired
    private BonuspaymentService self;

    @Autowired
    private TimesheetAccessService timesheetAccessService;

    @Override
    public boolean isInUse(Integer id) {
        return false;
    }

    @Override
    public Bonuspayment initEntity(BonuspaymentDTO dto) throws CustomValidationException {
        Bonuspayment bonuspayment = super.initEntity(dto);

        TimesheetAccess timesheetAccess = timesheetAccessService.findByUserIdAndTimesheetCategoryId(dto.getUserId(), dto.getTimesheetCategoryId());
        bonuspayment.setTimesheetAccess(timesheetAccess)
                .setTimesheetAccessId(timesheetAccess.getTimesheetCategoryId());

        return bonuspayment;
    }

//    public List<BonuspaymentDTO> findAllBonuspaymentsByUserIdAndYearMonth(Integer userId, Integer year, Integer month) {
//        return repository.findAllBonuspaymentsByUserIdAndYearMonth(userId, year, month)
//                .stream()
//                .map(this::initDto)
//                .collect(Collectors.toList());
//    }
//
//    public List<BonuspaymentDTO> findAllManagedBonuspaymentsByUserId(Integer userId, Integer year, Integer month) {
//        if (UserService.getPrincipal().getRole() == UserRole.ADMIN) {
//            return findAllBonuspaymentsByUserIdAndYearMonth(userId, year, month);
//        } else {
//            return repository.findAllManagedBonuspaymentsByUserId(UserService.getPrincipal().getId(), userId, year, month)
//                    .stream()
//                    .map(this::initDto)
//                    .collect(Collectors.toList());
//        }
//    }

    public List<BonuspaymentDTO> findAllManagedBonuspaymentsForLoggedUser(Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo, Integer departmentId) {
        return repository.findAllManagedBonuspaymentsByUser(UserService.getPrincipal(), yearFrom, monthFrom, yearTo, monthTo, departmentId)
                .stream()
                .map(bonuspayment -> new BonuspaymentDTO().createFromEntity(bonuspayment))
                .collect(Collectors.toList());
    }

}
