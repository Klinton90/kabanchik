package kabanchik.system.outcomeentry.dto;

import kabanchik.core.domain.IDto;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import kabanchik.system.outcomeentry.domain.OutcomeEntryType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class OutcomeEntrySimpleDTO implements IDto<OutcomeEntry, Integer> {

    private Integer id;

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("9999999.99")
    @Digits(integer = 7, fraction = 2)
    private BigDecimal amount;

    @NotNull
    private LocalDate date;

    @NotNull
    private OutcomeEntryType outcomeEntryType;

    @NotNull
    @Size(min = 3, max = 3)
    private String currencyId;

    @NotNull
    private Integer parentId;

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    private Boolean isForPlan;

    @NotNull
    @Size(max = 255)
    private String description;

}
