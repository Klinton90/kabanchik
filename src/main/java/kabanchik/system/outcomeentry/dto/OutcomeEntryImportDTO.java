package kabanchik.system.outcomeentry.dto;

import kabanchik.core.batch.ErrorReportingItemDTO;
import kabanchik.system.outcomeentry.domain.OutcomeEntryType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class OutcomeEntryImportDTO implements ErrorReportingItemDTO {

    @NotNull
    @DecimalMin("0.01")
    @DecimalMax("9999999.99")
    @Digits(integer = 7, fraction = 2)
    private BigDecimal amount;

    @NotNull
    private LocalDate date;

    @NotNull
    private OutcomeEntryType outcomeEntryType;

    @NotNull
    @Size(min = 3, max = 3)
    private String currencyId;

    @NotNull
    @Size(min = 8, max = 10)
    private String governmentId;

    @NotNull
    @Size(min = 1, max=50)
    private String projectName;

    private Integer itemCount;

    @NotNull
    @Size(max = 255)
    private String description;

    @Override
    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    @Override
    public String toErrorReportingString() {
        return  "\"" +
                projectName +
                "\",\"" +
                governmentId +
                "\",\"" +
                amount +
                "\",\"" +
                currencyId +
                "\",\"" +
                date +
                "\",\"" +
                outcomeEntryType +
                "\"";
    }

}
