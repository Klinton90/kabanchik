package kabanchik.system.outcomeentry.dto;

import kabanchik.core.util.BeanUtils;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutcomeEntryDTO extends OutcomeEntrySimpleDTO {

    private String timesheetCategoryName;

    private String currencyName;

    private String parentName;

    @Override
    public OutcomeEntryDTO createFromEntity(OutcomeEntry entity) {
        BeanUtils.copyProperties(entity, this, true);

        setTimesheetCategoryName(entity.getTimesheetCategory().getName());
        setCurrencyName(entity.getCurrency().getName());
        setParentName(entity.getParent().getName());

        return this;
    }
}
