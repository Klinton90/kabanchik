package kabanchik.system.outcomeentry.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.outcomeentry.domain.OutcomeEntryParent;
import kabanchik.system.outcomeentry.domain.OutcomeEntryType;

public interface OutcomeEntryParentDTO<E extends OutcomeEntryParent> extends ActivatableDomain, IDto<E, Integer> {

    String getName();

    OutcomeEntryParentDTO<E> setName(String name);

    String getGovernmentId();

    OutcomeEntryParentDTO<E> setGovernmentId(String governmentId);

    OutcomeEntryType getOutcomeEntryType();

    String getDescription();

    OutcomeEntryParentDTO<E> setDescription(String description);

}
