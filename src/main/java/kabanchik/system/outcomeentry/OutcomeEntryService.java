package kabanchik.system.outcomeentry;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import kabanchik.system.outcomeentry.domain.OutcomeEntryType;
import kabanchik.system.outcomeentry.dto.OutcomeEntryParentDTO;
import kabanchik.system.outcomeentry.dto.OutcomeEntryDTO;
import kabanchik.system.currency.CurrencyService;
import kabanchik.system.outcomeentry.dto.OutcomeEntrySimpleDTO;
import kabanchik.system.subcontractor.SubcontractorService;
import kabanchik.system.subcontractor.domain.Subcontractor;
import kabanchik.system.subcontractor.dto.SubcontractorDTO;
import kabanchik.system.supplier.SupplierService;
import kabanchik.system.supplier.dto.SupplierDTO;
import kabanchik.system.timesheetcategory.TimesheetCategoryService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class OutcomeEntryService extends RestService<OutcomeEntry, OutcomeEntryDTO, Integer> {

    @Getter
    @Autowired
    private OutcomeEntryRepository repository;

    @Getter
    @Autowired
    private OutcomeEntryService self;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private TimesheetCategoryService timesheetCategoryService;

    @Autowired
    private SubcontractorService subcontractorService;

    @Autowired
    private SupplierService supplierService;

    @Override
    public boolean isInUse(Integer id) {
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<OutcomeEntryParentDTO> getUniqueParents(String queryString, Integer limit, Integer offset) {
        return repository.getUniqueParents(queryString, limit, offset)
                .stream()
                .map(tuple -> {
                    if (tuple.get(0, Integer.class) != null) {
                        return new SubcontractorDTO()
                                .setId(tuple.get(0, Integer.class))
                                .setName(tuple.get(1, String.class))
                                .setDescription(tuple.get(2, String.class))
                                .setGovernmentId(tuple.get(3, String.class))
                                .setIsActive(tuple.get(4, Boolean.class))
                                .setProgressReportable(tuple.get(5, Boolean.class));
                    } else {
                        return new SupplierDTO()
                                .setId(tuple.get(6, Integer.class))
                                .setName(tuple.get(7, String.class))
                                .setDescription(tuple.get(8, String.class))
                                .setGovernmentId(tuple.get(9, String.class))
                                .setIsActive(tuple.get(10, Boolean.class));
                    }
                })
                .sorted(Comparator.comparing(OutcomeEntryParentDTO::getName))
                .collect(Collectors.toList());
    }

    public Integer countByCurrencyId(String id) {
        return repository.countByCurrencyId(id);
    }

    public Integer countByTimesheetCategoryId(Integer id) {
        return repository.countByTimesheetCategoryId(id);
    }

    public Integer countByOutcomeEntryTypeAndParentId(OutcomeEntryType type, Integer parentId){
        return repository.countByOutcomeEntryTypeAndParentId(type, parentId);
    }

    @Override()
    public void validate(OutcomeEntry entity) throws CustomValidationException {
        entity.setCurrency(currencyService.getOne(entity.getCurrencyId()));
        entity.setTimesheetCategory(timesheetCategoryService.getOne(entity.getTimesheetCategoryId()));

        switch (entity.getOutcomeEntryType()){
            case SUB_CONTRACTOR:
                entity.setParent(subcontractorService.getOne(entity.getParentId()));
                break;
            case SUPPLIER:
                entity.setParent(supplierService.getOne(entity.getParentId()));
                break;
            default:
                throw new RuntimeException("Unexpected type");
        }
    }

    public List<OutcomeEntrySimpleDTO> getAllByTimesheetCategoryId(Integer timesheetCategoryId, Boolean forPlan) {
        return repository.findAllByTimesheetCategoryId(timesheetCategoryId, forPlan)
                .stream()
                .map(this::initSimpleDto)
                .collect(Collectors.toList());
    }

    private OutcomeEntrySimpleDTO initSimpleDto(OutcomeEntry outcomeEntry) {
        return (OutcomeEntrySimpleDTO)new OutcomeEntrySimpleDTO().createFromEntity(outcomeEntry);
    }

    public List<OutcomeEntryParentDTO> getParents(String queryString, Integer limit, Integer offset) {
        return repository.getParents(queryString, limit, offset)
                .stream()
                .map(outcomeEntryParentView -> {
                    if (outcomeEntryParentView.getOutcomeEntryType().equals(OutcomeEntryType.SUB_CONTRACTOR.name())) {
                        return new SubcontractorDTO()
                                .setId(outcomeEntryParentView.getId())
                                .setIsActive(outcomeEntryParentView.getIsActive())
                                .setDescription(outcomeEntryParentView.getDescription())
                                .setGovernmentId(outcomeEntryParentView.getGovernmentId())
                                .setName(outcomeEntryParentView.getName())
                                .setProgressReportable(outcomeEntryParentView.getProgressReportable());
                    } else {
                        return new SupplierDTO()
                                .setId(outcomeEntryParentView.getId())
                                .setIsActive(outcomeEntryParentView.getIsActive())
                                .setDescription(outcomeEntryParentView.getDescription())
                                .setGovernmentId(outcomeEntryParentView.getGovernmentId())
                                .setName(outcomeEntryParentView.getName());
                    }
                })
                .collect(Collectors.toList());
    }
}