package kabanchik.system.outcomeentry;

import kabanchik.core.config.CustomConfig;
import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.service.FileStorageService;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import kabanchik.system.outcomeentry.dto.OutcomeEntryParentDTO;
import kabanchik.system.outcomeentry.dto.OutcomeEntryDTO;
import kabanchik.system.outcomeentry.dto.OutcomeEntrySimpleDTO;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@Controller
@RequestMapping(value = "/outcomeEntry")
public class OutcomeEntryController extends RestController<OutcomeEntry, OutcomeEntryDTO, Integer> {

    public static final String SUB_FOLDER = "outcomeEntry";

    private static final String EXAMPLE_FILE_NAME = "outcomeEntry_importExample.csv";

    @Getter
    @Autowired
    private OutcomeEntryService service;

    @Autowired
    private OutcomeEntryImportService outcomeImportService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private CustomConfig config;

    @RequestMapping(value = "/getAllByTimesheetCategoryId", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getAllByTimesheetCategoryId(
            @RequestParam Integer timesheetCategoryId,
            @RequestParam(required = false) Boolean forPlan
    ) {
        List<OutcomeEntrySimpleDTO> result = service.getAllByTimesheetCategoryId(timesheetCategoryId, forPlan);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getUniqueParents", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getUniqueParents(
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        List<OutcomeEntryParentDTO> result = service.getUniqueParents(queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getParents", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getParents(
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        List<OutcomeEntryParentDTO> result = service.getParents(queryString, limit, offset);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/csvImport", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> csvImport(@RequestParam MultipartFile file) throws Exception {
        String path = fileStorageService.storeMultipartFile(file, SUB_FOLDER, false);
        return ResponseEntity.ok(new RestResponse(outcomeImportService.csvImport(path)));
    }

    @RequestMapping(path = "/example.csv", method = RequestMethod.GET)
    public ResponseEntity<Resource> download() throws IOException, URISyntaxException {
        String examplePath = config.getFiles().getBase() + config.getFiles().getExample() + "/" + EXAMPLE_FILE_NAME;

        Resource resource = FileStorageService.readFileAsResource(examplePath, false);

        return ResponseEntity.ok()
                .contentLength(resource.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

}
