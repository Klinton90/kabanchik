package kabanchik.system.outcomeentry.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.system.outcomeentry.dto.OutcomeEntryParentDTO;

import java.util.List;

public interface OutcomeEntryParent<D extends OutcomeEntryParentDTO> extends ActivatableDomain, IDomain<D, Integer> {

    String getName();

    OutcomeEntryParent<D> setName(String name);

    String getGovernmentId();

    OutcomeEntryParent<D> setGovernmentId(String governmentId);

    String getDescription();

    OutcomeEntryParent<D> setDescription(String description);

    List<OutcomeEntry> getOutcomeEntries();

    OutcomeEntryParent<D> setOutcomeEntries(List<OutcomeEntry> outcomeEntries);

}
