package kabanchik.system.outcomeentry.domain;

import kabanchik.core.domain.LocalizedEnum;
import lombok.Getter;

@Getter
public enum OutcomeEntryType implements LocalizedEnum {
    SUB_CONTRACTOR,
    SUPPLIER
}
