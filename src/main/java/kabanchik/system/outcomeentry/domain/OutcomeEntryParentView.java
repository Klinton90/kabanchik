package kabanchik.system.outcomeentry.domain;

import kabanchik.core.domain.Field.ContainsFilterField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "outcome_entry_parent")
@Immutable
public class OutcomeEntryParentView implements Serializable {

    @Id
    @Column(name = "uid")
    private String uid;

    @Column(name = "id")
    private Integer id;

    @Column(name = "is_active")
    private Boolean isActive;

    @ContainsFilterField
    @Column(name = "name")
    private String name;

    @ContainsFilterField
    @Column(name = "description")
    private String description;

    @ContainsFilterField
    @Column(name = "government_id")
    private String governmentId;

    @Column(name = "progress_reportable")
    private Boolean progressReportable;

    @Column(name = "type")
    private String outcomeEntryType;

}
