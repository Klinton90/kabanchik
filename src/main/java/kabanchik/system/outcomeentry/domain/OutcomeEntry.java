package kabanchik.system.outcomeentry.domain;

import kabanchik.core.batch.ErrorReportingItemDTO;
import kabanchik.core.domain.IDomain;
import kabanchik.system.outcomeentry.dto.OutcomeEntryDTO;
import kabanchik.system.outcomeentry.dto.OutcomeEntryImportDTO;
import kabanchik.system.currency.domain.Currency;
import kabanchik.system.subcontractor.domain.Subcontractor;
import kabanchik.system.supplier.domain.Supplier;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.*;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@ToString
@Table(name = "outcome_entry")
@FilterDefs({
        @FilterDef(name = "outcomeEntryParentSubcontractor", defaultCondition = "outcome_entry_type = 'SUB_CONTRACTOR'"),
        @FilterDef(name = "outcomeEntryParentSupplier", defaultCondition = "outcome_entry_type = 'SUPPLIER'")
})
@NoArgsConstructor
@NamedEntityGraphs(value = {
    @NamedEntityGraph(
        name = "OutcomeEntry.default",
        attributeNodes = {
            @NamedAttributeNode(value = "timesheetCategory"),
            @NamedAttributeNode(value = "currency"),
            @NamedAttributeNode(value = "subcontractor"),
            @NamedAttributeNode(value = "supplier")
        }
    ),
    @NamedEntityGraph(
        name = "OutcomeEntry.empty"
    )
})
public class OutcomeEntry implements IDomain<OutcomeEntryDTO, Integer>, ErrorReportingItemDTO {

    @Transient
    Integer itemCount;

    @Override
    @Transient
    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "outcome_entry_id")
    private Integer id;

    @Column(name = "amount", nullable = false, precision = 19, scale = 2)
    private BigDecimal amount;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(name = "outcome_entry_type", nullable = false, columnDefinition = "enum('SUB_CONTRACTOR','SUPPLIER')")
    private OutcomeEntryType outcomeEntryType;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "currency_id", nullable = false, insertable = false, updatable = false)
    private String currencyId;

    @Column(name = "timesheet_category_id", nullable = false, insertable = false, updatable = false)
    private Integer timesheetCategoryId;

    @Column(name = "parent_id", nullable = false)
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Integer parentId;

    @Column(name = "is_for_plan", nullable = false)
    private Boolean isForPlan;

    @ToString.Exclude
    @PrimaryKeyJoinColumn
    @JoinColumn(name = "currency_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Currency currency;

    @ToString.Exclude
    @PrimaryKeyJoinColumn
    @JoinColumn(name = "timesheet_category_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private TimesheetCategory timesheetCategory;

    @ToString.Exclude
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    @PrimaryKeyJoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @JoinColumn(name = "parent_id", insertable = false, updatable = false, foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @ManyToOne(fetch = FetchType.LAZY)
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Subcontractor subcontractor;

    @ToString.Exclude
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    @PrimaryKeyJoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @JoinColumn(name = "parent_id", insertable = false, updatable = false, foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @ManyToOne(fetch = FetchType.LAZY)
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Supplier supplier;

    public OutcomeEntry(OutcomeEntryImportDTO dto) {
        amount = dto.getAmount();
        date = dto.getDate();
        outcomeEntryType = dto.getOutcomeEntryType();
        currencyId = dto.getCurrencyId();
        description = dto.getDescription();
    }

    public OutcomeEntryParent getParent() {
        switch (outcomeEntryType) {
            case SUB_CONTRACTOR:
                return subcontractor;
            case SUPPLIER:
                return supplier;
            default:
                return null;
        }
    }

    public OutcomeEntry setParent(OutcomeEntryParent parent) {
        if (parent instanceof Subcontractor) {
            setSubcontractor((Subcontractor) parent);
        } else if (parent instanceof Supplier) {
            setSupplier((Supplier) parent);
        } else {
            throw new RuntimeException("Cannot determine parent type");
        }
        return this;
    }

    @Override
    public String toErrorReportingString() {
        return  "\"" +
                timesheetCategory.getName() +
                "\",\"" +
                getParent().getName() +
                "\",\"" +
                getAmount() +
                "\",\"" +
                currencyId +
                "\",\"" +
                date +
                "\",\"" +
                outcomeEntryType +
                "\"";
    }
}
