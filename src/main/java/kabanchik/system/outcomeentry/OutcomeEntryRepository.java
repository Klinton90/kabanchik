package kabanchik.system.outcomeentry;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import kabanchik.system.outcomeentry.domain.OutcomeEntryParentView;
import kabanchik.system.outcomeentry.domain.OutcomeEntryType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.outcomeentry.domain.QOutcomeEntry.outcomeEntry;
import static kabanchik.system.outcomeentry.domain.QOutcomeEntryParentView.outcomeEntryParentView;
import static kabanchik.system.subcontractor.domain.QSubcontractor.subcontractor;
import static kabanchik.system.supplier.domain.QSupplier.supplier;

@Repository
public interface OutcomeEntryRepository extends RestRepository<OutcomeEntry, Integer> {

    default List<OutcomeEntry> findAllByTimesheetCategoryId(Integer timesheetCategoryId, Boolean forPlan) {
        BooleanExpression booleanExpression = outcomeEntry.timesheetCategoryId.eq(timesheetCategoryId);

        if (forPlan != null) {
            booleanExpression = booleanExpression.and(outcomeEntry.isForPlan.eq(forPlan));
        }

        return new JPAQuery<OutcomeEntry>(getEntityManager())
                .from(outcomeEntry)
                .setHint("javax.persistence.fetchgraph", getEntityManager().getEntityGraph("OutcomeEntry.empty"))
                .where(booleanExpression)
                .fetch();
    }

    Integer countByCurrencyId(String id);

    Integer countByTimesheetCategoryId(Integer id);

    Integer countByOutcomeEntryTypeAndParentId(OutcomeEntryType type, Integer parentId);

    default List<Tuple> getUniqueParents(String queryString, Integer limit, Integer offset) {
        BooleanBuilder booleanBuilder = new BooleanBuilder();

        if (StringUtils.isNotBlank(queryString)) {
            booleanBuilder = booleanBuilder.and(
                    subcontractor.name.contains(queryString)
                    .or(supplier.name.contains(queryString)));
        }

        JPAQuery<Tuple> query = new JPAQuery<Tuple>(getEntityManager())
                .from(outcomeEntry)
                .select(
                        subcontractor.id, subcontractor.name, subcontractor.description, subcontractor.governmentId, subcontractor.isActive, subcontractor.progressReportable,
                        supplier.id, supplier.name, supplier.description, supplier.governmentId, supplier.isActive
                )
                .distinct()
                .leftJoin(outcomeEntry.subcontractor, subcontractor).on(outcomeEntry.outcomeEntryType.eq(OutcomeEntryType.SUB_CONTRACTOR))
                .leftJoin(outcomeEntry.supplier, supplier).on(outcomeEntry.outcomeEntryType.eq(OutcomeEntryType.SUPPLIER))
                .where(booleanBuilder);

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

    default List<OutcomeEntryParentView> getParents(String queryString, Integer limit, Integer offset) {
        BooleanBuilder booleanBuilder = new BooleanBuilder();

        if (StringUtils.isNotBlank(queryString)) {
            booleanBuilder = booleanBuilder.and(outcomeEntryParentView.name.contains(queryString))
                .and(outcomeEntryParentView.description.contains(queryString));
        }

        JPAQuery<OutcomeEntryParentView> query = new JPAQuery<OutcomeEntryParentView>(getEntityManager())
                .from(outcomeEntryParentView)
                .where(booleanBuilder)
                .orderBy(outcomeEntryParentView.name.asc());

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        return query.fetch();
    }

}
