package kabanchik.system.outcomeentry;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.system.outcomeentry.domain.OutcomeEntry;
import kabanchik.system.outcomeentry.domain.OutcomeEntryParent;
import kabanchik.system.outcomeentry.dto.OutcomeEntryImportDTO;
import kabanchik.system.outcomeentry.dto.OutcomeEntryParentDTO;
import kabanchik.system.batchjobmonitor.BatchJobMonitorService;
import kabanchik.system.batchjobmonitor.dto.JobExecutionDTO;
import kabanchik.system.currency.CurrencyService;
import kabanchik.system.subcontractor.SubcontractorService;
import kabanchik.system.supplier.SupplierService;
import kabanchik.system.timesheetcategory.TimesheetCategoryService;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.Getter;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Service
public class OutcomeEntryImportService {

    private static final String[] ACCOUNT_ENTRY_IMPORT_FIELDS = {
            "ProjectName",
            "GovernmentId",
            "Amount",
            "CurrencyId",
            "Date",
            "OutcomeEntryType",
            "Description"
    };

    ItemProcessor<OutcomeEntryImportDTO, OutcomeEntry> itemProcessor;

    @Autowired
    private BatchJobMonitorService batchJobMonitorService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private TimesheetCategoryService timesheetCategoryService;

    @Autowired
    private SubcontractorService subcontractorService;

    @Autowired
    private SupplierService supplierService;

    @Getter
    @Autowired
    private OutcomeEntryImportService self;

    public JobExecutionDTO csvImport(String filePath) throws Exception {
        return batchJobMonitorService.runCsvImport(getItemProcessor(), filePath, ACCOUNT_ENTRY_IMPORT_FIELDS, OutcomeEntryImportDTO.class, true);
    }

    private ItemProcessor<OutcomeEntryImportDTO, OutcomeEntry> getItemProcessor() {
        if (itemProcessor == null) {
            itemProcessor = self::validate;
        }
        return itemProcessor;
    }

    @Validated
    public OutcomeEntry validate(@Valid OutcomeEntryImportDTO dto) throws CustomValidationException {
        OutcomeEntry result = new OutcomeEntry(dto);

        result.setCurrency(currencyService.getOne(dto.getCurrencyId()));

        TimesheetCategory timesheetCategory = timesheetCategoryService.getOneByName(dto.getProjectName());
        result.setTimesheetCategory(timesheetCategory);
        result.setTimesheetCategoryId(timesheetCategory.getId());

        OutcomeEntryParent<? extends OutcomeEntryParentDTO> parent = null;
        switch (result.getOutcomeEntryType()){
            case SUB_CONTRACTOR:
                parent = subcontractorService.getOneByGovernmentId(dto.getGovernmentId());
                break;
            case SUPPLIER:
                parent = supplierService.getOneByGovernmentId(dto.getGovernmentId());
                break;
        }
        result.setParent(parent);
        result.setParentId(parent.getId());

        result.setItemCount(dto.getItemCount());
        result.setIsForPlan(false);

        return result;
    }

}
