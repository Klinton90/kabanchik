package kabanchik.system.progress;

import kabanchik.core.controller.RestController;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.system.progress.dto.BulkProgressAdjustmentDTO;
import kabanchik.system.progress.dto.ProgressDTO;
import kabanchik.system.progress.domain.Progress;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/progress")
public class ProgressController extends RestController<Progress, ProgressDTO, Integer> {

    @Getter
    @Autowired
    private ProgressService service;

    @RequestMapping(value = "/findAllByTimesheetCategoryid", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> findAllByTimesheetCategoryid(@RequestParam Integer timesheetCategoryId) {
        List<ProgressDTO> result = service.findAllByTimesheetCategoryid(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/getForPSR", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getForPSR(@RequestParam Integer timesheetCategoryId) {
        List<ProgressDTO> result = service.getForPSR(timesheetCategoryId);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/bulkProgressAdjustment", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<RestResponse> bulkProgressAdjustment(@RequestBody BulkProgressAdjustmentDTO dto) throws CustomValidationException {
        service.bulkProgressAdjustment(dto);
        return ResponseEntity.ok(new RestResponse(null));
    }

}
