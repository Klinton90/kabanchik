package kabanchik.system.progress.dto;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDto;
import kabanchik.system.progress.domain.ProgressParent;
import kabanchik.system.progress.domain.ProgressType;

public interface ProgressParentDTO<E extends ProgressParent> extends ActivatableDomain, IDto<E, Integer> {

    String getName();

    ProgressParentDTO<E> setName(String name);

    ProgressType getProgressType();

    String getDescription();

    ProgressParentDTO<E> setDescription(String description);

    boolean isProgressReportable();

    ProgressParentDTO<E> setProgressReportable(boolean progressReportable);

}
