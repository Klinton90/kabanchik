package kabanchik.system.progress.dto;

import kabanchik.system.progress.domain.ProgressType;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class BulkProgressAdjustmentDTO {

    @NotNull
    private Integer timesheetCategoryId;

    @NotNull
    private ProgressType progressType;

    @NotNull
    private Integer parentId;

    @NotNull
    private Boolean isForPlan;

    @NotNull
    private Integer adjustmentWeeks;

    @Nullable
    private LocalDate adjustmentFromDate;

    @Nullable
    private LocalDate adjustmentToDate;
}
