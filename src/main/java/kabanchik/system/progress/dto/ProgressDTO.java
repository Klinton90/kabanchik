package kabanchik.system.progress.dto;

import kabanchik.core.domain.IDto;
import kabanchik.system.progress.domain.Progress;
import kabanchik.system.progress.domain.ProgressType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class ProgressDTO implements IDto<Progress, Integer> {

    private Integer id;

    @NotNull
    private LocalDate date;

    @NotNull
    @DecimalMax("100.00")
    @DecimalMin("0.01")
    @Digits(integer = 3, fraction = 2)
    private BigDecimal amount;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    private Boolean isForPlan;

    @NotNull
    private ProgressType progressType;

    @NotNull
    private Integer parentId;

    @NotNull
    private Integer timesheetCategoryId;

}
