package kabanchik.system.progress;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.repository.RestRepository;
import kabanchik.system.progress.domain.Progress;
import kabanchik.system.progress.domain.ProgressType;
import kabanchik.system.progress.dto.BulkProgressAdjustmentDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

import static kabanchik.system.departmentaccess.domain.QDepartmentAccess.departmentAccess;
import static kabanchik.system.progress.domain.QProgress.progress;
import static kabanchik.system.subcontractor.domain.QSubcontractor.subcontractor;

@Repository
public interface ProgressRepository extends RestRepository<Progress, Integer> {

    default List<Progress> findAllByTimesheetCategoryid(Integer timesheetCategoryId) {
        return new JPAQuery<Progress>(getEntityManager())
                .from(progress)
                .where(progress.timesheetCategoryId.eq(timesheetCategoryId))
                .fetch();
    }

    default List<Progress> getForPSR(Integer timesheetCategoryId) {
        return new JPAQuery<Progress>(getEntityManager())
                .distinct()
                .from(progress)
                .leftJoin(progress.department.departmentAccesses, departmentAccess)
                .leftJoin(progress.subcontractor, subcontractor)
                .where(progress.timesheetCategoryId.eq(timesheetCategoryId)
                        .and(
                            progress.progressType.eq(ProgressType.SUB_CONTRACTOR)
                            .and(subcontractor.progressReportable.isTrue())
                            .and(subcontractor.isActive.isTrue())
                            .or(
                                progress.progressType.eq(ProgressType.DEPARTMENT)
                                .and(departmentAccess.isActive.isTrue())
                                .and(departmentAccess.timesheetCategoryId.eq(timesheetCategoryId))
                            )
                        )
                )
                .fetch();
    }

    default long countByDepartmentId(Integer departmentId) {
        return new JPAQuery<Progress>(getEntityManager())
                .from(progress)
                .where(progress.parentId.eq(departmentId)
                        .and(progress.progressType.eq(ProgressType.DEPARTMENT)))
                .fetchCount();
    }

    default List<Progress> findAllForBulkProgressAdjustment(BulkProgressAdjustmentDTO dto) {
        BooleanExpression booleanExpression = progress.isForPlan.eq(dto.getIsForPlan())
                .and(progress.progressType.eq(dto.getProgressType()))
                .and(progress.parentId.eq(dto.getParentId()));

        if (dto.getAdjustmentFromDate() != null) {
            booleanExpression = booleanExpression.and(progress.date.after(dto.getAdjustmentFromDate()));
        }

        if (dto.getAdjustmentToDate() != null) {
            booleanExpression = booleanExpression.and(progress.date.before(dto.getAdjustmentToDate()));
        }

        return new JPAQuery<Progress>(getEntityManager())
                .from(progress)
                .where(booleanExpression)
                .orderBy(progress.date.asc())
                .fetch();
    }

}
