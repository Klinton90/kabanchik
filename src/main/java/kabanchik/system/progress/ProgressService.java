package kabanchik.system.progress;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.RestService;
import kabanchik.system.department.DepartmentService;
import kabanchik.system.department.domain.Department;
import kabanchik.system.departmentaccess.DepartmentAccessService;
import kabanchik.system.departmentaccess.domain.DepartmentAccess;
import kabanchik.system.progress.dto.BulkProgressAdjustmentDTO;
import kabanchik.system.progress.dto.ProgressDTO;
import kabanchik.system.progress.domain.Progress;
import kabanchik.system.subcontractor.SubcontractorService;
import kabanchik.system.timesheetcategory.TimesheetCategoryService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProgressService extends RestService<Progress, ProgressDTO, Integer> {

    @Getter
    @Autowired
    private ProgressRepository repository;

    @Getter
    @Autowired
    private ProgressService self;

    @Autowired
    private TimesheetCategoryService timesheetCategoryService;

    @Autowired
    private SubcontractorService subcontractorService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private DepartmentAccessService departmentAccessService;

    @Override
    public boolean isInUse(Integer integer) {
        return false;
    }

    @Deprecated
    public List<ProgressDTO> findAllByTimesheetCategoryid(Integer timesheetCategoryId) {
        return repository.findAllByTimesheetCategoryid(timesheetCategoryId)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

    @Override()
    public void validate(Progress entity) throws CustomValidationException {
        entity.setTimesheetCategory(timesheetCategoryService.getOne(entity.getTimesheetCategoryId()));

        switch (entity.getProgressType()){
            case SUB_CONTRACTOR:
                entity.setParent(subcontractorService.getOne(entity.getParentId()));
                break;
            case DEPARTMENT:
                Department department = departmentService.getOne(entity.getParentId());
                if (departmentAccessService.findOneByDepartmentIdAndTimesheetCategoryId(entity.getParentId(), entity.getTimesheetCategoryId()) != null) {
                    entity.setParent(department);
                    break;
                } else {
                    String objectName = DepartmentAccess.class.getSimpleName();
                    Object[] messageParams = {objectName, entity.getParentId()};
                    throw new CustomValidationException("rowNotFound", objectName, messageParams);
                }
            default:
                throw new RuntimeException("Unexpected type");
        }
    }

    public long countByDepartmentId(Integer departmentId) {
        return repository.countByDepartmentId(departmentId);
    }

    public List<ProgressDTO> getForPSR(Integer timesheetCategoryId) {
        return repository.getForPSR(timesheetCategoryId)
                .stream()
                .map(this::initDto)
                .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Exception.class)
    public void bulkProgressAdjustment(BulkProgressAdjustmentDTO dto) throws CustomValidationException {
        List<Progress> progressList = repository.findAllForBulkProgressAdjustment(dto);

        for (Progress progress : progressList) {
            repository.delete(progress);
        }

        for (Progress progress : progressList) {
            ProgressDTO progressDTO = initDto(progress);
            progressDTO.setId(0)
                    .setDate(progressDTO.getDate().plusDays(7 * dto.getAdjustmentWeeks()));

            _save(initEntity(progressDTO));
        }
    }
}
