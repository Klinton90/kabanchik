package kabanchik.system.progress.domain;

import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.IDomain;
import kabanchik.system.progress.dto.ProgressParentDTO;

import java.util.List;

public interface ProgressParent<D extends ProgressParentDTO> extends ActivatableDomain, IDomain<D, Integer> {

    String getName();

    ProgressParent<D> setName(String name);

    String getDescription();

    ProgressParent<D> setDescription(String description);

    List<Progress> getProgressList();

    ProgressParent<D> setProgressList(List<Progress> progressList);

}
