package kabanchik.system.progress.domain;

import kabanchik.core.domain.LocalizedEnum;

public enum ProgressType implements LocalizedEnum {
    SUB_CONTRACTOR,
    DEPARTMENT
}
