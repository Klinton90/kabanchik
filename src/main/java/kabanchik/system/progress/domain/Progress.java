package kabanchik.system.progress.domain;

import kabanchik.core.domain.IDomain;
import kabanchik.core.validator.Unique;
import kabanchik.core.validator.UniqueColumn;
import kabanchik.system.department.domain.Department;
import kabanchik.system.progress.dto.ProgressDTO;
import kabanchik.system.subcontractor.domain.Subcontractor;
import kabanchik.system.timesheetcategory.domain.TimesheetCategory;
import lombok.*;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "progress")
@FilterDefs({
        @FilterDef(name = "progressParentSubcontractor", defaultCondition = "progress_entry_type = 'SUB_CONTRACTOR'"),
        @FilterDef(name = "progressParentDepartment", defaultCondition = "progress_entry_type = 'DEPARTMENT'")
})
@Unique(columns = @UniqueColumn(fields = {"date", "progressType", "timesheetCategoryId", "parentId", "isForPlan"}))
public class Progress implements IDomain<ProgressDTO, Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "progress_id")
    private Integer id;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "is_for_plan", nullable = false)
    private Boolean isForPlan;

    @Enumerated(EnumType.STRING)
    @Column(name = "progress_entry_type", nullable = false, columnDefinition = "enum('SUB_CONTRACTOR', 'DEPARTMENT')")
    private ProgressType progressType;

    @Column(name = "timesheet_category_id", nullable = false)
    private Integer timesheetCategoryId;

    @Column(name = "parent_id", nullable = false)
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Integer parentId;

    @ToString.Exclude
    @PrimaryKeyJoinColumn
    @JoinColumn(name = "timesheet_category_id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private TimesheetCategory timesheetCategory;

    @ToString.Exclude
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    @PrimaryKeyJoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @JoinColumn(name = "parent_id", insertable = false, updatable = false, foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @ManyToOne(fetch = FetchType.LAZY)
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Subcontractor subcontractor;

    @ToString.Exclude
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    @PrimaryKeyJoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @JoinColumn(name = "parent_id", insertable = false, updatable = false, foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @ManyToOne(fetch = FetchType.LAZY)
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Department department;

    public ProgressParent getParent() {
        switch (progressType) {
            case SUB_CONTRACTOR:
                return subcontractor;
            case DEPARTMENT:
                return department;
            default:
                return null;
        }
    }

    public Progress setParent(ProgressParent parent) {
        if (parent instanceof Subcontractor) {
            setSubcontractor((Subcontractor) parent);
        } else if (parent instanceof Department) {
            setDepartment((Department) parent);
        } else {
            throw new RuntimeException("Cannot determine parent type");
        }
        return this;
    }

}
