package kabanchik.system.batchjobmonitor.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.item.ExecutionContext;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static kabanchik.core.batch.CustomExecutionContextPromotionListener.ERROR_KEY;

@Getter
@Setter
public class JobExecutionDTO {

    private long id;

    private String name;

    private long createdTime;

    private BatchStatus status;

    private int errorsCount;

    private String reportPathUrl;

    private Boolean isRunning;

    public JobExecutionDTO(JobExecution jobExecution) {
        id = jobExecution.getJobId();
        name = jobExecution.getJobInstance().getJobName();
        createdTime = jobExecution.getCreateTime().getTime();
        status = jobExecution.getStatus();
        isRunning = jobExecution.isRunning();

        ExecutionContext executionContext = jobExecution.getExecutionContext();

        List<Map.Entry> errors = executionContext.entrySet().stream()
                .filter(entry -> entry.getKey().startsWith(ERROR_KEY))
                .collect(Collectors.toList());

        this.errorsCount = errors.size();
    }

}
