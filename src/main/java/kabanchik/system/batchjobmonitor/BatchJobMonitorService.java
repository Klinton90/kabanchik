package kabanchik.system.batchjobmonitor;

import kabanchik.core.batch.BatchBeansFactory;
import kabanchik.core.batch.ErrorReportingItemDTO;
import kabanchik.core.config.CustomConfig;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.service.FileStorageService;
import kabanchik.system.batchjobmonitor.dto.JobExecutionDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static kabanchik.core.batch.BatchBeansFactory.*;

@Service
public class BatchJobMonitorService {

    private Map<ItemProcessor<? extends ErrorReportingItemDTO, ? extends ErrorReportingItemDTO>, Job> processorJobMap = new HashMap<>();

    @Autowired
    private JobExplorer jobExplorer;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private BatchBeansFactory batchBeansFactory;

    @Autowired
    private CustomConfig config;

    public List<JobExecutionDTO> getAllActiveJobExecutions() {
        return jobExplorer.getJobNames()
                .stream()
                .map(this::getActiveJobExecutionsByName)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    public JobExecutionDTO getJobExecutionByJobId(long id) {
        return new JobExecutionDTO(jobExplorer.getJobExecution(id));
    }

    public List<JobExecutionDTO> getActiveJobExecutionsByName(String jobName) {
        return jobExplorer.findRunningJobExecutions(jobName)
                .stream()
                .map(JobExecutionDTO::new)
                .collect(Collectors.toList());
    }

    public String getReportPath(long id) {
        JobExecution jobExecution = jobExplorer.getJobExecution(id);
        ExecutionContext executionContext = jobExecution.getExecutionContext();
        return executionContext.containsKey(REPORT_PATH_PAR) ? executionContext.getString(REPORT_PATH_PAR) : "";
    }

    public JobExecutionDTO run(Job job, JobParameters jobParameters) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, InvalidInputException {
        List<JobExecutionDTO> activeJobs = getActiveJobExecutionsByName(job.getName());
        if (activeJobs != null && activeJobs.size() > 0) {
            Object[] messageParams = {job.getName()};
            throw new InvalidInputException("app.error.batch.jobAlreadyRunning", messageParams);
        }

        return new JobExecutionDTO(jobLauncher.run(job, jobParameters));
    }

    public <I extends ErrorReportingItemDTO, O extends ErrorReportingItemDTO> JobExecutionDTO runCsvImport(
            ItemProcessor<I, O> processor,
            String filePath,
            String[] fieldNames,
            Class<? extends ErrorReportingItemDTO> clazz,
            Boolean skipFirstLine
    ) throws Exception {
        Job csvImportJob;
        if (processorJobMap.containsKey(processor)) {
            csvImportJob = processorJobMap.get(processor);
        } else {
            csvImportJob = batchBeansFactory.getCsvImportJob(processor);
            processorJobMap.put(processor, csvImportJob);
        }
        JobParameters jobParameters = getCsvImportParameters(filePath, fieldNames, clazz, skipFirstLine);
        return run(csvImportJob, jobParameters);
    }

//    public JobExecutionDTO runCsvImport(
//            Step csvImportStep,
//            String filePath,
//            String[] fieldNames,
//            Class clazz,
//            Boolean skipFirstLine
//    ) throws Exception {
//        return run(batchBeansFactory.getCsvImportJob(csvImportStep), getCsvImportParameters(filePath, fieldNames, clazz, skipFirstLine));
//    }

    public JobParameters getCsvImportParameters(
            String filePath,
            String[] fieldNames,
            Class clazz,
            Boolean skipFirstLine
    ) {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString(FILE_PATH_PAR, filePath);
        jobParametersBuilder.addString(JOINED_FIELDS_PAR, StringUtils.join(fieldNames, ","));
        jobParametersBuilder.addString(CLASS_NAME_PAR, clazz.getCanonicalName());
        jobParametersBuilder.addString(SKIP_FIRST_LINE_PAR, skipFirstLine ? ITEM_READER_SKIP_FIRST_LINE : "");
        jobParametersBuilder.addLong(TIMESTAMP_PAR, (new Date()).getTime());
        return jobParametersBuilder.toJobParameters();
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void deleteReportsTask() throws URISyntaxException {
        FileStorageService.deleteFolder(config.getFiles().getBase() + config.getFiles().getUpload());
    }

}
