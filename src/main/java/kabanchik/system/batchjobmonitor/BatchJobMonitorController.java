package kabanchik.system.batchjobmonitor;

import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@Controller
@RequestMapping(value = "/batchJobMonitor")
public class BatchJobMonitorController {

    @Autowired
    private BatchJobMonitorService service;

    @RequestMapping(value = {"/get", "/get/"}, method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getJobExecutionById(@RequestParam() long id) {
        return ResponseEntity.ok(new RestResponse(service.getJobExecutionByJobId(id)));
    }

    @RequestMapping(value = {"/getByName", "/getByName/"}, method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getActiveJobExecutionsByName(@RequestParam() String jobName) {
        return ResponseEntity.ok(new RestResponse(service.getActiveJobExecutionsByName(jobName)));
    }

    @RequestMapping(value = {"/list", "/list/"}, method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> getAllActiveJobExecutions() {
        return ResponseEntity.ok(new RestResponse(service.getAllActiveJobExecutions()));
    }

    @RequestMapping(path = "/report.csv", method = RequestMethod.GET)
    public ResponseEntity<Resource> download(@RequestParam() long id) throws IOException, URISyntaxException {
        String reportPath = service.getReportPath(id);

        Resource resource = FileStorageService.readFileAsResource(new URI(reportPath), true);

        return ResponseEntity.ok()
                .contentLength(resource.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

}
