package kabanchik.system.configuration.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Properties;

@Getter
@Setter(AccessLevel.PRIVATE)
public class ConfigurationDTO {

    private boolean timesheetMode;

    @Min(0)
    @Max(99)
    private int allowedAuthFailureCount;

    @NotNull
    @NotBlank
    @Size(min = 8, max = 255)
    @Email
    private String supportEmail;

    @Size(max = 255)
    private String userCron1;

    @Size(max = 255)
    private String userCron2;

    @Size(max = 255)
    private String managerCron1;

    @Size(max = 255)
    private String managerCron2;

    @Size(max = 255)
    private String bankCurrencyCron;

    @Min(0)
    @Max(100)
    private Integer userWeeklyWorkloadMax;

    @Min(0)
    @Max(100)
    private Integer executorWeeklyWorkloadMax;

    public static ConfigurationDTO getDefault(){
        ConfigurationDTO result = new ConfigurationDTO();
        result.setTimesheetMode(false);
        result.setAllowedAuthFailureCount(5);
        result.setSupportEmail("");
        result.setUserCron1("0 0 16 ? * FRI");
        result.setUserCron2("");
        result.setManagerCron1("0 0 16 ? * FRI");
        result.setManagerCron2("");
        result.setBankCurrencyCron("0 0 8 * * *");
        result.setUserWeeklyWorkloadMax(85);
        result.setExecutorWeeklyWorkloadMax(65);
        return result;
    }

    //TODO: rewrite this to guess simple type or use any provided ObjectMapper implementation
    public static ConfigurationDTO fromProperties(Properties props) {
        ConfigurationDTO result = new ConfigurationDTO();

        String timesheetMode = (String)props.getOrDefault("timesheetMode", "false");
        result.setTimesheetMode(Boolean.valueOf(timesheetMode));

        String allowedAuthFailureCount = (String)props.getOrDefault("allowedAuthFailureCount", "5");
        result.setAllowedAuthFailureCount(Integer.valueOf(allowedAuthFailureCount));

        String supportEmail = (String)props.getOrDefault("supportEmail", "");
        result.setSupportEmail(supportEmail);

        String userCron1 = (String)props.getOrDefault("userCron1", "");
        result.setUserCron1(userCron1);

        String userCron2 = (String)props.getOrDefault("userCron2", "");
        result.setUserCron2(userCron2);

        String managerCron1 = (String)props.getOrDefault("managerCron1", "");
        result.setManagerCron1(managerCron1);

        String managerCron2 = (String)props.getOrDefault("managerCron2", "");
        result.setManagerCron2(managerCron2);

        String bankCurrencyCron = (String) props.getOrDefault("bankCurrencyCron", "");
        result.setBankCurrencyCron(bankCurrencyCron);

        String userWeeklyWorkloadMax =  props.getOrDefault("userWeeklyWorkloadMax", "85").toString();
        result.setUserWeeklyWorkloadMax(Integer.valueOf(userWeeklyWorkloadMax));

        String executorWeeklyWorkloadMax = props.getOrDefault("executorWeeklyWorkloadMax", "65").toString();
        result.setExecutorWeeklyWorkloadMax(Integer.valueOf(executorWeeklyWorkloadMax));

        return result;
    }

}
