package kabanchik.system.configuration;

import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.configuration.dto.ConfigurationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
@RequestMapping(value = "/configuration")
public class ConfigurationController {

    @Autowired
    private ConfigurationService configurationService;

    @RequestMapping(value = {"/get"}, method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<RestResponse> get() throws IOException {
        return ResponseEntity.ok(new RestResponse(ConfigurationService.cachedConfig));
    }

    @RequestMapping(value = {"/save"}, method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<RestResponse> save(@RequestBody ConfigurationDTO dto) throws IOException {
        configurationService.save(dto);
        return ResponseEntity.ok(new RestResponse(true));
    }

}
