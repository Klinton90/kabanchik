package kabanchik.system.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import kabanchik.core.config.CustomConfig;
import kabanchik.core.service.ManagerReminderRunnable;
import kabanchik.core.service.ReminderService;
import kabanchik.core.service.UserReminderRunnable;
import kabanchik.system.configuration.dto.ConfigurationDTO;
import kabanchik.system.currency.BankCurrencyUpdaterRunnable;
import kabanchik.system.currency.CurrencyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.util.Pair;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ScheduledFuture;

@Slf4j
@Service
public class ConfigurationService {

    private static final String FILE_NAME = "custom_config";
    private static final String FILE_EXT = "properties";

    private ObjectMapper mapper = new ObjectMapper();

    public static ConfigurationDTO cachedConfig;

    private ScheduledFuture userTask1;
    private ScheduledFuture userTask2;
    private ScheduledFuture managerTask1;
    private ScheduledFuture managerTask2;

    @Autowired
    private CustomConfig config;

    @Autowired
    private TaskScheduler executor;

    @Autowired
    private ReminderService reminderService;

    @Autowired
    private CurrencyService currencyService;

    public static Pair<LocalDate, LocalDate> getDatesByDateCriteria(Integer yearFrom, Integer monthFrom, Integer yearTo, Integer monthTo) {
        LocalDate dateFrom;
        LocalDate dateTo;
        if (ConfigurationService.cachedConfig.isTimesheetMode()) {
            dateFrom = LocalDate.of(yearFrom, monthFrom + 1, 1);
            LocalDate dateTmp = LocalDate.of(yearTo, monthTo + 1, 1);
            dateTo = dateTmp.withDayOfMonth(dateTmp.lengthOfMonth());
        } else {
            dateFrom = LocalDate.of(yearFrom, monthFrom + 1, 1)
                    .with(WeekFields.ISO.getFirstDayOfWeek());
            LocalDate dateTmp = LocalDate.of(yearTo, monthTo + 1, 1)
                    .with(TemporalAdjusters.lastDayOfMonth());

            Integer adjustmentDays = DayOfWeek.SUNDAY.getValue() - dateTmp.getDayOfWeek().getValue();
            dateTo = dateTmp.plusDays(adjustmentDays);
        }

        return Pair.of(dateFrom, dateTo);
    }

    //TODO: figure out how to use caching
    //@Cacheable(value = "configuration", key = "config")
    public ConfigurationDTO init() throws IOException {
        log.info("ConfigurationDTO#init");
        File file = new File(config.getFiles().getBase() + config.getFiles().getCustomConfig() + "/" + FILE_NAME + "." + FILE_EXT);

        if(cachedConfig == null) {
            if (!file.exists()) {
                cachedConfig = ConfigurationDTO.getDefault();
            } else {
                Properties props = new Properties();

                FileInputStream fis = new FileInputStream(file);
                props.load(fis);
                fis.close();

                cachedConfig = ConfigurationDTO.fromProperties(props);
            }
        }

        registerRunnables(cachedConfig);

        return cachedConfig;
    }

    //@CachePut(value = "configuration", key = "config")
    @Valid
    public void save(@Validated ConfigurationDTO dto) throws IOException {
        Properties props = new Properties();
        Map<String, Object> map = mapper.convertValue(dto, Map.class);
        map.forEach((key, value) -> props.put(key, value != null ? value.toString() : ""));

        File folder = new File(config.getFiles().getBase() + config.getFiles().getCustomConfig());
        folder.mkdirs();
        File file = new File(folder.getAbsolutePath() + "/" + FILE_NAME + "." + FILE_EXT);
        props.store(new FileOutputStream(file), "");
        cachedConfig = dto;

        registerRunnables(cachedConfig);
    }

    private void registerRunnables(ConfigurationDTO dto) {
        if (userTask1 != null) {
            userTask1.cancel(false);
            userTask1 = null;
        }
        if (userTask2 != null) {
            userTask2.cancel(false);
            userTask2 = null;
        }
        if (managerTask1 != null) {
            managerTask1.cancel(false);
            managerTask1 = null;
        }
        if (managerTask2 != null) {
            managerTask2.cancel(false);
            managerTask2 = null;
        }

        if (dto.getUserCron1() != null && dto.getUserCron1().length() > 0) {
            userTask1 = executor.schedule(new UserReminderRunnable(reminderService, LocalDate.now()), new CronTrigger(dto.getUserCron1()));
        }
        if (dto.getUserCron2() != null && dto.getUserCron2().length() > 0) {
            userTask2 = executor.schedule(new UserReminderRunnable(reminderService, LocalDate.now().minusDays(7)), new CronTrigger(dto.getUserCron2()));
        }
        if (dto.getManagerCron1() != null && dto.getManagerCron1().length() > 0) {
            managerTask1 = executor.schedule(new ManagerReminderRunnable(reminderService, LocalDate.now()), new CronTrigger(dto.getManagerCron1()));
        }
        if (dto.getManagerCron2() != null && dto.getManagerCron2().length() > 0) {
            managerTask2 = executor.schedule(new ManagerReminderRunnable(reminderService, LocalDate.now().minusDays(7)), new CronTrigger(dto.getManagerCron2()));
        }
        if (dto.getBankCurrencyCron() != null && dto.getBankCurrencyCron().length() > 0) {
            managerTask2 = executor.schedule(new BankCurrencyUpdaterRunnable(currencyService), new CronTrigger(dto.getBankCurrencyCron()));
        }
    }

}
