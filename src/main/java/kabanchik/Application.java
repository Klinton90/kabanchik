package kabanchik;

import com.cosium.spring.data.jpa.entity.graph.repository.support.EntityGraphJpaRepositoryFactoryBean;
import kabanchik.core.repository.RestRepositoryImpl;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Properties;
import java.util.TimeZone;

@EnableAsync
@SpringBootApplication
@EnableSpringDataWebSupport
@EnableConfigurationProperties
@EnableBatchProcessing
@EnableScheduling
@EnableJpaRepositories(
        repositoryFactoryBeanClass = EntityGraphJpaRepositoryFactoryBean.class,
        repositoryBaseClass = RestRepositoryImpl.class,
        basePackages = "kabanchik"
)
@EntityScan(basePackageClasses = {Application.class, Jsr310JpaConverters.class})
//TODO: enable for caching
//@EnableCaching
//TODO: enable for SpringBoot 2.0
//@PropertySource("classpath:application.yml")
public class Application {

    public static void main(String[] args) {
        //SpringApplication application = new SpringApplication(Application.class);

        //Override some properties, as Spring cannot read them from application.yml anymore
        //Properties properties = new Properties();
        //properties.put("server.error.whitelabel.enabled", false);
        //application.setDefaultProperties(properties);

        //application.run(args);
        TimeZone.setDefault( TimeZone.getTimeZone( "UTC" ) );
        SpringApplication.run(Application.class, args);
    }

}
