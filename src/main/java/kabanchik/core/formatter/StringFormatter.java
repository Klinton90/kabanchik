package kabanchik.core.formatter;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class StringFormatter implements Formatter<String>{

    private Style style;

    public void setStyle (Style style) {
        this.style = style;
    }

    @Override
    public String parse(String text, Locale locale) throws ParseException{
        return text;
    }

    @Override
    public String print(String original, Locale locale){
        if(original == null){
            return "";
        }

        switch(style){
            case LOWERCASE:
                return original.toLowerCase();
            case UPPERCASE:
                return original.toUpperCase();
            default:
                return original;
        }
    }

    public enum Style{
        UPPERCASE,
        LOWERCASE
    }

}