package kabanchik.core.config;

import kabanchik.system.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Validated
@Configuration
@EnableWebSecurity
@ConfigurationProperties(prefix = "custom.security")
@EnableGlobalMethodSecurity(prePostEnabled = true)
//@Order(2147483640)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    UserService userService;

    @Autowired
    CustomConfig config;

    @Autowired
    CustomStatusAuthenticationFailureHandler customStatusAuthenticationFailureHandler;

    @Autowired
    CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Autowired
    CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

    @Autowired
    CustomAccessDeniedHandler customAccessDeniedHandler;

    static final String[] _restActions = {
        "",
        "/",
        "/list",
        "/list/",
        "/findLike",
        "/findByField",
        "/findByStringFieldAndType",
        "/findByGenericFilters",
        "/getUniqueValues",
        "/create",
        "/create/",
        "/update/{id}",
        "/update/{id}/",
        "/saveAll",
        "/saveAll/",
        "/delete/{id}",
        "/delete/{id}/",
        "/{id}",
        "/{id}/"
    };

    String[] _prepareResources(String[] resources){
        ArrayList<String> result = new ArrayList<>();
        Pattern pattern = Pattern.compile("rest\\((\\/{1}.+?)(?:, not\\((\\/{1}.+)\\))?\\)");
        for (String row: resources) {
            Matcher matcher = pattern.matcher(row);
            if (matcher.matches()) {
                String controller = matcher.group(1);
                List<String> exceptActions = matcher.group(2) != null ? Arrays.asList(matcher.group(2).split(",")) : new ArrayList<>();
                for (String action : _restActions) {
                    if (!exceptActions.contains(action)) {
                        result.add(controller+action);
                    }
                }
            }else{
                result.add(row);
            }
        }
        return result.toArray(new String[0]);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //CookieCsrfTokenRepository csrfTokenRepository = CookieCsrfTokenRepository.withHttpOnlyFalse();
        //csrfTokenRepository.setCookiePath(config.getUiBasePath());

        http.cors().and().httpBasic().disable()
            .authorizeRequests()
                .antMatchers(_prepareResources(config.getSecurity().getPermitAll())).permitAll()
                .antMatchers(_prepareResources(config.getSecurity().getPermitUser())).hasRole("USER")
                .antMatchers(_prepareResources(config.getSecurity().getPermitExecutor())).hasRole("EXECUTOR")
                .antMatchers(_prepareResources(config.getSecurity().getPermitAdmin())).hasRole("ADMIN")
                .anyRequest().denyAll()
            .and().formLogin()
                .loginPage("/login")
                .failureHandler(customStatusAuthenticationFailureHandler)
                .successHandler(customAuthenticationSuccessHandler)
                .usernameParameter("email")
                .permitAll()
            .and().logout()
                .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.ACCEPTED))
            .and()
                .exceptionHandling()
                .authenticationEntryPoint(customAuthenticationEntryPoint)
                .accessDeniedHandler(customAccessDeniedHandler)
            .and().csrf().disable();
                //.csrfTokenRepository(csrfTokenRepository);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(getCustomAuthenticationProvider());
    }

    private CustomUserDetailsAuthenticationProvider getCustomAuthenticationProvider(){
        CustomUserDetailsAuthenticationProvider customAuthenticationProvider = new CustomUserDetailsAuthenticationProvider();
        customAuthenticationProvider.setUserDetailsService(userService);
        customAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
        return customAuthenticationProvider;
    }

    @Bean
    public RoleHierarchyImpl roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy("ROLE_ADMIN > ROLE_EXECUTOR ROLE_EXECUTOR > ROLE_USER");
        return roleHierarchy;
    }

}