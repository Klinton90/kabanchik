package kabanchik.core.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.Arrays;

@Getter
@Setter
@Component
@Validated
@Configuration
@ConfigurationProperties(prefix = "custom")
public class CustomConfig{

    @Autowired
    protected Environment env;

    public Boolean isDevelopment(){
        return Arrays.asList(env.getActiveProfiles()).contains("dev");
    }

    private String[] uiHref;

    private String uiBasePath;

    private Security security;

    private Files files;

    @Getter
    @Setter
    public static class Security{

        private String[] permitAll;
        private String[] permitAdmin;
        private String[] permitExecutor;
        private String[] permitUser;

    }

    @Getter
    @Setter
    public static class Files{

        private String base;
        private String i18n;
        private String staticContent;
        private String customConfig;
        private String upload;
        private String example;

    }
}