package kabanchik.core.config;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.BeanDeserializerFactory;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext;
import com.fasterxml.jackson.databind.deser.DeserializerFactory;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import kabanchik.core.jackson.DateModule;
import kabanchik.core.jackson.EnumDeserializer;
import kabanchik.core.jackson.StringTrimModule;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.core.aspect.ValidationAspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.validation.SmartValidator;
import org.springframework.web.filter.CorsFilter;
import org.hibernate.SessionFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter{

    @Autowired
    protected CustomConfig config;

    @Autowired
    SmartValidator validator;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ActionInterceptor());
    }

    @Bean
    //Remove for SpringBoot 2
    public SessionFactory sessionFactory(EntityManagerFactory emf) {
        if (emf.unwrap(SessionFactory.class) == null) {
            throw new NullPointerException("factory is not a hibernate factory");
        }
        return emf.unwrap(SessionFactory.class);
    }

    @Bean
    public MessageSource messageSource() {
        String customLocalizationsPath = config.getFiles().getBase() + config.getFiles().getI18n();
        File folder = new File(customLocalizationsPath);
        folder.mkdirs();

        //default message service should be added to set as last entry, due to sequntial order, otherwise it will override actual properties
        ExposedResourceMessageBundleSource messageSource = new ExposedResourceMessageBundleSource();
        messageSource.setBasename("file:" + customLocalizationsPath + "/messages");
        messageSource.addBasenames("classpath:i18n/messages");
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setCacheSeconds(3600);
        return messageSource;
    }

    @Bean
    ObjectMapper getObjectMapper(){
        Deserializers deserializers = new Deserializers.Base() {
            @Override
            public JsonDeserializer<?> findEnumDeserializer(Class<?> type, DeserializationConfig config, BeanDescription beanDesc) throws JsonMappingException{
                return beanDesc.getType().getContentType() != null
                        ? new EnumDeserializer(beanDesc.getType().getContentType().getRawClass())
                        : new EnumDeserializer(beanDesc.getType().getRawClass());
            }
        };

        DeserializerFactory additionalDeserializers = BeanDeserializerFactory.instance.withAdditionalDeserializers(deserializers);
        DefaultDeserializationContext.Impl dc = new DefaultDeserializationContext.Impl(additionalDeserializers);

        ObjectMapper om = new ObjectMapper(null, null, dc);
        return om;
    }

    public MappingJackson2HttpMessageConverter jacksonMessageConverter(){
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = getObjectMapper()
                .registerModule(new Hibernate5Module())
                .registerModule(new StringTrimModule())
                .registerModule(new DateModule());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        messageConverter.setObjectMapper(mapper);
        return messageConverter;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(jacksonMessageConverter());
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
        return sessionLocaleResolver;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Arrays.asList(this.config.getUiHref()));
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        config.setMaxAge(1800L);
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

    @Bean
    @Qualifier("emailsExecutor")
    public Executor emailsExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("emailsExecutor-");
        executor.initialize();
        return executor;
    }

    @Bean
    public ValidationAspect springValidationAspect() {
        return new ValidationAspect(validator);
    }

    //TODO: enable for caching
    /*@Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("configuration");
    }*/

    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler();
    }

    // Of course , you can define the Executor too
    @Bean
    public Executor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }

}