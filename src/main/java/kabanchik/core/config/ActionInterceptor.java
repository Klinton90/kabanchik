package kabanchik.core.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class ActionInterceptor implements HandlerInterceptor{

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        Object path = httpServletRequest.getAttribute("javax.servlet.forward.request_uri");
        if(path != null){
            log.info("Redirected from: "+path.toString());
        }
        return true;
        //return httpServletRequest.getMethod().equals("GET");
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        if(o instanceof HandlerMethod){
            HandlerMethod hm = (HandlerMethod) o;
            String action = hm.getMethod().getName().toLowerCase();
            if(modelAndView != null && (modelAndView.getViewName().length() == 0 || action.equals("index"))){
                String controller = hm.getBeanType().getSimpleName().replace("Controller", "").toLowerCase();
                if(!action.equals("index") && !controller.equals("appservice")){
                    modelAndView.setViewName(controller + "/" + action);
                    modelAndView.addObject("controller", controller);
                    modelAndView.addObject("action", action);
                    log.info("We fixed your path to: '" + modelAndView.getViewName() + "'");
                }
            }
        }
        if(modelAndView != null){
            log.info("Redirect to: "+modelAndView.getViewName());
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
    }

}