package kabanchik.core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import kabanchik.core.controller.response.ErrorRestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        String content = objectMapper.writeValueAsString(new ErrorRestResponse(accessDeniedException.getMessage(), ""));
        response.getWriter().write(content);
        response.setStatus(HttpStatus.FORBIDDEN.value());
    }
}
