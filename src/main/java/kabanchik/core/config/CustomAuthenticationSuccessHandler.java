package kabanchik.core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import kabanchik.core.controller.response.AuthDTO;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.system.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Locale;

@Service
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler{

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    RoleHierarchy roleHierarchy;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException{
        User user = (User)authentication.getPrincipal();
        Collection<? extends GrantedAuthority> authorities = roleHierarchy.getReachableGrantedAuthorities(user.getAuthorities());
        String locale = user.getLocalization() != null ? user.getLocalization().getId() : "";
        localeResolver.setLocale(request, response, new Locale(locale));

        String content = objectMapper.writeValueAsString(new RestResponse(new AuthDTO(user, authorities, authentication.isAuthenticated())));

//        String cookie = response.getHeader("Set-Cookie");
//        if (cookie != null) {
//            response.setHeader("Set-Cookie", cookie + "; SameSite=None; Secure");
//        }

        response.getWriter().write(content);
        response.setStatus(HttpStatus.OK.value());
    }

}
