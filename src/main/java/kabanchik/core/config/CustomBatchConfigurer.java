package kabanchik.core.config;

import lombok.Getter;
import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.explore.support.JobExplorerFactoryBean;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.concurrent.Executor;

@Configuration
public class CustomBatchConfigurer implements BatchConfigurer {

    @Autowired
    DataSource dataSource;

    @Autowired
    Executor taskExecutor;

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    JpaProperties jpaProperties;

    @Getter
    JobRepository jobRepository;
    @Getter
    JobLauncher jobLauncher;
    @Getter
    JobExplorer jobExplorer;
    @Getter
    PlatformTransactionManager transactionManager;

    @PostConstruct
    public void initialize() throws Exception {
        JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactory);
        transactionManager.setDataSource(dataSource);
        transactionManager.setJpaPropertyMap(jpaProperties.getHibernateProperties(dataSource));
        transactionManager.afterPropertiesSet();
        this.transactionManager = transactionManager;

        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDataSource(dataSource);
        factory.setTransactionManager(transactionManager);
        factory.afterPropertiesSet();
        this.jobRepository = factory.getObject();

        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.setTaskExecutor((TaskExecutor) taskExecutor);
        jobLauncher.afterPropertiesSet();
        this.jobLauncher = jobLauncher;

        JobExplorerFactoryBean jobExplorerFactoryBean = new JobExplorerFactoryBean();
        jobExplorerFactoryBean.setDataSource(this.dataSource);
        jobExplorerFactoryBean.afterPropertiesSet();
        this.jobExplorer = jobExplorerFactoryBean.getObject();
    }

}
