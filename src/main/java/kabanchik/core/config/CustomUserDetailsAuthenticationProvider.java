package kabanchik.core.config;

import kabanchik.system.user.UserService;
import kabanchik.system.user.domain.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetailsAuthenticationProvider extends DaoAuthenticationProvider{

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException{
        try{
            super.additionalAuthenticationChecks(userDetails, authentication);
        }catch(AuthenticationException e){
            ((UserService)getUserDetailsService()).lockOnFailedAuthentication((User)userDetails);
            throw e;
        }
    }

    @Override
    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user){
        UserDetails _user = ((UserService)getUserDetailsService()).cleanupOnAuthenticationSuccess((User)user);
        return super.createSuccessAuthentication(principal, authentication, _user);
    }
}
