package kabanchik.core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import kabanchik.core.controller.response.ErrorRestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class CustomStatusAuthenticationFailureHandler implements AuthenticationFailureHandler{

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException{
        String content = objectMapper.writeValueAsString(new ErrorRestResponse(authException.getMessage(), ""));
        response.getWriter().write(content);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
    }

}
