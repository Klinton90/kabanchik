package kabanchik.core.domain;

import kabanchik.core.util.BeanUtils;

import java.io.Serializable;

public interface IDto<E extends IDomain, ID extends Serializable> extends IdAware<ID> {

    default IDto<E, ID> createFromEntity(E entity) {
        BeanUtils.copyProperties(entity, this, true);
        return this;
    }

}
