package kabanchik.core.domain;

import kabanchik.system.genericfieldvalue.dto.GenericFieldValueDTO;

import java.io.Serializable;
import java.util.List;

public interface IGenericFieldAwareDto<E extends IGenericFieldAwareDomain> extends IDto<E, Integer> {

    List<GenericFieldValueDTO> getGenericFieldValues();

    IGenericFieldAwareDto<E> setGenericFieldValues(List<GenericFieldValueDTO> genericFieldValues);

}
