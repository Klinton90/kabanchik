package kabanchik.core.domain;


public interface ActivatableDomain{
    String IS_ACTIVE_FIELD_NAME = "isActive";

    Boolean getIsActive();

    ActivatableDomain setIsActive(Boolean isActive);
}
