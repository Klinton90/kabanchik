package kabanchik.core.domain;

import kabanchik.system.genericfieldvalue.domain.GenericFieldValue;

import java.io.Serializable;
import java.util.List;

public interface IGenericFieldAwareDomain<D extends IGenericFieldAwareDto> extends IDomain<D, Integer> {

    List<GenericFieldValue> getGenericFieldValues();

    IGenericFieldAwareDomain<D> setGenericFieldValues(List<GenericFieldValue> genericFieldValues);

}
