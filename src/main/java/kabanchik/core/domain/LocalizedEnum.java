package kabanchik.core.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import kabanchik.core.service.ExposedResourceMessageBundleSource;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public interface LocalizedEnum{

    @JsonProperty("messageCode")
    default String getMessageCode(){
        return "enum." + this.getClass().getSimpleName() + "." + this.name();
    }

    @JsonProperty("label")
    default String getMessage(){
        return ExposedResourceMessageBundleSource.getMessageSource().getMessage(getMessageCode());
    }

    @JsonProperty("value")
    String name();
}