package kabanchik.core.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;

//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public interface IdAware<ID extends Serializable> extends Serializable {

    public static String ID_FIELD_NAME = "id";

    default Object getDefaultIdValue() {
        return 0;
    }

    ID getId();

    IdAware setId(ID id);
}
