package kabanchik.core.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LocalizedEnumDTO implements LocalizedEnum{
    @JsonProperty("label")
    String label;
    @JsonProperty("value")
    String value;
    @JsonProperty("messageCode")
    String messageCode;

    @Override
    public String name(){
        return value;
    }
}
