package kabanchik.core.domain;

import kabanchik.core.util.BeanUtils;

import java.io.Serializable;

public interface IDomain<D extends IDto, ID extends Serializable> extends IdAware<ID> {

    default IDomain<D, ID> createFromDto(D dto) {
        BeanUtils.copyProperties(dto, this, true);
        return this;
    }

}