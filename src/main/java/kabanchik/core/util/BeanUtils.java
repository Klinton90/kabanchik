package kabanchik.core.util;

import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.IDto;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;
import org.springframework.beans.*;
import org.springframework.util.Assert;

import java.beans.FeatureDescriptor;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kabanchik.core.exception.NotIDtoConvertableException;

public class BeanUtils {

    private static void writeValue(Method writeMethod, Object target, Object value) throws InvocationTargetException, IllegalAccessException {
        if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
            writeMethod.setAccessible(true);
        }
        writeMethod.invoke(target, value);
    }

    public static Object getFieldValue(Object source, String property){
        final BeanWrapper sourceWrapper = new BeanWrapperImpl(source);
        PropertyDescriptor propertyDescriptor = sourceWrapper.getPropertyDescriptor(property);
        if (propertyDescriptor != null) {
            Method readMethod = propertyDescriptor.getReadMethod();
            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                readMethod.setAccessible(true);
            }

            try {
                return readMethod.invoke(source);
            } catch (Throwable ex) {
                throw new FatalBeanException("Could not copy property '" + propertyDescriptor.getName() + "' from source to target", ex);
            }
        } else {
            throw new RuntimeException("Property '" + property + "' not found");
        }
    }

    //Supports deep copy, but be aware of circle references, as there is no protection for that
    public static void copyPropertiesRecursive(Object source, Object target, Boolean ignoreNulls, String ignorePrefix, String... ignoreProperties) {
        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");

        final BeanWrapper targetWrapped = new BeanWrapperImpl(target);
        final BeanWrapper sourceWrapper = new BeanWrapperImpl(source);
        List<String> ignoreList = (ignoreProperties != null ? Arrays.stream(ignoreProperties).map(String::toLowerCase).collect(Collectors.toList()) : null);

        for (PropertyDescriptor targetPd : targetWrapped.getPropertyDescriptors()) {
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null && (ignoreList == null || !ignoreList.contains((ignorePrefix + targetPd.getName()).toLowerCase()))) {

                try {
                    PropertyDescriptor sourcePd = sourceWrapper.getPropertyDescriptor(targetPd.getName());
                    if (sourcePd != null) {
                        Method readMethod = sourcePd.getReadMethod();
                        if (readMethod != null && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                            try {
                                if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                    readMethod.setAccessible(true);
                                }
                                Object value = readMethod.invoke(source);

                                if (value == null) {
                                    if (!ignoreNulls) {
                                        writeValue(writeMethod, target, value);
                                    }
                                } else if (org.springframework.beans.BeanUtils.isSimpleValueType(value.getClass()) || BigDecimal.class.isAssignableFrom(value.getClass())) {
                                    writeValue(writeMethod, target, value);
                                } else {
                                    Method targetReadMethod = targetPd.getReadMethod();
                                    if (targetReadMethod != null) {
                                        if (!Modifier.isPublic(targetReadMethod.getDeclaringClass().getModifiers())) {
                                            targetReadMethod.setAccessible(true);
                                        }
                                        Object targetValue = targetReadMethod.invoke(target);
                                        if (targetValue == null) {
                                            writeValue(writeMethod, target, value);
                                        } else {
                                            copyPropertiesRecursive(value, targetValue, ignoreNulls, ignorePrefix + "." + target.getClass().getSimpleName(), ignoreProperties);
                                        }
                                    } else {
                                        writeValue(writeMethod, target, value);
                                    }
                                }
                            } catch (Throwable ex) {
                                throw new FatalBeanException("Could not copy property '" + targetPd.getName() + "' from source to target", ex);
                            }
                        }
                    }
                } catch (InvalidPropertyException e) {
                    //setter doesn't exist, but that is expected, do nothing
                }
            }
        }
    }

    public static String[] getNullPropertyNames(Object source, List<String> nullableProperties) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> !nullableProperties.contains(propertyName) && wrappedSource.getPropertyValue(propertyName) == null)
                .toArray(String[]::new);
    }


    public static void copyProperties(Object src, Object target, Boolean ignoreNulls) {
        String[] ignoreProperties = {};
        copyProperties(src, target, ignoreNulls, ignoreProperties);
    }

    public static void copyProperties(Object src, Object target, Boolean ignoreNulls, String... ignoreProperties){
        String[] nullableProperties = {};
        copyProperties(src, target, ignoreNulls, ignoreProperties, nullableProperties);
    }

    public static void copyProperties(Object src, Object target, Boolean ignoreNulls, String[] ignoreProperties, String[] nullableProperties) {
        String[] joinedArray = ignoreProperties;
        if (ignoreNulls) {
            String[] nullPropertyNames = getNullPropertyNames(src, Arrays.asList(nullableProperties));
            joinedArray = ArrayUtils.addAll(ignoreProperties, nullPropertyNames);
        }
        org.springframework.beans.BeanUtils.copyProperties(src, target, joinedArray);
    }

    public static <T> Collector<T, ?, List<T>> toListOrEmpty() {
        return Collectors.collectingAndThen(
                Collectors.toList(),
                l -> l.isEmpty() ? new ArrayList<>() : l
        );
    }

    public static Field getDeclaredFieldRecursive(Class<?> clazz, String fieldName){
        Field field = null;
        while (clazz != null && field == null) {
            try {
                field = clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            clazz = clazz.getSuperclass();
        }
        return field;
    }

    public static List<IDto<IDomain, Serializable>> tryConvertListToIDtos(List<?> data) throws IllegalAccessException, InstantiationException, NotIDtoConvertableException {
        if (data != null && data.size() > 0 && data.get(0) != null) {
            if (IDomain.class.isAssignableFrom(data.get(0).getClass())) {
                List<IDto<IDomain, Serializable>> result = new ArrayList<>();

                ParameterizedType pType = null;
                for (Type type : data.get(0).getClass().getGenericInterfaces()) {
                    if (ParameterizedType.class.isAssignableFrom(type.getClass())) {
                        pType = (ParameterizedType) type;
                        break;
                    }
                }

                if (pType != null) {
                    for (Object item : data) {
                        result.add(convertToIDto(item, pType));
                    }

                    return result;
                }
            } else {
                throw new NotIDtoConvertableException();
            }
        }
        return null;
    }

    public static IDto<IDomain, Serializable> convertToIDto(Object data) throws IllegalAccessException, InstantiationException, NotIDtoConvertableException {
        if (IDomain.class.isAssignableFrom(data.getClass())) {
            ParameterizedType pType = null;
            for (Type type : data.getClass().getGenericInterfaces()) {
                if (ParameterizedType.class.isAssignableFrom(type.getClass())) {
                    pType = (ParameterizedType) type;
                    break;
                }
            }

            return convertToIDto(data, pType);
        } else {
            throw new NotIDtoConvertableException();
        }
    }

    public static IDto<IDomain, Serializable> convertToIDto(Object data, ParameterizedType pType) throws IllegalAccessException, InstantiationException, NotIDtoConvertableException {
        if (IDomain.class.isAssignableFrom(data.getClass()) && pType != null) {
            @SuppressWarnings("unchecked")
            IDto<IDomain, Serializable> dto = (IDto<IDomain, Serializable>) ((Class) ((pType).getActualTypeArguments()[0])).newInstance();
            return dto.createFromEntity((IDomain) data);
        } else {
            throw new NotIDtoConvertableException();
        }
    }

}
