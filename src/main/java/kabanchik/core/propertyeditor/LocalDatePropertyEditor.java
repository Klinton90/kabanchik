package kabanchik.core.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDatePropertyEditor extends PropertyEditorSupport {

    private final DateTimeFormatter formatter;

    public LocalDatePropertyEditor() {
        formatter = DateTimeFormatter.ISO_LOCAL_DATE;
    }

    public LocalDatePropertyEditor(String pattern) {
        formatter = DateTimeFormatter.ofPattern(pattern);
    }

    public String getAsText() {
        LocalDate value = (LocalDate)getValue();
        return value != null ? value.format(formatter) : "";
    }

    public void setAsText(String text) throws IllegalArgumentException {
        if (!text.isEmpty()) {
            try {
                setValue(LocalDate.parse(text, formatter));
            } catch(Exception e) {
                setValue(null);
            }
        } else {
            setValue(null);
        }
    }

}