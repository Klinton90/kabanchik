package kabanchik.core.batch;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.exception.SpringValidationException;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import lombok.Setter;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.listener.ItemListenerSupport;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.NonTransientFlatFileException;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;

import static kabanchik.core.batch.BatchBeansFactory.ITEM_READER_SKIP_FIRST_LINE;
import static kabanchik.core.batch.BatchBeansFactory.JOINED_FIELDS_PAR;
import static kabanchik.core.batch.BatchBeansFactory.SKIP_FIRST_LINE_PAR;
import static kabanchik.core.batch.CustomExecutionContextPromotionListener.ERROR_KEY;

@StepScope
public class ItemFailureLoggerListener extends ItemListenerSupport<ErrorReportingItemDTO, ErrorReportingItemDTO> implements StepExecutionListener, InitializingBean {

    private StepExecution stepExecution;

    private BeanWrapperFieldExtractor<ErrorReportingItemDTO> extractor;

    private CustomDelimitedLineAggregator<ErrorReportingItemDTO> lineAggregator;

    private String[] fieldNames;

    private Boolean skipFirstLine;

    @Setter
    private ExposedResourceMessageBundleSource messageSource;

    private String buildEmptyRow () {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < fieldNames.length - 1; i++) {
            sb.append(",");
        }

        return sb.toString();
    }

    public ItemFailureLoggerListener() {
        extractor = new BeanWrapperFieldExtractor<>();

        lineAggregator = new CustomDelimitedLineAggregator<>();
        lineAggregator.setDelimiter(",");
        lineAggregator.setFieldExtractor(extractor);
    }

    @Override
    @BeforeStep
    public void beforeStep(StepExecution stepExecution) {
        fieldNames = stepExecution.getJobParameters().getString(JOINED_FIELDS_PAR).split(",");
        skipFirstLine = stepExecution.getJobParameters().getString(SKIP_FIRST_LINE_PAR).equals(ITEM_READER_SKIP_FIRST_LINE);
        extractor.setNames(fieldNames);

        this.stepExecution = stepExecution;
    }

    @Override
    @AfterStep
    public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }

    @Override
    public void onReadError(Exception e) {
        String errorKey;
        String errorText;

        if (e instanceof NonTransientFlatFileException) {
            NonTransientFlatFileException ntffe = (NonTransientFlatFileException) e;
            Integer lineNumber = ntffe.getLineNumber();
            if (skipFirstLine) {
                lineNumber--;
            }
            errorKey = ERROR_KEY + lineNumber;
            errorText = ntffe.getInput() + "," + lineNumber + ",\"" + messageSource.getMessage("app.error.batch.flatFileRead") + "\"";
        } else if (e instanceof FlatFileParseException) {
            FlatFileParseException ffpe = (FlatFileParseException) e;
            Integer lineNumber = ffpe.getLineNumber();
            if (skipFirstLine) {
                lineNumber--;
            }
            errorKey = ERROR_KEY + lineNumber;
            errorText = ffpe.getInput() + "," + lineNumber + ",\"" + messageSource.getMessage("app.error.batch.flatFileParse") + "\"";
        } else {
            // TODO: check if `readCount` works in this case
            errorKey = ERROR_KEY + stepExecution.getReadCount();
            errorText = buildEmptyRow() + stepExecution.getReadCount() + ",\"" + messageSource.getMessage("app.error.batch.readUnhandled") + "\"";
        }

        stepExecution.getExecutionContext().put(errorKey, errorText);
    }

    @Override
    public void onProcessError(ErrorReportingItemDTO item, Exception e) {
        String errorText;

        if (e instanceof CustomValidationException) {
            CustomValidationException _e = (CustomValidationException)e;
            errorText = messageSource.getValidationMessage(_e).getMessage();
            if (!StringUtils.isEmpty(_e.getField()) && !StringUtils.isEmpty(_e.getObjectName())) {
                errorText = messageSource.tryResolveFieldName(_e.getObjectName(), _e.getField()) + ": " + errorText;
            }
        } else if (e instanceof InvalidInputException) {
            errorText = messageSource.getValidationMessage((InvalidInputException)e).getMessage();
        } else if (e instanceof MethodArgumentNotValidException) {
            BindingResult bindingResult = ((MethodArgumentNotValidException)e).getBindingResult();
            errorText = messageSource.tryResolveFieldName(bindingResult) + ": " + messageSource.getValidationMessage(bindingResult).getMessage();
        } else if (e instanceof SpringValidationException) {
            BindingResult bindingResult = ((SpringValidationException)e).getBindingResult();
            errorText = messageSource.tryResolveFieldName(bindingResult) + ": " + messageSource.getValidationMessage(bindingResult).getMessage();
        } else {
            errorText = messageSource.getMessage("app.error.batch.processingUnhandled", new Object[]{e.getMessage()});
        }

        stepExecution.getExecutionContext().put(ERROR_KEY + item.getItemCount(), lineAggregator.aggregate(item) + "," + item.getItemCount() + ",\"" + errorText + "\"");
    }

    @Override
    public void onWriteError(Exception e, List<? extends ErrorReportingItemDTO> items) {
        String errorMessage = messageSource.getMessage("app.error.batch.writeUnhandled", new Object[]{e.getMessage()});

        for (ErrorReportingItemDTO item: items) {
            stepExecution.getExecutionContext().put(ERROR_KEY + item.getItemCount(), item.toErrorReportingString() + "," + item.getItemCount() + ",\"" + errorMessage + "\"");
        }
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(messageSource, "`msgSource` must be set");
    }

}
