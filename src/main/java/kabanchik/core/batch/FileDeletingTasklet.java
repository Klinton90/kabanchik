package kabanchik.core.batch;

import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.core.service.FileStorageService;
import lombok.Setter;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import java.net.URI;

import static kabanchik.core.batch.BatchBeansFactory.FILE_PATH_PAR;
import static kabanchik.core.batch.CustomExecutionContextPromotionListener.ERROR_KEY;

public class FileDeletingTasklet implements Tasklet, InitializingBean {

    private static final String FOOTER_KEY = "FOOTER";

    @Setter
    private ExposedResourceMessageBundleSource messageSource;

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        try {
            FileStorageService.deleteFile(new URI(chunkContext.getStepContext().getStepExecution().getJobParameters().getString(FILE_PATH_PAR)));
        } catch (Exception e) {
            chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put(
                    ERROR_KEY + FOOTER_KEY,
                    e.getMessage() + messageSource.getMessage("app.error.batch.fileDeletingUnhandled"));
        }
        return RepeatStatus.FINISHED;
    }

    public void afterPropertiesSet() {
        Assert.notNull(messageSource, "`messageSource` must be set");
    }

}
