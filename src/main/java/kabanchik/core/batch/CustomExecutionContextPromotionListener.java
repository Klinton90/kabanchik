package kabanchik.core.batch;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.support.PatternMatcher;

import java.util.Map;

public class CustomExecutionContextPromotionListener extends StepExecutionListenerSupport {

    public static final String ERROR_KEY = "AT_ERROR_";

    private String[] statuses = new String[] { ExitStatus.COMPLETED.getExitCode(), ExitStatus.FAILED.getExitCode() };

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        ExecutionContext stepContext = stepExecution.getExecutionContext();
        ExecutionContext jobContext = stepExecution.getJobExecution().getExecutionContext();
        String exitCode = stepExecution.getExitStatus().getExitCode();
        for (String statusPattern : statuses) {
            if (PatternMatcher.match(statusPattern, exitCode)) {
                for (Map.Entry<String, Object> entry: stepContext.entrySet()) {
                    if (entry.getKey().startsWith(ERROR_KEY)) {
                        jobContext.put(entry.getKey(), entry.getValue());
                    }
                }
                break;
            }
        }

        return null;
    }

}
