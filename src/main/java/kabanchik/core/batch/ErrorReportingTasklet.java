package kabanchik.core.batch;

import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.core.service.FileStorageService;
import lombok.Setter;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.UrlResource;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

import static kabanchik.core.batch.BatchBeansFactory.FILE_PATH_PAR;
import static kabanchik.core.batch.BatchBeansFactory.JOINED_FIELDS_PAR;
import static kabanchik.core.batch.BatchBeansFactory.REPORT_PATH_PAR;
import static kabanchik.core.batch.CustomExecutionContextPromotionListener.ERROR_KEY;

public class ErrorReportingTasklet implements Tasklet, InitializingBean {

    private final static String REPORT_FILE_NAME_SUFFIX = "_report";

    @Setter
    private FlatFileItemWriter<String> itemWriter;

    @Setter
    private ExposedResourceMessageBundleSource messageSource;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        ExecutionContext jobExecutionContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();

        List<String> lines = jobExecutionContext.entrySet().stream()
                .filter(entry -> entry.getKey().startsWith(ERROR_KEY))
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(entry -> entry.getValue().toString())
                .collect(Collectors.toList());

        if (lines != null && lines.size() > 0) {
            Map<String, Object> jobParameters = chunkContext.getStepContext().getJobParameters();

            long jobId = chunkContext.getStepContext().getStepExecution().getJobExecution().getJobId();
            String reportPath = FileStorageService.appendToFileName(jobParameters.get(FILE_PATH_PAR).toString(), "_" + jobId + REPORT_FILE_NAME_SUFFIX);
            jobExecutionContext.put(REPORT_PATH_PAR, reportPath);

            itemWriter.setResource(new UrlResource(reportPath));
            itemWriter.setHeaderCallback(writer -> writer.write(jobParameters.get(JOINED_FIELDS_PAR).toString() + "," + messageSource.getMessage("app.error.batch.errorReportHeaderSuffix")));

            itemWriter.open(chunkContext.getStepContext().getStepExecution().getExecutionContext());
            itemWriter.write(lines);
            itemWriter.close();
        }

        return RepeatStatus.FINISHED;
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(itemWriter, "`itemWriter` must be set");
        Assert.notNull(messageSource, "`messageSource` must be set");
    }

}
