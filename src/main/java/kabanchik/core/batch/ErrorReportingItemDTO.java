package kabanchik.core.batch;

import org.springframework.batch.item.ItemCountAware;

public interface ErrorReportingItemDTO extends ItemCountAware {

    String toErrorReportingString();

    Integer getItemCount();

}
