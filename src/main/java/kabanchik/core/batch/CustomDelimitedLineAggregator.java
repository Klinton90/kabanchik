package kabanchik.core.batch;

import org.springframework.batch.item.file.transform.ExtractorLineAggregator;
import org.springframework.util.ObjectUtils;

public class CustomDelimitedLineAggregator<O extends ErrorReportingItemDTO> extends ExtractorLineAggregator<O> {

    private String delimiter = ",";

    /**
     * Public setter for the delimiter.
     * @param delimiter the delimiter to set
     */
    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public String doAggregate(Object[] fields) {
        if (ObjectUtils.isEmpty(fields)) {
            return "";
        }
        if (fields.length == 1) {
            return ObjectUtils.nullSafeToString(fields[0]);
        }

        StringBuilder sb = new StringBuilder();
        sb.append("\"");
        for (int i = 0; i < fields.length; i++) {
            if (i > 0) {
                sb.append("\"").append(delimiter).append("\"");
            }
            sb.append(fields[i]);
        }
        sb.append("\"");
        return sb.toString();
    }

}
