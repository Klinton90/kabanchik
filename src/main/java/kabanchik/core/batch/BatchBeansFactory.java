package kabanchik.core.batch;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.exception.SpringValidationException;
import kabanchik.core.propertyeditor.LocalDatePropertyEditor;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.builder.SimpleStepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.NonTransientFlatFileException;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.PassThroughLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TransactionRequiredException;
import java.beans.PropertyEditor;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class BatchBeansFactory {

    public static final String FILE_PATH_PAR = "filePath";

    public static final String SKIP_FIRST_LINE_PAR = "skipFirstLine";

    public static final String JOINED_FIELDS_PAR = "joinedFields";

    public static final String CLASS_NAME_PAR = "className";

    public static final String TIMESTAMP_PAR = "timestamp";

    public static final String REPORT_PATH_PAR = "reportPath";

    public static final String ITEM_READER_SKIP_FIRST_LINE = "TRUE";

    private static final String WILL_BE_INJECTED = null;

    private static final Integer CHUNK_SIZE = 2;

    private FileDeletingTasklet fileDeletingTasklet;

    private JpaItemWriter<ErrorReportingItemDTO> jpaItemWriter;

    private CustomExecutionContextPromotionListener customExecutionContextPromotionListener;

    private ItemFailureLoggerListener itemFailureLoggerListener;

    private ErrorReportingTasklet errorReportingTasklet;

    private Step fileCleanupStep;

    private Step errorReportingStep;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    ExposedResourceMessageBundleSource messageSource;

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    //@Bean
    public <I extends ErrorReportingItemDTO, O extends ErrorReportingItemDTO> Job getCsvImportJob(ItemProcessor<I, O> itemProcessor) throws Exception {
        return getCsvImportJob(getCsvImportStep(itemProcessor));
    }

    public Job getCsvImportJob(Step csvImportStep) throws Exception {
        return jobBuilderFactory.get("csvImportJob")
                .start(csvImportStep)
                .next(getFileCleanupStep())
                .next(getErrorReportingStep())
                .build();
    }

    //@Bean
    //@SuppressWarnings("unchecked")
    private <I extends ErrorReportingItemDTO, O extends ErrorReportingItemDTO> Step getCsvImportStep(ItemProcessor<I, O> itemProcessor) throws Exception {
        return ((SimpleStepBuilder)stepBuilderFactory
                .get("csvImportStep")
                .<I, O>chunk(CHUNK_SIZE)

                .reader(getItemReader(WILL_BE_INJECTED, WILL_BE_INJECTED, WILL_BE_INJECTED, WILL_BE_INJECTED))
                .processor(itemProcessor)
                .writer(getJpaItemWriter())

                .listener(getContextPromotionListener())
                .listener(getItemFailureListener()))

                .faultTolerant()
                .skipLimit(CHUNK_SIZE + 1)
                // Read exceptions
                .skip(FlatFileParseException.class)
                .skip(NonTransientFlatFileException.class)
                // Process exceptions
                .skip(CustomValidationException.class)
                .skip(InvalidInputException.class)
                .skip(MethodArgumentNotValidException.class)
                .skip(SpringValidationException.class)
                // Write exceptions
                .skip(IllegalArgumentException.class)
                .skip(TransactionRequiredException.class)
                .skip(DataAccessResourceFailureException.class)
                // Shared exceptions - think if this is needed
                .skip(Exception.class)
                .skip(RuntimeException.class)

                .build();
    }

    //@Bean
    public Step getFileCleanupStep() {
        if (fileCleanupStep == null) {
            fileCleanupStep = stepBuilderFactory
                    .get("fileCleanupStep")
                    .tasklet(getFileDeletingTasklet())
                    .build();
        }
        return fileCleanupStep;
    }

    //@Bean
    public Step getErrorReportingStep() throws Exception {
        if (errorReportingStep == null) {
            errorReportingStep = stepBuilderFactory
                    .get("errorReportingStep")
                    .tasklet(getErrorReportingTasklet())
                    .build();
        }
        return errorReportingStep;
    }

    //@Bean
    public JpaItemWriter<ErrorReportingItemDTO> getJpaItemWriter() {
        if (jpaItemWriter == null) {
            jpaItemWriter = new JpaItemWriter<>();
            jpaItemWriter.setEntityManagerFactory(entityManagerFactory);
        }
        return jpaItemWriter;
    }

    //@Bean
    public FileDeletingTasklet getFileDeletingTasklet() {
        if (fileDeletingTasklet == null) {
            fileDeletingTasklet = new FileDeletingTasklet();
            fileDeletingTasklet.setMessageSource(messageSource);
            fileDeletingTasklet.afterPropertiesSet();
        }

        return fileDeletingTasklet;
    }

    //@Bean
    public ErrorReportingTasklet getErrorReportingTasklet() throws Exception {
        if (errorReportingTasklet == null) {
            errorReportingTasklet = new ErrorReportingTasklet();
            errorReportingTasklet.setItemWriter(getErrorReportingItemWriter());
            errorReportingTasklet.setMessageSource(messageSource);

            errorReportingTasklet.afterPropertiesSet();
        }
        return errorReportingTasklet;
    }

    //@Bean
    public CustomExecutionContextPromotionListener getContextPromotionListener() {
        if (customExecutionContextPromotionListener == null) {
            customExecutionContextPromotionListener = new CustomExecutionContextPromotionListener();
        }

        return customExecutionContextPromotionListener;
    }

    //@Bean
    public ItemFailureLoggerListener getItemFailureListener() {
        if (itemFailureLoggerListener == null) {
            itemFailureLoggerListener = new ItemFailureLoggerListener();
            itemFailureLoggerListener.setMessageSource(messageSource);
            itemFailureLoggerListener.afterPropertiesSet();
        }

        return itemFailureLoggerListener;
    }

    //@Bean
    public FlatFileItemWriter<String> getErrorReportingItemWriter() throws Exception {
        FlatFileItemWriter<String> flatFileItemWriter = new FlatFileItemWriter<>();
        flatFileItemWriter.setLineAggregator(new PassThroughLineAggregator<>());
        flatFileItemWriter.setShouldDeleteIfExists(true);
        flatFileItemWriter.afterPropertiesSet();

        return flatFileItemWriter;
    }

    @Bean
    @StepScope
    public <I extends ErrorReportingItemDTO> FlatFileItemReader<I> getItemReader(
            @Value("#{jobParameters['"+FILE_PATH_PAR+"']}") String filePath,
            @Value("#{jobParameters['"+JOINED_FIELDS_PAR+"']}") String joinedFields,
            @Value("#{jobParameters['"+CLASS_NAME_PAR+"']}") String className,
            @Value("#{jobParameters['"+SKIP_FIRST_LINE_PAR+"']}") String skipFirstLine
    ) throws Exception {
        Class clazz = Class.forName(className);
        String[] fields = joinedFields.split(",");

        FlatFileItemReader<I> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setLineMapper(getLineMapper(clazz, fields));
        flatFileItemReader.setResource(new UrlResource(filePath));
        flatFileItemReader.afterPropertiesSet();
        if (skipFirstLine.equals(ITEM_READER_SKIP_FIRST_LINE)) {
            flatFileItemReader.setLinesToSkip(1);
        }

        return flatFileItemReader;
    }

    private static <I extends ErrorReportingItemDTO> BeanWrapperFieldSetMapper<I> getFieldSetMapper(Class<I> clazz) throws Exception {
        Map<Object, PropertyEditor> editors = new HashMap<>();
        editors.put(LocalDate.class, new LocalDatePropertyEditor());

        BeanWrapperFieldSetMapper<I> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(clazz);
        fieldSetMapper.setCustomEditors(editors);
        fieldSetMapper.afterPropertiesSet();

        return fieldSetMapper;
    }

    private static DelimitedLineTokenizer getLineTokenizer(String[] fields) {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setNames(fields);

        return lineTokenizer;
    }

    private static <I extends ErrorReportingItemDTO> DefaultLineMapper<I> getLineMapper(Class<I> clazz, String[] fields) throws Exception {
        DefaultLineMapper<I> lineMapper = new DefaultLineMapper<>();
        lineMapper.setFieldSetMapper(getFieldSetMapper(clazz));
        lineMapper.setLineTokenizer(getLineTokenizer(fields));
        lineMapper.afterPropertiesSet();
        return lineMapper;
    }

}
