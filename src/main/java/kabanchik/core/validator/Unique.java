package kabanchik.core.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {UniqueValidator.class})
@Target({TYPE})
@Retention(RUNTIME)
public @interface Unique{
    String message() default "Record [${name}] with parameter(s) [${allFields}] and value(s) [${values}] already exists in DataBase";

    UniqueColumn[] columns() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}