package kabanchik.core.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {NotValueValidator.class})
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RUNTIME)
public @interface NotValue{

    String message() default "Not valid value";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] value();

    boolean isCaseSensitive() default true;

}
