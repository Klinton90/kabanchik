package kabanchik.core.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordSizeValidator implements ConstraintValidator<PasswordSize, String>{
    private int min;
    private int max;

    @Override
    public void initialize(PasswordSize annotation){
        min = annotation.min();
        max = annotation.max();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context){
        return value == null || value.length() == 0 || (value.length() >= min && value.length() <= max);
    }
}