package kabanchik.core.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class NotValueValidator implements ConstraintValidator<NotValue, String>{

    List<String> values;

    boolean isCaseSensitive;

    @Override
    public void initialize(NotValue notValue){
        isCaseSensitive = notValue.isCaseSensitive();
        values = Arrays.asList(notValue.value());
        if(!isCaseSensitive){
            values.forEach(String::toUpperCase);
        }
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext){
        return !values.contains(isCaseSensitive ? s : s.toUpperCase());
    }

}
