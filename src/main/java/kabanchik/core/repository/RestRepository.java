package kabanchik.core.repository;

import com.cosium.spring.data.jpa.entity.graph.repository.EntityGraphJpaRepository;
import com.cosium.spring.data.jpa.entity.graph.repository.EntityGraphQueryDslPredicateExecutor;
//import com.cosium.spring.data.jpa.entity.graph.repository.EntityGraphQuerydslPredicateExecutor;
import kabanchik.core.domain.IdAware;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import java.io.Serializable;

@NoRepositoryBean
public interface RestRepository<E extends IdAware<ID>, ID extends Serializable> extends EntityGraphJpaRepository<E, ID>, EntityGraphQueryDslPredicateExecutor<E> {

    EntityManager getEntityManager();

    JpaEntityInformation<E, Integer> getEntityInformation();

}
