package kabanchik.core.repository;

//import com.cosium.spring.data.jpa.entity.graph.repository.support.EntityGraphQuerydslRepository;
import com.cosium.spring.data.jpa.entity.graph.repository.support.QueryDslEntityGraphRepository;
import kabanchik.core.domain.IdAware;
import lombok.Getter;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import java.io.Serializable;

@NoRepositoryBean
public class RestRepositoryImpl<T extends IdAware<ID>, ID extends Serializable> extends QueryDslEntityGraphRepository<T, ID> implements RestRepository<T, ID> {

    @Getter
    EntityManager entityManager;

    @Getter
    JpaEntityInformation<T, Integer> entityInformation;

    public RestRepositoryImpl(JpaEntityInformation<T, Integer> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.entityInformation = entityInformation;
    }

    /*public RestRepositoryImpl(JpaEntityInformation<T, Integer> entityInformation, EntityManager entityManager, EntityPathResolver resolver) {
        super(entityInformation, entityManager, resolver);
        this.entityManager = entityManager;
        this.entityInformation = entityInformation;
    }*/

}

