package kabanchik.core.aspect;

import kabanchik.core.exception.SpringValidationException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.annotation.Validated;

@Aspect
public class ValidationAspect{

    SmartValidator validator;

    public ValidationAspect(SmartValidator validator){
        this.validator = validator;
    }

    @Before("execution(public * *(@javax.validation.Valid (*), ..)) "
            + "&& @annotation(org.springframework.validation.annotation.Validated) "
            + "&& args(validationTarget, ..)")
    public void beforeMethodThatNeedsValidation(JoinPoint call, Object validationTarget) throws SpringValidationException{
        validate(call, validationTarget);
    }

    private void validate(JoinPoint call, Object arg) throws SpringValidationException{
        MethodSignature signature = (MethodSignature)call.getSignature();
        Validated annotation = signature.getMethod().getAnnotation(Validated.class);

        BindingResult errors = new BeanPropertyBindingResult(arg, arg.getClass().getSimpleName());
        validator.validate(arg, errors, annotation.value());
        if (errors.hasErrors()) {
            throw new SpringValidationException(errors);
        }
    }
}