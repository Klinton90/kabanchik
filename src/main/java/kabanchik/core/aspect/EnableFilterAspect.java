package kabanchik.core.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Aspect
@Component
public class EnableFilterAspect {

    @AfterReturning(
            pointcut="bean(entityManagerFactory) && execution(* createEntityManager(..))",
            returning="retVal")
    public void getSessionAfter(JoinPoint joinPoint, Object retVal) {
        if (retVal != null && EntityManager.class.isInstance(retVal)) {
            Session session = ((EntityManager) retVal).unwrap(Session.class);

            session.enableFilter("outcomeEntryParentSubcontractor");
            session.enableFilter("outcomeEntryParentSupplier");

            session.enableFilter("progressParentSubcontractor");
            session.enableFilter("progressParentDepartment");

            session.enableFilter("genericFieldValueTimesheetCategory");
            session.enableFilter("genericFieldValueUser");
        }
    }

}
