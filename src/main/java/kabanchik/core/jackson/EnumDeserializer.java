package kabanchik.core.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import kabanchik.core.domain.LocalizedEnumDTO;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class EnumDeserializer<E extends Enum<E>> extends StdDeserializer<E> implements ContextualDeserializer{

    private Map<String, E> mapper;

    public EnumDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public E deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException{
        String key = "";
        TreeNode node = parser.readValueAsTree();
        if (node instanceof IntNode) {
            key = this.handledType().getEnumConstants()[((IntNode) node).asInt()].toString();
        }else if(node instanceof TextNode){
            key = ((TextNode)node).asText();
        }else if(node instanceof ObjectNode){
            JsonParser dtoParser = node.traverse();
            dtoParser.setCodec(parser.getCodec());
            LocalizedEnumDTO dto = dtoParser.readValueAs(new TypeReference<LocalizedEnumDTO>(){});
            key = dto.name();
        }

        return mapper.get(prepareKey(key));
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException{
        @SuppressWarnings("unchecked")
        Class<E> clazz = (Class<E>)ctxt.getContextualType().getRawClass();
        E[] enums = clazz.getEnumConstants();
        Map<String, E> temp = new HashMap<>(enums.length);
        for(E _enum: enums){
            temp.put(prepareKey(_enum.toString()), _enum);
        }
        mapper = Collections.unmodifiableMap(temp);
        return this;
    }

    private String prepareKey(String key){
        return key.replace("_", "").toLowerCase();
    }
}
