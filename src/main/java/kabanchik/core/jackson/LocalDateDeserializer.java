package kabanchik.core.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.TextNode;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;

public class LocalDateDeserializer extends StdDeserializer<LocalDate> {

    public LocalDateDeserializer() {
        this(null);
    }

    public LocalDateDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LocalDate deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
        Long value;
        TreeNode node = parser.readValueAsTree();
        if (node instanceof LongNode) {
            value = ((LongNode) node).longValue();
        } else if (node instanceof TextNode) {
            String text = ((TextNode) node).asText();
            if (StringUtils.isNotBlank(text)) {
                value = Long.valueOf(text);
            } else {
                return null;
            }
        } else if (node instanceof IntNode) {
            value = ((IntNode) node).longValue();
        } else {
            throw new IOException("Cannot convert provided value to LocalDate: " + node.toString());
        }

        return timestampToDate(value);
    }

    public static LocalDate timestampToDate(Long timestamp) {
        return Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.UTC).toLocalDate();
    }

}
