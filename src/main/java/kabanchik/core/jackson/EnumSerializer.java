package kabanchik.core.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.util.StringUtils;

import java.io.IOException;

public class EnumSerializer<E extends Enum<E>> extends StdSerializer<E>{

    public EnumSerializer() {
        this(null);
    }

    public EnumSerializer(Class t) {
        super(t);
    }

    @Override
    public void serialize(E value, JsonGenerator gen, SerializerProvider provider) throws IOException{
        String[] enumNameParts = value.toString().toLowerCase().split("_");

        StringBuilder result = new StringBuilder();
        for(String enumNamePart : enumNameParts){
            result.append(StringUtils.capitalize(enumNamePart));
        }

        gen.writeString(result.toString());
    }
}
