package kabanchik.core.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeDeserializer extends StdDeserializer<LocalDateTime> {

    public LocalDateTimeDeserializer() {
        this(null);
    }

    public LocalDateTimeDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LocalDateTime deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Long value;
        TreeNode node = parser.readValueAsTree();
        if (node instanceof LongNode) {
            value = ((LongNode) node).longValue();
        } else if (node instanceof TextNode) {
            value = Long.valueOf(((TextNode) node).asText());
        } else if (node instanceof IntNode) {
            value = ((IntNode) node).longValue();
        } else {
            throw new IOException("Cannot convert provided value to LocalDateTime: " + node.toString());
        }

        return timestampToDateTime(value);
    }

    public static LocalDateTime timestampToDateTime(Long timestamp) {
        return Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.UTC).toLocalDateTime();
    }

}
