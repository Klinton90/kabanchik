package kabanchik.core.exception;

import lombok.Getter;
import org.springframework.validation.BindingResult;

public class SpringValidationException extends RuntimeException {

    @Getter
    private BindingResult bindingResult;

    public SpringValidationException(final BindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }
}
