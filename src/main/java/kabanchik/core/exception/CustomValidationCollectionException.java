package kabanchik.core.exception;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class CustomValidationCollectionException extends Exception {

    private List<CustomValidationException> exceptions = new ArrayList<>();

    public void add(CustomValidationCollectionException e) {
        e.exceptions.forEach(this::add);
    }

    public void add(CustomValidationException e) {
        exceptions.add(e);
    }

    public void throwIfAny() throws CustomValidationCollectionException {
        if (!exceptions.isEmpty()) {
            throw this;
        }
    }

}
