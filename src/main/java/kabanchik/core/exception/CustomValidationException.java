package kabanchik.core.exception;

import kabanchik.core.domain.IDomain;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;

@Getter
@Setter
@Accessors(chain = true)
public class CustomValidationException extends BaseException{

    private String objectName;

    private String field;

    private String id;

    public CustomValidationException(String messageCode, String objectName) {
        this(messageCode, objectName, null);
    }

    public CustomValidationException(String messageCode, String objectName, Object... messageParams) {
        this(messageCode, objectName, "", "", null, messageParams);
    }

    public CustomValidationException(String messageCode, String objectName, String field, String id, Object data, Object... messageParams) {
        super(messageCode, messageParams, data);
        this.objectName = objectName;
        this.field = field;
        this.id = id;
    }

    public CustomValidationException(BindingResult bindingResult) {
        this(
                StringUtils.uncapitalize(bindingResult.getFieldError().getCode()),
                bindingResult.getObjectName(),
                bindingResult.getFieldError().getField(),
                IDomain.class.isAssignableFrom(bindingResult.getTarget().getClass()) ? ((IDomain)bindingResult.getTarget()).getId().toString() : "",
                bindingResult.getTarget(),
                ArrayUtils.addAll(
                        bindingResult.getFieldError().getArguments(),
                        bindingResult.getFieldError().getField(),
                        bindingResult.getFieldError().getRejectedValue(),
                        bindingResult.getObjectName()));
    }

}
