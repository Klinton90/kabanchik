package kabanchik.core.exception;

/**
 * Use that exception only for real error handling when it has nothing to do with real validation.
 */
public class InvalidInputException extends BaseException{

    public InvalidInputException(String messageCode){
        this(messageCode, null);
    }

    public InvalidInputException(String message, Object[] messageParams){
        this(message, messageParams, null);
    }

    public InvalidInputException(String message, Object[] messageParams, Object data){
        super(message, messageParams, data);
    }

}
