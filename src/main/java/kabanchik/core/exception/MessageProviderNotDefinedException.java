package kabanchik.core.exception;

public class MessageProviderNotDefinedException extends Error{

    public MessageProviderNotDefinedException(){
        super("Message Source is not defined. Critical problem in application.");
    }
}
