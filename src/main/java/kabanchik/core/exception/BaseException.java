package kabanchik.core.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public abstract class BaseException extends Exception{

    private Object data;

    private Object[] messageParams;

    public BaseException(String messageCode){
        this(messageCode, null);
    }

    public BaseException(String messageCode, Object[] messageParams){
        this(messageCode, messageParams, null);
    }

    public BaseException(String messageCode, Object[] messageParams, Object data){
        super(messageCode);
        this.data = data;
        this.messageParams = messageParams;
    }

}