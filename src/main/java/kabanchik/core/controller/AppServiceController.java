package kabanchik.core.controller;

import kabanchik.core.controller.response.AuthDTO;
import kabanchik.core.controller.response.BaseRestResponse;
import kabanchik.core.controller.response.ErrorRestResponse;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.config.CustomConfig;
import kabanchik.core.domain.LocalizedEnum;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.service.EnumResolveService;
import kabanchik.system.user.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Collection;

@Slf4j
@Controller
public class AppServiceController{

    @Autowired
    CustomConfig customConfig;

    @Autowired
    EnumResolveService enumResolveService;

    @Autowired
    RoleHierarchy roleHierarchy;

    @RequestMapping(value = {"/ng", "/ng/**"})
    public String index(){
        return "redirect:" + customConfig.getUiHref()[0];
    }

    @RequestMapping(value = "/")
    public String gotoIndex(){
        return "redirect:" + customConfig.getUiHref()[0];
    }

    @RequestMapping("/auth")
    public ResponseEntity<BaseRestResponse> user(Principal user){
        if (user != null) {
            UsernamePasswordAuthenticationToken _user = (UsernamePasswordAuthenticationToken) user;
            Collection<? extends GrantedAuthority> authorities = roleHierarchy.getReachableGrantedAuthorities(_user.getAuthorities());
            return new ResponseEntity<>(new RestResponse(new AuthDTO((User)_user.getPrincipal(), authorities, _user.isAuthenticated())), HttpStatus.OK);
        } else {
            ErrorRestResponse response = new ErrorRestResponse("", "app.error.user.notAuthorized");
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping("/enum/localized")
    public <E extends Enum & LocalizedEnum> ResponseEntity<BaseRestResponse> getLocalizedEnum(@RequestParam("name") String enumName) throws InvalidInputException{
        E[] enums = enumResolveService.resolveEnum(enumName);
        return new ResponseEntity<>(new RestResponse(enums).setField(enumName), HttpStatus.OK);
    }
}