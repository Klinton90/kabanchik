package kabanchik.core.controller;

import kabanchik.core.controller.request.BulkSaveDTO;
import kabanchik.core.controller.response.RestResponse;
import kabanchik.core.domain.IDto;
import kabanchik.core.exception.CustomValidationCollectionException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.service.model.SearchType;
import kabanchik.core.domain.IDomain;
import kabanchik.core.service.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.HashMap;

@Slf4j
public abstract class RestController<E extends IDomain<D, ID>, D extends IDto<E, ID>, ID extends Serializable>{

    protected abstract RestService<E, D, ID> getService();

    @RequestMapping(value = {"/list", "/list/"}, method = RequestMethod.GET)
    public ResponseEntity<RestResponse> listAll(
            Pageable pageable,
            @RequestParam(required = false, defaultValue = "false") boolean showActive
    ){
        Iterable<D> result = getService().getAllDto(pageable.getSort(), showActive);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @RequestMapping(value = "/findLike", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> findLike(
            Pageable pageable,
            @RequestParam(required = false, defaultValue = "false") boolean showActive,
            @RequestParam() String likeValue
    ) throws InvalidInputException {
        Iterable<D> result = getService().findLike(pageable, showActive, likeValue);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @Deprecated
    @RequestMapping(value = "/findByField", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> findByField(
            Pageable pageable,
            @RequestParam(required = false, defaultValue = "false") boolean showActive,
            @RequestParam() String field,
            @RequestParam() String value
    ) throws InvalidInputException{
        Iterable<D> result = getService().findByField(pageable, showActive, field, value);
        return ResponseEntity.ok(new RestResponse(result));
    }

    @Deprecated
    @RequestMapping(value = "/findByStringFieldAndType", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> findByFieldAndType(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean showActive,
        @RequestParam() String field,
        @RequestParam() String value,
        @RequestParam(required = false, defaultValue = "equals") SearchType searchType
    ) throws InvalidInputException{
        Iterable<D> list = getService().getByStringFieldAndSearchType(pageable, showActive, field, value, searchType);
        return ResponseEntity.ok(new RestResponse(list));
    }

    @RequestMapping(value = "/findByGenericFilters", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> findByGenericFilters(
            Pageable pageable,
            @RequestParam(required = false, defaultValue = "false") boolean showActive,
            HttpServletRequest request
    ) throws InvalidInputException{
        Page<D> list = getService().getByGenericFiltersAsDTO(pageable, showActive, new HashMap<>(request.getParameterMap()));
        return ResponseEntity.ok(new RestResponse(list));
    }

    @RequestMapping(value = "/getUniqueValues", method = RequestMethod.GET)
    public ResponseEntity<RestResponse> getUniqueValues(
            @RequestParam() String field,
            @RequestParam(required = false) String[] orderByFieldNames,
            @RequestParam(required = false) String queryString,
            @RequestParam(required = false) String[] filterByFieldNames,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) throws InvalidInputException{
        Iterable<?> result = getService().getUniqueValues(field, orderByFieldNames, queryString, filterByFieldNames, limit, offset);
        return ResponseEntity.ok(new RestResponse(result).setField(field));
    }

    @RequestMapping(value = {"/get/{id}", "/get/{id}/"}, method = RequestMethod.GET)
    public ResponseEntity<RestResponse> get(@PathVariable ID id){
        D result = getService().findOneDto(id);
        return ResponseEntity.ok(new RestResponse(result).setId(id));
    }

    @RequestMapping(value = {"/create", "/create/"}, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<RestResponse> create(@RequestBody D json) throws CustomValidationException {
        D result = getService().create(json);
        return ResponseEntity.ok(new RestResponse(result).setId(result.getId()));
    }

    @RequestMapping(value = {"/update/{id}", "/update/{id}/"}, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RestResponse> update(@RequestBody D json) throws CustomValidationException{
        D result = getService().update(json);
        return ResponseEntity.ok(new RestResponse(result).setId(result.getId()));
    }

    @PostMapping(value = {"/saveAll", "/saveAll/"})
    public ResponseEntity<RestResponse> saveAll(@RequestBody BulkSaveDTO<D, E, ID> bulkSaveDTO) throws CustomValidationCollectionException {
        getService().saveAll(bulkSaveDTO);
        return ResponseEntity.ok(new RestResponse(null));
    }

    @RequestMapping(value = {"/delete/{id}", "/delete/{id}/"}, method = RequestMethod.DELETE)
    public ResponseEntity<RestResponse> delete(@PathVariable ID id) throws CustomValidationException{
        getService().delete(id);
        return ResponseEntity.ok(new RestResponse(null).setId(id));
    }

}
