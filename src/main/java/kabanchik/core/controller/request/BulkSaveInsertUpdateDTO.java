package kabanchik.core.controller.request;

import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.IDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BulkSaveInsertUpdateDTO<D extends IDto<E, ID>, E extends IDomain<D, ID>, ID extends Serializable> extends BulkSaveAbstractDTO {
    private D data;
}
