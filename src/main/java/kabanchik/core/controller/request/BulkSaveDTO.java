package kabanchik.core.controller.request;

import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.IDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class BulkSaveDTO<D extends IDto<E, ID>, E extends IDomain<D, ID>, ID extends Serializable> {
    private List<BulkSaveInsertUpdateDTO<D, E, ID>> rowsForUpdate;
    private List<BulkSaveDeleteDTO<ID>> idsToDelete;
}
