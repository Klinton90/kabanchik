package kabanchik.core.controller.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulkSaveAbstractDTO {
    private Integer uiId;
}
