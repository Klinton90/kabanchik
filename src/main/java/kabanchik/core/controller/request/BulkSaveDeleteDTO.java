package kabanchik.core.controller.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BulkSaveDeleteDTO<ID extends Serializable> extends BulkSaveAbstractDTO {
    private ID id;
}
