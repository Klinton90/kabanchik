package kabanchik.core.controller.response;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ValidationRestResponse extends BaseRestResponse{

    private String message;
    private String messageCode;
    private Object data;
    private String field;
    private String id;

    public ValidationRestResponse(String message, String messageCode, CustomValidationException ex){
        this(message, messageCode, ex.getData(), ex.getField(), ex.getId());
    }

    public ValidationRestResponse(String message, InvalidInputException ex){
        this(message, ex.getMessage(), ex.getData(), "", "");
    }

    public ValidationRestResponse(String message, String messageCode, Object data, String field, String id) {
        super(RestResponseType.VALIDATION);
        this.message = message;
        this.data = data;
        this.id = id;
        this.field = field;
        this.messageCode = messageCode;
    }
}
