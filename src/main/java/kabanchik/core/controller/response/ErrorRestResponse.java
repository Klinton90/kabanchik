package kabanchik.core.controller.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ErrorRestResponse extends BaseRestResponse{

    private String message;
    private String messageCode;

    public ErrorRestResponse(String message, String messageCode){
        super(RestResponseType.ERROR);
        this.message = message;
        this.messageCode = messageCode;
    }
}
