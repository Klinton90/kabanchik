package kabanchik.core.controller.response;

import kabanchik.system.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
public class AuthDTO {
    User principal;
    Collection<? extends GrantedAuthority> authorities;
    Boolean authenticated;
}
