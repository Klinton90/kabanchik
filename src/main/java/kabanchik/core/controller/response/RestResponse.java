package kabanchik.core.controller.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class RestResponse<ID extends Serializable> extends BaseRestResponse{
    private Object data;
    private String field;
    private ID id;

    public RestResponse(Object data){
        this(data, "", null);
    }

    public RestResponse(Object data, String field, ID id) {
        this(RestResponseType.SUCCESS, data, field, id);
    }

    public RestResponse(RestResponseType type, Object data, String field, ID id) {
        super(type);
        this.data = data;
        this.id = id;
        this.field = field;
    }

}
