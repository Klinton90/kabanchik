package kabanchik.core.controller.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class BaseRestResponse{

    protected RestResponseType type;

    public BaseRestResponse(RestResponseType type){
        this.type = type;
    }
}
