package kabanchik.core.controller.response;

import java.io.Serializable;

public enum RestResponseType implements Serializable{
    SUCCESS,
    INFO,
    WARNING,
    ERROR,
    VALIDATION
}
