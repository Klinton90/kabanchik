package kabanchik.core.controller;

import kabanchik.core.controller.response.ErrorRestResponse;
import kabanchik.core.controller.response.ValidationRestResponse;
import kabanchik.core.domain.IDomain;
import kabanchik.core.exception.CustomValidationCollectionException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.SpringValidationException;
import kabanchik.core.service.ExposedResourceMessageBundleSource;
import kabanchik.core.service.model.ValidationMessage;
import kabanchik.core.util.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@ControllerAdvice
public class AppErrorController implements ErrorController {

    @Autowired
    protected ExposedResourceMessageBundleSource msgSource;

    protected static final String ERROR_PATH = "/error";

    @Override
    public String getErrorPath(){
        return ERROR_PATH;
    }

    @RequestMapping(value = ERROR_PATH)
    public String error(HttpServletResponse response){
        response.setStatus(HttpStatus.NOT_FOUND.value());
        return "errorpage";
    }

    /**
     * Used for resolving non-FormField validation
     * @param ex
     * @return
     */
    @ExceptionHandler(CustomValidationException.class)
    public ResponseEntity<ValidationRestResponse> processCustomValidationError(CustomValidationException ex){
        ValidationMessage validationMessage = msgSource.getValidationMessage(ex);
        ValidationRestResponse validateRestResponse = new ValidationRestResponse(validationMessage.getMessage(), validationMessage.getMessageCode(), ex);
        return new ResponseEntity<>(validateRestResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(CustomValidationCollectionException.class)
    public ResponseEntity<List<ValidationRestResponse>> processCustomValidationCollectionException(CustomValidationCollectionException ex) {
        List<ValidationRestResponse> result = ex.getExceptions()
                .stream()
                .map(e -> {
                    ValidationMessage validationMessage = msgSource.getValidationMessage(e);
                    return new ValidationRestResponse(validationMessage.getMessage(), validationMessage.getMessageCode(), e);
                })
                .collect(Collectors.toList());
        return new ResponseEntity<>(result, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Used for resolving invalid user input validation. Basically, if that Exception is thrown -
     * system cannot process user input. Which means received request is malformed.
     * @param ex
     * @return
     */
    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<ValidationRestResponse> processInvalidInputError(InvalidInputException ex){
        ValidationMessage validationMessage = msgSource.getValidationMessage(ex);
        ValidationRestResponse validationRestResponse = new ValidationRestResponse(validationMessage.getMessage(), ex);
        return new ResponseEntity<>(validationRestResponse, HttpStatus.BAD_REQUEST);
    }

    /**
     * User for resolving FormField validation
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationRestResponse> processValidationError(MethodArgumentNotValidException ex){
        return processValidationError(ex.getBindingResult());
    }

    @ExceptionHandler(SpringValidationException.class)
    public ResponseEntity<ValidationRestResponse> processValidationError(SpringValidationException ex){
        return processValidationError(ex.getBindingResult());
    }

    /**
     * All unhandled exceptions. Main fallback point. When possible, system should resolve Exceptions via other methods
     * and return human readable error message.
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorRestResponse> ajaxError(Exception ex){
        log.error("Unhandled Exception: " + ex.getMessage());
        ex.printStackTrace();
        ErrorRestResponse response = new ErrorRestResponse(ex.getMessage(), "");
        return new ResponseEntity<>(response , HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ValidationRestResponse> processValidationError(BindingResult bindingResult){
        ValidationMessage validationMessage = msgSource.getValidationMessage(bindingResult);

        Object data = bindingResult.getTarget();

        try {
            data = BeanUtils.convertToIDto(data);
        } catch (Exception e) {/*IGNORE*/}

        ValidationRestResponse result = new ValidationRestResponse(
                validationMessage.getMessage(),
                validationMessage.getMessageCode(),
                data,
                bindingResult.getFieldError().getField(),
                IDomain.class.isAssignableFrom(data.getClass()) ? ((IDomain)data).getId().toString() : ""
        );

        return new ResponseEntity<>(result, HttpStatus.UNPROCESSABLE_ENTITY);
    }

}
