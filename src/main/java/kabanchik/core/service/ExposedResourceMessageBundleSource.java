package kabanchik.core.service;

import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.exception.MessageProviderNotDefinedException;
import kabanchik.core.service.model.ValidationMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

@Slf4j
public class ExposedResourceMessageBundleSource extends ReloadableResourceBundleMessageSource{

    private static final String VALIDATION_CODE_PREFIX = "app.validation.";
    private static final String VALIDATION_CODE_DEFAULT = "default.";
    private static final String APP_FIELD_PREFIX = "app.text.";

    private static ExposedResourceMessageBundleSource msgSource;

    public static ExposedResourceMessageBundleSource getMessageSource(){
        if(msgSource == null){
            throw new MessageProviderNotDefinedException();
        }else{
            return msgSource;
        }
    }

    public ExposedResourceMessageBundleSource(){
        super();
        ExposedResourceMessageBundleSource.msgSource = this;
    }

    /**
     * Gets all messages for presented Locale.
     * @param locale user request's locale
     * @return all messages
     */
    public Properties getMessages(Locale locale){
        return getMergedProperties(locale).getProperties();
    }

    /**
     * Get message based on code only
     * @param code
     * @return
     */
    public String getMessage(String code){
        Object[] tmp = {};
        return this.getMessage(code, tmp, LocaleContextHolder.getLocale());
    }

    public String getMessage(String code, Object[] params) {
        return this.getMessage(code, params, LocaleContextHolder.getLocale());
    }

    public String tryResolveFieldName(BindingResult bindingResult) {
        return tryResolveFieldName(bindingResult.getObjectName(), bindingResult.getFieldError().getField());
    }

    public String tryResolveFieldName(String objectName, String fieldName) {
        return this.getMessage(APP_FIELD_PREFIX + StringUtils.uncapitalize(objectName) + "." + fieldName);
    }

    public ValidationMessage getValidationMessage(CustomValidationException ex) {
        return getValidationMessage(ex.getObjectName(), ex.getMessage(), ex.getMessageParams(), LocaleContextHolder.getLocale());
    }

    public ValidationMessage getValidationMessage(InvalidInputException ex) {
        return new ValidationMessage(getMessage(ex.getMessage(), ex.getMessageParams(), LocaleContextHolder.getLocale()), ex.getMessage());
    }

    public ValidationMessage getValidationMessage(BindingResult bindingResult) {
        FieldError fieldError = bindingResult.getFieldError();
        String objectName = bindingResult.getObjectName();
        Object[] messageArgs = {
                bindingResult.getFieldError().getField(),
                fieldError.getRejectedValue(),
                objectName
        };

        return getValidationMessage(
                objectName,
                StringUtils.uncapitalize(fieldError.getCode()),
                ArrayUtils.addAll(fieldError.getArguments(), messageArgs),
                LocaleContextHolder.getLocale(),
                fieldError.getDefaultMessage()
        );
    }

    public ValidationMessage getValidationMessage(String objectName, String errorCode, Object[] messageArgs, Locale locale){
        return getValidationMessage(objectName, errorCode, messageArgs, locale, "ui.message.default.error.body.default");
    }

    public ValidationMessage getValidationMessage(String objectName, String errorCode, Object[] messageArgs, Locale locale, String defaultMessage){
        String message = null;
        String messageCode = VALIDATION_CODE_PREFIX + StringUtils.uncapitalize(objectName) + "." + errorCode;

        try{
            message = getMessage(messageCode, messageArgs, locale);
        }catch(NoSuchMessageException ex){
            //ignore as that is expected
        }

        if(message == null || message.length() == 0 || message.equals(messageCode)){
            String newMessageCode = VALIDATION_CODE_PREFIX + VALIDATION_CODE_DEFAULT + errorCode;
            if(!newMessageCode.equals(messageCode)){
                messageCode = newMessageCode;
                try{
                    message = getMessage(messageCode, messageArgs, locale);
                }catch(NoSuchMessageException ex){
                    //ignore as that is expected
                }
            }
        }

        if(message == null || message.length() == 0 || message.equals(messageCode)){
            message = renderDefaultMessage(defaultMessage, messageArgs, locale);
        }

        log.warn(message);

        return new ValidationMessage(message, messageCode);
    }

    @Override
    protected Properties loadProperties(Resource resource, String fileName) throws IOException{
        log.info("Load " + fileName);
        return super.loadProperties(resource, fileName);
    }

}
