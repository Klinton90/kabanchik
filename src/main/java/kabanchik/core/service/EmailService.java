package kabanchik.core.service;

import kabanchik.system.configuration.ConfigurationService;
import kabanchik.system.user.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Slf4j
@Service
public class EmailService{

    private final String IMAGES_PATH = "static/images/";
    private final String FAVICON = "favicon.png";
    private final String LOGO = "logo.png";

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    JavaMailSender mailSender;

    @Autowired
    protected ExposedResourceMessageBundleSource msgSource;

    /**
     * Wrapper for sending email asynchronously
     * @param templateName HTML template name
     * @param ctx context for HTML template
     * @param toUser recipient
     * @param subject is not resolved internally, as subject could be dynamic, and has to be resolved outside
     * @throws MessagingException
     */
    @Async("emailsExecutor")
    public void sendEmail(String templateName, Context ctx, User toUser, String subject){
        String supportEmail = ConfigurationService.cachedConfig.getSupportEmail();
        if (supportEmail != null && supportEmail.length() > 0) {
            try {
                MimeMessage mimeMessage = mailSender.createMimeMessage();
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                message.setTo(toUser.getEmail());
                message.setFrom(supportEmail);
                message.setSubject(msgSource.getMessage("ui.text.default.name") + " - " + subject);

                // Set additional langVars
                // From docs: Be sure to first add the text and after that the resources. If you are doing it the other way around, it won’t work!
                ctx.setVariable("supportEmail", supportEmail);
                ctx.setVariable("supportHref", getSupportHref(subject));
                ctx.setVariable("favicon", FAVICON);
                ctx.setVariable("logo", LOGO);
                ctx.setVariable("toUser", toUser);

                final String htmlContent = this.templateEngine.process(templateName, ctx);
                message.setText(htmlContent, true);

                Resource favicon = new ClassPathResource(IMAGES_PATH + FAVICON);
                message.addInline(FAVICON, favicon, "image/x-icon");
                Resource logo = new ClassPathResource(IMAGES_PATH + LOGO);
                message.addInline(LOGO, logo);

                try {
                    mailSender.send(mimeMessage);
                } catch (MailException ex) {
                    log.warn(ex.getMessage());
                }
            } catch (MessagingException ex) {
                log.warn(ex.getMessage());
            }
        }
    }

    /**
     * Generate URL to Support Email.
     */
    public String getSupportHref(String subject){
        return "mailto:" + ConfigurationService.cachedConfig.getSupportEmail() + "?Subject=" + msgSource.getMessage("app.email.global.response") + " " + subject;
    }

}
