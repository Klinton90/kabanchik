package kabanchik.core.service;

import java.time.LocalDate;

public class UserReminderRunnable implements Runnable {

    ReminderService reminderService;

    LocalDate date;

    public UserReminderRunnable(ReminderService reminderService, LocalDate date) {
        this.reminderService = reminderService;
        this.date = date;
    }

    @Override
    public void run() {
        reminderService.autoSendUserReminders(date);
    }

}
