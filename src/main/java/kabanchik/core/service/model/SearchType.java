package kabanchik.core.service.model;

public enum SearchType{
    CONTAINS,
    NOT_CONTAINS,
    STARTS_WITH,
    ENDS_WITH,
    EQUALS,
    NOT_EQUAL,
    IN,
    DATE,
    GREATER_THAN,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN,
    LESS_THAN_OR_EQUAL,
    IN_RANGE
}
