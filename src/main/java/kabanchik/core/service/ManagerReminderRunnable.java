package kabanchik.core.service;

import java.time.LocalDate;

public class ManagerReminderRunnable implements Runnable {

    ReminderService reminderService;

    LocalDate date;

    public ManagerReminderRunnable(ReminderService reminderService, LocalDate date) {
        this.reminderService = reminderService;
        this.date = date;
    }

    @Override
    public void run() {
        reminderService.autoSendManagerReminders(date);
    }

}
