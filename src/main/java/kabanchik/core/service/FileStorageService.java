package kabanchik.core.service;

import kabanchik.core.config.CustomConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService {

    @Autowired
    CustomConfig config;

    public String storeMultipartFile(MultipartFile file, String subFolder, Boolean replace) throws IOException {
        Path folderPath = Paths.get(config.getFiles().getBase() + config.getFiles().getUpload(), subFolder);
        Files.createDirectories(folderPath);
        String cleanFileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path filePath = folderPath.resolve(cleanFileName);

        if (replace) {
            Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        } else {
            Integer i = 0;
            while (Files.exists(filePath)) {
                i++;
                filePath = folderPath.resolve(appendToFileName(cleanFileName, i.toString()));
            }
            Files.copy(file.getInputStream(), filePath);
        }

        return filePath.toAbsolutePath().toUri().toString();
    }

    public static void deleteFile(String path) throws IOException, URISyntaxException {
        deleteFile(Paths.get(path).toAbsolutePath());
    }

    public static void deleteFile(URI path) throws IOException {
        deleteFile(Paths.get(path));
    }

    public static void deleteFile(Path path) throws IOException {
        Files.deleteIfExists(path);
    }

    public static void deleteFolder(String folderPath) throws URISyntaxException {
        deleteFolder(Paths.get(folderPath).toAbsolutePath());
    }

    public static void deleteFolder(URI folderPath) {
        deleteFolder(Paths.get(folderPath));
    }

    public static void deleteFolder(Path folderPath) {
        FileSystemUtils.deleteRecursively(folderPath.toFile());
    }

    public static Resource readFileAsResource(String filePath, Boolean deleteFile) throws IOException, URISyntaxException {
        return readFileAsResource(Paths.get(filePath).toAbsolutePath(), deleteFile);
    }

    public static Resource readFileAsResource(URI filePath, Boolean deleteFile) throws IOException {
        return readFileAsResource(Paths.get(filePath), deleteFile);
    }

    public static Resource readFileAsResource(Path filePath, Boolean deleteFile) throws IOException {
        Resource resource = new ByteArrayResource(Files.readAllBytes(filePath));

        if (deleteFile) {
            deleteFile(filePath);
        }

        return resource;
    }

    public static String getExtension(String fileName) {
        int lastDot = fileName.lastIndexOf('.');
        return fileName.substring(lastDot);
    }

    public static String appendToFileName(String fileName, String append) {
        String extension = getExtension(fileName);
        return fileName.replace(extension, append + extension);
    }

}
