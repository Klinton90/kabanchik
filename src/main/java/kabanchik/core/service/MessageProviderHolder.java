package kabanchik.core.service;

import kabanchik.core.exception.MessageProviderNotDefinedException;

public class MessageProviderHolder{

    private static ExposedResourceMessageBundleSource msgSource;

    private MessageProviderHolder(){}

    public static ExposedResourceMessageBundleSource getMessageSource(){
        if(msgSource == null){
            throw new MessageProviderNotDefinedException();
        }else{
            return msgSource;
        }
    }

    public static void setMessageSource(ExposedResourceMessageBundleSource msgSource){
        MessageProviderHolder.msgSource = msgSource;
    }

}
