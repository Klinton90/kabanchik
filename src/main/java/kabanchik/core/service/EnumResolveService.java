package kabanchik.core.service;

import kabanchik.core.domain.LocalizedEnum;
import kabanchik.core.exception.InvalidInputException;
import org.reflections.Reflections;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class EnumResolveService{

    Map<String, Object[]> mapper = new Reflections("kabanchik").getSubTypesOf(LocalizedEnum.class).stream()
        .filter(s -> !s.getSimpleName().equals("LocalizedEnumDTO"))
        .collect(Collectors.toMap(
            (Function<Class<?>, String>)Class::getName,
            (Function<Class<?>, Object[]>)Class::getEnumConstants
        ));

    @Deprecated
    @SuppressWarnings("unchecked")
    public <E extends Enum & LocalizedEnum> E[] resolveEnum_old(String enumName) throws InvalidInputException{
        E[] result;

        if(mapper.containsKey(enumName)){
            result = (E[])mapper.get(enumName);
        }else{
            try{
                Class<E> clazz = (Class<E>)Class.forName(enumName);
                result = clazz.getEnumConstants();
                mapper.put(enumName, result);
            }catch(ClassNotFoundException e){
                Object[] messageParams = {enumName};
                throw new InvalidInputException("app.error.rest.enumNotFound", messageParams);
            }
        }

        return result;
    }

    public <E extends Enum & LocalizedEnum> E[] resolveEnum(String enumName) throws InvalidInputException{
        @SuppressWarnings("unchecked")
        E[] result = (E[])mapper.get(enumName);
        if(result == null){
            Object[] messageParams = {enumName};
            throw new InvalidInputException("app.error.rest.enumNotFound", messageParams);
        }
        return result;
    }
}
