package kabanchik.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.controller.request.BulkSaveAbstractDTO;
import kabanchik.core.controller.request.BulkSaveDTO;
import kabanchik.core.controller.request.BulkSaveDeleteDTO;
import kabanchik.core.controller.request.BulkSaveInsertUpdateDTO;
import kabanchik.core.domain.ActivatableDomain;
import kabanchik.core.domain.Field.ContainsFilterField;
import kabanchik.core.domain.IDomain;
import kabanchik.core.domain.IDto;
import kabanchik.core.domain.IdAware;
import kabanchik.core.exception.*;
import kabanchik.core.repository.RestRepository;
import kabanchik.core.service.model.SearchType;
import kabanchik.core.util.BeanUtils;
import kabanchik.system.user.UserService;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.persistence.EntityGraph;
import javax.validation.Valid;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.temporal.Temporal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Component
public abstract class RestService<E extends IDomain<D, ID>, D extends IDto<E, ID>, ID extends Serializable>{

    public abstract RestRepository<E, ID> getRepository();

    public abstract RestService<E, D, ID> getSelf();

    /**
     * Runs before entity to be deleted.
     * Validate if selected {@link IDomain} is In Use somewhere in application
     */
    public abstract boolean isInUse(ID id);

    @Autowired
    public ObjectMapper objectMapper;

    @Autowired
    SmartValidator validator;

    public RestService(){
        this(null);
    }

    public RestService(List<String> likeFields){
        this.likeFields = likeFields;
    }

    @Getter
    private List<String> likeFields;

    @Getter
    private PathBuilder<E> pathBuilder;

    @Getter
    private Class domainClass;

    @Getter
    private Querydsl querydsl;

    protected static final String SEARCH_TYPE_SUFFIX = "MatchMode";

    protected static final List<String> ignoredRequestParams = Arrays.asList("sort", "size", "page", "showActive");

    @PostConstruct
    @SuppressWarnings("unchecked")
    protected void init() throws ClassNotFoundException {
        ParameterizedType parameterizedType = (ParameterizedType)getClass().getGenericSuperclass();
        Type domainType = parameterizedType.getActualTypeArguments()[0];
        domainClass = Class.forName(domainType.getTypeName());

        if (likeFields == null) {
            likeFields = new ArrayList<>();
            for (Field field : domainClass.getDeclaredFields()) {
                if (field.isAnnotationPresent(ContainsFilterField.class)) {
                    likeFields.add(field.getName());
                }
            }
        }

        pathBuilder = new PathBuilder(domainClass, StringUtils.uncapitalize(domainClass.getSimpleName()));

        this.querydsl = new Querydsl(getRepository().getEntityManager(), pathBuilder);
    }

    public <T> Page<T> convertToPage(JPAQuery<T> query, Pageable pageable) {
        return convertToPage(query, pageable, null);
    }

    public <T> Page<T> convertToPage(JPAQuery<T> query, Pageable pageable, EntityGraph<?> entityGraph) {
        long total = query.clone(getRepository().getEntityManager()).fetchCount();

        if (entityGraph != null) {
            query.setHint("javax.persistence.fetchgraph", entityGraph);
        }

        JPQLQuery<T> pagedQuery = getQuerydsl().applyPagination(pageable, query);
        List<T> content = total > pageable.getOffset() ? pagedQuery.fetch() : Collections.emptyList();
        return new PageImpl<>(content, pageable, total);
    }

    public Iterable<E> getAll(Sort sort, boolean showActive) {
        BooleanBuilder where = applyIsActivePredicate(showActive, getDefaultBuilder());

        Iterable<E> resultSet = getRepository().findAll(where, sort);
        return sortIterable(resultSet, sort);
    }

    public List<E> findAllByIdIn(List<ID> ids) {
        BooleanBuilder where = getDefaultBuilder().and(getPathBuilder().getComparable(IdAware.ID_FIELD_NAME, domainClass).in(ids));
        return Lists.newArrayList(getRepository().findAll(where));
    }

    protected Iterable<E> sortIterable(Iterable<E> resultSet, Sort sort) {
        Set<E> set = Sets.newHashSet(resultSet);
        if (set.size() != Lists.newArrayList(resultSet).size() && sort != null) {
            List<E> sortedList = new ArrayList<>(set);
            sortedList.sort(new SortComparator<>(sort, domainClass));

            return sortedList;
        } else {
            return resultSet;
        }
    }

    public Iterable<D> getAllDto(Sort sort, boolean showActive){
        return StreamSupport.stream(getAll(sort, showActive).spliterator(), false)
                .map(this::initDto)
                .collect(Collectors.toList());
    }

    public Iterable<D> findLike(Pageable pageable, boolean showActive, String likeValue) throws InvalidInputException {
        BooleanBuilder where = applyIsActivePredicate(showActive, getDefaultBuilder());

        if (likeValue != null && likeValue.length() > 0 && likeFields != null && likeFields.size() > 0) {
            BooleanBuilder whereLike = new BooleanBuilder();
            for (String likeField: likeFields) {
                whereLike = whereLike.or(Expressions.booleanTemplate("{0} like concat('%', {1}, '%')", _getSimpleExpressionFromField(likeField), likeValue));
            }
            where.and(whereLike);
        }

        return getRepository().findAll(where, pageable).map(this::initDto);
    }

    @Deprecated
    public Iterable<D> getByStringFieldAndSearchType(Pageable pageable, boolean showActive, String field, String value, SearchType searchType) throws InvalidInputException{
        Object[] messageParams = {domainClass.getSimpleName(), field};
        try {
            if (!String.class.isAssignableFrom(domainClass.getDeclaredField(field).getType())) {
                throw new InvalidInputException("app.error.rest.notStringField", messageParams, value);
            }
        } catch(NoSuchFieldException e) {
            throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams, value);
        }

        BooleanBuilder where = applyIsActivePredicate(showActive, getDefaultBuilder())
                .and(_getStringPredicateBySearchType(field, Lists.newArrayList(value), searchType));

        return getRepository().findAll(where, pageable).map(this::initDto);
    }

    @Deprecated
    public Iterable<D> findByField(Pageable pageable, boolean showActive, String fieldName, String value) throws InvalidInputException{
        Field field;
        Object[] messageParams = {domainClass.getSimpleName(), fieldName};

        try {
            field = domainClass.getDeclaredField(fieldName);
        } catch(NoSuchFieldException e) {
            throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams, value);
        } catch(TypeMismatchException e) {
            throw new InvalidInputException("app.error.rest.typeMismatch", messageParams, value);
        }

        BooleanBuilder where = applyIsActivePredicate(showActive, getDefaultBuilder())
                .and(_getSimpleExpressionFromField(fieldName).eq((SimpleExpression)convertToType(value, field.getType())));

        return getRepository().findAll(where, pageable).map(this::initDto);
    }

    public Page<E> getByGenericFilters(Pageable pageable, boolean showActive, Map<String, String[]> requestParams) throws InvalidInputException{
        BooleanBuilder where = applyIsActivePredicate(showActive, getDefaultBuilder());
        where = _getParsedGenericFilters(where, requestParams, null);

        return getRepository().findAll(where, pageable);
    }

    public Page<D> getByGenericFiltersAsDTO(Pageable pageable, boolean showActive, Map<String, String[]> requestParams) throws InvalidInputException{
        return getByGenericFilters(pageable, showActive, requestParams)
                .map(this::initDto);
    }

    @SneakyThrows(value = {IllegalAccessException.class, InstantiationException.class})
    public List<?> getUniqueValues(
            @Nonnull String fieldName,
            @Nullable String[] orderByFieldNames,
            @Nullable String queryString,
            @Nullable String[] filterByFieldNames,
            @Nullable Integer limit,
            Integer offset
    ) throws InvalidInputException{
        ComparablePath<?> expression = _getSimpleExpressionFromField(fieldName);

        BooleanBuilder where = getDefaultBuilder();

        JPAQuery<?> query = new JPAQuery<>(getRepository().getEntityManager())
                .from(pathBuilder)
                .select(expression)
                .distinct();

        if (orderByFieldNames != null && orderByFieldNames.length > 0) {
            List<OrderSpecifier<?>> orderSpecifiers = new ArrayList<>();
            for (String orderByFieldName: orderByFieldNames) {
                if (orderByFieldName.equalsIgnoreCase(ActivatableDomain.IS_ACTIVE_FIELD_NAME)) {
                    orderSpecifiers.add(_getSimpleExpressionFromField(fieldName + "." + orderByFieldName).desc());
                } else {
                    orderSpecifiers.add(_getSimpleExpressionFromField(fieldName + "." + orderByFieldName).asc());
                }
            }
            query.orderBy(orderSpecifiers.toArray(new OrderSpecifier[0]));
        }

        if (StringUtils.isNotBlank(queryString)) {
            if ((filterByFieldNames != null && filterByFieldNames.length > 0)) {
                BooleanBuilder subPredicate = new BooleanBuilder();
                for (String filterByFieldName: filterByFieldNames) {
                    subPredicate = subPredicate.or(_getStringPathFromField(fieldName + "." + filterByFieldName).contains(queryString));
                }
                where = where.and(subPredicate);
            } else {
                Object[] messageParams = {domainClass.getSimpleName(), fieldName};
                throw new InvalidInputException("app.error.rest.filterByFieldNamesRequired", messageParams);
            }
        }

        if (limit != null && limit > 0) {
            query.limit(limit);
        }

        if (offset != null && offset > 0) {
            query.offset(offset);
        }

        List<?> result = query.where(where).fetch();

        try {
            List<?> convertedResult = BeanUtils.tryConvertListToIDtos(result);
            if (convertedResult != null) {
                return convertedResult;
            }
        } catch (NotIDtoConvertableException e) {/*IGNORE*/}
        return result;
    }

    public boolean isActivatable(boolean showActive){
        return showActive && ActivatableDomain.class.isAssignableFrom(domainClass);
    }

    public D findOneDto(ID id){
        //return initDto(getRepository().findById(id).get());
        return initDto(getRepository().findOne(id));
    }

    /**
     * Get entity or {@code null}
     *
     * @param id - Entity ID
     * @return Entity
     */
    public E findOne(ID id){
        //return getRepository().findById(id).get();
        return getRepository().findOne(id);
    }

    /**
     * Get Entity by ID and throw exception, if doesn't exist
     *
     * @param id - Entity ID
     * @return Entity
     * @throws CustomValidationException when cannot find result
     */
    public E getOne(ID id) throws CustomValidationException {
        E entity = getRepository().findOne(id);
        if (entity == null) {
            String objectName = domainClass.getSimpleName();
            Object[] messageParams = {objectName, id};
            throw new CustomValidationException("rowNotFound", objectName, messageParams);
        }

        return entity;
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public D create(@Valid D dto) throws CustomValidationException {
        return initDto(create(initEntity(dto)));
    }

    @Transactional(rollbackFor = Exception.class)
    public E createSkipValidation(D dto) throws CustomValidationException {
        return create(initEntity(dto));
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public E create(@Valid E json) throws CustomValidationException {
        validate(json);
        E savedEnitity = getSelf()._save(json);
        // TODO: think if this is needed
//        getRepository().getEntityManager().refresh(savedEnitity);
        return savedEnitity;
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public D update(@Valid D dto) throws CustomValidationException {
        return initDto(update(initEntity(dto)));
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    @SneakyThrows(value = {IllegalAccessException.class, InstantiationException.class})
    public E update(@Valid E json) throws CustomValidationException {
        validate(json);
        E entity = findOne(json.getId());
        if (entity != null) {
            @SuppressWarnings("unchecked")
            E entityCopy = (E)entity.getClass().newInstance();
            BeanUtils.copyProperties(entity, entityCopy, true);
            BeanUtils.copyProperties(json, entity, true, null, getNullableProperties());
            return getSelf()._save(entity, entityCopy);
        } else {
            String objectName = domainClass.getSimpleName();
            Object[] messageParams = {objectName, json.getId()};
            throw new CustomValidationException("rowNotFound", objectName, "", json.getId().toString(), json, messageParams);
        }
    }

    protected String[] getNullableProperties() {
        return new String[]{};
    }

    public void validate(E json) throws CustomValidationException {

    }

    public void delete(ID id) throws CustomValidationException{
        String objectName = domainClass.getSimpleName();
        Object[] messageParams = {objectName, id};

        if (isInUse(id)) {
            throw new CustomValidationException("isInUse", objectName, messageParams).setId(id.toString());
        } else {
            try {
                //getRepository().deleteById(id);
                getRepository().delete(id);
            } catch(IllegalArgumentException e) {
                throw new CustomValidationException("rowNotFound", objectName, messageParams).setId(id.toString());
            }
        }
    }

    public <W> Object convertToType(String value, Class<W> clazz) throws TypeMismatchException{
        try {
            return this.objectMapper.readValue("\"" + value + "\"", clazz);
        } catch(IOException e) {
            throw new TypeMismatchException(value, clazz);
        }
    }

    protected BooleanBuilder applyIsActivePredicate(boolean showActive, BooleanBuilder where){
        if (isActivatable(showActive)) {
            where.and(pathBuilder.getBoolean(ActivatableDomain.IS_ACTIVE_FIELD_NAME).isTrue());
        }

        return where;
    }

    // Override to apply specific request logic. E.g. apply security. See {@link TimesheetAccess}.
    protected BooleanBuilder getDefaultBuilder(){
        return new BooleanBuilder();
    }

    /**
     * That method exists in case when Ancestor needs to apply some custom conditions before saving
     * by overwriting that method (see {@link UserService})
     */
    @Validated
    @Transactional(rollbackFor = Exception.class)
    public E _save(@Valid E preparedEntity){
        return _save(preparedEntity, preparedEntity);
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public E _save(@Valid E preparedEntity, E originalEntity){
        return getRepository().save(preparedEntity);
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    public D initDto(E entity){
        D instance = (D)(((Class<?>)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[1]).newInstance());
        return (D)instance.createFromEntity(entity);
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    public E initEntity(D dto) throws CustomValidationException {
        E instance = (E)(((Class<?>)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).newInstance());
        return (E)instance.createFromDto(dto);
    }

    protected Field _resolveFieldByName(String fieldName) throws InvalidInputException {
        return _resolveFieldByName(fieldName, domainClass);
    }

    protected Field _resolveFieldByName(String fieldName, Class<?> clazz) throws InvalidInputException {
        Field field;

        String[] fieldNameParts = fieldName.split("\\.");

        field = FieldUtils.getField(clazz, fieldNameParts[0], true);
        if (field == null) {
            Object[] messageParams = {domainClass.getSimpleName(), fieldName, true};
            throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams);
        }

        for (int i = 1; i < fieldNameParts.length; i++) {
            field = FieldUtils.getField(field.getType(), fieldNameParts[i], true);
            if (field == null) {
                Object[] messageParams = {domainClass.getSimpleName(), fieldName};
                throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams);
            }
        }

        return field;
    }

    protected BooleanBuilder _getParsedGenericFilters(BooleanBuilder booleanExpression, Map<String, String[]> requestParams, List<String> _ignoredRequestParams) throws InvalidInputException {
        for (Map.Entry<String, String[]> param: requestParams.entrySet()) {
            String fieldName = param.getKey();
            if (!fieldName.endsWith(SEARCH_TYPE_SUFFIX) && !ignoredRequestParams.contains(fieldName) && (_ignoredRequestParams == null || !_ignoredRequestParams.contains(fieldName))) {
                SearchType searchType = _processSearchType(requestParams, fieldName);
                List<Object> values = _processValues(fieldName, searchType, param.getValue());
                booleanExpression.and(_getGenericPredicateByFieldName(fieldName, values, searchType));
            }
        }

        return booleanExpression;
    }

    protected SearchType _processSearchType(Map<String, String[]> params, String fieldName) throws InvalidInputException{
        SearchType searchType = SearchType.EQUALS;
        String searchTypeKey = fieldName + SEARCH_TYPE_SUFFIX;
        String[] searchTypeRaw = params.get(searchTypeKey);
        String className = domainClass.getSimpleName();
        if (searchTypeRaw != null) {
            if (searchTypeRaw.length > 1) {
                Object[] messageParams = {className, fieldName};
                throw new InvalidInputException("app.error.rest.multipleSearchTypes", messageParams, searchTypeRaw);
            } else if(searchTypeRaw.length == 1) {
                try {
                    searchType = (SearchType)convertToType(searchTypeRaw[0], SearchType.class);
                } catch(IllegalArgumentException e) {
                    Object[] messageParams = {className, searchTypeKey};
                    throw new InvalidInputException("app.error.rest.unknownSearchType", messageParams, searchTypeRaw[0]);
                }
            }
        }

        return searchType;
    }

    protected List<Object> _processValues(String fieldName, SearchType searchType, String[] rawValues) throws InvalidInputException {
        return _processValues(fieldName, searchType, rawValues, domainClass);
    }

    protected List<Object> _processValues(String fieldName, SearchType searchType, String[] rawValues, Class clazz) throws InvalidInputException {
        List<Object> values = new ArrayList<>();
        Object[] messageParams = {clazz.getSimpleName(), fieldName};

        if (!Lists.newArrayList(SearchType.IN, SearchType.IN_RANGE, SearchType.DATE).contains(searchType) && rawValues.length > 1) {
            throw new InvalidInputException("app.error.rest.multipleSearchValues", messageParams, rawValues);
        }

        if (Lists.newArrayList(
                SearchType.GREATER_THAN,
                SearchType.GREATER_THAN_OR_EQUAL,
                SearchType.LESS_THAN,
                SearchType.LESS_THAN_OR_EQUAL,
                SearchType.IN,
                SearchType.EQUALS,
                SearchType.NOT_EQUAL,
                SearchType.IN_RANGE,
                SearchType.DATE
        ).contains(searchType)) {
            for (String _value : rawValues) {
                try {
                    Field field = _resolveFieldByName(fieldName, clazz);
                    if (List.class.isAssignableFrom(field.getType())) {
                        ParameterizedType genericType = (ParameterizedType) field.getGenericType();
                        Field genericField = _resolveFieldByName(IdAware.ID_FIELD_NAME, (Class)genericType.getActualTypeArguments()[0]);
                        values.add(convertToType(_value, genericField.getType()));
                    } else {
                        values.add(convertToType(_value, field.getType()));
                    }
                } catch(TypeMismatchException e) {
                    throw new InvalidInputException("app.error.rest.typeMismatch", messageParams, _value);
                }
            }
        } else {
            values.addAll(Arrays.asList(rawValues));
        }

        return values;
    }

    @Deprecated
    private BooleanExpression _getStringPredicateBySearchType(String field, List<String> value, SearchType searchType) throws InvalidInputException {
        StringExpression template = _getStringPathFromField(field);
        BooleanExpression predicate;
        switch (searchType) {
            case STARTS_WITH: {
                predicate = template.startsWithIgnoreCase(value.get(0));
                break;
            }
            case ENDS_WITH: {
                predicate = template.endsWithIgnoreCase(value.get(0));
                break;
            }
            case CONTAINS: {
                predicate = template.containsIgnoreCase(value.get(0));
                break;
            }
            case NOT_CONTAINS: {
                predicate = template.containsIgnoreCase(value.get(0)).not();
                break;
            }
            case IN: {
                predicate = template.in(value);
                break;
            }
            case NOT_EQUAL: {
                predicate = template.ne(value.get(0));
                break;
            }
            default: {
                predicate = template.eq(value.get(0));
                break;
            }
        }
        return predicate;
    }

    protected BooleanExpression _getGenericPredicateByFieldName(String fieldName, List<Object> value, SearchType searchType) throws InvalidInputException {
        switch (searchType) {
            case STARTS_WITH: {
                return _getStringPathFromField(fieldName).startsWithIgnoreCase(value.get(0).toString());
            }
            case ENDS_WITH: {
                return _getStringPathFromField(fieldName).endsWithIgnoreCase(value.get(0).toString());
            }
            case CONTAINS: {
                return _getStringPathFromField(fieldName).containsIgnoreCase(value.get(0).toString());
            }
            case NOT_CONTAINS: {
                return _getStringPathFromField(fieldName).containsIgnoreCase(value.get(0).toString()).not();
            }
            case DATE: {
                DateTimePath dateTimePath = _getDateTimePathFromField(fieldName);
                BooleanExpression result = null;
                if (value.get(0) != null) {
                    result = dateTimePath.after((Comparable<Temporal>) value.get(0));
                }
                if (value.size() > 1 && value.get(1) != null) {
                    BooleanExpression result2 = dateTimePath.before((Comparable<Temporal>) value.get(1));
                    if (result != null) {
                        result = result.and(result2);
                    } else {
                        result = result2;
                    }
                }
                return result;
            }
            case GREATER_THAN:
                return _getNumberPathFromField(fieldName).gt((Number)value.get(0));
            case GREATER_THAN_OR_EQUAL:
                return _getNumberPathFromField(fieldName).goe((Number)value.get(0));
            case LESS_THAN:
                return _getNumberPathFromField(fieldName).lt((Number)value.get(0));
            case LESS_THAN_OR_EQUAL:
                return _getNumberPathFromField(fieldName).loe((Number)value.get(0));
            case IN_RANGE:
                return _getNumberPathFromField(fieldName).between((Number)value.get(0), (Number)value.get(1));
            case IN: {
                return _getComparableOrListPathFromField(fieldName).in(value);
            }
            case NOT_EQUAL: {
                return _getComparableOrListPathFromField(fieldName).ne(value.get(0));
            }
            case EQUALS:
            default: {
                return _getComparableOrListPathFromField(fieldName).eq(value.get(0));
            }
        }
    }

    @Deprecated
    protected BooleanExpression _getGenericPredicateBySimpleExpression(SimpleExpression template, List<Object> value, SearchType searchType){
        switch (searchType) {
            case STARTS_WITH: {
                return Expressions.booleanTemplate("{0} like concat({1}, '%')", template, value.get(0));
            }
            case ENDS_WITH: {
                return Expressions.booleanTemplate("{0} like concat('%', {1})", template, value.get(0));
            }
            case CONTAINS: {
                return Expressions.booleanTemplate("{0} like concat('%', {1}, '%')", template, value.get(0));
            }
            case NOT_CONTAINS: {
                return Expressions.booleanTemplate("not {0} like concat('%', {1}, '%')", template, value.get(0));
            }
            case DATE: {
                BooleanExpression result = null;
                if (value.get(0) != null) {
                    result = Expressions.booleanTemplate("{0} > {1}", template, value.get(0));
                }
                if (value.size() > 1 && value.get(1) != null) {
                    BooleanExpression result2 = Expressions.booleanTemplate("{0} < {1}", template, value.get(1));
                    if (result != null) {
                        result = result.and(result2);
                    } else {
                        result = result2;
                    }
                }
                return result;
            }
            case GREATER_THAN:
                return Expressions.booleanTemplate("{0} > {1}", template, value.get(0));
            case GREATER_THAN_OR_EQUAL:
                return Expressions.booleanTemplate("{0} >= {1}", template, value.get(0));
            case LESS_THAN:
                return Expressions.booleanTemplate("{0} < {1}", template, value.get(0));
            case LESS_THAN_OR_EQUAL:
                return Expressions.booleanTemplate("{0} <= {1}", template, value.get(0));
            case IN_RANGE:
                return Expressions.booleanTemplate("{0} between {1} and {2}", template, value.get(0), value.get(1));
            case IN: {
                return template.in(value);
            }
            case NOT_EQUAL: {
                return template.ne(value.get(0));
            }
            case EQUALS:
            default: {
                return template.eq(value.get(0));
            }
        }
    }

    protected ComparablePath _getComparableOrListPathFromField(String fieldName) throws InvalidInputException {
        Field field = _resolveFieldByName(fieldName);
        if (List.class.isAssignableFrom(field.getType())) {
            return _getSimpleExpressionFromField(IdAware.ID_FIELD_NAME, Object.class, _getListPathFromField(fieldName).any());
        } else {
            return _getSimpleExpressionFromField(fieldName);
        }
    }

    protected ComparablePath _getSimpleExpressionFromField(String fieldName) throws InvalidInputException {
        return _getSimpleExpressionFromField(fieldName, Object.class);
    }

    protected ComparablePath _getSimpleExpressionFromField(String fieldName, Class type) throws InvalidInputException {
        return _getSimpleExpressionFromField(fieldName, type, pathBuilder);
    }

    protected ComparablePath _getSimpleExpressionFromField(String fieldName, Class type, PathBuilder pathBuilder) throws InvalidInputException {
        try {
            return pathBuilder.getComparable(fieldName, type);
        } catch(Exception e) {
            Object[] messageParams = {domainClass.getSimpleName(), fieldName};
            throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams);
        }
    }

    protected StringPath _getStringPathFromField(String fieldName) throws InvalidInputException {
        try {
            return pathBuilder.getString(fieldName);
        } catch(Exception e) {
            Object[] messageParams = {domainClass.getSimpleName(), fieldName};
            throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams);
        }
    }

    protected NumberPath _getNumberPathFromField(String fieldName) throws InvalidInputException {
        try {
            Class type = _resolveFieldByName(fieldName).getType();
            return pathBuilder.getNumber(fieldName, type);
        } catch(Exception e) {
            Object[] messageParams = {domainClass.getSimpleName(), fieldName};
            throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams);
        }
    }

    protected DateTimePath _getDateTimePathFromField(String fieldName) throws InvalidInputException {
        try {
            Class type = _resolveFieldByName(fieldName).getType();
            return pathBuilder.getDateTime(fieldName, type);
        } catch(Exception e) {
            Object[] messageParams = {domainClass.getSimpleName(), fieldName};
            throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams);
        }
    }

    protected ListPath<Object, PathBuilder<Object>> _getListPathFromField(String fieldName) throws InvalidInputException {
        try {
            return pathBuilder.getList(fieldName, Object.class);
        } catch(Exception e) {
            Object[] messageParams = {domainClass.getSimpleName(), fieldName};
            throw new InvalidInputException("app.error.rest.fieldNotFound", messageParams);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveAll(BulkSaveDTO<D, E, ID> bulkSaveDTO) throws CustomValidationCollectionException {
        CustomValidationCollectionException errors = new CustomValidationCollectionException();

        if (bulkSaveDTO.getRowsForUpdate() != null && bulkSaveDTO.getRowsForUpdate().size() > 0) {
            Map<Boolean, List<BulkSaveInsertUpdateDTO<D, E, ID>>> collect = bulkSaveDTO.getRowsForUpdate().stream()
                    .collect(Collectors.groupingBy(d ->
                            d.getData().getId() != null
                            && StringUtils.isNotBlank(d.getData().getId().toString())
                            // TODO: I think this is not needed. UI should send `null` in order to initialize ID
                            && !d.getData().getId().equals(d.getData().getDefaultIdValue())
                    ));

            // Create
            if (collect.containsKey(false)) {
                for (BulkSaveInsertUpdateDTO<D, E, ID> dto : collect.get(false)) {
                    try {
                        getSelf().create(dto.getData());
                    } catch (SpringValidationException e) {
                        errors.add(buildExceptionFromBindingResult(e.getBindingResult(), dto));
                    } catch (CustomValidationException e) {
                        errors.add(e.setId(dto.getUiId().toString()));
                    } catch (Exception e) {
                        errors.add(new CustomValidationException(e.getMessage(), null, null, dto.getUiId().toString(), null));
                    }
                }
            }

            // Update
            if (collect.containsKey(true)) {
                List<ID> ids = collect.get(true)
                        .stream()
                        .map(d -> d.getData().getId())
                        .collect(Collectors.toList());
                List<E> dbEntities = findAllByIdIn(ids);

                if (dbEntities.size() != ids.size()) {
                    throw new RuntimeException("Some records doesn't exist in Database");
                }

                for (BulkSaveInsertUpdateDTO<D, E, ID> dto : collect.get(true)) {
                    try {
                        getSelf().update(dto.getData());
                    } catch (SpringValidationException e) {
                        errors.add(buildExceptionFromBindingResult(e.getBindingResult(), dto));
                    } catch (CustomValidationException e) {
                        errors.add(e.setId(dto.getUiId().toString()));
                    } catch (Exception e) {
                        errors.add(new CustomValidationException(e.getMessage(), null, null, dto.getUiId().toString(), null));
                    }
                }
            }
        }

        // Delete
        if (bulkSaveDTO.getIdsToDelete() != null && bulkSaveDTO.getIdsToDelete().size() > 0) {
            for (BulkSaveDeleteDTO<ID> dto : bulkSaveDTO.getIdsToDelete()) {
                try {
                    getSelf().delete(dto.getId());
                } catch (SpringValidationException e) {
                    errors.add(buildExceptionFromBindingResult(e.getBindingResult(), dto));
                } catch (CustomValidationException e) {
                    errors.add(e.setId(dto.getUiId().toString()));
                } catch (Exception e) {
                    errors.add(new CustomValidationException(e.getMessage(), null, null, dto.getUiId().toString(), null));
                }
            }
        }

        errors.throwIfAny();
    }

    private CustomValidationException buildExceptionFromBindingResult(BindingResult bindingResult, BulkSaveAbstractDTO dto) {
        return new CustomValidationException(bindingResult)
                .setId(dto.getUiId().toString());
    }

}