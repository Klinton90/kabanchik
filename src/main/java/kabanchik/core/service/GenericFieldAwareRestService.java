package kabanchik.core.service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.impl.JPAQuery;
import kabanchik.core.domain.IGenericFieldAwareDomain;
import kabanchik.core.domain.IGenericFieldAwareDto;
import kabanchik.core.exception.CustomValidationException;
import kabanchik.core.exception.InvalidInputException;
import kabanchik.core.service.model.SearchType;
import kabanchik.system.genericfield.domain.GenericFieldEntity;
import kabanchik.system.genericfieldvalue.GenericFieldValueService;
import kabanchik.system.genericfieldvalue.domain.GenericFieldValue;
import kabanchik.system.genericfieldvalue.domain.QGenericFieldValue;
import kabanchik.system.genericfieldvalue.dto.GenericFieldValueDTO;
import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public abstract class GenericFieldAwareRestService<E extends IGenericFieldAwareDomain<D>, D extends IGenericFieldAwareDto<E>> extends RestService<E , D, Integer> {

    private static final String GENERIC_FIELD_PREFIX = "GenericField";

    private static final String GENERIC_FIELD_NAME = "genericFieldValues";

    protected abstract GenericFieldEntity getGenericFieldEntityType();

    @Autowired
    private GenericFieldValueService genericFieldValueService;

    @Getter
    private Querydsl querydsl;

    @Override
    protected void init() throws ClassNotFoundException {
        super.init();

        querydsl = new Querydsl(getRepository().getEntityManager(), getPathBuilder());
    }

    @Validated
    @Transactional(rollbackFor = Exception.class)
    public D create(@Valid D dto) throws CustomValidationException {
        E entity = initEntity(dto);

        List<GenericFieldValue> genericFieldValues = entity.getGenericFieldValues();
        entity.setGenericFieldValues(null);
        final E _entity = create(entity);

        genericFieldValues.forEach(genericFieldValue -> genericFieldValue.setParentId(_entity.getId()));
        _entity.setGenericFieldValues(genericFieldValues);
        update(_entity);

        return initDto(_entity);
    }

    @Override
    @SneakyThrows
    @SuppressWarnings("unchecked")
    public E initEntity(D dto) throws CustomValidationException {
        E instance = (E)(((Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).newInstance());
        instance.createFromDto(dto);
        instance.setGenericFieldValues(initGenericFieldValues(dto, instance));
        return instance;
    }

    @Override
    @SneakyThrows
    @SuppressWarnings("unchecked")
    public D initDto(E entity){
        D instance = (D)(((Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[1]).newInstance());
        instance.createFromEntity(entity);
        instance.setGenericFieldValues(initGenericFieldValueDtos(entity));
        return instance;
    }

    @Override
    public Page<E> getByGenericFilters(Pageable pageable, boolean showActive, Map<String, String[]> requestParams) throws InvalidInputException{
        BooleanBuilder where = applyIsActivePredicate(showActive, getDefaultBuilder());

        Map<String, String[]> simpleParams = new HashMap<>();
        Map<String, String[]> genericParams = new HashMap<>();
        requestParams.forEach((key, values) -> {
            if (key.startsWith(GENERIC_FIELD_PREFIX)) {
                genericParams.put(key, values);
            } else {
                simpleParams.put(key, values);
            }
        });

        if (simpleParams.size() > 0) {
            where = _getParsedGenericFilters(where, simpleParams, null);
        }

        JPAQuery<E> baseQuery = new JPAQuery<>(getRepository().getEntityManager())
                .from(getPathBuilder())
                .select(getPathBuilder());

        Map<String, QGenericFieldValue> joinedGenericFields = null;
        if (genericParams.size() > 0) {
            joinedGenericFields = getParsedGenericFiltersFromGenericFields(baseQuery, where, genericParams);
        }

        baseQuery.where(where);
        JPAQuery countQuery = baseQuery.clone();

        if (pageable.getSort() != null) {
            for (Sort.Order order : pageable.getSort()) {
                String fieldName = order.getProperty();
                if (fieldName.startsWith(GENERIC_FIELD_PREFIX)) {
                    if (joinedGenericFields != null && joinedGenericFields.containsKey(fieldName)) {
                        QGenericFieldValue joinField = joinedGenericFields.get(fieldName);
                        baseQuery.orderBy(getOrderSpecifierFromJoinField(joinField, order));
                    } else {
                        ListPath<GenericFieldValue, QGenericFieldValue> genericFieldPath = getPathBuilder().getList(GENERIC_FIELD_NAME, GenericFieldValue.class, QGenericFieldValue.class);
                        QGenericFieldValue joinField = new QGenericFieldValue(fieldName);

                        baseQuery.leftJoin(genericFieldPath, joinField)
                                .on(joinField.genericFieldId.eq(getGenericFieldId(fieldName)))
                                .orderBy(getOrderSpecifierFromJoinField(joinField, order));
                    }
                } else {
                    getQuerydsl().applySorting(new Sort(order), baseQuery);
                }
            }
        }

        // TODO: this cause `firstResult/maxResults specified with collection fetch; applying in memory!`. To prevent that use `@Fetch(FetchMode.SUBSELECT)`.
//        ListPath<GenericFieldValue, QGenericFieldValue> genericFieldPath = getPathBuilder().getList(GENERIC_FIELD_NAME, GenericFieldValue.class, QGenericFieldValue.class);
//        QGenericFieldValue joinField = new QGenericFieldValue(GENERIC_FIELD_PREFIX);
//        baseQuery.leftJoin(genericFieldPath, joinField).fetchJoin();

        List<E> entities = baseQuery
                // TODO: this cause `firstResult/maxResults specified with collection fetch; applying in memory!`. To prevent that use `@Fetch(FetchMode.SUBSELECT)`.
//                .setHint("javax.persistence.fetchgraph", getRepository().getEntityManager().getEntityGraph("TimesheetCategory.generics"))
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetch();

        long total = countQuery.fetchCount();

        return new PageImpl<>(entities, pageable, total);
    }

    protected Map<String, QGenericFieldValue> getParsedGenericFiltersFromGenericFields(JPAQuery<E> baseQuery, BooleanBuilder booleanBuilder, Map<String, String[]> requestParams) throws InvalidInputException {
        Map<String, QGenericFieldValue> joinedGenericFields = new HashMap<>();

        for (Map.Entry<String, String[]> param: requestParams.entrySet()) {
            String fieldName = param.getKey();

            if (!fieldName.endsWith(SEARCH_TYPE_SUFFIX) && !ignoredRequestParams.contains(fieldName)) {
                SearchType searchType = _processSearchType(requestParams, fieldName);
                List<String> values = _processValues("value", searchType, param.getValue(), GenericFieldValue.class)
                        .stream()
                        .map(o -> o.toString())
                        .collect(Collectors.toList());

                ListPath<GenericFieldValue, QGenericFieldValue> genericFieldPath = getPathBuilder().getList(GENERIC_FIELD_NAME, GenericFieldValue.class, QGenericFieldValue.class);
                QGenericFieldValue joinField = new QGenericFieldValue(fieldName);

                baseQuery.innerJoin(genericFieldPath, joinField);
                booleanBuilder.and(joinField.genericFieldId.eq(getGenericFieldId(fieldName))
                        .and(_getGenericPredicateForGenericField(joinField.value, values, searchType)));

                joinedGenericFields.put(fieldName, joinField);
            }
        }

        return joinedGenericFields;
    }

    protected List<GenericFieldValue> initGenericFieldValues(D dto, E entity) throws CustomValidationException {
        if (dto.getGenericFieldValues() != null) {
            List<Integer> genericFieldValuesIds = dto.getGenericFieldValues()
                    .stream()
                    .map(GenericFieldValueDTO::getId)
                    .collect(Collectors.toList());

            List<GenericFieldValue> genericFieldValues = genericFieldValueService.findAllByIdInWithGenericFields(genericFieldValuesIds);

            Optional<GenericFieldValue> wrongTypeValue = genericFieldValues.stream()
                    .filter(genericFieldValue -> !genericFieldValue.getGenericField().getEntity().equals(getGenericFieldEntityType()))
                    .findAny();

            if (wrongTypeValue.isPresent()) {
                throw new CustomValidationException("genericFieldTypeMismatch", GenericFieldValue.class.getSimpleName());
            }

            return dto.getGenericFieldValues()
                    .stream()
                    .map(genericFieldValueDTO -> genericFieldValues.stream()
                            .filter(genericFieldValue -> genericFieldValueDTO.getId().equals(genericFieldValue.getId()))
                            .findAny()
                            .orElseGet(() -> new GenericFieldValue(genericFieldValueDTO))
                            .setValue(genericFieldValueDTO.getValue())
                            .setParentId(entity.getId()))
                    .collect(Collectors.toList());
        } else {
            return null;
        }
    }

    protected List<GenericFieldValueDTO> initGenericFieldValueDtos(E entity) {
        if (entity.getGenericFieldValues() != null) {
            return entity.getGenericFieldValues()
                    .stream()
                    .map(GenericFieldValueDTO::new)
                    .collect(Collectors.toList());
        } else {
            return null;
        }
    }

    @Transactional
    public E updateGenericFieldValues(D dto) throws CustomValidationException {
        E entity = getRepository().getOne(dto.getId());
        if (entity == null) {
            String objectName = getDomainClass().getSimpleName();
            Object[] messageParams = {objectName, dto.getId()};
            throw new CustomValidationException("rowNotFound", objectName, "", dto.getId().toString(), dto, messageParams);
        }
        entity.setGenericFieldValues(initGenericFieldValues(dto, entity));

        getRepository().save(entity);

        return entity;
    }

    private Integer getGenericFieldId(String fieldName) {
        return Integer.valueOf(fieldName.substring(GENERIC_FIELD_PREFIX.length()));
    }

    private OrderSpecifier<String> getOrderSpecifierFromJoinField(QGenericFieldValue joinField, Sort.Order order) {
        return order.isAscending() ? joinField.value.asc() : joinField.value.desc();
    }

    protected BooleanExpression _getGenericPredicateForGenericField(StringPath stringPath, List<String> value, SearchType searchType){
        switch (searchType) {
            case STARTS_WITH: {
                return stringPath.startsWithIgnoreCase(value.get(0));
            }
            case ENDS_WITH: {
                return stringPath.endsWithIgnoreCase(value.get(0));
            }
            case CONTAINS: {
                return stringPath.containsIgnoreCase(value.get(0));
            }
            case NOT_CONTAINS: {
                return stringPath.containsIgnoreCase(value.get(0)).not();
            }
            case DATE: {
                BooleanExpression result = null;
                if (value.get(0) != null) {
                    result = stringPath.gt(value.get(0));
                }
                if (value.size() > 1 && value.get(1) != null) {
                    BooleanExpression result2 = stringPath.lt(value.get(1));
                    if (result != null) {
                        result = result.and(result2);
                    } else {
                        result = result2;
                    }
                }
                return result;
            }
            case GREATER_THAN:
                return stringPath.castToNum(BigDecimal.class).gt(new BigDecimal(value.get(0)));
            case GREATER_THAN_OR_EQUAL:
                return stringPath.castToNum(BigDecimal.class).goe(new BigDecimal(value.get(0)));
            case LESS_THAN:
                return stringPath.castToNum(BigDecimal.class).lt(new BigDecimal(value.get(0)));
            case LESS_THAN_OR_EQUAL:
                return stringPath.castToNum(BigDecimal.class).loe(new BigDecimal(value.get(0)));
            case IN_RANGE:
                return stringPath.castToNum(BigDecimal.class).between(new BigDecimal(value.get(0)), new BigDecimal(value.get(1)));
            case IN: {
                return stringPath.in(value);
            }
            case NOT_EQUAL: {
                return stringPath.ne(value.get(0));
            }
            case EQUALS:
            default: {
                return stringPath.eq(value.get(0));
            }
        }
    }

}
