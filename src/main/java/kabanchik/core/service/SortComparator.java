package kabanchik.core.service;

import kabanchik.core.util.BeanUtils;
import lombok.SneakyThrows;
import org.springframework.data.domain.Sort;

import java.util.Comparator;

public class SortComparator<E> implements Comparator<E> {

    Sort sort;

    Class domainClass;

    public SortComparator(Sort sort, Class clazz) {
        this.sort = sort;
        this.domainClass = clazz;
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows({IllegalAccessException.class, NoSuchFieldException.class})
    public int compare(E o1, E o2) {

        for (Sort.Order order: sort) {
            Comparable val1 = getField(o1, order.getProperty());
            Comparable val2 = getField(o2, order.getProperty());

            int result = order.getDirection().equals(Sort.Direction.ASC) ? val1.compareTo(val2) : val2.compareTo(val1);
            if (result != 0) {
                return result;
            }
        }

        return 0;
    }

    @SuppressWarnings("unchecked")
    private Comparable<E> getField(E obj, String property) throws NoSuchFieldException, IllegalAccessException {
        Object curObj = obj;

        for (String prop: property.split("\\.")) {
            curObj = BeanUtils.getFieldValue(curObj, prop);
        }

        return (Comparable<E>)curObj;
    }
}
