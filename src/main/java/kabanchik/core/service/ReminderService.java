package kabanchik.core.service;

import kabanchik.core.config.CustomConfig;
import kabanchik.system.holiday.HolidayService;
import kabanchik.system.timesheet.TimesheetService;
import kabanchik.system.timesheet.domain.Timesheet;
import kabanchik.system.user.UserService;
import kabanchik.system.user.domain.User;
import kabanchik.system.user.domain.UserRole;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.context.Context;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;

@Service
public class ReminderService {

    @Autowired
    private CustomConfig config;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ExposedResourceMessageBundleSource msgSource;

    @Autowired
    private UserService userService;

    @Autowired
    private TimesheetService timesheetService;

    @Autowired
    private HolidayService holidayService;

    public void autoSendUserReminders(LocalDate date) {
        userService.findAllForUserReminderReport(date).forEach(user -> sendUserReminderByUserAndDate(user, date));
    }

    public Boolean sendUserReminder(Integer userId, LocalDate date) {
        Pair<LocalDate, LocalDate> weekStartEnd = TimesheetService.getWeekStartEnd(date, false);
        User user = userService.findOne(userId);
        if (!user.getRole().equals(UserRole.ADMIN) && user.getReceiveReminder()) {
            Integer weeklyMax = holidayService.calcWeeklyMax(weekStartEnd.getLeft(), false);

            BigDecimal totalHours = BigDecimal.ZERO;
            List<Timesheet> timesheets = timesheetService.findAllTimesheetsByUserIdAndPeriod(userId, weekStartEnd.getLeft(), weekStartEnd.getRight());
            for (Timesheet timesheet : timesheets) {
                if (timesheet.getAmount() != null) {
                    totalHours = totalHours.add(timesheet.getAmount());
                }
            }

            if (totalHours.compareTo(new BigDecimal(weeklyMax)) < 0) {
                sendUserReminderByUserAndDate(user, date);
                return true;
            }
        }

        return false;
    }

    public void sendUserReminderByUserAndDate(User user, LocalDate date) {
        final Context ctx = new Context(LocaleContextHolder.getLocale());

        ctx.setVariable("timesheetUrl", _getTimesheetUrl());
        ctx.setVariable("weekNumber", _getWeekNumber(date));

        emailService.sendEmail("email/user-reminder", ctx, user, msgSource.getMessage("app.email.userReminder.subject"));
    }

    public void autoSendManagerReminders(LocalDate date) {
        userService.getManagers().forEach(manager -> sendManagerReminderByManagerAndDate(manager, date));
    }

    public Boolean sendManagerReminder(Integer managerId, LocalDate date) {
        User manager = userService.findOne(managerId);
        return sendManagerReminderByManagerAndDate(manager, date);
    }

    public Boolean sendManagerReminderByManagerAndDate(User manager, LocalDate date) {
        if (manager.getRole() == UserRole.EXECUTOR && manager.getReceiveReminder()) {
            List<User> usersForReminder = userService.findAllForManagerReminderReport(manager.getId(), date);
            if (usersForReminder != null && usersForReminder.size() > 0) {
                final Context ctx = new Context(LocaleContextHolder.getLocale());

                ctx.setVariable("timesheetUrl", _getTimesheetUrl());
                ctx.setVariable("weekNumber", _getWeekNumber(date));
                ctx.setVariable("subordinates", usersForReminder);

                emailService.sendEmail("email/manager-reminder", ctx, manager, msgSource.getMessage("app.email.managerReminder.subject"));

                return true;
            }
        }

        return false;
    }

    private String _getTimesheetUrl(){
        return UriComponentsBuilder
                .fromHttpUrl(config.getUiHref()[0] + config.getUiBasePath() + "/timesheet/report")
                .build()
                .toUriString();
    }

    private static Integer _getWeekNumber(LocalDate date){
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        return date.get(weekFields.weekOfWeekBasedYear());
    }

}
